
//  AppDelegate.swift
//  FeetOnStreet
//
//  Created by admin on 7/27/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var internetRechability:Reachability?
    var networkStatus:NetworkStatus?
    var isAlertShown : Bool? = false
    var oldDeviceLanguage = ""
    var changedDeviceLanguage = ""
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        oldDeviceLanguage = LanguagesViewController.getDeviceLanguage()
        changedDeviceLanguage = Locale.current.languageCode!

//        if(oldDeviceLanguage != changedDeviceLanguage){
//
//            LanguagesViewController.setSelectedLanguage(lang: changedDeviceLanguage!)
//            Language.setAppleLAnguageTo(lang: changedDeviceLanguage!)
//            LanguagesViewController.setDeviceLanguage(lang: changedDeviceLanguage!)
//        }
//        else{
//            if let lang = LanguagesViewController.getSelectedLanguage() {
//                Language.setAppleLAnguageTo(lang: lang)
//            } else {
//                //only for the first time.
//                LanguagesViewController.setSelectedLanguage(lang: changedDeviceLanguage!)
//                Language.setAppleLAnguageTo(lang: changedDeviceLanguage!)
//            }
//        }
        
        
//        if let lang = LanguagesViewController.getSelectedLanguage() {
//            Language.setAppleLAnguageTo(lang: lang)
//        } else {
//            LanguagesViewController.setSelectedLanguage(lang: firstDeviceLanguage!)
//           Language.setAppleLAnguageTo(lang: changedDeviceLanguage!)
//        }
        
        changeApplicationLanguage()
        
        Localizer.DoTheMagic()
        
        guard let gai = GAI.sharedInstance() else {
            assert(false, "Google Analytics not configured correctly")
            return true//Base on your function return type, it may be returning something else
        }
        gai.tracker(withTrackingId: "UA-59811223-1")
        // Optional: automatically report uncaught exceptions.
        gai.trackUncaughtExceptions = true
        gai.dispatchInterval = 20;
        
        // Optional: set Logger to VERBOSE for debug information.
        // Remove before app release.
        gai.logger.logLevel = .verbose;
        
        // Override point for customization after application launch.
        UINavigationBar.appearance().barStyle = .blackOpaque
        GMSServices.provideAPIKey("AIzaSyB7xlgy-rKGJ-WWmbR42UBxdfOevc7Tp90") //TODO: Make the key Constant see DashboardViewController
        // GMSServices key
        //GMSServices.provideAPIKey("AIzaSyAw5QstwbU-1cc2C4jTIlVaUbgIWvIDFYE")
        
        // Key created using paid account
//        GMSServices.provideAPIKey("AIzaSyAYP3NIanhDTOXdf347RwkUQRrfcy-R1tg") //TODO: Make the key Constant see DashboardViewController
        
        
        internetRechability = Reachability.forInternetConnection()
        internetRechability?.startNotifier()
        networkStatus=internetRechability?.currentReachabilityStatus()
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(notification:)), name: NSNotification.Name.reachabilityChanged, object: nil)
        let isAlreadyLoggedIn = UserDefaults.standard.value(forKey: "isAlreadyLoggedIn")
        
        if isAlreadyLoggedIn != nil {
            let tempVal = isAlreadyLoggedIn as! Bool
            
            if tempVal == true {
                
                let isTAndCAccepted = UserDefaults.standard.value(forKey: "isTAndCAccepted")
                if(isTAndCAccepted == nil || (isTAndCAccepted as! Bool) == false) {
                    window = UIWindow(frame: UIScreen.main.bounds)
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "TermsAndCondNavigationVC")
                    self.window?.rootViewController = initialViewController
                    self.window?.makeKeyAndVisible()
                }
                else{
                    window = UIWindow(frame: UIScreen.main.bounds)
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController")
                    self.window?.rootViewController = initialViewController
                    self.window?.makeKeyAndVisible()
                }
            }
        }
        
        return true
    }
    
    func reachabilityChanged(notification:Notification) {
        print("Rechability changed")
        let tempRechability = notification.object as! Reachability
        networkStatus=tempRechability.currentReachabilityStatus()
    }
    func changeApplicationLanguage() {
        
        if(oldDeviceLanguage != changedDeviceLanguage){
            
            LanguagesViewController.setSelectedLanguage(lang: changedDeviceLanguage)
            Language.setAppleLAnguageTo(lang: changedDeviceLanguage)
            LanguagesViewController.setDeviceLanguage(lang: changedDeviceLanguage)
        }
        else{
            if let lang = LanguagesViewController.getSelectedLanguage() {
                Language.setAppleLAnguageTo(lang: lang)
            } else {
                //only for the first time.
                LanguagesViewController.setSelectedLanguage(lang: changedDeviceLanguage)
                Language.setAppleLAnguageTo(lang: changedDeviceLanguage)
            }
        }
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
       // UIApplication.shared.windows[0].rootViewController?.viewWillDisappear(true)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        changeApplicationLanguage()
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //UIApplication.shared.windows[0].view?.viewDidAppear(true)
        changeApplicationLanguage()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        
        if #available(iOS 10.0, *) {
            self.saveContext()
        } else {
            self.saveContextBelow()
        }
    }
    
    // MARK: - Core Data stack version below10.0
    @available(iOS 8.0, *)
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "net.colt.apps.native.FeetOnStreet" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    @available(iOS 8.0, *)
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "FeetOnStreet", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    @available(iOS 8.0, *)
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    @available(iOS 8.0, *)
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    @available(iOS 8.0, *)
    func saveContextBelow () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    // MARK: - Core Data stack
    
    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "FeetOnStreet")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    @available(iOS 10.0, *)
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func changeRoot(language lang:String?) {
        if let language = lang {
            Language.setAppleLAnguageTo(lang: language)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController")
            self.window?.rootViewController = initialViewController
            DashBoardViewController.s_gmsPlace = nil
            self.window?.makeKeyAndVisible()
        }
    }
    
}

