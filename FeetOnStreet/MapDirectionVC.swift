//
//  MapDirectionVC.swift
//  FeetOnStreet
//
//  Created by admin on 4/4/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import MapKit

class MapDirectionVC: UIViewController, MKMapViewDelegate {

   @IBOutlet var mapView: MKMapView!
    
    var kmlData:Data? = nil
    var kmlParser:KMLParser? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AnalyticsHelper().analyticLogScreen(screen: "Map Direction Screen")

//        let request = MKDirectionsRequest()
//        request.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: 40.7127, longitude: -74.0059), addressDictionary: nil))
//        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: 37.783333, longitude: -122.416667), addressDictionary: nil))
//        request.requestsAlternateRoutes = true
//        request.transportType = .automobile
//
//        let directions = MKDirections(request: request)
//
//        directions.calculate { [unowned self] response, error in
//            guard let unwrappedResponse = response else { return }
//
//            for route in unwrappedResponse.routes {
//                self.mapView.add(route.polyline)
//                self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
//            }
//        }
        
        
        
        if(kmlData == nil){
            if let filepath = Bundle.main.url(forResource: "doc2", withExtension: "kml") {
                do {
                    let contents = try Data.init(contentsOf: filepath)
                    kmlData = contents
                } catch {
                    // contents could not be loaded
                }
            } else {
                // example.txt not found!
            }
            
            
//            do{
//                if var documentsPathURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
//                    //This gives you the URL of the path
//                    documentsPathURL.appendPathComponent("KMLDirectory")
//                    let fileManager = FileManager.init()
//
//                    try fileManager.copyItem(at: URL.init(string: Bundle.main.path(forResource: "doc", ofType: "txt")!)!, to: documentsPathURL)
//                    documentsPathURL.appendPathComponent("doc.txt")
//                    kmlData = try Data.init(contentsOf: documentsPathURL)
//
//                }
//            }
//            catch{
//                print("cannot read kml")
//            }
        }
        
        if let _kmlData = kmlData {
            if let parser = KMLParser.init(data: _kmlData) {
                self.kmlParser = parser
                parser.parseKML()
            
                var completeFlyTo = MKMapRectNull
                
                
                // Add all of the MKOverlay objects parsed from the KML file to the map.
                if let overlays = parser.overlays as NSArray as? [MKOverlay]{
                    self.mapView.addOverlays(overlays)
                    
                    // Walk the list of overlays and annotations and create a MKMapRect that
                    // bounds all of them and store it into flyTo.
                    var flyTo = MKMapRectNull
                    for overlay in overlays{
                        if MKMapRectIsNull(flyTo) {
                            flyTo = overlay.boundingMapRect
                        }else{
                            flyTo = MKMapRectUnion(flyTo, overlay.boundingMapRect)
                        }
                    }
                    completeFlyTo = flyTo
                }
                
                // Add all of the MKAnnotation objects parsed from the KML file to the map.
                if let annotations = parser.points as NSArray as? [MKAnnotation]{
                    self.mapView.addAnnotations(annotations)
                    
                    for annotation in annotations{
                        let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
                        let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0)
                        if MKMapRectIsNull(completeFlyTo){
                            completeFlyTo = pointRect
                        }else{
                            completeFlyTo = MKMapRectUnion(completeFlyTo, pointRect)
                        }
                    }
                }
                // Position the map so that all overlays and annotations are visible on screen.
                self.mapView.setVisibleMapRect(completeFlyTo, animated: true)

            }
            
        }
        
    }
    
//    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
//        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
//        renderer.strokeColor = UIColor.blue
//        return renderer
//    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let parser = self.kmlParser{
            return parser.renderer(for: overlay)
        }
//                let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
//                renderer.strokeColor = UIColor.blue
//                return renderer
        return MKPolylineRenderer(polyline: overlay as! MKPolyline)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if let parser = self.kmlParser{
            return parser.view(for: annotation)
        }
        return MKAnnotationView.init()
    }

    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
