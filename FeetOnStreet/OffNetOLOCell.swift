//
//  OffNetOLOCell.swift
//  FeetOnStreet
//
//  Created by admin on 9/8/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class OffNetOLOCell: UITableViewCell {

    @IBOutlet weak var checkBoxImgView: UIImageView!
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var supplierNameLbl: UILabel!
    @IBOutlet weak var accessTypeLbl: UILabel!
    @IBOutlet weak var supplierProductLbl: UILabel!
    @IBOutlet weak var coltProductLbl: UILabel!
    @IBOutlet weak var supplierTypelbl: UILabel!
    @IBOutlet weak var bandwidthLbl: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
