//
//  SavedSearchDetailsVC.swift
//  FeetOnStreet
//
//  Created by admin on 3/20/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import SwiftOverlays

class SavedSearchDetailsVC: UIViewController {
    
    var savedSearchModel:SavedSearchModel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var productLbl: UILabel!
    @IBOutlet weak var bandwidthLbl: UILabel!
    @IBOutlet weak var aEndAddressLbl: UILabel!
    
    var serviceCode:String?
    var err_msg : String?
    
    var no_internet_connection = ""
    var Please_wait = ""
    var ServerErrorPleaseTryTgain = ""
    
    // BackgroundApiCall
    var backgroundTask:UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        // BackgroundApiCall
        NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        // BackgroundApiCall
        registerForBackgroundTask()
        
        serviceCode = savedSearchModel?.servicecode
        getProductNamewithProductCode(productCode:serviceCode!)

                nameLbl.text = savedSearchModel?.search_name
                setProductNames() // Changing Products names
                productLbl.text = serviceCode
                bandwidthLbl.text = savedSearchModel?.bandwidthcode
                if savedSearchModel?.locationa_housenumber == nil{
                    savedSearchModel?.locationa_housenumber = ""
                }
                if savedSearchModel?.locationa_streetname == nil{
                    savedSearchModel?.locationa_streetname = ""
                }
                if savedSearchModel?.locationa_housenumber == nil{
                    savedSearchModel?.locationa_housenumber = ""
                }
                if savedSearchModel?.locationa_cityname == nil{
                    savedSearchModel?.locationa_cityname = ""
                }
                if savedSearchModel?.locationa_countryname == nil{
                    savedSearchModel?.locationa_countryname = ""
                }
                if savedSearchModel?.locationb_postcode == nil{
                    savedSearchModel?.locationb_postcode = ""
                }
        
        var aEndAddress = ""
        if let houseNo = savedSearchModel.locationa_housenumber {
            if !(houseNo.isEmpty) {
                aEndAddress = "\(aEndAddress) \(houseNo),"
            }
        }
        if let streetName = savedSearchModel.locationa_streetname {
            if !(streetName.isEmpty) {
                aEndAddress = "\(aEndAddress) \(streetName),"
            }
        }
        if let cityName = savedSearchModel.locationa_cityname {
            if !(cityName.isEmpty) {
                aEndAddress = "\(aEndAddress) \(cityName),"
            }
        }
        if let countryName = savedSearchModel.locationa_countryname {
            if !(countryName.isEmpty) {
                aEndAddress = "\(aEndAddress) \(countryName),"
            }
        }
        if let postCode = savedSearchModel.locationb_postcode {
            if !(postCode.isEmpty) {
                aEndAddress = "\(aEndAddress) \(postCode),"
            }
        }
        
        if !(aEndAddress.isEmpty) {
            aEndAddress = aEndAddress.substring(to: aEndAddress.index(before: aEndAddress.endIndex))
        }
        
        aEndAddressLbl.text = aEndAddress
        
//        if !((savedSearchModel?.locationa_streetname?.isEmpty)!) {
//            savedSearchModel?.locationa_streetname?.append(contentsOf: ", ")
//
//        }
//                let aEndAddress = "\(savedSearchModel.locationa_housenumber!),\(savedSearchModel.locationa_streetname!),\(savedSearchModel.locationa_cityname!),\(savedSearchModel.locationa_countryname!),\(savedSearchModel.locationb_postcode!)"
//                aEndAddressLbl.text = aEndAddress
    }
    override func viewWillAppear(_ animated: Bool) {
        AnalyticsHelper().analyticLogScreen(screen: "SavedSearch Deatils Screen")
        no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
        Please_wait = NSLocalizedString("Please wait", comment: "")
        ServerErrorPleaseTryTgain = NSLocalizedString("Server error. Please try again.", comment: "")
    }
    
    // BackgroundApiCall
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // BackgroundApiCall
    @objc func reinstateBackgroundTask() {
        if backgroundTask == UIBackgroundTaskInvalid {
            registerForBackgroundTask()
        }
    }
    // BackgroundApiCall
    func registerForBackgroundTask() {
        if backgroundTask == UIBackgroundTaskInvalid {
            backgroundTask=UIApplication.shared.beginBackgroundTask(expirationHandler: {
                self.endBackGroundTask()
            })
        }
    }
    
    // BackgroundApiCall
    func endBackGroundTask() {
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask=UIBackgroundTaskInvalid
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setProductNames() {
        if serviceCode == "Colt VoIP Access" {
            serviceCode = "Colt SIP Trunking"
        }else if serviceCode == "Colt LANLink Point to Point (Ethernet Point to Point)" {
            serviceCode = "Colt Ethernet Line"
            
        }else if serviceCode == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
            serviceCode = "Colt Ethernet Hub and Spoke"
        }else if serviceCode == "Colt Ethernet Private Network (EPN)"{
            serviceCode = "ColT Ethernet VPN"
        }
    }
    
    @IBAction func searchBtnAction(_ sender: Any) {
        AnalyticsHelper().analyticsEventsAction(category: "SavedSearch Search", action: "SEARCH_SavedSearch_click", label: "Search_SavedSearch")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.window?.rootViewController = initialViewController
        
        CommonDataHelper.sharedInstance.isFromSavedsearch = true
        CommonDataHelper.sharedInstance.savedSearchModelList = savedSearchModel
        
        getProductNamewithProductCode(productCode:(savedSearchModel?.servicecode)!)
    }
    
    func getProductNamewithProductCode(productCode:String) {
        if let products = CoreDataHelper.sharedInstance.getAllProductsWithProductCode() {
            for product in products {
                if product.productCode == productCode {
                    CommonDataHelper.sharedInstance.selectedProduct = product.productName
                    serviceCode = product.productName
                    CommonDataHelper.sharedInstance.selectedBandwidth = savedSearchModel?.bandwidthcode
                    CommonDataHelper.sharedInstance.selectedProductCode = product.productCode
                }
            }
        }
    }
    
    @IBAction func deleteBtnAction(_ sender: Any) {
        let delete = NSLocalizedString("Delete", comment: "")
        let areYouSureTodelete = NSLocalizedString("Are you sure you want to delete?", comment: "")
        let yES = NSLocalizedString("YES", comment: "")
        let nO = NSLocalizedString("NO", comment: "")
        let alertController = UIAlertController(title: delete, message: areYouSureTodelete, preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: yES, style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.deleteSavedSearchList ()
            NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: nO, style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func deleteSavedSearchList (){
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
            // BackgroundApiCall
            endBackGroundTask()
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
            
        }else{
            DispatchQueue.main.async {
                //            UIHelper.sharedInstance.showLoadingOnViewController(loadVC: self)
                SwiftOverlays.showBlockingWaitOverlayWithText(self.Please_wait)
            }
            
            let url = NSURL(string: "https://rfsdcp.colt.net/aptApi/deleteSavedSearchCriteria?coltAccount=\(savedSearchModel.userid!)&serachid=\(savedSearchModel.searchid!)")!
            //            let url = NSURL(string: "https://rfsdcp.colt.net/aptApi/getSavedSearchCriteria?coltAccount=\(userName)")!
            let request = NSMutableURLRequest(url: url as URL, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 500.0)
            //        let request = NSMutableURLRequest(url: url as URL)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")  // the request is JSON
            request.setValue("Basic YXB0Y2xpZW50OmFwdDEyJDU2", forHTTPHeaderField: "Authorization")        // the expected response is also JSON
            request.httpMethod = "POST"
            request.timeoutInterval = 600
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data,response,error in
                if error != nil{
                    print(error?.localizedDescription)
                    // BackgroundApiCall
                    self.endBackGroundTask()
                    DispatchQueue.main.async(execute: {
                        SwiftOverlays.removeAllBlockingOverlays()
                    })
                    UIHelper.showAlert(message: self.ServerErrorPleaseTryTgain, inViewController: self)
                    return
                }
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                    
                    if let parseJSON = json {
                        var srtatusValue:String = parseJSON["status"] as! String
                        var messageValue:String = parseJSON["message"] as! String
                        print("result: \(srtatusValue)")
                        print(parseJSON)
                        // BackgroundApiCall
                        self.endBackGroundTask()
                        
                        if srtatusValue == "SUCCESS"{
                            srtatusValue = "Search Record Deleted!"
                            messageValue = ""
                            AnalyticsHelper().analyticsEventsAction(category: "Delete SavedSearch", action: "DELETE_SavedSearch_click", label: "Delete_SavedSearch")
                            self.deleteAlert(srtatusValue: srtatusValue)
                        }
                        DispatchQueue.main.async(execute: {
                            SwiftOverlays.removeAllBlockingOverlays()
                        })
//                        UIHelper.showAlert(message: srtatusValue, inViewController: self)
                    }
                } catch let error as NSError {
                    print(error)
                    // BackgroundApiCall
                    self.endBackGroundTask()
                    DispatchQueue.main.async(execute: {
                        SwiftOverlays.removeAllBlockingOverlays()
                    })
                    UIHelper.showAlert(message: self.ServerErrorPleaseTryTgain, inViewController: self)
                }
            }
            task.resume()
        }
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func deleteAlert(srtatusValue:String) {
        let alertController = UIAlertController(title: srtatusValue, message: "", preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.navigationController?.popViewController(animated: true)
            NSLog("OK Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
