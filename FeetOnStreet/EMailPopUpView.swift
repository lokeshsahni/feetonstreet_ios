//
//  EMailPopUpView.swift
//  FeetOnStreet
//
//  Created by admin on 9/15/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import SwiftOverlays

class EMailPopUpView: UIView {
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var eMailTxtFld: UITextField!
    @IBOutlet weak var subjectTxtFld: UITextField!
    var priceList:[Price]=[]
    var htmlBody: String = ""
    var productType:String?
    var a_endAddress:String?
    
    var eMailPopUpView:EMailPopUpView?
    
    var please_wait = ""
    

   
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
         AnalyticsHelper().analyticLogScreen(screen: "E-mail View Screen")
        
        // Drawing code
//        UIHelper.sharedInstance.showLoading()
        please_wait = NSLocalizedString("Please wait", comment: "")
        SwiftOverlays.showBlockingWaitOverlayWithText(please_wait)
        
        if (priceList.count > 0) {
            
            let address = NSLocalizedString("ADDRESS", comment: "")
            let product = NSLocalizedString("PRODUCT", comment: "")
            let supplier = NSLocalizedString("SUPPLIER", comment: "")
            let bandwidth = NSLocalizedString("BANDWIDTH", comment: "")
            let contactTerm = NSLocalizedString("CONTRACT TERM", comment: "")
            let year1Charge = NSLocalizedString("YEAR 1 CHARGE", comment: "")
            let currency = NSLocalizedString("CURRENCY", comment: "")
            let accessType = NSLocalizedString("ACCESS TYPE", comment: "")
            let remarks = NSLocalizedString("REMARKS", comment: "")
        
        htmlBody = ("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd' > <html xmlns = 'http://www.w3.org/1999/xhtml' > <head > <meta http - equiv = 'Content-Type' content = 'text/html; charset=utf-8' /> <title > Pricing Information </title > <style type = 'text/css'> body { line - height: 1.2em; } #hor-minimalist-a {font-family: 'Lucida Sans Unicode', 'Lucida Grande', Sans-Serif; font-size: 12px; background: #fff; margin: 10px; width: 100%; border-collapse: collapse; text-align: left; } #hor-minimalist-a th {font-size: 14px; font-weight: normal; color: #236491; padding: 10px; border-bottom: 2px solid #00A59B; text-align: left; } #hor-minimalist-a td {color: #00A59B; padding: 5px 0px 5px 0px; } #hor-minimalist-a tbody tr:hover td {color: #00A59B; } .divider {border-bottom: 1px solid #00A59B; } </style> </head> <body> <table id='hor-minimalist-a'  style=\"width: 100% \" summary='Pricing Information'> <thead> <tr> <th scope='col'>\(address)</th> <th scope='col'>\(product)</th> <th scope='col'>\(supplier)</th> <th scope='col'>\(bandwidth)</th> <th scope='col'>\(contactTerm)</th> <th scope='col'>NRC</th> <th scope='col'>MRC</th> <th scope='col' style=\"width: 30 % \">\(year1Charge)</th> <th scope='col'>\(currency)</th><th scope='col' style=\"width: 30 % \">\(accessType)</th><th scope='col' style=\"width: 40 % \" padding-right:20px; >\(remarks)</th></tr> </thead> <tbody>")
        
        for price in priceList {
  
                htmlBody.append("<tr>");
                if a_endAddress==nil{
                htmlBody.append("<td align=\"center\" rowspan='3'>" + "" + "</td>");
                }else{
                htmlBody.append("<td align=\"center\" rowspan='3'>" + a_endAddress! + "</td>");
                }
                htmlBody.append("<td align=\"center\" rowspan='3'>" + productType! + "</td>");
                if price.bEndDeliverySupplier == nil {
                htmlBody.append("<td align=\"center\" rowspan='3'>" + "" + "</td>");
                }else{
                htmlBody.append("<td align=\"center\" rowspan='3'>" + price.bEndDeliverySupplier! + "</td>");
                }
//                htmlBody.append("<td align=\"center\" rowspan='3'>" + price.bEndDeliverySupplier! + "</td>");
                htmlBody.append("<td align=\"center\" rowspan='3'>" + price.bandwidthDesc! + "</td>");
                
                htmlBody.append("<td align=\"center\">1</td>");
                htmlBody.append("<td align=\"center\">" + price.priceNrc1! + "</td>");
                htmlBody.append("<td align=\"center\">" + price.priceMrc1! + "</td>");
                htmlBody.append("<td align=\"center\">" + price.totalPrice1! + "</td>");
                htmlBody.append("<td align=\"center\">" + price.currency! + "</td>");
                if price.accessTypeAEnd == nil {
                htmlBody.append("<td align=\"center\">" + "Lease Line" + "</td>");
                }else{
                htmlBody.append("<td align=\"center\">" + price.accessTypeAEnd! + "</td>");
                }
                if price.remarksPrice == nil {
                htmlBody.append("<td align=\"center\" padding-right:30px;>" + "" + "</td>");
                }else{
                htmlBody.append("<td align=\"center\" padding-right:30px;>" + price.remarksPrice! + "</td>");
                }
                htmlBody.append("</tr>");
                htmlBody.append("<tr>");
                htmlBody.append("<td align=\"center\">2</td>");
                htmlBody.append("<td align=\"center\">" + price.priceNrc2! + "</td>");
                htmlBody.append("<td align=\"center\">" + price.priceMrc2! + "</td>");
                htmlBody.append("<td align=\"center\">" + price.totalPrice2! + "</td>");
                htmlBody.append("<td align=\"center\">" + price.currency! + "</td>");
                if price.accessTypeAEnd == nil {
                htmlBody.append("<td align=\"center\">" + "Lease Line" + "</td>");
                }else{
                htmlBody.append("<td align=\"center\">" + price.accessTypeAEnd! + "</td>");
                }
//                htmlBody.append("<td align=\"center\" padding-right:30px;>" + price.remarksPrice! + "</td>");
                htmlBody.append("</tr>");
                
                htmlBody.append("<tr>");
                htmlBody.append("<td align=\"center\">3</td>");
                htmlBody.append("<td align=\"center\">" + price.priceNrc3! + "</td>");
                htmlBody.append("<td align=\"center\">" + price.priceMrc3! + "</td>");
                htmlBody.append("<td align=\"center\">" + price.totalPrice3! + "</td>");
                htmlBody.append("<td align=\"center\">" + price.currency! + "</td>");
                if price.accessTypeAEnd == nil {
                htmlBody.append("<td align=\"center\">" + "Lease Line" + "</td>");
                }else{
                htmlBody.append("<td align=\"center\">" + price.accessTypeAEnd! + "</td>");
                }
                if price.remarksPrice == nil {
                htmlBody.append("<td align=\"center\" padding-right:30px;>" + "" + "</td>");
                }else{
                htmlBody.append("<td align=\"center\" padding-right:30px;>" + price.remarksPrice! + "</td>");
                }
                htmlBody.append("</tr>");
                
                htmlBody.append("<tr><td class='divider' colspan='12'></td></tr>");
                htmlBody.append("</tr>");
        }
            htmlBody.append("</tbody></table></body></html>");
            webView.loadHTMLString(htmlBody, baseURL: nil)
            setProductNames()
            let pricingInformation = NSLocalizedString("Pricing Information", comment: "")
            subjectTxtFld.text = "\(pricingInformation) | " + productType!
     }
//        UIHelper.sharedInstance.hideLoading()
        SwiftOverlays.removeAllBlockingOverlays()
  }
  
    @IBAction func sendBtnAction(_ sender: Any) {
        
        AnalyticsHelper().analyticsEventsAction(category: "Email", action: "emailSend_click", label: "Send")
        
        if (eMailTxtFld.text == nil) || (eMailTxtFld.text == ""){
//          UIHelper.sharedInstance.showError("Please enter email iD")
            let PleaseEnteremailiD = NSLocalizedString("Please enter email iD", comment: "")
            showAlertWithNotification(PleaseEnteremailiD)
        }
        if subjectTxtFld.text == nil || (subjectTxtFld.text == "") {
//          UIHelper.sharedInstance.showError("Please enter subject")
            let PleaseEnterSubject = NSLocalizedString("Please enter subject", comment: "")
            showAlertWithNotification(PleaseEnterSubject)
            
        }else{
             let isEmailAddressValid = isValidEmailAddress(emailAddressString: eMailTxtFld.text!)
            if isEmailAddressValid
            {
                print("Email address is valid")
                sendEmail()

            } else {
                print("Email address is not valid")
//                UIHelper.sharedInstance.showError("Email address is not valid")
                let emailAddressisNotValid = NSLocalizedString("Email address is not valid", comment: "")
                showAlertWithNotification(emailAddressisNotValid)
            }
            

//            let alertController = UIAlertController(title: "", message:"Email queued for delivery.", preferredStyle: .alert)
//            DispatchQueue.main.async {
//                self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
//            }
//            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) {
//                UIAlertAction in
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Close_Email_Popup"), object: nil)
//                
//            }
//            alertController.addAction(okAction)
        }
        
    }
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    func sendEmail() {
//        UIHelper.sharedInstance.showLoading()
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
        DispatchQueue.main.async {
         let no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
         self.showAlertWithNotification(no_internet_connection)
         }
        }else{
        SwiftOverlays.showBlockingWaitOverlayWithText(please_wait)
           var err_msg : String?
           let fromEMailId = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "email")
        let requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:util=\"http://utility.web.colt.net\"><soapenv:Header/> <soapenv:Body> <util:send> <util:to>\(eMailTxtFld.text!)</util:to><util:from>\(fromEMailId!)</util:from><util:subject><![CDATA[\(subjectTxtFld.text!)]]></util:subject><util:body>"+"<![CDATA[\(htmlBody)]]></util:body><util:pass /><util:auth>b3JhY2xlOkNvbHQxMjM0</util:auth></util:send></soapenv:Body></soapenv:Envelope>"
        
        let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/services/Email")!)
        request.httpMethod = "POST"
        let requestData = requestBody.data(using: String.Encoding.utf8)
        request.httpBody = requestData
        request.addValue("text/xml", forHTTPHeaderField: "Content-Type")
        request.addValue("SendEmail", forHTTPHeaderField: "SOAPAction")
        request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
        
        let session = URLSession.shared
        _ = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            print ("Email Response Came >>>>>>>>>>>>>>>");
            
            if error == nil
            {
                if let data = data, let result = String(data: data, encoding: String.Encoding.utf8)
                {
                    let xml = SWXMLHash.parse(result)
                    print(xml)
                    self.getEmailResponse(xmlResponseData: xml)
                }
                else
                {
//                    UIHelper.sharedInstance.hideLoading()
                    SwiftOverlays.removeAllBlockingOverlays()
                    print("Failed to get valid response from server.")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "EMail_Success"), object: nil)
                }
            }
            else
            {
                print("failure")
                print(error?.localizedDescription ?? "Error")
//                UIHelper.sharedInstance.hideLoading()
                SwiftOverlays.removeAllBlockingOverlays()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "EMail_Success"), object: nil)
            }
            
         }
        .resume()
      }
    }
    func getEmailResponse(xmlResponseData:XMLIndexer) {
        let tempArray = xmlResponseData["soapenv:Envelope"]["soapenv:Body"]["sendResponse"]["sendReturn"]
        print(tempArray)
        let emailResponse = tempArray.element?.text
        if emailResponse == "success" {
//            UIHelper.sharedInstance.hideLoading()
            SwiftOverlays.removeAllBlockingOverlays()
           NotificationCenter.default.post(name: NSNotification.Name(rawValue: "EMail_Success"), object: nil)
        }
    }
    func setProductNames() {
        if productType == "Colt VoIP Access" {
            productType = "Colt SIP Trunking"
        }else if productType == "Colt LANLink Point to Point (Ethernet Point to Point)" {
            productType = "Colt Ethernet Line"
            
        }else if productType == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
            productType = "Colt Ethernet Hub and Spoke"
            
        }else if productType == "Colt Ethernet Private Network (EPN)"{
            productType = "ColT Ethernet VPN"
        }
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        
      AnalyticsHelper().analyticsEventsAction(category: "Email", action: "emailCancel_click", label: "Cancel")
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Close_Email_Popup"), object: nil)
    }
    func showAlertWithNotification(_ msg: String) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "ShowAlert"), object: msg)
    }

}
