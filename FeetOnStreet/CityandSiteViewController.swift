//
//  CityandSiteViewController.swift
//  FeetOnStreet
//
//  Created by admin on 3/1/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import MessageUI
import SwiftOverlays


enum CityOrSiteType {
    case ZoneAtype
    case ZoneBtype
    case CityAtype
    case SiteAtype
    case CityBtype
    case SiteBtype
    case SubSeaType
}

class CityandSiteViewController: UIViewController,MFMailComposeViewControllerDelegate {
    
    
    @IBOutlet weak var btn_selectSubSea: UIButton!
    @IBOutlet weak var view_subSeaSelection: UIView!
    @IBOutlet weak var btn_customSubSea: UIButton!
    @IBOutlet weak var layoutHeight_submitView: NSLayoutConstraint!
    @IBOutlet weak var view_subsea: UIView!
    @IBOutlet weak var zoneAbackgroundView: UIView!
     @IBOutlet weak var cityAbackgroundView: UIView!
     @IBOutlet weak var siteAbackgroundView: UIView!
     @IBOutlet weak var zoneBbackgroundView: UIView!
     @IBOutlet weak var cityBbackgroundView: UIView!
     @IBOutlet weak var siteBbackgroundView: UIView!
     @IBOutlet weak var addressAsubView: UIView!
     @IBOutlet weak var addressBsubView: UIView!
     @IBOutlet weak var popUpAlertView: UIView!
    
     @IBOutlet weak var zoneBtopContraint: NSLayoutConstraint!
     @IBOutlet weak var short_DiverseRouteViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var addressA_Lbl: UILabel!
    @IBOutlet weak var addressA_siteOwner_Lbl: UILabel!
    @IBOutlet weak var addressA_buildingID_Lbl: UILabel!
    @IBOutlet weak var addressB_Lbl: UILabel!
    @IBOutlet weak var addressB_siteOwner_Lbl: UILabel!
    @IBOutlet weak var addressB_buildingID_Lbl: UILabel!
    
    @IBOutlet weak var selectZoneAbtn: UIButton!
    @IBOutlet weak var selectCityAbtn: UIButton!
    @IBOutlet weak var selectSiteAbtn: UIButton!
    @IBOutlet weak var selectZoneBbtn: UIButton!
    @IBOutlet weak var selectCity_B_btn: UIButton!
    @IBOutlet weak var selectSite_B_btn: UIButton!
    @IBOutlet weak var popUpsBackgroundBtn: UIButton!
    @IBOutlet weak var shortestRouteBtn: UIButton!
    @IBOutlet weak var diverseRouteBtn: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var furtherAnalyticsRequiredLbl: UILabel!
    @IBOutlet weak var eMailBtn: UIButton!
    @IBOutlet weak var forSupportToInvestigateLbl: UILabel!
    
    var no_internet_connection = ""
    var ServerErrorPleaseTryAgain = ""
    var furtherAnalysisIsRequired = ""
    var oK = ""
    var please_wait = ""
    var select_Zone_A = ""
    var select_City_A = ""
    var select_Site_A = ""
    var select_Zone_B = ""
    var select_City_B = ""
    var select_Site_B = ""
    var shortestRoute = ""
    var diverseRoute = ""
    var select_subSea_link = ""
    var linkIDStr : Any = ""
    
    var selectCityandSitePopUp:SelectCityandSitePopUp?
    var popUpTopSpace:CGFloat?
    var isShortestRouteSelected = false
    var isDiverseRouteSelected = false
    var isSubSeaSelected = false
    var isZoneType = false
    var isCityType = false
    var isSiteTye = false
    var isSubSeaType = false

    
    var zonesArr = NSArray()
    var sitesArrA = NSArray()
    var sitesArrB = NSArray()
    var citiesArr = NSArray()
    var subSeaArr: [String] = []
    var subSeaDataModalArray = NSArray()

    var selectedSubSeaDict: [String:Any]?

    var citiesArrForZoneName = ""
    
    var sitesInfoA = [SiteInfoModel]()
    var indexSitesInfoA = -1
    func siteInfoA() -> SiteInfoModel? {
        
        if( indexSitesInfoA > -1 && indexSitesInfoA < sitesInfoA.count ) {
            return sitesInfoA[indexSitesInfoA]
        }
        return nil
    }
    var sitesAfetchedForCityName = ""

    var sitesInfoB = [SiteInfoModel]()
    var indexSitesInfoB = -1
    func siteInfoB() -> SiteInfoModel? {
        if( indexSitesInfoB > -1 && indexSitesInfoB < sitesInfoB.count ) {
            return sitesInfoB[indexSitesInfoB]
        }
        return nil
    }
    var sitesBfetchedForCityName = ""

    
    // BackgroundApiCall
    var backgroundTask:UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    var cityOrSiteType : CityOrSiteType = CityOrSiteType.ZoneAtype
    
    var selectedName:String = ""{
        didSet {
//            select_City_A = NSLocalizedString("Select City A", comment: "")
//            select_Site_A = NSLocalizedString("Select Site A", comment: "")
//            select_City_B = NSLocalizedString("Select City B", comment: "")
//            select_Site_B = NSLocalizedString("Select Site B", comment: "")
          popUpsBackgroundBtn.isHidden = true
          selectCityandSitePopUp?.removeFromSuperview()
            if cityOrSiteType == .ZoneAtype{
                selectZoneAbtn.setTitle(self.selectedName, for: .normal)
                selectZoneAbtn.setTitleColor(UIColor.black, for: .normal)
                zoneBtopContraint.constant = 0
                addressAsubView.isHidden = true
                indexSitesInfoA = -1
                selectCityAbtn.setTitle(select_City_A, for: .normal)
                selectSiteAbtn.setTitle(select_Site_A, for: .normal)
                selectCityAbtn.setTitleColor(UIColor.darkGray, for: .normal)
                selectSiteAbtn.setTitleColor(UIColor.darkGray, for: .normal)

                resetSubSea_selection()
                setSubmitButtonAction()

            }else if cityOrSiteType == .CityAtype{
               selectCityAbtn.setTitle(self.selectedName, for: .normal)
               selectCityAbtn.setTitleColor(UIColor.black, for: .normal)
                addressAsubView.isHidden = true
                zoneBtopContraint.constant = 0
                indexSitesInfoA = -1
                selectSiteAbtn.setTitle(select_Site_A, for: .normal)
                selectSiteAbtn.setTitleColor(UIColor.darkGray, for: .normal)

                resetSubSea_selection()
                setSubmitButtonAction()

//            }else if cityOrSiteType == .SiteAtype{
//                selectSiteAbtn.setTitle(self.selectedName, for: .normal)
//                selectSiteAbtn.setTitleColor(UIColor.black, for: .normal)
//                zoneBtopContraint.constant = 153
//                addressAsubView.isHidden = false
//                setSubmitButtonAction()
            }else if cityOrSiteType == .ZoneBtype{
                selectZoneBbtn.setTitle(self.selectedName, for: .normal)
                selectZoneBbtn.setTitleColor(UIColor.black, for: .normal)
                indexSitesInfoB = -1
                selectCity_B_btn.setTitle(select_City_B, for: .normal)
                selectSite_B_btn.setTitle(select_Site_B, for: .normal)
                selectCity_B_btn.setTitleColor(UIColor.darkGray, for: .normal)
                selectSite_B_btn.setTitleColor(UIColor.darkGray, for: .normal)
                short_DiverseRouteViewTopConstraint.constant = 20
                addressBsubView.isHidden = true

                resetSubSea_selection()
                setSubmitButtonAction()

            }else if cityOrSiteType == .CityBtype{
                selectCity_B_btn.setTitle(self.selectedName, for: .normal)
                selectCity_B_btn.setTitleColor(UIColor.black, for: .normal)
                addressBsubView.isHidden = true
                short_DiverseRouteViewTopConstraint.constant = 20
                indexSitesInfoB = -1
                selectSite_B_btn.setTitle(select_Site_B, for: .normal)
                selectSite_B_btn.setTitleColor(UIColor.darkGray, for: .normal)

                resetSubSea_selection()
                setSubmitButtonAction()

//            }else{
//                selectSite_B_btn.setTitle(self.selectedName, for: .normal)
//                selectSite_B_btn.setTitleColor(UIColor.black, for: .normal)
//                short_DiverseRouteViewTopConstraint.constant = 180
//                addressBsubView.isHidden = false
//                setSubmitButtonAction()
            }else if cityOrSiteType == .SubSeaType{
                btn_selectSubSea.setTitle(self.selectedName, for: .normal)
                btn_selectSubSea.setTitleColor(UIColor.black, for: .normal)
//                addressBsubView.isHidden = true
                setSubmitButtonAction()
//                short_DiverseRouteViewTopConstraint.constant = 20
//                indexSitesInfoB = -1
//                selectSite_B_btn.setTitle(select_Site_B, for: .normal)
//                selectSite_B_btn.setTitleColor(UIColor.darkGray, for: .normal)
            }
            checkForSubseaOption()
        }
    }
    
    var selectedIndex:Int = -1 {
        didSet {
            if cityOrSiteType == .SiteAtype{
                indexSitesInfoA = self.selectedIndex
                if let siteInfo = siteInfoA() {
                    selectSiteAbtn.setTitle(siteInfo.siteName , for: .normal)
                    selectSiteAbtn.setTitleColor(UIColor.black, for: .normal)
                    zoneBtopContraint.constant = 153
                    addressAsubView.isHidden = false
                    
                    addressA_Lbl.text = siteInfo.xngSiteName
                    addressA_siteOwner_Lbl.text = siteInfo.siteOwner
                    addressA_buildingID_Lbl.text = siteInfo.buildingId

                    checkForSubseaOption()
                    resetSubSea_selection()

                    setSubmitButtonAction()
                }
            }else if cityOrSiteType == .SiteBtype{
                indexSitesInfoB = self.selectedIndex
                if let siteInfo = siteInfoB() {
                    selectSite_B_btn.setTitle(siteInfo.siteName, for: .normal)
                    selectSite_B_btn.setTitleColor(UIColor.black, for: .normal)
                    short_DiverseRouteViewTopConstraint.constant = 180
                    addressBsubView.isHidden = false
                    
                    addressB_Lbl.text = siteInfo.xngSiteName
                    addressB_siteOwner_Lbl.text = siteInfo.siteOwner
                    addressB_buildingID_Lbl.text = siteInfo.buildingId

                    checkForSubseaOption()
                    resetSubSea_selection()

                    setSubmitButtonAction()
                }
            }else if cityOrSiteType == .SubSeaType{

                selectedSubSeaDict = subSeaDataModalArray[self.selectedIndex] as? [String : Any]
            }
            
            if let siteInfoA = siteInfoA() {
                if let siteInfoB = siteInfoB() {
                    if siteInfoA.siteId == siteInfoB.siteId {
                        UIHelper.showAlert(message: "Error! Site A and Site B cannot be same.", inViewController: self)
                    }
                }
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addressAsubView.isHidden = true
        addressBsubView.isHidden = true
        zoneBtopContraint.constant = 0
        short_DiverseRouteViewTopConstraint.constant = 20
        popUpsBackgroundBtn.isHidden = true
        popUpAlertView.isHidden = true
        
        popUpAlertView.layer.masksToBounds = true
        popUpAlertView.layer.cornerRadius = 5
        
        
        selectZoneAbtn.contentHorizontalAlignment = .left
        selectCityAbtn.contentHorizontalAlignment = .left
        selectSiteAbtn.contentHorizontalAlignment = .left
        selectZoneBbtn.contentHorizontalAlignment = .left
        selectCity_B_btn.contentHorizontalAlignment = .left
        selectSite_B_btn.contentHorizontalAlignment = .left
        
        shortestRouteBtn.setImage(UIImage(named: "ic_round_check_box.png"), for: .normal)
        shortestRouteBtn.setTitleColor(UIColor.c_Teal, for: .normal)
        isShortestRouteSelected = true
        
        please_wait = NSLocalizedString("Please wait", comment: "")
        select_Zone_A = NSLocalizedString("Select Zone A", comment: "")
        select_City_A = NSLocalizedString("Select City A", comment: "")
        select_Site_A = NSLocalizedString("Select Site A", comment: "")
        select_Zone_B = NSLocalizedString("Select Zone B", comment: "")
        select_City_B = NSLocalizedString("Select City B", comment: "")
        select_Site_B = NSLocalizedString("Select Site B", comment: "")
        shortestRoute = NSLocalizedString("Unprotected Route", comment: "")
        diverseRoute = NSLocalizedString("Protected Route", comment: "") //
        select_subSea_link = "Select Subsea link"
        
        shortestRouteBtn.setTitle(shortestRoute, for: .normal)
        diverseRouteBtn.setTitle(diverseRoute, for: .normal)
        selectZoneAbtn.setTitle(select_Zone_A, for: .normal)
        selectCityAbtn.setTitle(select_City_A, for: .normal)
        selectSiteAbtn.setTitle(select_Site_A, for: .normal)
        selectZoneBbtn.setTitle(select_Zone_B, for: .normal)
        selectCity_B_btn.setTitle(select_City_B, for: .normal)
        selectSite_B_btn.setTitle(select_Site_B, for: .normal)
        btn_selectSubSea.setTitle(select_subSea_link, for: .normal)
        
        // Do any additional setup after loading the view.
        
//        var parsedData =
//            ["siteId":1,"city":"AMSTERDAM","buildingId":"NLAMS-0000003222","xngSiteName":"NL_1101EC_LUTTENBERGWEG_4-8_G_00EQ1.1","siteOwner":"EQUINIX AM1","isVisible":true,"zone":"EU","siteName":"EQUINIX AM1/AM2","siteLongName":"NL_1101EC_LUTTENBERGWEG_4-8_G_00EQ1.1"] as [String : Any]
//        let siteId = parsedData["siteId"]
//        let city = parsedData["city"]
//        let buildingId = parsedData["buildingId"]
//        let xngSiteName = parsedData["xngSiteName"]
//        let siteOwner = parsedData["siteOwner"]
//        let isVisible = parsedData["isVisible"]
//        let zone = parsedData["zone"]
//        let siteName = parsedData["siteName"]
//        let siteLongName = parsedData["siteLongName"]
        
//        zonesArr = ["COLT ASIA","EU"]
//        citiesArr = ["AICHI","HONG_KONG","OSAKA","SHIGA","SINGAPORE","TOKYO","UENO"]
//        sitesArr = ["ARK DATACENTRES","COLT LON KJC","COLT LON PBP","COLT PGT","COLT WGC","COLTWGC","DRT BONNINGTON HOUSE","DRT CHESSINGTON","DRT MERIDIAN GATE","DRT SOVEREIGN HOUSE","DRT WOKING","EQUINIX LD5","EQUINIX LD6","EQUINIX LD8 HBX8-9","EQUINIX LD9","GLOBAL SWITCH LONDON 1","GLOBAL SWITCH LONDON 2","GYRON","INTERXION LON1 (HANBURY)","TELEHOUSE NORTH 2","TELEHOUSE WEST","VIRTUS COREIX"]
        
        // BackgroundApiCall
        NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        // BackgroundApiCall
        registerForBackgroundTask()
        showAndHideSubSeaView(isHide: true)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
        ServerErrorPleaseTryAgain = NSLocalizedString("Server error. Please try again.", comment: "")
        furtherAnalysisIsRequired = NSLocalizedString("Further analysis is required for the queried route.", comment: "")
        oK = NSLocalizedString("OK", comment: "")
        
        
        AnalyticsHelper().analyticLogScreen(screen: "RTD Tool Screen")
        
    }
    // BackgroundApiCall
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // BackgroundApiCall
    @objc func reinstateBackgroundTask() {
        if backgroundTask == UIBackgroundTaskInvalid {
            registerForBackgroundTask()
        }
    }
    // BackgroundApiCall
    func registerForBackgroundTask() {
        if backgroundTask == UIBackgroundTaskInvalid {
            backgroundTask=UIApplication.shared.beginBackgroundTask(expirationHandler: {
                self.endBackGroundTask()
            })
        }
    }

    func resetSubSea_selection() {

        btn_selectSubSea.setTitleColor(UIColor.darkGray, for: .normal)
        btn_selectSubSea.setTitle(select_subSea_link, for: .normal)
//        isSubSeaSelected = false
        subSeaArr.removeAll()
    }

    // BackgroundApiCall
    func endBackGroundTask() {
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask=UIBackgroundTaskInvalid
    }
    func setSubmitButtonAction() {
        if selectZoneAbtn.currentTitle != "Select Zone A" && selectCityAbtn.currentTitle != "Select City A" && selectSiteAbtn.currentTitle != "Select Site A" && selectZoneBbtn.currentTitle != "Select Zone B" && selectCity_B_btn.currentTitle != "Select City B" && selectSite_B_btn.currentTitle != "Select Site B" && siteInfoA() != nil && siteInfoB() != nil && siteInfoA()?.buildingId != siteInfoB()?.buildingId {

            if !isSubSeaSelected {
                submitBtn.isUserInteractionEnabled = true
                submitBtn.setTitleColor(UIColor.c_Teal, for: .normal)
            }else{
                if btn_selectSubSea.currentTitle != "Select Subsea link" {
                    submitBtn.isUserInteractionEnabled = true
                    submitBtn.setTitleColor(UIColor.c_Teal, for: .normal)
                }else{

                    submitBtn.isUserInteractionEnabled = false
                    submitBtn.setTitleColor(UIColor.darkGray, for: .normal)
                }
            }
        }else{
            submitBtn.isUserInteractionEnabled = false
            submitBtn.setTitleColor(UIColor.darkGray, for: .normal)
        }
    }
    
    @IBAction func selectZoneAbtnAction(_ sender: Any) {
        isZoneType = true
        isCityType = false
        isSiteTye = false
        isSubSeaType = false
        popUpTopSpace = cityAbackgroundView.frame.size.height+64
        cityOrSiteType = CityOrSiteType.ZoneAtype
        callZoneslistAPI ()
    }
    @IBAction func selectCityAbtnAction(_ sender: Any) {
        if selectZoneAbtn.currentTitle == "Select Zone A" || selectZoneAbtn.currentTitle == nil {
            UIHelper.showAlert(message: "Please select Zone A", inViewController: self)
        } else {
            isCityType = true
            isSiteTye = false
            isZoneType = false
            isSubSeaType = false
            popUpTopSpace = cityAbackgroundView.frame.size.height+64
            cityOrSiteType = CityOrSiteType.CityAtype
            callCitieslistAPI (with: selectZoneAbtn.currentTitle!)
     }
    }
    
    @IBAction func selectSiteAbtnAction(_ sender: Any) {
        if selectCityAbtn.currentTitle == "Select City A" || selectCityAbtn.currentTitle == nil {
           UIHelper.showAlert(message: "Please select City A", inViewController: self)
        } else {
            isSiteTye = true
            isCityType = false
            isZoneType = false
            isSubSeaType = false
            popUpTopSpace = cityAbackgroundView.frame.size.height+siteAbackgroundView.frame.size.height+64
            cityOrSiteType = CityOrSiteType.SiteAtype

            weak var weakself = self
            if(sitesAfetchedForCityName != selectCityAbtn.currentTitle!){
                callSiteslistAPI (with: selectCityAbtn.currentTitle!, isSiteA: true, andCompletionCode: {(siteInfos:[SiteInfoModel]) in
                    weakself?.sitesInfoA = siteInfos
                    weakself?.sitesAfetchedForCityName = (weakself?.selectCityAbtn.currentTitle!)!
                    weakself?.OpenCity_SitePopUp()
                })
            }
            else{
                OpenCity_SitePopUp()
            }
     }
    }
    @IBAction func selectZoneBbtnAction(_ sender: Any) {
        isZoneType = true
        isCityType = false
        isSiteTye = false
        isSubSeaType = false
        popUpTopSpace = cityAbackgroundView.frame.size.height+siteAbackgroundView.frame.size.height+cityBbackgroundView.frame.size.height+64
        cityOrSiteType = CityOrSiteType.ZoneBtype
        callZoneslistAPI ()
    }
   
    @IBAction func selectCityBbtnAction(_ sender: Any) {
        if selectZoneBbtn.currentTitle == "Select Zone B" || selectZoneBbtn.currentTitle == nil {
            UIHelper.showAlert(message: "Please select Zone B", inViewController: self)
        } else {
            isCityType = true
            isSiteTye = false
            isZoneType = false
            isSubSeaType = false
            popUpTopSpace = cityAbackgroundView.frame.size.height+siteAbackgroundView.frame.size.height+cityBbackgroundView.frame.size.height+64
            cityOrSiteType = CityOrSiteType.CityBtype
            callCitieslistAPI (with: selectZoneBbtn.currentTitle!)
      }
    }
    
    @IBAction func selectsiteBbtnAction(_ sender: Any) {
        
        
        if selectCity_B_btn.currentTitle == "Select City B" || selectCity_B_btn.currentTitle == nil {
            UIHelper.showAlert(message: "Please select City B", inViewController: self)
        }else {
            isSiteTye = true
            isCityType = false
            isZoneType = false
            isSubSeaType = false
            popUpTopSpace = cityAbackgroundView.frame.size.height+siteAbackgroundView.frame.size.height+cityBbackgroundView.frame.size.height+siteBbackgroundView.frame.size.height+64
            cityOrSiteType = CityOrSiteType.SiteBtype

            weak var weakself = self
            if(sitesBfetchedForCityName != selectCity_B_btn.currentTitle!){
                callSiteslistAPI (with: selectCity_B_btn.currentTitle!, isSiteA: false, andCompletionCode: {(siteInfos:[SiteInfoModel]) in
                    weakself?.sitesInfoB = siteInfos
                    weakself?.sitesBfetchedForCityName = (weakself?.selectCity_B_btn.currentTitle!)!
                    weakself?.OpenCity_SitePopUp()
                })
            }
            else{
                OpenCity_SitePopUp()
            }
        }
    }
    
    @IBAction func shortestRouteBtnAction(_ sender: Any) {
        diverseRouteBtn.setImage(UIImage(named: "ic_round_uncheck_box.png"), for: .normal)
        diverseRouteBtn.setTitleColor(UIColor.darkGray, for: .normal)
        isDiverseRouteSelected = false

        btn_customSubSea.setImage(UIImage(named: "ic_round_uncheck_box.png"), for: .normal)
        btn_customSubSea.setTitleColor(UIColor.darkGray, for: .normal)
        isSubSeaSelected = false

//        if isShortestRouteSelected == false {
            shortestRouteBtn.setImage(UIImage(named: "ic_round_check_box.png"), for: .normal)
            shortestRouteBtn.setTitleColor(UIColor.c_Teal, for: .normal)
            isShortestRouteSelected = true
//      }
        if !view_subsea.isHidden {
            layoutHeight_submitView.constant = 96.0
            view_subSeaSelection.isHidden = true
        }
    }
    
    @IBAction func diverseRouteBtnAction(_ sender: Any) {
        shortestRouteBtn.setImage(UIImage(named: "ic_round_uncheck_box.png"), for: .normal)
        shortestRouteBtn.setTitleColor(UIColor.darkGray, for: .normal)
        isShortestRouteSelected = false

        btn_customSubSea.setImage(UIImage(named: "ic_round_uncheck_box.png"), for: .normal)
        btn_customSubSea.setTitleColor(UIColor.darkGray, for: .normal)
        isSubSeaSelected = false

//        if isDiverseRouteSelected == false {
           diverseRouteBtn.setImage(UIImage(named: "ic_round_check_box.png"), for: .normal)
           diverseRouteBtn.setTitleColor(UIColor.c_Teal, for: .normal)
           isDiverseRouteSelected = true
//     }
        if !view_subsea.isHidden {
            layoutHeight_submitView.constant = 96.0
            view_subSeaSelection.isHidden = true
        }
    }

    @IBAction func selectCustomSubSea_action(_ sender: Any) {

        isDiverseRouteSelected = false
        isShortestRouteSelected = false
        diverseRouteBtn.setImage(UIImage(named: "ic_round_uncheck_box.png"), for: .normal)
        diverseRouteBtn.setTitleColor(UIColor.darkGray, for: .normal)
        shortestRouteBtn.setImage(UIImage(named: "ic_round_uncheck_box.png"), for: .normal)
        shortestRouteBtn.setTitleColor(UIColor.darkGray, for: .normal)
        btn_customSubSea.setImage(UIImage(named: "ic_round_check_box.png"), for: .normal)
        btn_customSubSea.setTitleColor(UIColor.c_Teal, for: .normal)
        isSubSeaSelected = true
        showAndHideSubSeaView(isHide: false)
        setSubmitButtonAction()
    }
    
    @IBAction func submitBtnAction(_ sender: Any) {
        if selectCityAbtn.currentTitle == "Select City A" || selectCityAbtn.currentTitle == nil {
                UIHelper.showAlert(message: "Please select City A", inViewController: self)
        }else if selectSiteAbtn.currentTitle == "Select Site A" || selectSiteAbtn.currentTitle == nil{
            UIHelper.showAlert(message: "Please select Site A", inViewController: self)
        }else if selectCity_B_btn.currentTitle == "Select City B" || selectCity_B_btn.currentTitle == nil{
            UIHelper.showAlert(message: "Please select City B", inViewController: self)
        }else if selectSite_B_btn.currentTitle == "Select Site B" || selectSite_B_btn.currentTitle == nil{
            UIHelper.showAlert(message: "Please select Site B", inViewController: self)
        }else if (btn_selectSubSea.currentTitle == "Select Subsea link" || btn_selectSubSea.currentTitle == nil) && isSubSeaSelected == true{
            UIHelper.showAlert(message: "Please select Sub sea link", inViewController: self)
        }
        else{
            
            AnalyticsHelper().analyticsEventsAction(category: "RTD Tool list", action: "SUBMIT_Button_click", label: "RTD Tool_Count")
            
            if let siteA = siteInfoA() {
                if let siteB = siteInfoB() {
                    if(siteA.siteId == siteB.siteId){
                        UIHelper.showAlert(message: "Site A and Site B cannot be equal", inViewController: self)
                    }
                    else{
                        if(isShortestRouteSelected){
                            callShortestRouteAPI()
                        }else if(isDiverseRouteSelected){
                            callDiverseRouteAPI()
                        }else if isSubSeaSelected {
                            call_subSeaRouteAPI()
                        }
                    }
                }
            }
        }
    }

    func call_subSeaRouteAPI() {

        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {

            // BackgroundApiCall
            endBackGroundTask()
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
        }else{
            DispatchQueue.main.async {
                SwiftOverlays.showBlockingWaitOverlayWithText(self.please_wait)
            }
            var errMsg:String?
            linkIDStr = selectedSubSeaDict!["linkId"]!
            let email = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "email")
            let user_name = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "username")

//            let urlString = "https://dcp.colt.net/rtdapi/src/getShortestSubSeaRoute?siteA=\((siteInfoA()?.siteId)!)&siteB=\((siteInfoB()?.siteId)!)&linkId=\(linkIDStr!)"  // Old

            let urlString = "http://loncis01/rtdapi/src/getShortestSubSeaRouteNew?siteA=\((siteInfoA()?.siteId)!)&siteB=\((siteInfoB()?.siteId)!)&linkId=\(linkIDStr)&userId=\(user_name!)&email=\(email!)&sourceSystem=I"
            
//            let urlString = "http://lonrtd02:8080/rtdapi/src/getShortestSubSeaRouteNew?siteA=\((siteInfoA()?.siteId)!)&siteB=\((siteInfoB()?.siteId)!)&linkId=\(linkIDStr!)&userId=\(user_name!)&email=\(email!)&sourceSystem=I"
            
//            let request = NSMutableURLRequest(url: URL(string: urlString)!)
//            request.httpMethod = "GET"
//            request.addValue("*/*", forHTTPHeaderField: "Accept")
//            request.timeoutInterval=600
            
            let tokenString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "token")
            let accountTypeString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "accountType")
            let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
            let authKey = "rtdapiuser:zAq1#mLp0"
            let encodedAuthKey = authKey.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64AuthKey = encodedAuthKey?.base64EncodedString(options: .init(rawValue: 0))
            
            let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/connectnew")!)
                request.httpMethod = "GET"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
                request.addValue(base64String!, forHTTPHeaderField: "X-CONNECT-U")
                request.addValue(tokenString!, forHTTPHeaderField: "X-GATEWAY-A")
                request.addValue(accountTypeString!, forHTTPHeaderField: "X-GATEWAY-F")
//                request.addValue("Basic \(base64AuthKey!)", forHTTPHeaderField: "Authorization")
                request.timeoutInterval=600
            
            let session = URLSession.shared
            weak var weakSelf = self
            _ = session.dataTask(with: request as URLRequest) { (data, response, error) in

                if error == nil
                {
                    if let data = data
                    {

                        do {

                            if let route = RouteDetailModel.getRoute(from: data){

                                DispatchQueue.main.async {
                                    SwiftOverlays.removeAllBlockingOverlays()
                                if route.error.isEmpty {
                                    weakSelf?.goToRTDtoolVC(with:[route])
                                }else{
                                    var err_msg : String?
                                    let tempVal2 =  CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "employeeType")
                                    //                                    var message = route.error
                                    if tempVal2 != "Colt Employee" {
                                        let message = self.furtherAnalysisIsRequired
                                        let alertCity = UIAlertController(title: "", message: message, preferredStyle: .alert)
                                        let okAction = UIAlertAction(title: self.oK, style: .default, handler: nil)
                                        alertCity.addAction(okAction)
                                        weakSelf?.present(alertCity, animated: true, completion: nil)
                                    }else {
                                        weakSelf?.popUpsBackgroundBtn.isHidden = false
                                        weakSelf?.popUpAlertView.isHidden = false
                                    }
                                }
                                }
                            }

//                            let tempAarray = try JSONSerialization.jsonObject(with: data, options: [])
//                            print(tempAarray)

//                            if let array = tempAarray {
//                                //                                print(array)
//                                var subSeaNameArr: [String] = []
//                                for sModal in array {
//                                    let lName = sModal["linkName"] as! String
//                                    subSeaNameArr.append(lName)
//                                }
//
//                                weakSelf?.subSeaArr = subSeaNameArr
//                                weakSelf?.subSeaDataModalArray = NSArray.init(array: array)

                                DispatchQueue.main.async {
                                    SwiftOverlays.removeAllBlockingOverlays()
//                                    weakSelf?.RefreshContentCity_SitePopUp()
//                                    weakSelf?.OpenCity_SitePopUp()
                                }
//                            }
                        }
                        catch {
                            print(error.localizedDescription)
                        }
                        DispatchQueue.main.async {
                            SwiftOverlays.removeAllBlockingOverlays()
                        }

                    }
                }else{
                    // BackgroundApiCall
                    self.endBackGroundTask()
                    print(error?.localizedDescription ?? "Error")

                    DispatchQueue.main.async {
                        //                    UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                        SwiftOverlays.removeAllBlockingOverlays()
                    }
                    UIHelper.showAlert(message: self.ServerErrorPleaseTryAgain, inViewController: self)
                }
                }
                .resume()
        }
    }

    @IBAction func proceedBtnAction(_ sender: Any) {
        closePopUpAlertView()
        //goToRTDtoolVC()
    }

    @IBAction func btn_selectSubSeaAction(_ sender: Any) {
    }

    @IBAction func selectSubSea_action(_ sender: Any) {

        if selectSite_B_btn.currentTitle == select_Site_B || selectSite_B_btn.currentTitle == nil {
            UIHelper.showAlert(message: "Please select site B", inViewController: self)
        } else if selectSiteAbtn.currentTitle == select_Site_A || selectSiteAbtn.currentTitle == nil {
            UIHelper.showAlert(message: "Please select site A", inViewController: self)
        }
        else{

            isSiteTye = false
            isCityType = false
            isZoneType = false
            isSubSeaType = true

            let tempFrame = btn_selectSubSea.convert(btn_selectSubSea.frame, to: self.view)
            popUpTopSpace = tempFrame.origin.y - (190.0+50.0)
            cityOrSiteType = CityOrSiteType.SubSeaType
            callSubSea_API()

        }
    }

    func callSubSea_API() {

        if(subSeaArr.count > 0) {
            OpenCity_SitePopUp()
            return
        }

        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {

            // BackgroundApiCall
            endBackGroundTask()
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
        }else{
            DispatchQueue.main.async {
                SwiftOverlays.showBlockingWaitOverlayWithText(self.please_wait)
            }
            
            let urlString = "http://loncis01/rtdapi/ci/getSubSeaConnections?siteA=\((siteInfoA()?.siteId)!)&siteB=\((siteInfoB()?.siteId)!)"
//            let urlString = "http://lonrtd02:8080/rtdapi/ci/getSubSeaConnections?siteA=\((siteInfoA()?.siteId)!)&siteB=\((siteInfoB()?.siteId)!)"
//            let request = NSMutableURLRequest(url: URL(string: urlString)!)
//            request.httpMethod = "GET"
//            request.addValue("*/*", forHTTPHeaderField: "Accept")
            
            var errMsg:String?
            let tokenString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "token")
            let accountTypeString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "accountType")
            let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
            let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/connectnew")!)
                request.httpMethod = "GET"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
                request.addValue(base64String!, forHTTPHeaderField: "X-CONNECT-U")
                request.addValue(tokenString!, forHTTPHeaderField: "X-GATEWAY-A")
                request.addValue(accountTypeString!, forHTTPHeaderField: "X-GATEWAY-F")
                request.timeoutInterval=600
            let authKey = "rtdapiuser:zAq1#mLp0"
            let encodedAuthKey = authKey.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64AuthKey = encodedAuthKey?.base64EncodedString(options: .init(rawValue: 0))
//                request.addValue("Basic \(base64AuthKey!)", forHTTPHeaderField: "Authorization")
            
            let session = URLSession.shared
            weak var weakSelf = self
            _ = session.dataTask(with: request as URLRequest) { (data, response, error) in

                if error == nil
                {
                    if let data = data
                    {

                        do {

                            let tempAarray = try JSONSerialization.jsonObject(with: data, options: []) as? [[String:Any]]

                            if let array = tempAarray {
//                                print(array)
                                if array.count > 0 {

                                    var subSeaNameArr: [String] = []
                                    for sModal in array {
                                        let lName = sModal["linkName"] as! String
                                        subSeaNameArr.append(lName)
                                    }

                                    weakSelf?.subSeaArr = subSeaNameArr
                                    weakSelf?.subSeaDataModalArray = NSArray.init(array: array)

                                    DispatchQueue.main.async {
                                        SwiftOverlays.removeAllBlockingOverlays()
                                        weakSelf?.RefreshContentCity_SitePopUp()
                                        weakSelf?.OpenCity_SitePopUp()
                                    }
                                }else{
                                    DispatchQueue.main.async {
                                        SwiftOverlays.removeAllBlockingOverlays()
                                    }
                                    UIHelper.showAlert(message:"Please try again later", inViewController: self)
                                }

                            }else{
                                DispatchQueue.main.async {
                                    SwiftOverlays.removeAllBlockingOverlays()
                                }
                               UIHelper.showAlert(message:"Please try again later", inViewController: self)
                            }
                        }
                        catch {
                            print(error.localizedDescription)
                        }
                        DispatchQueue.main.async {
                            SwiftOverlays.removeAllBlockingOverlays()
                        }

                    }
                }else{
                    // BackgroundApiCall
                    self.endBackGroundTask()
                    print(error?.localizedDescription ?? "Error")

                    DispatchQueue.main.async {
                        //                    UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                        SwiftOverlays.removeAllBlockingOverlays()
                    }
                    UIHelper.showAlert(message: self.ServerErrorPleaseTryAgain, inViewController: self)
                }
                }
                .resume()
        }
    }

    func checkForSubseaOption() {

        let kSelectedAZone = (selectZoneAbtn.titleLabel?.text)!
        let kSelectedBZone = (selectZoneBbtn.titleLabel?.text)!

        let kSelectedACity = (selectCityAbtn.titleLabel?.text)!
        let kSelectedBCity = (selectCity_B_btn.titleLabel?.text)!


        let kSelectedCountryNameA = siteInfoA()?.countryName
        let kSelectedCountryNameB = siteInfoB()?.countryName

        if !(kSelectedAZone.isEmpty) && !(kSelectedBZone.isEmpty) && kSelectedAZone != select_Zone_A && kSelectedBZone != select_Zone_B {
            if kSelectedAZone != kSelectedBZone {
                showAndHideSubSeaView(isHide: false)
                return
            }

            if !(kSelectedACity.isEmpty) && !(kSelectedBCity.isEmpty) && kSelectedACity != select_City_A && kSelectedBCity != select_City_B {
                if kSelectedAZone == kSelectedBZone && kSelectedAZone == "ASIA" && kSelectedACity != kSelectedBCity  {
                    showAndHideSubSeaView(isHide: false)

                    if kSelectedAZone == kSelectedBZone && kSelectedAZone == "ASIA" && kSelectedCountryNameA == kSelectedCountryNameB {

                        showAndHideSubSeaView(isHide: true)
                        return
                    }

                    let kSelectedSiteA = (selectSiteAbtn.titleLabel?.text)!
                    let kSelectedSiteB = (selectSite_B_btn.titleLabel?.text)!

                    if !(kSelectedSiteA.isEmpty) && !(kSelectedSiteB.isEmpty) && kSelectedSiteA != select_Site_A && kSelectedSiteB != select_Site_B {


                        if kSelectedCountryNameA == kSelectedCountryNameB {
                            showAndHideSubSeaView(isHide: true)
                            return
                        }
                    }

                    return
                }

                if kSelectedAZone == kSelectedBZone {
                    showAndHideSubSeaView(isHide: true)
                    return
                }


            }
            if kSelectedAZone == kSelectedBZone {
                showAndHideSubSeaView(isHide: true)
                return
            }
            if !isSubSeaSelected {
                showAndHideSubSeaView(isHide: true)
            }

        }
        setSubmitButtonAction()
    }

    func showAndHideSubSeaView(isHide: Bool) {

        if isHide {
            view_subsea.isHidden = true
            layoutHeight_submitView.constant = 67.0

            if isSubSeaSelected {
                isSubSeaSelected = false
                btn_customSubSea.setImage(UIImage(named: "ic_round_uncheck_box.png"), for: .normal)
                btn_customSubSea.setTitleColor(UIColor.darkGray, for: .normal)
                shortestRouteBtn.setImage(UIImage(named: "ic_round_check_box.png"), for: .normal)
                shortestRouteBtn.setTitleColor(UIColor.c_Teal, for: .normal)
                isShortestRouteSelected = true
            }
        }else{
            view_subsea.isHidden = false
            if isSubSeaSelected {
                layoutHeight_submitView.constant = 200.0
                view_subSeaSelection.isHidden = false
            }else{
                layoutHeight_submitView.constant = 96.0
                view_subSeaSelection.isHidden = true
            }
        }
    }

    @IBAction func sendE_MailBtnAction(_ sender: Any) {
        
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self

        // Configure the fields of the interface.
        composeVC.setToRecipients(["NWSCoreNetworkCapacityManagement@colt.com"])
        composeVC.setSubject("")
        composeVC.setMessageBody("", isHTML: false)

        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
        
    }
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue :
            print("Cancelled")
            
        case MFMailComposeResult.failed.rawValue :
            print("Failed")
            
        case MFMailComposeResult.saved.rawValue :
            print("Saved")
            
        case MFMailComposeResult.sent.rawValue :
            print("Sent")
            
        default: break
            
            
        }
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func goToRTDtoolVC(with route:[RouteDetailModel]) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let rTDtoolViewController = storyBoard.instantiateViewController(withIdentifier: "RTDtoolViewController") as! RTDtoolViewController
            rTDtoolViewController.isDiverseRouteSelected = self.isDiverseRouteSelected
            rTDtoolViewController.isShortestRouteSelected = self.isShortestRouteSelected
            rTDtoolViewController.isSubSeaRouteSelected = self.isSubSeaSelected
            rTDtoolViewController.routes = route
            rTDtoolViewController.siteA_ID = siteInfoA()?.siteId
            rTDtoolViewController.siteB_ID = siteInfoB()?.siteId     
            rTDtoolViewController.subSeaLink = isSubSeaSelected == true ? route[0].subSeaLinkNmae:nil
        if isSubSeaSelected == true {
            rTDtoolViewController.searchType = "C"
            rTDtoolViewController.subSeaLinkID = linkIDStr
        }else{
            rTDtoolViewController.searchType = isShortestRouteSelected == true ? "U":"P"
        }
        self.navigationController?.pushViewController(rTDtoolViewController, animated: true)

//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let rTDtoolViewController = storyBoard.instantiateViewController(withIdentifier: "RTDtoolViewController") as! RTDtoolViewController
//        rTDtoolViewController.isDiverseRouteSelected = self.isDiverseRouteSelected
//        rTDtoolViewController.isShortestRouteSelected = self.isShortestRouteSelected
//        rTDtoolViewController.routes = route
//        self.navigationController?.pushViewController(rTDtoolViewController, animated: true)
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        closePopUpAlertView()
    }
    @IBAction func popUpsBackgroundBtnBtnAction(_ sender: Any) {
       popUpsBackgroundBtn.isHidden = true
        selectCityandSitePopUp?.removeFromSuperview()
    }
    
    func closePopUpAlertView()  {
        popUpsBackgroundBtn.isHidden = true
        popUpAlertView.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func OpenCity_SitePopUp() {
        popUpsBackgroundBtn.isHidden = false
        selectCityandSitePopUp = Bundle.main.loadNibNamed("SelectCityandSitePopUp", owner: self, options: nil)?[0] as? SelectCityandSitePopUp
        let viewHeight = selectCityandSitePopUp?.frame.size.height
        selectCityandSitePopUp?.viewHeight = viewHeight
        selectCityandSitePopUp?.viewController = self
        selectCityandSitePopUp?.frame = CGRect(x: 8, y: popUpTopSpace!, width: selectCityAbtn.frame.size.width, height: 190)
        self.view.addSubview(selectCityandSitePopUp!)
        RefreshContentCity_SitePopUp()
    }
    
    func RefreshContentCity_SitePopUp(){
        if let popUp = selectCityandSitePopUp {
            if(popUp.superview == nil){
                return
            }
            
            if isZoneType==true  {
                popUp.list = zonesArr
            }
            if isCityType==true  {
                popUp.list = citiesArr
            }
            if isSiteTye==true{
                if cityOrSiteType == .SiteAtype {
                    popUp.list = sitesArrA
                }else{
                    popUp.list = sitesArrB
                }
            }
            if isSubSeaType==true{
                popUp.list = NSArray.init(array: subSeaArr)
            }
            popUp.tableView.reloadData()
        }
    }
    
    func callZoneslistAPI () {
        if(zonesArr.count > 0) {
            OpenCity_SitePopUp()
            return
        }
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
        // BackgroundApiCall
        endBackGroundTask()
        DispatchQueue.main.async {
            UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
        }
        }else{
            DispatchQueue.main.async {
                SwiftOverlays.showBlockingWaitOverlayWithText(self.please_wait)
            }
            var errMsg:String?
            let tokenString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "token")
            let accountTypeString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "accountType")
            let urlString = "http://loncis01/rtdapi/zi/getAvailableZone";
//            let urlString = "http://lonrtd02:8080/rtdapi/zi/getAvailableZone"
            
            let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
            let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/connectnew")!)
                request.httpMethod = "GET"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
                request.addValue(base64String!, forHTTPHeaderField: "X-CONNECT-U")
                request.addValue(tokenString!, forHTTPHeaderField: "X-GATEWAY-A")
                request.addValue(accountTypeString!, forHTTPHeaderField: "X-GATEWAY-F")
                //        request.timeoutInterval=TimeInterval(MAXFLOAT)
            let authKey = "rtdapiuser:zAq1#mLp0"
            let encodedAuthKey = authKey.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64AuthKey = encodedAuthKey?.base64EncodedString(options: .init(rawValue: 0))
//            let authbase64 = "Basic \(base64AuthKey!)"
//                request.addValue(authbase64 , forHTTPHeaderField: "Authorization")
                request.timeoutInterval=600
            let session = URLSession.shared
            weak var weakSelf = self
            _ = session.dataTask(with: request as URLRequest) { (data, response, error) in
                
                if error == nil
                {
                    if let data = data
                    {
                        do {
                            if let array = try JSONSerialization.jsonObject(with: data, options: []) as? [String] {
                                
                                DispatchQueue.main.async {
                                    weakSelf?.zonesArr = NSArray.init(array: array)
                                    weakSelf?.RefreshContentCity_SitePopUp()
                                    weakSelf?.OpenCity_SitePopUp()
                                }
                            }
                        }
                        catch {}
                        DispatchQueue.main.async {
                            SwiftOverlays.removeAllBlockingOverlays()
                        }
                    }
                }else{
                    // BackgroundApiCall
                    self.endBackGroundTask()
                    print(error?.localizedDescription ?? "Error")
                  
                    DispatchQueue.main.async {
                        //                    UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                        SwiftOverlays.removeAllBlockingOverlays()
                    }
                    UIHelper.showAlert(message: self.ServerErrorPleaseTryAgain, inViewController: self)
                }
             }
             .resume()
          }
    }

    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
 
    func callCitieslistAPI (with zoneName:String) {
        if(citiesArr.count > 0 && citiesArrForZoneName == zoneName){
            OpenCity_SitePopUp()
            return
        }
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
            // BackgroundApiCall
            endBackGroundTask()
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
        }else{
            DispatchQueue.main.async {
                SwiftOverlays.showBlockingWaitOverlayWithText(self.please_wait)
            }
            var errMsg:String?
            let tokenString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "token")
            let accountTypeString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "accountType")
            let urlString = "http://loncis01/rtdapi/si/getCitiesByZone?zone=\(zoneName)"
//            string url = String.Format("http://loncis01/rtdapi/si/getCitiesByZone?zone={0}", zone);
//            let urlString = "http://lonrtd02:8080/rtdapi/si/getCitiesByZone?zone=\(zoneName)"
            let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
            let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/connectnew")!)
                request.httpMethod = "GET"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
                request.addValue(base64String!, forHTTPHeaderField: "X-CONNECT-U")
                request.addValue(tokenString!, forHTTPHeaderField: "X-GATEWAY-A")
                request.addValue(accountTypeString!, forHTTPHeaderField: "X-GATEWAY-F")
                //        request.timeoutInterval=TimeInterval(MAXFLOAT)
            let authKey = "rtdapiuser:zAq1#mLp0"
            let encodedAuthKey = authKey.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64AuthKey = encodedAuthKey?.base64EncodedString(options: .init(rawValue: 0))
//                request.addValue("Basic \(base64AuthKey!)", forHTTPHeaderField: "Authorization")
                request.timeoutInterval=600
            weak var weakSelf = self
            let session = URLSession.shared
            _ = session.dataTask(with: request as URLRequest) { (data, response, error) in
                
                if error == nil
                {
                    if let data = data
                    {
                        do {
                            if let array = try JSONSerialization.jsonObject(with: data, options: []) as? [String] {
                                
                                DispatchQueue.main.async {
                                    weakSelf?.citiesArr = NSArray.init(array: array)
                                    weakSelf?.citiesArrForZoneName = zoneName
                                    weakSelf?.OpenCity_SitePopUp()
                                    weakSelf?.RefreshContentCity_SitePopUp()
                                }
                            }
                        }
                        catch {}
                        DispatchQueue.main.async {
                            SwiftOverlays.removeAllBlockingOverlays()
                        }
                    }
                }else{
                    // BackgroundApiCall
                    self.endBackGroundTask()
                    print(error?.localizedDescription ?? "Error")
                    
                    DispatchQueue.main.async {
                        //                    UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                        SwiftOverlays.removeAllBlockingOverlays()
                    }
                    UIHelper.showAlert(message: self.ServerErrorPleaseTryAgain, inViewController: self)
                }
                }
                .resume()
        }
    }

    func callSiteslistAPI (with cityName:String,isSiteA:Bool, andCompletionCode block:@escaping(([SiteInfoModel])->Void)) {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
            // BackgroundApiCall
            endBackGroundTask()
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
        }else{
            DispatchQueue.main.async {
                SwiftOverlays.showBlockingWaitOverlayWithText(self.please_wait)
            }
            var errMsg:String?
            let tokenString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "token")
            let accountTypeString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "accountType")
            let urlString = "http://loncis01/rtdapi/si/getVisibleSitesByCity?city=\(cityName)"
//            let urlString = "http://lonrtd02:8080/rtdapi/si/getVisibleSitesByCity?city=\(cityName)"
            let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
            let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/connectnew")!)
                request.httpMethod = "GET"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
                request.addValue(base64String!, forHTTPHeaderField: "X-CONNECT-U")
                request.addValue(tokenString!, forHTTPHeaderField: "X-GATEWAY-A")
                request.addValue(accountTypeString!, forHTTPHeaderField: "X-GATEWAY-F")
                //        request.timeoutInterval=TimeInterval(MAXFLOAT)
            let authKey = "rtdapiuser:zAq1#mLp0"
            let encodedAuthKey = authKey.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64AuthKey = encodedAuthKey?.base64EncodedString(options: .init(rawValue: 0))
//                request.addValue("Basic \(base64AuthKey!)", forHTTPHeaderField: "Authorization")
                request.timeoutInterval=600
            weak var weakSelf = self
            let session = URLSession.shared
            _ = session.dataTask(with: request as URLRequest) { (data, response, error) in
                
                if error == nil
                {
                    if let data = data
                    {
                        let siteInfos = SiteInfoModel.getSites(from: data)
                        if(siteInfos.count > 0){
                            let siteNames = siteInfos.map({ (model) -> String in
                                return model.siteName
                            })
                            DispatchQueue.main.async {
                                if isSiteA {
                                    weakSelf?.sitesArrA = NSArray.init(array: siteNames)
                                }else{
                                    weakSelf?.sitesArrB = NSArray.init(array: siteNames)
                                }

                                weakSelf?.RefreshContentCity_SitePopUp()
                            }
                        }
                        DispatchQueue.main.async {
                            SwiftOverlays.removeAllBlockingOverlays()
                            block(siteInfos)
                        }
                    }
                }else{
                    // BackgroundApiCall
                    self.endBackGroundTask()
                    print(error?.localizedDescription ?? "Error")
                    
                    DispatchQueue.main.async {
                        //                    UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                        SwiftOverlays.removeAllBlockingOverlays()
                    }
                    UIHelper.showAlert(message: self.ServerErrorPleaseTryAgain, inViewController: self)
                }
                }
                .resume()
        }
    }
    func callAddressAandBdetailsAPI () {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
            // BackgroundApiCall
            endBackGroundTask()
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
        }else{
            DispatchQueue.main.async {
                SwiftOverlays.showBlockingWaitOverlayWithText(self.please_wait)
            }
            var errMsg:String?
            let tokenString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "token")
            let accountTypeString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "accountType")
            let urlString = "http://loncis01/rtdapi/si/getSiteInfo?site=1"
//            let urlString = "http://lonrtd02:8080/rtdapi/si/getSiteInfo?site=1"
            let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
            let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/connectnew")!)
                request.httpMethod = "GET"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
                request.addValue(base64String!, forHTTPHeaderField: "X-CONNECT-U")
                request.addValue(tokenString!, forHTTPHeaderField: "X-GATEWAY-A")
                request.addValue(accountTypeString!, forHTTPHeaderField: "X-GATEWAY-F")
            let authKey = "rtdapiuser:zAq1#mLp0"
            let encodedAuthKey = authKey.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64AuthKey = encodedAuthKey?.base64EncodedString(options: .init(rawValue: 0))
//                request.addValue("Basic \(base64AuthKey!)", forHTTPHeaderField: "Authorization")
                //        request.timeoutInterval=TimeInterval(MAXFLOAT)
                request.timeoutInterval=600
            let session = URLSession.shared
            _ = session.dataTask(with: request as URLRequest) { (data, response, error) in
                
                if error == nil
                {
                    if let data = data, let result = String(data: data, encoding: String.Encoding.utf8)
                    {
                        let xml = SWXMLHash.parse(result)
                        DispatchQueue.main.async {
                            SwiftOverlays.removeAllBlockingOverlays()
                        }
                    }
                }else{
                    // BackgroundApiCall
                    self.endBackGroundTask()
                    print(error?.localizedDescription ?? "Error")
                    
                    DispatchQueue.main.async {
                        //                    UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                        SwiftOverlays.removeAllBlockingOverlays()
                    }
                    UIHelper.showAlert(message: self.ServerErrorPleaseTryAgain, inViewController: self)
                }
                }
                .resume()
        }
    }
 
    func callShortestRouteAPI () {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
            // BackgroundApiCall
            endBackGroundTask()
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
        }else{
            DispatchQueue.main.async {
                SwiftOverlays.showBlockingWaitOverlayWithText(self.please_wait)
            }
            var errMsg:String?
            let tokenString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "token")
            let accountTypeString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "accountType")
            //let urlString = "http://loncis01/rtdapi/sr/getShortestRoute?siteA=\((siteInfoA()?.siteId)!)&siteB=\((siteInfoB()?.siteId)!)"  //Old
            let email = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "email")
            let user_name = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "username")
            
            let urlString = "http://loncis01/rtdapi/sr/getShortestRouteNew?siteA=\((siteInfoA()?.siteId)!)&siteB=\((siteInfoB()?.siteId)!)&userId=\(user_name!)&email=\(email!)&sourceSystem=I"
//            let urlString = "http://lonrtd02:8080/rtdapi/sr/getShortestRouteNew?siteA=\((siteInfoA()?.siteId)!)&siteB=\((siteInfoB()?.siteId)!)&userId=\(user_name!)&email=\(email!)&sourceSystem=I"

            let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
            let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/connectnew")!)
//                     let request = NSMutableURLRequest(url: URL(string: urlString)!)
                request.httpMethod = "GET"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
//                request.addValue("*/*", forHTTPHeaderField: "Accept")
                request.addValue(base64String!, forHTTPHeaderField: "X-CONNECT-U")
                request.addValue(tokenString!, forHTTPHeaderField: "X-GATEWAY-A")
                request.addValue(accountTypeString!, forHTTPHeaderField: "X-GATEWAY-F")
                //        request.timeoutInterval=TimeInterval(MAXFLOAT)
            let authKey = "rtdapiuser:zAq1#mLp0"
            let encodedAuthKey = authKey.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64AuthKey = encodedAuthKey?.base64EncodedString(options: .init(rawValue: 0))
//                request.addValue("Basic \(base64AuthKey!)", forHTTPHeaderField: "Authorization")
                request.timeoutInterval=600
            let session = URLSession.shared
            weak var weakSelf = self
            _ = session.dataTask(with: request as URLRequest) { (data, response, error) in
                
                if error == nil
                {
                    if let data = data
                    {
                        DispatchQueue.main.async {
                            SwiftOverlays.removeAllBlockingOverlays()
                            if let route = RouteDetailModel.getRoute(from: data){
                                if route.error.isEmpty {
                                   weakSelf?.goToRTDtoolVC(with:[route])
                                }else{
                                    var err_msg : String?
                                    let tempVal2 =  CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "employeeType")
//                                    var message = route.error
                                    if tempVal2 != "Colt Employee" {
                                        let message = self.furtherAnalysisIsRequired
                                        let alertCity = UIAlertController(title: "", message: message, preferredStyle: .alert)
                                        let okAction = UIAlertAction(title: self.oK, style: .default, handler: nil)
                                        alertCity.addAction(okAction)
                                        weakSelf?.present(alertCity, animated: true, completion: nil)
                                    }else {
                                        weakSelf?.popUpsBackgroundBtn.isHidden = false
                                        weakSelf?.popUpAlertView.isHidden = false
                                   }
                                }
                            }
                        }
                    }
                }else{
                    // BackgroundApiCall
                    self.endBackGroundTask()
                    print(error?.localizedDescription ?? "Error")
                    
                    DispatchQueue.main.async {
                        //                    UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                        SwiftOverlays.removeAllBlockingOverlays()
                    }
                    UIHelper.showAlert(message: self.ServerErrorPleaseTryAgain, inViewController: self)
                }
            }
            .resume()
        }
    }
    func callDiverseRouteAPI () {
       
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
            // BackgroundApiCall
            endBackGroundTask()
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
        }else{
            DispatchQueue.main.async {
                SwiftOverlays.showBlockingWaitOverlayWithText(self.please_wait)
            }
            
            
            var errMsg:String?
            let tokenString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "token")
            let accountTypeString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "accountType")
            let email = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "email")
            let user_name = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "username")
            let urlString = "http://loncis01/rtdapi/dr/getDiverseRouteNew?siteA=\((siteInfoA()?.siteId)!)&siteB=\((siteInfoB()?.siteId)!)&userId=\(user_name!)&email=\(email!)&sourceSystem=I"
//            let urlString = "http://lonrtd02:8080/rtdapi/dr/getDiverseRouteNew?siteA=\((siteInfoA()?.siteId)!)&siteB=\((siteInfoB()?.siteId)!)&userId=\(user_name!)&email=\(email!)&sourceSystem=I"
            let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
            let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/connectnew")!)
                request.httpMethod = "GET"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
                request.addValue(base64String!, forHTTPHeaderField: "X-CONNECT-U")
                request.addValue(tokenString!, forHTTPHeaderField: "X-GATEWAY-A")
                request.addValue(accountTypeString!, forHTTPHeaderField: "X-GATEWAY-F")
            let authKey = "rtdapiuser:zAq1#mLp0"
            let encodedAuthKey = authKey.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64AuthKey = encodedAuthKey?.base64EncodedString(options: .init(rawValue: 0))
//                request.addValue("Basic \(base64AuthKey!)", forHTTPHeaderField: "Authorization")
            
//            http://lonrtd02:8080/rtdapi/dr/getDiverseRouteNew?siteA=92&siteB=94&userId=lsahni_cim&email=deovrat.singh@colt.net&sourceSystem=A
            
//            var errMsg:String?
//            let tokenString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "token")
//            let accountTypeString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "accountType")
//            let urlString = "http://loncis01/rtdapi/dr/getDiverseRoute?siteA=\((siteInfoA()?.siteId)!)&siteB=\((siteInfoB()?.siteId)!)"  // old
//            let email = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "email")
//            let user_name = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "username")
//            let urlString = "https://dcp.colt.net/rtdapi/dr/getDiverseRouteNew?siteA=\((siteInfoA()?.siteId)!)&siteB=\((siteInfoB()?.siteId)!)&userId=\(user_name!)&email=\(email!)&sourceSystem=I"
//            let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
//            let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
//            let request = NSMutableURLRequest(url: URL(string: urlString)!)
//                request.httpMethod = "GET"
//                request.addValue("*/*", forHTTPHeaderField: "Accept")
//                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//                request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
//                request.addValue(base64String!, forHTTPHeaderField: "X-CONNECT-U")
//                request.addValue(tokenString!, forHTTPHeaderField: "X-GATEWAY-A")
//                request.addValue(accountTypeString!, forHTTPHeaderField: "X-GATEWAY-F")
                //        request.timeoutInterval=TimeInterval(MAXFLOAT)
                request.timeoutInterval=600
            let session = URLSession.shared
            weak var weakSelf = self
            _ = session.dataTask(with: request as URLRequest) { (data, response, error) in
                
                if error == nil
                {
                    if let data = data
                    {
                        DispatchQueue.main.async {
                            SwiftOverlays.removeAllBlockingOverlays()
                            let routes = RouteDetailModel.getRoutes(from: data)
                            if(routes.count > 0){
                                weakSelf?.goToRTDtoolVC(with:routes)
                            }else{
                                var err_msg : String?
                                let tempVal2 =  CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "employeeType")
                                if tempVal2 != "Colt Employee" {
                                    let message = self.furtherAnalysisIsRequired
                                    let alertCity = UIAlertController(title: "", message: message, preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: self.oK, style: .default, handler: nil)
                                    alertCity.addAction(okAction)
                                    weakSelf?.present(alertCity, animated: true, completion: nil)
                                }
                                else{
                                    weakSelf?.popUpsBackgroundBtn.isHidden = false
                                    weakSelf?.popUpAlertView.isHidden = false
                                }
                            }
                        }
                    }
                }else{
                    // BackgroundApiCall
                    self.endBackGroundTask()
                    print(error?.localizedDescription ?? "Error")
                    
                    DispatchQueue.main.async {
                        //                    UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                        SwiftOverlays.removeAllBlockingOverlays()
                    }
                    UIHelper.showAlert(message: self.ServerErrorPleaseTryAgain, inViewController: self)
                }
             }
            .resume()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
