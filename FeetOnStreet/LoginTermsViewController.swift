//
//  LoginTermsViewController.swift
//  FeetOnStreet
//
//  Created by admin on 9/8/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import SwiftOverlays

class LoginTermsViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var nextBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
//        let statusBarColor = UIColor(red: 0/255, green: 105/255, blue: 95/255, alpha: 1.0)
//        statusBarView.backgroundColor = statusBarColor
//        view.addSubview(statusBarView)
        nextBtn.setTitleColor(UIColor.darkGray, for: .disabled)
        nextBtn.isEnabled = false
        let lang = LanguagesViewController.getSelectedLanguage()
        var url:URL!
        if lang == "fr" {
            url = Bundle.main.url(forResource: "French_legal", withExtension:"html")
        }
        else if lang == "de" {
            url = Bundle.main.url(forResource: "German_legal", withExtension:"html")
        }
        else if lang == "it" {
            url = Bundle.main.url(forResource: "Italian_legal", withExtension:"html")
        }
        else if lang == "ja" {
            url = Bundle.main.url(forResource: "Japanese_legal", withExtension:"html")
        }
        else if lang == "pt-PT" {
            url = Bundle.main.url(forResource: "Portugese_legal", withExtension:"html")
        }
        else if lang == "es-ES" {
            url = Bundle.main.url(forResource: "Spanish_legal", withExtension:"html")
        }else{
            url = Bundle.main.url(forResource: "LegalNotice", withExtension:"html")
        }

//        if let url = Bundle.main.url(forResource: "LegalNotice", withExtension: "html") {
          DispatchQueue.main.async {
            self.webView.loadRequest(URLRequest(url: url))
            }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
//        AnalyticsHelper().analyticLogScreen(screen: "Terms of use screen")
        let next = NSLocalizedString("NEXT", comment: "")
        nextBtn.setTitle(next, for: .normal)
        
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked {
            //            UIApplication.shared.openURL(request.url!)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(request.url!)
            }
            return false
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func nextBtnAction(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let buildingDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "LoginPrivacyViewController") as! LoginPrivacyViewController
        self.navigationController?.pushViewController(buildingDetailsViewController, animated: true)
    }
    
    
    public func webViewDidStartLoad(_ webView: UIWebView){
//        AlertManager.shared.showLoading(self)
        let Please_wait = NSLocalizedString("Please wait", comment: "")
        SwiftOverlays.showBlockingWaitOverlayWithText(Please_wait)
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView){
//        AlertManager.shared.hideLoading()
        SwiftOverlays.removeAllBlockingOverlays()
        nextBtn.isEnabled = true
    }
    
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        SwiftOverlays.removeAllBlockingOverlays()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
