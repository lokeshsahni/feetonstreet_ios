//
//  NearnetAlertView.swift
//  FeetOnStreet
//
//  Created by admin on 2/15/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class NearnetAlertView: UIView {
    
    var viewHeight:CGFloat?
    var viewController: UIViewController?
    var dashBoardViewController: DashBoardViewController?
    var currentMarker:GMSMarker?
    var currentAddress:NSString?
    var address:String?
    
    @IBOutlet weak var addressTextLbl: UILabel!
    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        if currentMarker == nil {
            address = currentAddress as String?
        }else{
//            let addressStr = currentMarker?.accessibilityValue
            let addressDescription = currentMarker?.accessibilityValue?.trimmingCharacters(in: CharacterSet(charactersIn:" ,"))
            print(addressDescription)
            address = addressDescription
        }
        addressTextLbl.text = address
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "CloseNearnetPopUp"), object: nil)
        dashBoardViewController?.selectedName = ""
    }
    
}
