//
//  Language.swift
//  FeetOnStreet
//
//  Created by admin on 4/11/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

// constants
let APPLE_LANGUAGE_KEY = "AppleLanguages"
/// Language
class Language {
    /// get current Apple language
    class func currentAppleLanguage() -> String {
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let current = langArray.firstObject as! String
        let endIndex = current.startIndex
        let currentWithoutLocale = current.substring(to: current.index(endIndex, offsetBy: 2))
        return currentWithoutLocale
    }
    
    class func currentAppleLanguageFull() -> String{
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let current = langArray.firstObject as! String
        return current
    }
    
    /// set @lang to be the first in Applelanguages list
    class func setAppleLAnguageTo(lang: String) {
        let userdef = UserDefaults.standard
        userdef.set([lang], forKey: APPLE_LANGUAGE_KEY)
        userdef.synchronize()
    }
    
    /// get @lang to be the first in Applelanguages list
    class func getAppleLanguage() -> [String] {
        let userdef = UserDefaults.standard
        return userdef.value(forKey: APPLE_LANGUAGE_KEY) as! [String]
    }
    
//    class var isRTL: Bool {
//        return Language.currentAppleLanguage() == "ar"
//    }
}

