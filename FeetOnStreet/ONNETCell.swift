//
//  ONNETCell.swift
//  FeetOnStreet
//
//  Created by admin on 8/18/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class ONNETCell: UITableViewCell {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var inActiveBtn: UIButton!
    @IBOutlet weak var inHouseCablingBtn: UIButton!
    @IBOutlet weak var locationBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
