
//
//  OLOService.swift
//  FeetOnStreet
//
//  Created by admin on 9/5/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class OLOService: NSObject {
    
    var oLOArray:[OLOModel]?
    
    func getOLOdetails(_ permisesnum : String, streetName: String, cityTown: String, postCode: String, country: String, selectedProduct: String, selectedBandwidth: String, latitude: String, longitude: String, onComplete:@escaping((Any)-> Void) ) {
        
        let requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"> <soapenv:Body> <ax:checkConnectivityRequest ax:schemaVersion=\"3.0\" xmlns:ax=\"http://aat.colt.com/connectivityservice\"> <ax:sequenceId>1</ax:sequenceId> <ax:requestType>ALL</ax:requestType> <ax:requestMode> <ax:requestID>4495</ax:requestID> <ax:siteAddress> <ax:premisesNumber>\(permisesnum)</ax:premisesNumber>  <ax:streetName>\(streetName)</ax:streetName> <ax:cityTown>\(cityTown)</ax:cityTown> <ax:postalZipCode>\(postCode)</ax:postalZipCode> <ax:latitude>\(latitude)</ax:latitude> <ax:longitude>\(longitude)</ax:longitude>  <ax:coltOperatingCountry>\(country)</ax:coltOperatingCountry> <ax:requiredProduct>\(selectedProduct)</ax:requiredProduct>  <ax:bandwidth>\(selectedBandwidth)</ax:bandwidth> <ax:connectivityType>Leased Line</ax:connectivityType> </ax:siteAddress> </ax:requestMode> </ax:checkConnectivityRequest> </soapenv:Body> </soapenv:Envelope>"
        
        var errMsg:String?
        let tokenString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "token")
        let accountTypeString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "accountType")
        let urlString = "http://loncis01/checkConnectivity/connectivity.wsdl"
        let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
        let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
        let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/connectnew")!)
        request.httpMethod = "POST"
        let requestData = requestBody.data(using: String.Encoding.utf8)
        request.httpBody = requestData
        request.addValue("text/xml", forHTTPHeaderField: "Content-Type")
        request.addValue("olo", forHTTPHeaderField: "SOAPAction")
        request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
        request.addValue(base64String!, forHTTPHeaderField: "X-CONNECT-U")
        request.addValue(tokenString!, forHTTPHeaderField: "X-GATEWAY-A")
        request.addValue(accountTypeString!, forHTTPHeaderField: "X-GATEWAY-F")
//        request.timeoutInterval=TimeInterval(MAXFLOAT)
        request.timeoutInterval=600
        
        let session = URLSession.shared
//        session.configuration.timeoutIntervalForRequest = TimeInterval(MAXFLOAT)
        _ = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            if error == nil
            {
                if let data = data, let result = String(data: data, encoding: String.Encoding.utf8)
                {
                    let xml = SWXMLHash.parse(result)
                    print(xml)
                    self.oLOArray = OLOModel.getOLOresponse(xmlResponseData: xml)
//                    CommonDataHelper.sharedInstance.oLO_results = self.oLOArray as! NSMutableArray
                    CommonDataHelper.sharedInstance.oLO_results = (self.oLOArray as! NSArray).mutableCopy() as! NSMutableArray
                    let tempDict = ["ProductType":selectedProduct ,"Bandwidthype":selectedBandwidth ]
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "OLO_data"), object: tempDict)
                    onComplete(tempDict)
                }
                else
                {
                    print("Failed to get valid response from server.")
                     NotificationCenter.default.post(name: Notification.Name(rawValue: "OLO_data"), object: "failure")
                    onComplete("failure")
                }
            }
            else
            {
                print(error?.localizedDescription ?? "Error")
                 NotificationCenter.default.post(name: Notification.Name(rawValue: "OLO_data"), object: "failure")
                onComplete("failure")
                
            }
            
            }
            .resume()
    }
    
}
