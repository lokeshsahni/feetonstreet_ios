//
//  AlertManager.swift
//  FeetOnStreet
//
//  Created by Syed Saud Arif on 19/09/17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation
import UIKit

class AlertManager {
    
    var alertView:UIAlertController? = nil
    var currentVC:UIViewController? = nil
    //var isShowRequested:Bool = false
    var isShowing = false
    var isHideRequested = true
    //var isHideCompleted = true
    
    static let shared = AlertManager()
    
    init(){
        self.alertView = nil;
        self.currentVC = nil;
        //self.isShowRequested = false
        self.isShowing = false
        self.isHideRequested = false
    }
    
    
    func showLoading(_ vc:UIViewController){
        showLoading(title: "Please Wait", message: nil, preferredStyle: UIAlertControllerStyle.alert, on: vc)
    }
    
    func showLoading(title: String?, message: String?, on vc:UIViewController){
        showLoading(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert, on: vc)
    }
    
    func ChangeMessage(title: String?, message: String?){
        DispatchQueue.main.sync {
            synced(self) {
            if(self.isShowing == true && self.isHideRequested == false){
                alertView?.title = title
                alertView?.message = message
            }
        }
        }
    }
    
    func ChangeMessage_AddOK(withCompletionHandler:((UIAlertAction) -> Void)? = nil)
    {
        DispatchQueue.main.sync {
            synced(self) {
                if(self.isShowing == true && self.isHideRequested == false){
                    let continueAction = UIAlertAction(title: "OK", style: .default, handler: {(a:UIAlertAction) in
                        withCompletionHandler!(a)
                        self.currentVC = nil
                        self.isShowing = false
                        self.isHideRequested = false
                    })
                    alertView?.addAction(continueAction)
                }
            }
        }
    }
    
    
    func showLoading(title: String?, message: String?, preferredStyle: UIAlertControllerStyle, on vc:UIViewController){
        
        synced(self) {
            
            if(self.currentVC != nil){
                return
            }
            alertView = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
            self.currentVC = vc
            vc.present(alertView!, animated: true, completion: {() in
                self.isShowing = true
                if(self.isHideRequested){
                    self.hideLoading()
                }
            })
        }
    }
    
    func hideLoading(){
        synced(self) {
            self.isHideRequested = true;
            if(self.currentVC == nil) {
                return;
            }
            if(self.isShowing){
                self.currentVC?.dismiss(animated: true, completion: {() in
                    self.currentVC = nil
                    self.isShowing = false
                    self.isHideRequested = false
                })
            }
        }
    }
    
    func synced(_ lock: Any, closure: () -> ()) {
        objc_sync_enter(lock)
        closure()
        objc_sync_exit(lock)
    }
}



