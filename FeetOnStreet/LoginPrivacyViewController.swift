//
//  LoginPrivacyViewController.swift
//  FeetOnStreet
//
//  Created by admin on 9/8/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import SwiftOverlays

class LoginPrivacyViewController: UIViewController, UIWebViewDelegate  {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var checkMarkImgView: UIImageView!
    @IBOutlet weak var iHaveReadLbl: UILabel!
    
    var isCheckMarkbtnselected = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        continueBtn.isEnabled = false
        continueBtn.setTitleColor(UIColor.lightGray, for: .normal)
        
        let lang = LanguagesViewController.getSelectedLanguage()
        var url:URL!
        if lang == "fr" {
            url = Bundle.main.url(forResource: "French_privacy", withExtension:"html")
        }
        else if lang == "de" {
            url = Bundle.main.url(forResource: "German_privacy", withExtension:"html")
        }
        else if lang == "it" {
            url = Bundle.main.url(forResource: "Italian_privacy", withExtension:"html")
        }
        else if lang == "ja" {
            url = Bundle.main.url(forResource: "Japanese_privacy", withExtension:"html")
        }
        else if lang == "pt-PT" {
            url = Bundle.main.url(forResource: "Portugese_privacy", withExtension:"html")
        }
        else if lang == "es-ES" {
            url = Bundle.main.url(forResource: "Spanish_privacy", withExtension:"html")
        }else{
            url = Bundle.main.url(forResource: "privacy", withExtension:"html")
        }
        

//        if let url = Bundle.main.url(forResource: "privacy", withExtension: "html") {
            DispatchQueue.main.async {
            self.webView.loadRequest(URLRequest(url: url))
            }
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        AnalyticsHelper().analyticLogScreen(screen: "Legal Notice/Privacy Policy Screen")
        let continu = NSLocalizedString("CONTINUE", comment: "")
        continueBtn.setTitle(continu, for: .normal)
        let iHaveRead = NSLocalizedString("I have read", comment: "")
        iHaveReadLbl.text = iHaveRead
        
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked {
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(request.url!)
            }
            
            return false
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func checkMarkBtnAction(_ sender: Any) {
        
        if isCheckMarkbtnselected == false {
            checkMarkImgView.image = UIImage(named: "ic_check_box.png")
            continueBtn.isEnabled = true
            continueBtn.setTitleColor(UIColor.black, for: .normal)
            isCheckMarkbtnselected = true
           
        }else{
            checkMarkImgView.image = UIImage(named: "ic_uncheck_box.png")
            continueBtn.setTitleColor(UIColor.darkGray, for: .normal)
            isCheckMarkbtnselected = false
            
        }
    }
    @IBAction func continueBtnAction(_ sender: Any) {
        
         AnalyticsHelper().analyticsEventsAction(category: "Terms Acceptance", action: "termsAcceptanceContinue_click", label: "Continue")
        if isCheckMarkbtnselected == false {
            continueBtn.isEnabled = false
        }else{
            var _err_msg : String?
        
            
            let temp_userName = UserDefaults.standard.value(forKey: "temp_userName")
            let temp_password = UserDefaults.standard.value(forKey: "temp_password")
            
            if(temp_userName == nil || temp_password == nil){
                //TODO: User needs to login again...
            }
            else{

                UserDefaults.standard.set(true, forKey: "isTAndCAccepted")
                CoreDataHelper.sharedInstance.add_user_Credentials(&_err_msg, username: (temp_userName as! String), password: (temp_password as! String))
            
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let mainViewController = storyBoard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                self.navigationController?.pushViewController(mainViewController, animated: true)
                
            }
        }
     }
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    public func webViewDidStartLoad(_ webView: UIWebView){
        //        AlertManager.shared.showLoading(self)
        let Please_wait = NSLocalizedString("Please wait", comment: "")
        SwiftOverlays.showBlockingWaitOverlayWithText(Please_wait)
    }
    public func webViewDidFinishLoad(_ webView: UIWebView){
        //        AlertManager.shared.hideLoading()
        SwiftOverlays.removeAllBlockingOverlays()
    }
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        SwiftOverlays.removeAllBlockingOverlays()
    }
    

}
