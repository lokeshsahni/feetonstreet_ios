
//
//  DashBoardViewController.swift
//  FeetOnStreet
//
//  Created by admin on 7/27/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CoreLocation
import STZPopupView
import SwiftOverlays

class DashBoardViewController: UIViewController,GMSMapViewDelegate,UISearchBarDelegate,GMSAutocompleteViewControllerDelegate,PopupDelegate,ProductBandwithPopupDelegate
{
    
    @IBOutlet weak var mapsBackgroundView: UIView!
    @IBOutlet weak var onnetCount: UIBarButtonItem!
    @IBOutlet var mapViewTopSpace : NSLayoutConstraint!
    
    var menuShoing = false
    let menuListArray = ["Connectivity Checker","Account Settings","Data Privacy Statement","Terms of Use","Logout"]
    var menuIconsArray = ["ic_map_view","ic_account_settings","ic_data_privacy","ic_terms_of_use1x","ic_logout"]
    
    var currentMarker,currentLocationMarker,searchedMarker:GMSMarker?
    var tempSearchedMarker:GMSMarker?
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var placesClient: GMSPlacesClient!
    //    var zoomLevel: Float = 15.0
    var isCurrentLocationAdded = false
    var currenPlaceAddressComponents:[Any]?
    var searchedPlaceAddressComponents:[GMSAddressComponent]?
    var numberApiCall:Int = 0
    var numberOfResponseGot:Int = 0
    var currentPopup:Fos_Popup?
    var allMarkers:[GMSMarker] = []
    var isLongPressedOnMap = false
    
    // An array to hold the list of likely places.
    var likelyPlaces: [GMSPlace] = []
    
    // The currently selected place.
    var selectedPlace: GMSPlace?
    
//    let apiKey = "AIzaSyB7xlgy-rKGJ-WWmbR42UBxdfOevc7Tp90" //TODO: Make the key Constant see AppDelegate
    
    let apiKey = "AIzaSyAw5QstwbU-1cc2C4jTIlVaUbgIWvIDFYE"
    
    // Key created using paid account
//    let apiKey = "AIzaSyAYP3NIanhDTOXdf347RwkUQRrfcy-R1tg"
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var locaationSearch: UISearchBar!
    @IBOutlet weak var sideMenuView: UIView!
    @IBOutlet weak var sideMenuBackgroundBtn: UIButton!
    @IBOutlet weak var sideViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var sideViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var onNetBtn: UIButton!
    
    var isFirstTime : Bool = true
    var abArray:[ActiveAndInactiveBuildings]?
    var selectedSearchedAddress:ActiveAndInactiveBuildings?
    
    var oLOlist:[Any] = []
    var eMFDSLlist:[Any] = []
    
   
    static var s_gmsPlace:GMSPlace? = nil
    static var s_latitude:CLLocationDegrees? = nil
    static var s_longitude:CLLocationDegrees? = nil
    
    static var s_premissesNumber:String? = nil
    static var s_streetName:String? = nil
    static var s_cityName:String? = nil
    static var s_postcode:String? = nil
    static var s_country:String? = nil
    
    static var s_slectedBuildingLatitude:CLLocationDegrees? = nil
    static var s_selectedAddress:String? = nil
    static var s_searchedPlaceAddressComponents:[GMSAddressComponent]?
    static var s_selectedSearchedAddress:ActiveAndInactiveBuildings?
    var pDetails = PopupDetails()
    
    var productBandwithpopupView:ProductBandwithPopUpView?
    var selectedProduct:String?
    var selectedBandwidth:String?
    var selectedB_EndAddress:String?
    var aEndAddress_id:String?
    var selectedBendAdderess,selectedAddressModal:ActiveAndInactiveBuildings?
    var permisesNumber:String?
    var englishCityName:String?
    
    var bend_Address:String?
    var locationAddress:String?
    
    var nearnetPopUp:NearnetAlertView?
    
    // for nrarnet popup use
    var nearnetMarker: GMSMarker?
    var onnetListArray:[ActiveAndInactiveBuildings]?
    var isClickedOnMArkerIcon = false
    var tempIconMarker: GMSMarker?
    
    var savedSearchModelList:SavedSearchModel?
    
    var selectedSavedAddress:String?

    
    // BackgroundApiCall
    var backgroundTask:UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    // Localizable strings
    var no_internet_connection = ""
    var please_select_voiceChannel = ""
    var please_select_product = ""
    var please_select_bandwidth = ""
    var Please_select_hub_name = ""
    var Please_search_BEnd_address = ""
    var Please_select_BEnd_address = ""
    var SearchLocation = ""
    var connectivityChecker = ""
    var Please_wait = ""
    var ServerErrorPleaseTryTgain = ""
    
    
    var selectedName:String = ""{
        didSet {
            if currentMarker == nil {
                currentMarker = searchedMarker
            }
            self.sideMenuController?.isLeftViewDisabled = false
            self.nearnetPopUp?.removeFromSuperview()
            openOffnetPopUp(marker: currentMarker!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        locaationSearch.showsCancelButton = false
//        [locaationSearch.appearance().setImage(UIImage(named: ""), forSearchBarIcon: UISearchBarIcon.Clear, state: UIControlState.Normal)]
//        [locaationSearch setImage:clearImage forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal]
//        locaationSearch.setShowsCancelButton(false, animated: false)
//        locaationSearch.setImage(UIImage(), for: .clear, state: .normal)
//        locaationSearch.showsCancelButton = true
//        locaationSearch.delegate = self
        
        getLocalizedStrings()  // adding localized Strings
        
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
            
        }else {
        
        let searchBarTextColor = [NSForegroundColorAttributeName:UIColor.black,NSFontAttributeName:UIFont.systemFont(ofSize: 15.0)] as [String : Any]
        if #available(iOS 9.0, *) {
            let sBar = UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.classForCoder() as! UIAppearanceContainer.Type])
            sBar.defaultTextAttributes=searchBarTextColor
        } else {
            // Fallback on earlier versions
        }
        
        if CommonDataHelper.sharedInstance.isFromSavedsearch == true {
            
            searchedPlaceAddressComponents = nil // Need to confirm (assigning SavedSearcModel list to popup address detais )
            currenPlaceAddressComponents = nil
            clearSavedSearchCommonHelperData() // Clearing savedSearch data from CommonHelper class
            isLongPressedOnMap = false // Making false isLongPressedOnMap
            
            savedSearchModelList = CommonDataHelper.sharedInstance.savedSearchModelList
            if savedSearchModelList?.locationa_housenumber == nil{
                savedSearchModelList?.locationa_housenumber = ""
            }
            if savedSearchModelList?.locationa_streetname == nil{
                savedSearchModelList?.locationa_streetname = ""
            }
            if savedSearchModelList?.locationa_cityname == nil{
                savedSearchModelList?.locationa_cityname = ""
            }
            if savedSearchModelList?.locationa_countryname == nil{
                savedSearchModelList?.locationa_countryname = ""
            }
            if savedSearchModelList?.locationa_postcode == nil{
                savedSearchModelList?.locationa_postcode = ""
            }
            
            DashBoardViewController.s_premissesNumber = savedSearchModelList?.locationa_housenumber
            DashBoardViewController.s_streetName = savedSearchModelList?.locationa_streetname
            DashBoardViewController.s_cityName = savedSearchModelList?.locationa_cityname
            DashBoardViewController.s_country = savedSearchModelList?.locationa_countryname
            DashBoardViewController.s_postcode = savedSearchModelList?.locationa_postcode
            
            
            
            // Caling Google Api to get Place_ID
            getPlace_IDfromSelectedAddress(premissesNo: (savedSearchModelList?.locationa_housenumber)!, streetName: (savedSearchModelList?.locationa_streetname)!, cityName: (savedSearchModelList?.locationa_cityname)!, country: (savedSearchModelList?.locationa_countryname)!,postcode:(savedSearchModelList?.locationa_postcode)!)
            
            addingSavedSearchCommonHelperData() // Adding SavedSearch Data to CommonHelper Variables
        
        }else {
       
            mapView.delegate = self
            
        if(DashBoardViewController.s_gmsPlace == nil){
//                mapView.clear()
            
//            if isLongPressedOnMap == true{
//
//                if DashBoardViewController.s_premissesNumber == nil{
//                    DashBoardViewController.s_premissesNumber = ""
//                }
//                if DashBoardViewController.s_streetName == nil{
//                    DashBoardViewController.s_streetName = ""
//                }
//                if DashBoardViewController.s_cityName == nil{
//                    DashBoardViewController.s_cityName = ""
//                }
//                if DashBoardViewController.s_country == nil{
//                    DashBoardViewController.s_country = ""
//                }
//                if DashBoardViewController.s_postcode == nil{
//                    DashBoardViewController.s_postcode = ""
//                }
//
//                getPlace_IDfromSelectedAddress(premissesNo: DashBoardViewController.s_premissesNumber!, streetName: DashBoardViewController.s_streetName!, cityName: DashBoardViewController.s_cityName!, country: DashBoardViewController.s_country!,postcode:DashBoardViewController.s_postcode!)
//
//
//            }else {
            
            DispatchQueue.main.async {
                SwiftOverlays.showBlockingWaitOverlayWithText(self.Please_wait)
            }
            
               locationManager = CLLocationManager()
               locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
                
                if  CLAuthorizationStatus.denied == CLLocationManager.authorizationStatus() || CLAuthorizationStatus.restricted == CLLocationManager.authorizationStatus() {
                    
                     locationManager.requestAlwaysAuthorization()
                }
                
               locationManager.distanceFilter = 50
               locationManager.delegate = self
               locationManager.startUpdatingLocation()
               GMSPlacesClient.provideAPIKey(apiKey)
            
               placesClient = GMSPlacesClient.shared()
            
               //let camera = GMSCameraPosition.camera(withLatitude: 5.0436099,
                                                  //longitude: 27.5271744,
                                                  //zoom: 2.0)
              //mapView.camera=camera
               let tempTxtFld = locaationSearch.value(forKey: "_searchField") as! UITextField
               tempTxtFld.clearButtonMode=UITextFieldViewMode.never
//             }
            }else{
                locaationSearch.text = DashBoardViewController.s_selectedAddress
//            if !(locaationSearch.text == DashBoardViewController.s_selectedAddress) {
//                SetupSearchedPlaceAddressComponents(place: DashBoardViewController.s_gmsPlace!)
            if DashBoardViewController.s_premissesNumber == nil{
                DashBoardViewController.s_premissesNumber = ""
            }
            if DashBoardViewController.s_streetName == nil{
                DashBoardViewController.s_streetName = ""
            }
            if DashBoardViewController.s_cityName == nil{
                DashBoardViewController.s_cityName = ""
            }
            if DashBoardViewController.s_country == nil{
                DashBoardViewController.s_country = ""
            }
            if DashBoardViewController.s_postcode == nil{
                DashBoardViewController.s_postcode = ""
            }
            
            addingSavedSearchCommonHelperData() // Assigning SavedSearch Data to commonHelper variables
            
            getPlace_IDfromSelectedAddress(premissesNo: DashBoardViewController.s_premissesNumber!, streetName: DashBoardViewController.s_streetName!, cityName: DashBoardViewController.s_cityName!, country: DashBoardViewController.s_country!,postcode:DashBoardViewController.s_postcode!)
                searchedPlaceAddressComponents=DashBoardViewController.s_searchedPlaceAddressComponents
                selectedSearchedAddress=DashBoardViewController.s_selectedSearchedAddress
//            }
            
        }
    }
        
    NotificationCenter.default.addObserver(self, selector: #selector(CloseProductBandwidthPopUp), name: Notification.Name("CloseProductBandwidthPopUp"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(GoToCheckPriceController), name: Notification.Name("GoToCheckPriceController"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(self.dismissEmfPopupView), name: Notification.Name("EMF_DSL_Popup"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(showAlert), name: Notification.Name("ShowAlert"), object: nil)
                
    // BackgroundApiCall
    NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
    // BackgroundApiCall
    registerForBackgroundTask()
        
    if let navFont = UIFont(name: "HelveticaNeue", size: 13.0) {
        let navBarAttributesDictionary: [String: AnyObject]? = [
                NSForegroundColorAttributeName: (UIColor.white),
                NSFontAttributeName: navFont]
        self.onnetCount.setTitleTextAttributes(navBarAttributesDictionary, for: UIControlState.normal)
      }
     }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
        
        getLocalizedStrings()  // adding localized Strings
        
        DispatchQueue.main.async {
            self.title = self.connectivityChecker
        }
        getLocalizedStrings()
        AnalyticsHelper().analyticLogScreen(screen: "Connectivity Checker Screen")
        
//        if  CLAuthorizationStatus.denied == CLLocationManager.authorizationStatus() || CLAuthorizationStatus.restricted == CLLocationManager.authorizationStatus() {
//
//              // To allow location services
//                allowLocationServices()
//        }
    }
    
    func getLocalizedStrings() {
        no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
        please_select_voiceChannel = NSLocalizedString("Please select Voice Channel", comment: "")
        please_select_product = NSLocalizedString("Please select Product", comment: "")
        please_select_bandwidth = NSLocalizedString("Please select Bandwidth", comment: "")
        Please_select_hub_name = NSLocalizedString("Please_select_hub_name", comment: "")
        connectivityChecker = NSLocalizedString("Connectivity Checker", comment: "")
        Please_wait = NSLocalizedString("Please wait", comment: "")
        ServerErrorPleaseTryTgain = NSLocalizedString("Server error. Please try again.", comment: "")
        let connectivityCeecker = NSLocalizedString("Connectivity Checker", comment: "")
        self.title = connectivityCeecker
    }
        
    func allowLocationServices() {

//            if  CLAuthorizationStatus.denied == CLLocationManager.authorizationStatus() || CLAuthorizationStatus.restricted == CLLocationManager.authorizationStatus() {
        let allowFeetOnStreetToAccessYourLocation = NSLocalizedString("AllowFeetOnStreetToAccessYourLocation", comment: "")
        let alertMsg = "Allow \"FeetOnStreet\" to access your location.Your location is used to show in the \"FeetOnStreet\" map screen."
        let alert = UIAlertController(title: nil, message: allowFeetOnStreetToAccessYourLocation, preferredStyle: .alert)
        let continueAction = UIAlertAction(title: "Continue", style: .default, handler: nil)
        let goToSettings = UIAlertAction(title: "Settings", style: .default, handler: {
            
            UIAlertAction in

            if !CLLocationManager.locationServicesEnabled() {
                
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                // If general location settings are disabled then open general location settings
                if UIApplication.shared.canOpenURL(settingsUrl)  {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    }
                    else  {
                        UIApplication.shared.openURL(settingsUrl)
                    }
                }
                
                
//                if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION") {
//                    // If general location settings are disabled then open general location settings
//                    if UIApplication.shared.canOpenURL(url) {
//                        if #available(iOS 10.0, *) {
//                            UIApplication.shared.open(url, completionHandler: { (success) in
//                                print("Settings opened: \(success)") // Prints true
//                            })
//                        } else {
//                            UIApplication.shared.openURL(url)
//                        }
//                    }
//                }
            } else {
                if let url = URL(string: UIApplicationOpenSettingsURLString) {
                    // If general location settings are enabled then open location settings for the app
                    if UIApplication.shared.canOpenURL(url) {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, completionHandler: { (success) in
                                print("Settings opened: \(success)") // Prints true
                            })
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
            }
        })
        
        alert.addAction(goToSettings)
        alert.addAction(continueAction)
        
        self.present(alert, animated: true, completion: nil)
        
        //                    }
//        }
        
    }
    
    // BackgroundApiCall
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // BackgroundApiCall
    @objc func reinstateBackgroundTask() {
        if backgroundTask == UIBackgroundTaskInvalid {
            registerForBackgroundTask()
        }
    }
    // BackgroundApiCall
    func registerForBackgroundTask() {
        if backgroundTask == UIBackgroundTaskInvalid {
            backgroundTask=UIApplication.shared.beginBackgroundTask(expirationHandler: {
                self.endBackGroundTask()
            })
        }
    }
    
    // BackgroundApiCall
    func endBackGroundTask() {
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask=UIBackgroundTaskInvalid
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        
        AnalyticsHelper().analyticsEventsAction(category: "Map Tapped", action: "map_tapped", label: "Map Tapped")
        
//        let marker = GMSMarker(position: coordinate)
        let latitude = coordinate.latitude
        print(latitude)
        let longitude = coordinate.longitude
        print(longitude)
        
        DashBoardViewController.s_longitude = longitude
        DashBoardViewController.s_latitude = latitude
        let appDel = UIApplication.shared.delegate as! AppDelegate
        
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
            
        }else{
            
            DispatchQueue.main.async {
                SwiftOverlays.showBlockingWaitOverlayWithText(self.Please_wait)
            }
            currenPlaceAddressComponents = nil
            searchedPlaceAddressComponents = nil
            isLongPressedOnMap = true // isLongPressedOnMap making true
        getLocationAddres(lattitude: latitude, longitude: longitude)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        mapViewTopSpace.constant = 43
        self.viewDidLayoutSubviews()
//
//        self.navigationController?.isNavigationBarHidden = false
//        DispatchQueue.main.async {
//            self.title = "Connectivity Checker"
//        }
        
//        switch (CLLocationManager.authorizationStatus()) {
//        case CLAuthorizationStatus.notDetermined:
//            allowLocationServices()
//        case CLAuthorizationStatus.denied:
//            allowLocationServices()
//        default:
//            locationManager.startUpdatingLocation()
//        }
//        return
        
        
        //TODO:: Check how this isFirstTimeLaunchMap is working.
        let authorizeVal = UserDefaults.standard.value(forKey: "isFirstTimeLaunchMap") as! Bool
        
        if authorizeVal == false {
            UserDefaults.standard.set(true, forKey: "isFirstTimeLaunchMap")
            if CLAuthorizationStatus.notDetermined == CLLocationManager.authorizationStatus() {
                locationManager.startUpdatingLocation()
            }
        }else{
            //TODO:: When will this code be executed??
            if  CLAuthorizationStatus.notDetermined == CLLocationManager.authorizationStatus() {
                locationManager.requestWhenInUseAuthorization()
            }
            
            if  CLAuthorizationStatus.denied == CLLocationManager.authorizationStatus() || CLAuthorizationStatus.restricted == CLLocationManager.authorizationStatus() {
                
           }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func zoomInAction(_ sender: Any) {
        var tempZoomLevel = mapView.camera.zoom
        tempZoomLevel+=1
        mapView.animate(toZoom: tempZoomLevel)
    }
    
    @IBAction func zoomOutAction(_ sender: Any) {
        var tempZoomLevel = mapView.camera.zoom
        tempZoomLevel-=1
        mapView.animate(toZoom: tempZoomLevel)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        self.sideMenuController?.isLeftViewDisabled = true
        DashBoardViewController.s_slectedBuildingLatitude  = marker.position.latitude
        
        let addressDescription = marker.accessibilityValue?.trimmingCharacters(in: CharacterSet(charactersIn:" ,"))
        print(addressDescription)
        
        if marker == searchedMarker {
            
            isClickedOnMArkerIcon = true
            AnalyticsHelper().analyticsEventsAction(category: "Marker Location", action: "locationMarker_click", label: "Marker Clicked")
            CommonDataHelper.sharedInstance.isNearnetMarkerClicked = false
            // To open Offnet popup
            openOffnetPopUp(marker: marker)
            
            return true
        }else{
            
            tempIconMarker = marker
            isClickedOnMArkerIcon = false
            AnalyticsHelper().analyticsEventsAction(category: "Buildings(ACTIVE/INACTIVE)", action: "active/inactive_buildingIcon_click", label: "Buildings(ACTIVE/INACTIVE)")
        }
        var  tempMarkerModal=currentMarker?.accessibilityElements?[1] as? ActiveAndInactiveBuildings
        
        if currentMarker != nil {
            if tempMarkerModal?.bType == buildingType.Nearnet {
                currentMarker?.icon=UIImage(named: "Nearnet")
//                tempSearchedMarker  = marker
               
            }else{
                // Making nearnet marker click false
                CommonDataHelper.sharedInstance.isNearnetMarkerClicked = false
                if currentMarker?.accessibilityHint == "ACTIVE"  {
                    currentMarker?.icon=UIImage(named: "ActiveBuilding")
                }else if currentMarker?.accessibilityHint == "IN PROGRESS"{
                    currentMarker?.icon=UIImage(named: "inprogress")
                }else if currentMarker?.accessibilityHint == "Pre approved On Net"{
                    currentMarker?.icon=UIImage(named: "PAON")
                }else{
                    currentMarker?.icon=UIImage(named: "InActiveBuilding")
                }
            }
        }
        currentMarker=marker
        
      tempMarkerModal = currentMarker?.accessibilityElements?[1] as? ActiveAndInactiveBuildings
        
        selectedAddressModal = tempMarkerModal
        if tempMarkerModal?.bType == buildingType.Nearnet {
           currentMarker?.icon=UIImage(named: "Nearnet")
//            CommonDataHelper.sharedInstance.isNearnetMarkerClicked = true
        }else{
             CommonDataHelper.sharedInstance.isNearnetMarkerClicked = false
            if marker.accessibilityHint == "ACTIVE" {
                let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "acitve_25", withExtension: "gif")!)
                let advTimeGif = UIImage.gifImageWithData(imageData!)
                marker.icon=advTimeGif
            }else if marker.accessibilityHint == "IN PROGRESS" {
                let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "inProgress_25", withExtension: "gif")!)
                let advTimeGif = UIImage.gifImageWithData(imageData!)
                marker.icon=advTimeGif
            }else if marker.accessibilityHint == "Pre approved On Net" {
                let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "preApproved_25", withExtension: "gif")!)
                let advTimeGif = UIImage.gifImageWithData(imageData!)
                marker.icon=advTimeGif
            }else{
                let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "inactive_25", withExtension: "gif")!)
                let advTimeGif = UIImage.gifImageWithData(imageData!)
                marker.icon=advTimeGif
            }
        }
        if tempMarkerModal?.bType == buildingType.Nearnet {
            // To open Nearnet popup
            nearnetMarker = marker
//            OpenNearnetPopUp(marker: nearnetMarker! )
            OpenNearnetPopUp(marker: marker, currentAddress: addressDescription as! NSString)
            
        }else{
            // To open Onnet popup
           openOnnetPopUp(marker: marker)
       }
        
        return true
    }
     func openOffnetPopUp(marker: GMSMarker) {
        
        let cellTypes = [PopupcellTypes.AddressCell,PopupcellTypes.LocationCell,PopupcellTypes.ProductCell,PopupcellTypes.BandWidthCell,PopupcellTypes.EmfDslCell,PopupcellTypes.SearchAndCancelCell]
        let pDetails = PopupDetails()
        pDetails.pType=PopupType.MapviewPopup5
        pDetails.popupController=self
        let addressDescription = marker.accessibilityValue?.trimmingCharacters(in: CharacterSet(charactersIn:" ,"))
        pDetails.locationAddress=addressDescription as? NSString
        //let tempAdStr = marker.accessibilityValue as NSString?
        
        if searchedPlaceAddressComponents != nil {
            for item11 in searchedPlaceAddressComponents! {
                
                if item11.type == "street_number" || item11.type == "premise" {
                    pDetails.permissionNumber = item11.name
                }else if item11.type == "route" {
                    pDetails.streetName = item11.name
                }else if item11.type == "locality" || item11.type == "postal_town" || item11.type == "sublocality" || item11.type == "sublocality_level_2" || item11.type == "sublocality_level_1" || item11.type == "administrative_area_level_2" {
                    if(pDetails.streetName == nil){
                        pDetails.streetName = pDetails.cityName
                    }
                    pDetails.cityName = item11.name
                }else if item11.type == "postal_code" || item11.type == "postal_code_prefix"{
                    pDetails.postcode = item11.name
                }else if item11.type == "country" || item11.type == "administrative_area_level_1"{
                    pDetails.country = item11.name
                }
            }
            
        }else{
            if (currenPlaceAddressComponents != nil) {
                for item11 in self.currenPlaceAddressComponents! {
                    print(item11)
                    let item12 = item11 as! [String:Any]
                    let tempArr1 = item12["types"]
                    let keyVal1 = tempArr1 as! [String]
                    let keyVal2 = keyVal1[0]
                    
                    if keyVal2 == "street_number" || keyVal2 == "premise" {
                        pDetails.permissionNumber=item12["long_name"] as? String
                    }else if keyVal2 == "route"  {
                        pDetails.streetName = item12["long_name"] as? String
                    }else if keyVal2 == "locality" || keyVal2 == "sublocality" || keyVal2 == "sublocality_level_2" || keyVal2 == "sublocality_level_1" || keyVal2 == "administrative_area_level_2" {
                        if(pDetails.streetName == nil){
                            pDetails.streetName = pDetails.cityName
                        }
                        pDetails.cityName = item12["long_name"] as? String
                    }else if keyVal2 == "postal_code" {
                        pDetails.postcode = item12["long_name"] as? String
                    }else if keyVal2 == "country" || keyVal2 == "administrative_area_level_1" {
                        pDetails.country = item12["long_name"] as? String
                    }
                }
            }else{
                
                if isLongPressedOnMap == true {
                    pDetails.permissionNumber = DashBoardViewController.s_premissesNumber
                    pDetails.streetName = DashBoardViewController.s_streetName
                    pDetails.cityName = DashBoardViewController.s_cityName
                    pDetails.country = DashBoardViewController.s_country
                    pDetails.postcode = DashBoardViewController.s_postcode
                }else{
                    // assigning saved search model list responsedata
                    pDetails.permissionNumber = savedSearchModelList?.locationa_housenumber
                    pDetails.streetName = savedSearchModelList?.locationa_streetname
                    pDetails.cityName = savedSearchModelList?.locationa_cityname
                    pDetails.country = savedSearchModelList?.locationa_countryname
                    pDetails.postcode = savedSearchModelList?.locationa_postcode
                }
            }
        }
        
        pDetails.lattitudeLocation = marker.position.latitude
        pDetails.longitudeLocation = marker.position.longitude
        let fosPopup = Fos_Popup.initWithPopupElements(cells: cellTypes, popupDetails: pDetails)
        fosPopup.delegate=self
        presentPopupView(fosPopup)
        
        if isClickedOnMArkerIcon == false {
            if currentMarker != nil {
                if selectedAddressModal?.bType != buildingType.Nearnet {
                    if currentMarker?.accessibilityHint == "ACTIVE"  {
                        currentMarker?.icon=UIImage(named: "ActiveBuilding")
                    }else if currentMarker?.accessibilityHint == "Pre approved On Net"{
                        marker.icon=UIImage(named: "PAON")
                    }else if currentMarker?.accessibilityHint == "IN PROGRESS"{
                        marker.icon=UIImage(named: "inprogress")
                    }else{
                        currentMarker?.icon=UIImage(named: "InActiveBuilding")
                    }
                }
                
                currentMarker=nil
            }
        }else{
            if tempIconMarker != nil {
                if selectedAddressModal?.bType != buildingType.Nearnet {
                    if currentMarker?.accessibilityHint == "ACTIVE"  {
                        tempIconMarker?.icon=UIImage(named: "ActiveBuilding")
                    }else if tempIconMarker?.accessibilityHint == "Pre approved On Net"{
                        tempIconMarker?.icon=UIImage(named: "PAON")
                    }else if tempIconMarker?.accessibilityHint == "IN PROGRESS"{
                        tempIconMarker!.icon=UIImage(named: "inprogress")
                    }else{
                        tempIconMarker?.icon=UIImage(named: "InActiveBuilding")
                    }
                }
                
                tempIconMarker=nil
            }
        }
    }
    
    func openOnnetPopUp(marker: GMSMarker) {
        
        // Add Onnet popup
        let cellTypes = [PopupcellTypes.AddressCell,PopupcellTypes.CheckPriceCell,PopupcellTypes.CheckOffnetConnectivityCell,PopupcellTypes.CancelCell]
        
        pDetails.pType=PopupType.MapviewPopup1
        pDetails.popupController=self
        pDetails.locationAddress=marker.accessibilityValue as NSString?
        pDetails.lattitudeLocation = marker.position.latitude
        pDetails.longitudeLocation = marker.position.longitude
        pDetails.isBuildingDetailEnable=true
        let fosPopup = Fos_Popup.initWithPopupElements(cells: cellTypes, popupDetails: pDetails)
        fosPopup.delegate=self
        presentPopupView(fosPopup)
        selectedSearchedAddress=marker.accessibilityElements?[1] as? ActiveAndInactiveBuildings
        
        aEndAddress_id = marker.accessibilityElements?[0] as? String
        
        let tempAddress = pDetails.locationAddress
        let tempAdComponents = tempAddress?.components(separatedBy: CharacterSet.init(charactersIn: ","))
        var tempStrtName = tempAdComponents?[1]
        tempStrtName=tempStrtName?.replacingOccurrences(of: " ", with: "")
        pDetails.mapViewStreetName=tempStrtName
        
    }
    func OpenNearnetPopUp(marker: GMSMarker , currentAddress: NSString) {
        
        nearnetPopUp = Bundle.main.loadNibNamed("NearnetAlertView", owner: self, options: nil)?[0] as? NearnetAlertView
        nearnetPopUp?.dashBoardViewController = self
        nearnetPopUp?.frame = CGRect(x: 0, y: 20, width: self.view.frame.size.width, height: self.view.frame.size.height-20)
        nearnetPopUp?.currentMarker = marker
        nearnetPopUp?.currentAddress = currentAddress
        self.view.addSubview(nearnetPopUp!)
        
    }
    
    func selectMarker(locationMarker:ActiveAndInactiveBuildings){
       
        if locationMarker != self.selectedSearchedAddress {
            let allMarkers = self.allMarkers
            for kMarker in allMarkers {
                let tempBuilding = kMarker.accessibilityElements?[1] as? ActiveAndInactiveBuildings
                if tempBuilding == locationMarker {
                    if self.currentMarker != nil {
                        if self.currentMarker?.accessibilityHint == "ACTIVE"  {
                            self.currentMarker?.icon=UIImage(named: "ActiveBuilding")
                        }else if self.currentMarker?.accessibilityHint == "Pre approved On Net"{
                            self.currentMarker?.icon=UIImage(named: "PAON")
                        }else if self.currentMarker?.accessibilityHint == "IN PROGRESS"{
                            self.currentMarker?.icon=UIImage(named: "inprogress")
                        }else{
                            self.currentMarker?.icon=UIImage(named: "InActiveBuilding")
                        }
                    }
                    
                    self.currentMarker=kMarker
                    
                    // for temporary adding later need to remopve and do proper way
                    tempIconMarker = self.currentMarker
                    
                    DashBoardViewController.s_slectedBuildingLatitude=kMarker.position.latitude
                    if kMarker.accessibilityHint == "ACTIVE" {
                        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "acitve_25", withExtension: "gif")!)
                        let advTimeGif = UIImage.gifImageWithData(imageData!)
                        kMarker.icon=advTimeGif
                    }else if kMarker.accessibilityHint == "IN PROGRESS" {
                        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "inProgress_25", withExtension: "gif")!)
                        let advTimeGif = UIImage.gifImageWithData(imageData!)
                        kMarker.icon=advTimeGif
                    }else if kMarker.accessibilityHint == "Pre approved On Net" {
                        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "preApproved_25", withExtension: "gif")!)
                        let advTimeGif = UIImage.gifImageWithData(imageData!)
                        kMarker.icon=advTimeGif
                    }else{
                        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "inactive_25", withExtension: "gif")!)
                        let advTimeGif = UIImage.gifImageWithData(imageData!)
                        kMarker.icon=advTimeGif
                    }
                    self.selectedSearchedAddress = locationMarker
                    break
                }
            }
        }
    }
    
    
    func dismissPopup(selectionType:String,popupFos:Fos_Popup) {
        self.sideMenuController?.isLeftViewDisabled = false
        if selectionType == "cancel" || selectionType == "checkprice" {
            
            dismissPopupView()
            
            if(selectionType == "checkprice"){
                
                //Need to show that view....
//                ShowCheckPricePopupView()
                CommonDataHelper.sharedInstance.npc = nil
                
                productBandwithpopupView = Bundle.main.loadNibNamed("ProductBandwithPopUp", owner: self, options: nil)?[0] as? ProductBandwithPopUpView
//            eMFpopupView?.eMFDSLlistArray = eMFDSLlist
//            eMFpopupView?.oLOlistArray = oLOlist
//            eMFpopupView?.addresslist = self.addressMatchedList!
                let viewHeight = productBandwithpopupView?.frame.size.height
                productBandwithpopupView?.viewHeight = viewHeight
                productBandwithpopupView?.productBandwithpopupViewFrame = productBandwithpopupView?.frame
                productBandwithpopupView?.viewController = self
                productBandwithpopupView?.frame = CGRect(x: 0, y: 20, width: self.view.frame.size.width, height: self.view.frame.size.height-20)
                productBandwithpopupView?.englishCityName = self.englishCityName
                productBandwithpopupView?.address1 =  pDetails.locationAddress as? String
                productBandwithpopupView?.isBuildiningDeytailsBtnHidden = true
                productBandwithpopupView?.delegate = self
                productBandwithpopupView?.popupController = self
                self.view.addSubview(productBandwithpopupView!)
                
                
            }
            
        }else if selectionType == "checkprice" {
            popupFos.popupFosDetails?.isBuildingDetailEnable=false
            if popupFos.popupFosDetails?.pType == PopupType.MapviewPopup1 {
                let cellTypes = [PopupcellTypes.AddressCell,PopupcellTypes.ProductCell,PopupcellTypes.BandWidthCell,PopupcellTypes.CheckPriceCell,PopupcellTypes.CancelCell]
                popupFos.popupFosDetails?.pType=PopupType.MapviewPopup2
                popupFos.replacePopupCells(cells: cellTypes, popupDetails: popupFos.popupFosDetails!, fosPopup: popupFos)
            }else{
                
                if popupFos.popupFosDetails?.selectedProductType == nil {
//                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select product")
//                    self.showError("Please select product")
                    UIHelper.showAlert(message: please_select_product, inViewController: self)
                    return
                }
                if popupFos.popupFosDetails?.selectedBandWidth == nil {
                    if popupFos.popupFosDetails?.productName == "Colt SIP Trunking" {
//                        UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select voice channel")
//                        self.showError("Please select voice channel")
                        UIHelper.showAlert(message: please_select_voiceChannel, inViewController: self)
                        
                    }else{
//                        UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select bandwidth")
//                        self.showError("Please select bandwidth")
                        UIHelper.showAlert(message: please_select_bandwidth, inViewController: self)

                    }
                    
                    return
                }
                if popupFos.popupFosDetails?.pType == PopupType.MapviewPopup3 && ((popupFos.popupFosDetails?.selectedHubName) == nil) {
//                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select hub name")
//                    self.showError("Please select hub name")
                    UIHelper.showAlert(message: Please_select_hub_name, inViewController: self)
                    return
                }
                if popupFos.popupFosDetails?.pType == PopupType.MapviewPopup4 {
                    if popupFos.popupFosDetails?.bEndAddress == nil {
//                        UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select B-End address")
//                        self.showError("Please select B-End address")
                        let PleaseSelectBendAddress = NSLocalizedString("Please select B-End address", comment: "")
                        UIHelper.showAlert(message: PleaseSelectBendAddress, inViewController: self)

                        return
                    }
                    if popupFos.popupFosDetails?.isBendAddressSearched == false {
//                        UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please search B-End address")
//                      self.showError("Please search B-End address")
                        UIHelper.showAlert(message: "Please search B-End address", inViewController: self)

                        return
                    }
                    
                }
                dismissPopupView()
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Secondary", bundle:nil)
                let checkPriceVC = storyBoard.instantiateViewController(withIdentifier: "CheckPriceController") as! CheckPriceController
                checkPriceVC.locationStr=popupFos.popupFosDetails?.locationAddress as String!
                checkPriceVC.productType=popupFos.popupFosDetails?.selectedProductType
                checkPriceVC.bandWidthType=popupFos.popupFosDetails?.selectedBandWidth
                checkPriceVC.bEndAddress_id = popupFos.popupFosDetails?.aEndAddress_id
                self.navigationController?.pushViewController(checkPriceVC, animated: true)
            }
        }else if selectionType == "Hub" {
            let cellTypes = [PopupcellTypes.AddressCell,PopupcellTypes.ProductCell,PopupcellTypes.BandWidthCell,PopupcellTypes.HubCell,PopupcellTypes.CheckPriceCell,PopupcellTypes.CancelCell]
            popupFos.popupFosDetails?.pType=PopupType.MapviewPopup3
            popupFos.replacePopupCells(cells: cellTypes, popupDetails: popupFos.popupFosDetails!, fosPopup: popupFos)
        }else if selectionType == "B-End address" {
            let cellTypes = [PopupcellTypes.AddressCell,PopupcellTypes.ProductCell,PopupcellTypes.BandWidthCell,PopupcellTypes.BEndAddressCell,PopupcellTypes.CheckPriceCell,PopupcellTypes.CancelCell]
            popupFos.popupFosDetails?.pType=PopupType.MapviewPopup4
            popupFos.replacePopupCells(cells: cellTypes, popupDetails: popupFos.popupFosDetails!, fosPopup: popupFos)
        }else if selectionType == "ReloadInitial" {
            let cellTypes = [PopupcellTypes.AddressCell,PopupcellTypes.ProductCell,PopupcellTypes.BandWidthCell,PopupcellTypes.CheckPriceCell,PopupcellTypes.CancelCell]
            popupFos.popupFosDetails?.pType=PopupType.MapviewPopup2
            popupFos.replacePopupCells(cells: cellTypes, popupDetails: popupFos.popupFosDetails!, fosPopup: popupFos)
        }else if selectionType == "Normalpopup" {
            let cellTypes = [PopupcellTypes.AddressCell,PopupcellTypes.ProductCell,PopupcellTypes.BandWidthCell,PopupcellTypes.CheckPriceCell,PopupcellTypes.CancelCell]
            popupFos.popupFosDetails?.pType=PopupType.MapviewPopup2
            popupFos.replacePopupCells(cells: cellTypes, popupDetails: popupFos.popupFosDetails!, fosPopup: popupFos)
        }else if selectionType == "search" {
            
            AnalyticsHelper().analyticsEventsAction(category: "Marker Location", action: "OFFNET(EFM/DSL,OLO)_search_click", label: "Search")
            let appDel = UIApplication.shared.delegate as! AppDelegate
            
            if appDel.networkStatus! == NetworkStatus.NotReachable {
                
                DispatchQueue.main.async {
                    UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
             }
            }else{
                if  CommonDataHelper.sharedInstance.selectedProduct != nil  {
                    popupFos.popupFosDetails?.selectedProductType = CommonDataHelper.sharedInstance.selectedProduct
                }
                if CommonDataHelper.sharedInstance.selectedBandwidth != nil {
                    popupFos.popupFosDetails?.selectedBandWidth = CommonDataHelper.sharedInstance.selectedBandwidth
                }
                if popupFos.popupFosDetails?.selectedProductType == nil {
                    //                UIHelper.sharedInstance.showError("Please select product")
//                    self.showError("Please select product")
                    UIHelper.showAlert(message: please_select_product, inViewController: self)

                    return
                }
                if popupFos.popupFosDetails?.selectedProductType == "Select Product" {
//                    self.showError("Please select product")
                    UIHelper.showAlert(message: please_select_product, inViewController: self)
                    return
                }
                if popupFos.popupFosDetails?.selectedBandWidth == nil {
                    if CommonDataHelper.sharedInstance.selectedProduct == "Colt SIP Trunking" || CommonDataHelper.sharedInstance.selectedProduct == "Colt VoIP Access" {
                        //                    UIHelper.sharedInstance.showError("Please select voice channel")
//                        self.showError("Please select voice channel")
                        UIHelper.showAlert(message: please_select_voiceChannel, inViewController: self)

                    }
//                    if CommonDataHelper.sharedInstance.selectedProduct == "Colt VoIP Access"{
//                        //                    UIHelper.sharedInstance.showError("Please select voice channel")
//                        self.showError("Please select voice channel")
//                    }
                    else{
                        //                    UIHelper.sharedInstance.showError("Please select bandwidth")
//                        self.showError("Please select bandwidth")
                        UIHelper.showAlert(message: please_select_bandwidth, inViewController: self)
                    }
                    
                    return
                }
                
                var numberOfSelection:Int = 0
                
                if popupFos.popupFosDetails?.isOloSelected == true {
                    numberOfSelection+=1
                    numberApiCall+=1
                }
                if popupFos.popupFosDetails?.isEfmSelected == true {
                    numberOfSelection+=1
                    numberApiCall+=1
                }
                
                if numberOfSelection==0 {
                    //                UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please check atleast EFM/DSL or OLO")
//                    self.showError("Please check atleast EFM/DSL or OLO")
                    let checkAtleastEFMDSL  = NSLocalizedString("Please check atleast EFM/DSL or OLO", comment: "")
                     UIHelper.showAlert(message: checkAtleastEFMDSL, inViewController: self)
                    return
                }
                dismissPopupView()
                
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Secondary", bundle:nil)
                let offNetViewController = storyBoard.instantiateViewController(withIdentifier: "OffNetViewController") as! OffNetViewController
                offNetViewController.fosPopUp=popupFos
                self.navigationController?.pushViewController(offNetViewController, animated: true)
            }

        }else if selectionType == "buildingDetails" {
            let appDel = UIApplication.shared.delegate as! AppDelegate
            
            if appDel.networkStatus! == NetworkStatus.NotReachable {
                
                DispatchQueue.main.async {
                    UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
                }
            }else{
            dismissPopupView()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let buildingDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "BuildingDetailsViewController") as! BuildingDetailsViewController
                buildingDetailsViewController.bEndAddress_id = aEndAddress_id
            
            self.navigationController?.pushViewController(buildingDetailsViewController, animated: true)
         }
        }else if selectionType == "CheckOffnetConnectivity" {
            
            let cellTypes = [PopupcellTypes.AddressCell,PopupcellTypes.LocationCell,PopupcellTypes.ProductCell,PopupcellTypes.BandWidthCell,PopupcellTypes.EmfDslCell,PopupcellTypes.SearchAndCancelCell]
            //            let pDetails = PopupDetails()
            popupFos.popupFosDetails?.pType=PopupType.MapviewPopup5
            //            pDetails.popupController=self
            //            pDetails.locationAddress = popupFos.popupFosDetails?.locationAddress
            let addComponents = popupFos.popupFosDetails?.locationAddress?.components(separatedBy: ",")
            
            
            var componetIndex:Int = 0
            
            
            for item11 in addComponents! {
                print(item11)
                //                let item12 = item11 as! [String:Any]
                //                let tempArr1 = item12["types"]
                //                let keyVal1 = tempArr1 as! [String]
                //                let keyVal2 = keyVal1[0]
                
                if componetIndex == 0 {
                    popupFos.popupFosDetails?.permissionNumber=item11
                }else if componetIndex == 1 {
                    popupFos.popupFosDetails?.streetName = item11
                }else if componetIndex == 2 {
                    popupFos.popupFosDetails?.cityName = item11
                }else if componetIndex == 3 {
                    popupFos.popupFosDetails?.postcode = item11
                }else if componetIndex == 4 {
                    popupFos.popupFosDetails?.country = item11
                }
                
                componetIndex+=1
            }
            //            popupFos.popupFosDetails=pDetails
            //            pDetails.lattitudeLocation = popupFos.popupFosDetails?.lattitudeLocation
            //            pDetails.longitudeLocation = popupFos.popupFosDetails?.longitudeLocation
            popupFos.replacePopupCells(cells: cellTypes, popupDetails: popupFos.popupFosDetails!, fosPopup: popupFos)
            
        }
        currentPopup=popupFos
    }
    
    func CloseProductBandwidthPopUp() {
        
        self.sideMenuController?.isLeftViewDisabled = false
         DispatchQueue.main.async {
        self.productBandwithpopupView?.removeFromSuperview()
        }
    }
    
    @objc func dataAvilable(notification:Notification) {
        
        
        if notification.object is String {
            
            if notification.object as! String == "failure" {
                DispatchQueue.main.async {
//                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Server not response.Try again")
//                    self.showError("Server error. Please try again.")
                    UIHelper.showAlert(message: self.ServerErrorPleaseTryTgain, inViewController: self)
                    
                }
                DispatchQueue.main.async {
                    
//                    UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                    SwiftOverlays.removeAllBlockingOverlays()
                    
                }
                return
            }
        }
        print("response offnet")
        numberOfResponseGot+=1
        if numberApiCall==numberOfResponseGot {
            DispatchQueue.main.async {
                
//                UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                SwiftOverlays.removeAllBlockingOverlays()
            }
        }
       
    }
    
    
    func searchBarShouldBegiFnEditing(_ searchBar: UISearchBar) -> Bool {
        
    AnalyticsHelper().analyticsEventsAction(category: "Search Address", action: "searchAddress", label: "Search Address")

        
        //        let appDel = UIApplication.shared.delegate as! AppDelegate
        //
        //        if appDel.networkStatus! == NetworkStatus.NotReachable {
        //
        //            DispatchQueue.main.async {
        //                UIHelper.sharedInstance.showReachabilityWarning("No internet connection", _onViewController: self)
        //            }
        //
        //        }else{
        
        Language.setAppleLAnguageTo(lang: "en")
        let autocompleteController = GMSAutocompleteViewController()
        UINavigationBar.appearance().tintColor=UIColor.white
        //        UINavigationBar.appearance().barTintColor = UIColor(colorLiteralRed: 0, green: 165.0/255.0, blue: 156.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().barTintColor = UIColor.c_Teal
        let button1 = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(barButtonItemClicked))
//        let button1 = UIBarButtonItem(image: UIImage(named: ""), style: .plain, target: self, action: #selector(getter: UIDynamicBehavior.action))
        autocompleteController.navigationItem.rightBarButtonItem  = button1
        let searchBarTextColor = [NSForegroundColorAttributeName:UIColor.white,NSFontAttributeName:UIFont.systemFont(ofSize: 15.0)] as [String : Any]
        if #available(iOS 9.0, *) {
            let sBar = UITextField.appearance(whenContainedInInstancesOf: [UINavigationBar.classForCoder() as! UIAppearanceContainer.Type])
            sBar.attributedPlaceholder = NSAttributedString(string: "Search",
                                                            attributes: [NSForegroundColorAttributeName: UIColor.white])
            sBar.defaultTextAttributes=searchBarTextColor
        } else {
            // Fallback on earlier versions
        }
        
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
        //        }
        
        return false
    }
    func barButtonItemClicked() {
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        if let language = LanguagesViewController.getSelectedLanguage() {
            Language.setAppleLAnguageTo(lang: language)
        }
        currenPlaceAddressComponents = nil // Making CurrenPlaceAddressComponents nil
        isLongPressedOnMap = false // Making isLongPressedOnMap false
        SetupSearchedPlaceAddressComponents(place: place)
        CommonDataHelper.sharedInstance.selectedProduct = "Select Product"
        CommonDataHelper.sharedInstance.selectedBandwidth = "Select Bandwidth"
//        getActiveAndInactiveBuildings(lattitudePlace: place.coordinate.latitude, longitudePlace: place.coordinate.longitude, addressPlace: place.formattedAddress!)
        DashBoardViewController.s_gmsPlace = place
        onnetViewCtrl = nil
        dismiss(animated: true, completion: nil)
    }
    
    func SetupSearchedPlaceAddressComponents(place: GMSPlace){
        
        DashBoardViewController.s_premissesNumber = nil
        DashBoardViewController.s_streetName = nil
        DashBoardViewController.s_cityName = nil
        DashBoardViewController.s_postcode = nil
        DashBoardViewController.s_country = nil
        
        
        clearSavedSearchCommonHelperData()  // clearing SavedSearch Data before adding
        
        for item11 in place.addressComponents! {
           print(item11.type)
            print(item11.name)
            if item11.type == "street_number" {
                DashBoardViewController.s_premissesNumber = item11.name
            }else if item11.type == "route" {
                DashBoardViewController.s_streetName = item11.name
            }else if item11.type == "postal_town" || item11.type == "locality" || item11.type == "neighborhood" {
                if DashBoardViewController.s_cityName == nil{
                 DashBoardViewController.s_cityName = item11.name
                }
            }else if item11.type == "postal_code" || item11.type == "postal_code_prefix" {
                DashBoardViewController.s_postcode = item11.name
            }else if item11.type == "country" {
                DashBoardViewController.s_country = item11.name
            }
        }
        
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        locaationSearch.text=place.formattedAddress
        DashBoardViewController.s_selectedAddress = place.formattedAddress
        DashBoardViewController.s_latitude = place.coordinate.latitude
        DashBoardViewController.s_longitude = place.coordinate.longitude
        searchedPlaceAddressComponents=place.addressComponents
        DashBoardViewController.s_searchedPlaceAddressComponents = place.addressComponents
        
        addingSavedSearchCommonHelperData() // Assigning SavedSearch Data to commonhelper variables
        
        DispatchQueue.main.async {
            SwiftOverlays.showBlockingWaitOverlayWithText(self.self.Please_wait)
        }
        
        // calling ONNET api
        getActiveAndInactiveBuildings(lattitudePlace: DashBoardViewController.s_latitude!, longitudePlace: DashBoardViewController.s_longitude!, addressPlace: place.formattedAddress!)
    }
    
    func clearSavedSearchCommonHelperData()  {
        CommonDataHelper.sharedInstance.saveSearchPermisesNumber = ""
        CommonDataHelper.sharedInstance.saveSearchStreetName = ""
        CommonDataHelper.sharedInstance.saveSearchCityName = ""
        CommonDataHelper.sharedInstance.saveSearchPostCode = ""
        CommonDataHelper.sharedInstance.saveSearchColtOperatingCountry = ""
    }
    func addingSavedSearchCommonHelperData()  {
        CommonDataHelper.sharedInstance.saveSearchPermisesNumber = DashBoardViewController.s_premissesNumber
        CommonDataHelper.sharedInstance.saveSearchStreetName = DashBoardViewController.s_streetName
        CommonDataHelper.sharedInstance.saveSearchCityName = DashBoardViewController.s_cityName
        CommonDataHelper.sharedInstance.saveSearchPostCode = DashBoardViewController.s_postcode
        CommonDataHelper.sharedInstance.saveSearchColtOperatingCountry = DashBoardViewController.s_country
        
    }
    
    func getActiveAndInactiveBuildings(lattitudePlace:CLLocationDegrees,longitudePlace:CLLocationDegrees,addressPlace:String) {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
        // BackgroundApiCall
        endBackGroundTask()
         DispatchQueue.main.async {
            UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
          }
            
        }else{
        
        struct staticHolder{
            static var lattitudePlace:CLLocationDegrees = 0.0
            static var longitudePlace:CLLocationDegrees = 0.0
        }
        
//        if(staticHolder.lattitudePlace == lattitudePlace && staticHolder.longitudePlace == longitudePlace){
//            //This means the service is going on....
//            return;
//        }
        
        if(staticHolder.lattitudePlace == 0 && staticHolder.longitudePlace == 0){
            staticHolder.lattitudePlace = lattitudePlace
            staticHolder.longitudePlace = longitudePlace
        }
        
//        DispatchQueue.main.async {
        
//            UIHelper.sharedInstance.showLoadingOnViewController(loadVC: self)
//            SwiftOverlays.showBlockingWaitOverlayWithText("Please wait...")
//        }
        
//        let requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"> <soapenv:Body> <ax:checkConnectivityRequest ax:schemaVersion=\"3.0\" xmlns:ax=\"http://aat.colt.com/connectivityservice\"> <ax:sequenceId>1</ax:sequenceId> <ax:requestType>ALL</ax:requestType> <ax:requestMode> <ax:requestID>4495</ax:requestID> <ax:siteAddress> <ax:latitude>\(lattitudePlace)</ax:latitude> <ax:longitude>\(longitudePlace)</ax:longitude> <ax:radius>500</ax:radius> <ax:connectivityType>COLT Fibre</ax:connectivityType> </ax:siteAddress> </ax:requestMode> </ax:checkConnectivityRequest> </soapenv:Body> </soapenv:Envelope>"
            
            if DashBoardViewController.s_premissesNumber == nil{
                DashBoardViewController.s_premissesNumber = ""
            }
            if DashBoardViewController.s_streetName == nil{
                DashBoardViewController.s_streetName = ""
            }
            if DashBoardViewController.s_cityName == nil{
                DashBoardViewController.s_cityName = ""
            }
            if DashBoardViewController.s_postcode == nil{
               DashBoardViewController.s_postcode = ""
            }
            if DashBoardViewController.s_country == nil{
                DashBoardViewController.s_country = ""
            }
//            let requestBody = "<?xml version=\"1.0\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"><soapenv:Body><ax:checkConnectivityRequest xmlns:ax=\"http://aat.colt.com/connectivityservice\" ax:schemaVersion=\"3.0\"><ax:sequenceId>1</ax:sequenceId><ax:requestType>SITE</ax:requestType><ax:requestMode><ax:requestID>4495</ax:requestID><ax:siteAddress><ax:latitude>\(lattitudePlace)</ax:latitude><ax:longitude>\(longitudePlace)</ax:longitude><ax:premisesNumber>\(DashBoardViewController.s_premissesNumber!)</ax:premisesNumber><ax:streetName>\(DashBoardViewController.s_streetName!)</ax:streetName><ax:cityTown>\(DashBoardViewController.s_cityName!)</ax:cityTown><ax:postalZipCode>\(DashBoardViewController.s_postcode!)</ax:postalZipCode><ax:coltOperatingCountry>\(DashBoardViewController.s_country!)</ax:coltOperatingCountry><ax:radius>500</ax:radius><ax:connectivityType>COLT Fibre</ax:connectivityType></ax:siteAddress></ax:requestMode></ax:checkConnectivityRequest></soapenv:Body></soapenv:Envelope>"
            
            
      let requestBody = "<?xml version=\"1.0\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"> <soapenv:Body> <ax:checkConnectivityRequest ax:schemaVersion=\"3.0\" xmlns:ax=\"http://aat.colt.com/connectivityservice\"> <ax:sequenceId>1</ax:sequenceId> <ax:requestType>ALL</ax:requestType> <ax:requestMode> <ax:requestID>4495</ax:requestID> <ax:siteAddress>   <ax:premisesNumber>\(DashBoardViewController.s_premissesNumber!)</ax:premisesNumber>  <ax:streetName>\(DashBoardViewController.s_streetName!)</ax:streetName> <ax:cityTown>\(DashBoardViewController.s_cityName!)</ax:cityTown> <ax:postalZipCode>\(DashBoardViewController.s_postcode!)</ax:postalZipCode> <ax:coltOperatingCountry>\(DashBoardViewController.s_country!)</ax:coltOperatingCountry><ax:radius>500</ax:radius> <ax:connectivityType>COLT Fibre</ax:connectivityType> </ax:siteAddress> </ax:requestMode> </ax:checkConnectivityRequest> </soapenv:Body> </soapenv:Envelope>"

        
        var errMsg:String?
        let tokenString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "token")
        let accountTypeString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "accountType")
        let urlString = "http://loncis01/checkConnectivity/connectivity.wsdl"
        let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
        let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
        let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/connectnew")!)
        request.httpMethod = "POST"
        let requestData = requestBody.data(using: String.Encoding.utf8)
        request.httpBody = requestData
        request.addValue("text/xml", forHTTPHeaderField: "Content-Type")
        request.addValue("radius_search", forHTTPHeaderField: "SOAPAction")
        request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
        request.addValue(base64String!, forHTTPHeaderField: "X-CONNECT-U")
        request.addValue(tokenString!, forHTTPHeaderField: "X-GATEWAY-A")
        request.addValue(accountTypeString!, forHTTPHeaderField: "X-GATEWAY-F")
//        request.timeoutInterval=TimeInterval(MAXFLOAT)
        request.timeoutInterval=600
        
        let session = URLSession.shared
        _ = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            if error == nil
            {
                if let data = data, let result = String(data: data, encoding: String.Encoding.utf8)
                {
                    let xml = SWXMLHash.parse(result)
                    //print(xml)
                    
//                    self.abArray = ActiveAndInactiveBuildings.getActiceAndInactiveBuildingsFromResponse(xmlResponseData: xml)
                    let onnetArray = ActiveAndInactiveBuildings.getOnnetLocations(xmlResponseData: xml)
                    let nearnetArray   = ActiveAndInactiveBuildings.getNearnrtLocations(xmlResponseData: xml)
                    
                    // Only onnet array is required for ONNET vieccontroller list
                    self.onnetListArray = onnetArray
            
                    let tempAbArray = onnetArray+nearnetArray
                    self.abArray = tempAbArray
                    
                    if self.abArray?.count == nil || self.abArray?.count == 0 {
                        
                        self.getActiveAndInactiveBuildingsWithLatitudeAndLongitude(lattitudePlace: lattitudePlace, longitudePlace: longitudePlace, addressPlace: addressPlace)
                        
                    }else{
                    
                        self.addAddressListOnMapView(abArray:self.abArray!,lattitudePlace:lattitudePlace,longitudePlace:longitudePlace,addressPlace:addressPlace)
//                    DispatchQueue.main.async {
//
//                       self.mapView.clear()
//
//                        self.currentMarker=nil
//                        self.allMarkers.removeAll()
//                        //                        self.zoomLevel=15.0
//                        let camera = GMSCameraPosition.camera(withLatitude: lattitudePlace,
//                                                              longitude: longitudePlace,
//                                                              zoom: 15.0)
//
//
//                        //                        self.mapView.camera = camera
//                        self.mapView.animate(to: camera)
//                        let cordinatePlace = CLLocationCoordinate2D(latitude: lattitudePlace, longitude: longitudePlace)
//                        let  cMarker = GMSMarker(position: cordinatePlace)
//                        cMarker.map = self.mapView
////                        cMarker.title=addressPlace
//                        cMarker.icon=UIImage(named: "CurrentLocation")
//                        cMarker.accessibilityValue=addressPlace
//                        //                        cMarker.isTappable=false
//                        self.mapView.selectedMarker=cMarker
//                        self.searchedMarker=cMarker
//
//                        if (self.abArray?.count)!>0 {
//
//                            self.onnetCount.title="ONNET(\(onnetArray.count))"
//
//                            // BackgroundApiCall
//                            self.endBackGroundTask()
//                            DispatchQueue.main.async {
////                                UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
//                                SwiftOverlays.removeAllBlockingOverlays()
//                            }
//
//                        }else{
//                            self.onnetCount.title="ONNET"
//                            // BackgroundApiCall
//                            self.endBackGroundTask()
//                            DispatchQueue.main.async {
////                                UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
//                                SwiftOverlays.removeAllBlockingOverlays()
//                            }
//                            DispatchQueue.main.async {
//                            UIHelper.showAlert(message: "This location doesn't provide Onnet.Please try for another location", inViewController: self)
////                            self.showError("This location doesn't provide Onnet.Please try for another location")
//                            }
//
//                        }
//
////                        UIHelper.sharedInstance.showError("This location doesn't provide Onnet.Please try for another location")
//                        for aiBuilding in self.abArray!{
//
//                            let cordna2d = CLLocationCoordinate2D(latitude: aiBuilding.lattitude!, longitude: aiBuilding.longitude!)
//                            let marker = GMSMarker(position: cordna2d)
//
//                            //  marker.title = selectedPlace?.name
//                            //  marker.snippet = selectedPlace?.formattedAddress
//                            marker.accessibilityHint=aiBuilding.buildingStatus
//
//                            if aiBuilding.bType == buildingType.Onnet {
//                               if aiBuilding.buildingStatus == "ACTIVE" {
//                                marker.icon=UIImage(named: "ActiveBuilding")
//                               }else if aiBuilding.buildingStatus == "Pre approved On Net"{
//                                marker.icon=UIImage(named: "PAON")
//                               }else if aiBuilding.buildingStatus == "IN PROGRESS"{
//                                marker.icon=UIImage(named: "inprogress")
//                               }else{
//                                marker.icon=UIImage(named: "InActiveBuilding")
//                               }
//                            }else{
//                                // for nearnet result
//                                marker.icon=UIImage(named: "Nearnet.png")
//                            }
//
//                            let permisesNumber =  aiBuilding.permisesNumber
//                            let streetName =  aiBuilding.streetName
//                            self.englishCityName =  aiBuilding.englishCityName
//                            let postCode =  aiBuilding.postCode
//                            let coltOperatingCountry =  aiBuilding.coltOperatingCountry
//                            let address1 = "\(permisesNumber!), \(streetName!), \(self.englishCityName!), \(postCode!), \(coltOperatingCountry!)"
//                            marker.accessibilityValue=address1
//                            marker.accessibilityElements = [aiBuilding.pointID!,aiBuilding]
//                            marker.map = self.mapView
//                            //                    marker.title=nLocation1.name
//                            self.allMarkers.append(marker)
//                            if DashBoardViewController.s_slectedBuildingLatitude == aiBuilding.lattitude {
//                                self.currentMarker=marker
//                                DashBoardViewController.s_selectedSearchedAddress=aiBuilding
//                                self.selectedSearchedAddress=aiBuilding
//                                if self.selectedSearchedAddress?.bType != buildingType.Nearnet {
//                                    if aiBuilding.buildingStatus == "ACTIVE" {
//                                        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "acitve_25", withExtension: "gif")!)
//                                        let advTimeGif = UIImage.gifImageWithData(imageData!)
//                                        marker.icon=advTimeGif
//                                    }else if aiBuilding.buildingStatus == "Pre approved On Net" {
//                                        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "preApproved_25", withExtension: "gif")!)
//                                        let advTimeGif = UIImage.gifImageWithData(imageData!)
//                                        marker.icon=advTimeGif
//                                    }else if aiBuilding.buildingStatus == "IN PROGRESS" {
//                                        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "inProgress_25", withExtension: "gif")!)
//                                        let advTimeGif = UIImage.gifImageWithData(imageData!)
//                                        marker.icon=advTimeGif
//                                    }else{
//                                        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "inactive_25", withExtension: "gif")!)
//                                        let advTimeGif = UIImage.gifImageWithData(imageData!)
//                                        marker.icon=advTimeGif
//                                    }
//                                }
//                            }
//                        }
                        staticHolder.lattitudePlace = 0
                        staticHolder.longitudePlace = 0
//                    }
                  }
                    
                }
                else
                {
                    // BackgroundApiCall
                    self.endBackGroundTask()
                    print("Failed to get valid response from server.")
                    staticHolder.lattitudePlace = 0
                    staticHolder.longitudePlace = 0
                    DispatchQueue.main.async {
//                        UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                        SwiftOverlays.removeAllBlockingOverlays()
                    }
                    UIHelper.showAlert(message: self.ServerErrorPleaseTryTgain, inViewController: self)
                }
            }
            else
            {
                // BackgroundApiCall
                self.endBackGroundTask()
                print(error?.localizedDescription ?? "Error")
                staticHolder.lattitudePlace = 0
                staticHolder.longitudePlace = 0
                DispatchQueue.main.async {
//                    UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                    SwiftOverlays.removeAllBlockingOverlays()
                }
                UIHelper.showAlert(message: self.ServerErrorPleaseTryTgain, inViewController: self)
            }
            
            }
            .resume()
    }
}
    
    
    // Again calling api if first resonse comes nill, need to remove later
    
    func getActiveAndInactiveBuildingsWithLatitudeAndLongitude(lattitudePlace:CLLocationDegrees,longitudePlace:CLLocationDegrees,addressPlace:String) {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
            // BackgroundApiCall
            endBackGroundTask()
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
            
        }else{
            
            struct staticHolder{
                static var lattitudePlace:CLLocationDegrees = 0.0
                static var longitudePlace:CLLocationDegrees = 0.0
            }
            
//            if(staticHolder.lattitudePlace == lattitudePlace && staticHolder.longitudePlace == longitudePlace){
//                //This means the service is going on....
//                return;
//            }
            
            if(staticHolder.lattitudePlace == 0 && staticHolder.longitudePlace == 0){
                staticHolder.lattitudePlace = lattitudePlace
                staticHolder.longitudePlace = longitudePlace
            }
            
//            DispatchQueue.main.async {
//                UIHelper.sharedInstance.showLoadingOnViewController(loadVC: self)
//                SwiftOverlays.showBlockingWaitOverlayWithText("Please wait...")
//            }
            
            if DashBoardViewController.s_premissesNumber == nil{
                DashBoardViewController.s_premissesNumber = ""
            }
            if DashBoardViewController.s_streetName == nil{
                DashBoardViewController.s_streetName = ""
            }
            if DashBoardViewController.s_cityName == nil{
                DashBoardViewController.s_cityName = ""
            }
            if DashBoardViewController.s_postcode == nil{
                DashBoardViewController.s_postcode = ""
            }
            if DashBoardViewController.s_country == nil{
                DashBoardViewController.s_country = ""
            }
            let requestBody = "<?xml version=\"1.0\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"><soapenv:Body><ax:checkConnectivityRequest xmlns:ax=\"http://aat.colt.com/connectivityservice\" ax:schemaVersion=\"3.0\"><ax:sequenceId>1</ax:sequenceId><ax:requestType>SITE</ax:requestType><ax:requestMode><ax:requestID>4495</ax:requestID><ax:siteAddress><ax:latitude>\(lattitudePlace)</ax:latitude><ax:longitude>\(longitudePlace)</ax:longitude><ax:premisesNumber>\(DashBoardViewController.s_premissesNumber!)</ax:premisesNumber><ax:streetName>\(DashBoardViewController.s_streetName!)</ax:streetName><ax:cityTown>\(DashBoardViewController.s_cityName!)</ax:cityTown><ax:postalZipCode>\(DashBoardViewController.s_postcode!)</ax:postalZipCode><ax:coltOperatingCountry>\(DashBoardViewController.s_country!)</ax:coltOperatingCountry><ax:radius>500</ax:radius><ax:connectivityType>COLT Fibre</ax:connectivityType></ax:siteAddress></ax:requestMode></ax:checkConnectivityRequest></soapenv:Body></soapenv:Envelope>"
            
            
            
            var errMsg:String?
            let tokenString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "token")
            let accountTypeString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "accountType")
            let urlString = "http://loncis01/checkConnectivity/connectivity.wsdl"
            let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
            let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/connectnew")!)
            request.httpMethod = "POST"
            let requestData = requestBody.data(using: String.Encoding.utf8)
            request.httpBody = requestData
            request.addValue("text/xml", forHTTPHeaderField: "Content-Type")
            request.addValue("radius_search", forHTTPHeaderField: "SOAPAction")
            request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
            request.addValue(base64String!, forHTTPHeaderField: "X-CONNECT-U")
            request.addValue(tokenString!, forHTTPHeaderField: "X-GATEWAY-A")
            request.addValue(accountTypeString!, forHTTPHeaderField: "X-GATEWAY-F")
//            request.timeoutInterval=TimeInterval(MAXFLOAT)
            request.timeoutInterval=600
            
            let session = URLSession.shared
            _ = session.dataTask(with: request as URLRequest) { (data, response, error) in
                
                if error == nil
                {
                    if let data = data, let result = String(data: data, encoding: String.Encoding.utf8)
                    {
                        let xml = SWXMLHash.parse(result)
                        //print(xml)
                        
                        let onnetArray = ActiveAndInactiveBuildings.getOnnetLocations(xmlResponseData: xml)
                        let nearnetArray   = ActiveAndInactiveBuildings.getNearnrtLocations(xmlResponseData: xml)
                        
                        // Only onnet array is required for ONNET ViewController list
                        self.onnetListArray = onnetArray
                        
                        let tempAbArray = onnetArray+nearnetArray
                        self.abArray = tempAbArray
                        
                        self.addAddressListOnMapView(abArray:self.abArray!,lattitudePlace:lattitudePlace,longitudePlace:longitudePlace,addressPlace:addressPlace)
                        
                            staticHolder.lattitudePlace = 0
                            staticHolder.longitudePlace = 0
//                        }
                        
                        
                    }
                    else
                    {
                        // BackgroundApiCall
                        self.endBackGroundTask()
                        print("Failed to get valid response from server.")
                        staticHolder.lattitudePlace = 0
                        staticHolder.longitudePlace = 0
                        DispatchQueue.main.async {
//                            UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                            SwiftOverlays.removeAllBlockingOverlays()
                        }
                        UIHelper.showAlert(message:self.ServerErrorPleaseTryTgain , inViewController: self)
                    }
                }
                else
                {
                    // BackgroundApiCall
                    self.endBackGroundTask()
                    print(error?.localizedDescription ?? "Error")
                    staticHolder.lattitudePlace = 0
                    staticHolder.longitudePlace = 0
                    DispatchQueue.main.async {
//                        UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                        SwiftOverlays.removeAllBlockingOverlays()
                    }
                    UIHelper.showAlert(message: self.ServerErrorPleaseTryTgain, inViewController: self)
                }
                
                }
                .resume()
        }
    }
    
    func addAddressListOnMapView(abArray:[ActiveAndInactiveBuildings],lattitudePlace:CLLocationDegrees,longitudePlace:CLLocationDegrees,addressPlace:String) {
        
        DispatchQueue.main.async {
            
            self.mapView.clear()
            
            self.currentMarker=nil
            self.allMarkers.removeAll()
            //                        self.zoomLevel=15.0
            let camera = GMSCameraPosition.camera(withLatitude: lattitudePlace,
                                                  longitude: longitudePlace,
                                                  zoom: 15.0)
            
            
            //                        self.mapView.camera = camera
            self.mapView.animate(to: camera)
            let cordinatePlace = CLLocationCoordinate2D(latitude: lattitudePlace, longitude: longitudePlace)
            let  cMarker = GMSMarker(position: cordinatePlace)
            cMarker.map = self.mapView
            //                        cMarker.title=addressPlace
            cMarker.icon=UIImage(named: "CurrentLocation")
            cMarker.accessibilityValue=addressPlace
            //                        cMarker.isTappable=false
            self.mapView.selectedMarker=cMarker
            self.searchedMarker=cMarker
            let onnetTitle = NSLocalizedString("ONNET", comment: "")
            if (self.abArray?.count)!>0 {
                
                self.onnetCount.title="\(onnetTitle)(\(self.onnetListArray!.count))"
                
                // BackgroundApiCall
                self.endBackGroundTask()
                DispatchQueue.main.async {
//                    UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                    SwiftOverlays.removeAllBlockingOverlays()
                }
                
            }else{
                self.onnetCount.title=onnetTitle
                // BackgroundApiCall
                self.endBackGroundTask()
                DispatchQueue.main.async {
//                    UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                    SwiftOverlays.removeAllBlockingOverlays()
                    }
                DispatchQueue.main.async {
                    let alertTitle = NSLocalizedString("MAPS_LOCAION_DOSENT_PROVIDE_ONNET", comment: "")
                    UIHelper.showAlert(message: alertTitle, inViewController: self)
                    //                            self.showError("This location doesn't provide Onnet.Please try for another location")
                }
                
            }
            
            //                        UIHelper.sharedInstance.showError("This location doesn't provide Onnet.Please try for another location")
            for aiBuilding in self.abArray!{
                
                let cordna2d = CLLocationCoordinate2D(latitude: aiBuilding.lattitude!, longitude: aiBuilding.longitude!)
                let marker = GMSMarker(position: cordna2d)
                
                //  marker.title = selectedPlace?.name
                //  marker.snippet = selectedPlace?.formattedAddress
                marker.accessibilityHint=aiBuilding.buildingStatus
                
                if aiBuilding.bType == buildingType.Onnet {
                    if aiBuilding.buildingStatus == "ACTIVE" {
                        marker.icon=UIImage(named: "ActiveBuilding")
                    }else if aiBuilding.buildingStatus == "Pre approved On Net"{
                        marker.icon=UIImage(named: "PAON")
                    }else if aiBuilding.buildingStatus == "IN PROGRESS"{
                        marker.icon=UIImage(named: "inprogress")
                    }else{
                        marker.icon=UIImage(named: "InActiveBuilding")
                    }
                }else{
                    // for nearnet result
                    marker.icon=UIImage(named: "Nearnet.png")
                }
                
                let permisesNumber =  aiBuilding.permisesNumber
                let streetName =  aiBuilding.streetName
                self.englishCityName =  aiBuilding.englishCityName
                let postCode =  aiBuilding.postCode
                let coltOperatingCountry =  aiBuilding.coltOperatingCountry
                let address1 = "\(permisesNumber!), \(streetName!), \(self.englishCityName!), \(postCode!), \(coltOperatingCountry!)"
                marker.accessibilityValue=address1
                marker.accessibilityElements = [aiBuilding.pointID!,aiBuilding]
                marker.map = self.mapView
                //                    marker.title=nLocation1.name
                self.allMarkers.append(marker)
                if DashBoardViewController.s_slectedBuildingLatitude == aiBuilding.lattitude {
                    self.currentMarker=marker
                    DashBoardViewController.s_selectedSearchedAddress=aiBuilding
                    self.selectedSearchedAddress=aiBuilding
                    if self.selectedSearchedAddress?.bType != buildingType.Nearnet {
                        if aiBuilding.buildingStatus == "ACTIVE" {
                            let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "acitve_25", withExtension: "gif")!)
                            let advTimeGif = UIImage.gifImageWithData(imageData!)
                            marker.icon=advTimeGif
                        }else if aiBuilding.buildingStatus == "Pre approved On Net" {
                            let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "preApproved_25", withExtension: "gif")!)
                            let advTimeGif = UIImage.gifImageWithData(imageData!)
                            marker.icon=advTimeGif
                        }else if aiBuilding.buildingStatus == "IN PROGRESS" {
                            let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "inProgress_25", withExtension: "gif")!)
                            let advTimeGif = UIImage.gifImageWithData(imageData!)
                            marker.icon=advTimeGif
                        }else{
                            let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "inactive_25", withExtension: "gif")!)
                            let advTimeGif = UIImage.gifImageWithData(imageData!)
                            marker.icon=advTimeGif
                        }
                    }
                }
            }
            //                staticHolder.lattitudePlace = 0
            //                staticHolder.longitudePlace = 0
        }
        
    }
    
    public func dismissEmfPopupView(notification:Notification) {
        if notification.object == nil{
//            eMFpopupView?.removeFromSuperview()
            
        }else{
            selectedBendAdderess = notification.object as? ActiveAndInactiveBuildings
            CommonDataHelper.sharedInstance.npc = selectedBendAdderess?.pointID
            
        }
        
    }

    func dismissProductBandwithPopup(selectionType:String,productBandwithPopUpView:ProductBandwithPopUpView) {
        if selectionType == "CheckPrice" {
          selectedProduct = productBandwithPopUpView.selectedProduct
          selectedBandwidth = productBandwithPopUpView.selectedBandwidth
//          aEndAddress_id = productBandwithPopUpView.aEndAddress_id
          bend_Address = productBandwithPopUpView.selectedBEndBtn.currentTitle
          selectedPlace = productBandwithPopUpView.selectedPlace
          selectedB_EndAddress = productBandwithPopUpView.selectedB_EndAddress
         GoToCheckPriceController()
        }
        
    }
    
    func GoToCheckPriceController() {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
            
        }else{
        
            if selectedProduct == "Colt LANLink Point to Point (Ethernet Point to Point)" || selectedProduct == "Colt Wave" || selectedProduct == "Colt Private Ethernet" {
                if selectedPlace == nil {
//                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please Search B-End Address")
//                    self.showError("Please Search B-End Address")
                    let searchBEndAddress = NSLocalizedString("Please search B-End address", comment: "")
                    UIHelper.showAlert(message: searchBEndAddress, inViewController: self)

                }
                else if selectedB_EndAddress == nil {
//                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select B-End Address")
//                    self.showError("Please select B-End Address")
                    let selectBEndAddress = NSLocalizedString("Please select B-End Address", comment: "")
                    UIHelper.showAlert(message: selectBEndAddress, inViewController: self)

                }else{
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Secondary", bundle:nil)
                    let checkPriceVC = storyBoard.instantiateViewController(withIdentifier: "CheckPriceController") as! CheckPriceController
                    checkPriceVC.locationStr = pDetails.locationAddress as String?
                    checkPriceVC.productType = selectedProduct
                    checkPriceVC.bandWidthType = selectedBandwidth
                    checkPriceVC.bEndAddress_id = aEndAddress_id
                    checkPriceVC.bendAddress = bend_Address
                    CloseProductBandwidthPopUp()
//                    self.CheckAndPushViewController(checkPriceVC, animated: true)
                    self.navigationController?.pushViewController(checkPriceVC, animated: true)
                }
            }else if selectedProduct == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
                if CommonDataHelper.sharedInstance.npc == nil {
//                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select Hub")
//                    self.showError("Please select Hub")
                    UIHelper.showAlert(message: "Please select Hub", inViewController: self)
                    
                }else{
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Secondary", bundle:nil)
                    let checkPriceVC = storyBoard.instantiateViewController(withIdentifier: "CheckPriceController") as! CheckPriceController
                    checkPriceVC.locationStr = pDetails.locationAddress as String?
                    checkPriceVC.productType = selectedProduct
                    checkPriceVC.bandWidthType = selectedBandwidth
                    checkPriceVC.bEndAddress_id = aEndAddress_id
//                    self.CheckAndPushViewController(checkPriceVC, animated: true)
                    CloseProductBandwidthPopUp()
                    self.navigationController?.pushViewController(checkPriceVC, animated: true)
                }
            }
            else{
                let storyBoard : UIStoryboard = UIStoryboard(name: "Secondary", bundle:nil)
                let checkPriceVC = storyBoard.instantiateViewController(withIdentifier: "CheckPriceController") as! CheckPriceController
                checkPriceVC.locationStr = pDetails.locationAddress as String?
                checkPriceVC.productType = selectedProduct
                checkPriceVC.bandWidthType = selectedBandwidth
                checkPriceVC.bEndAddress_id = aEndAddress_id
//                self.CheckAndPushViewController(checkPriceVC, animated: true)
                CloseProductBandwidthPopUp()
                self.navigationController?.pushViewController(checkPriceVC, animated: true)
            }
//       }
//        CloseProductBandwidthPopUp()
        }
    }
    
    @IBAction func onNetBtnAction(_ sender: Any) {
        
        AnalyticsHelper().analyticsEventsAction(category: "OnnetCount", action: "ONNET_Count_click", label: "ONNET_Count")
        CreateInstanceOfOnNetVctrl(false);
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        if let language = LanguagesViewController.getSelectedLanguage() {
            Language.setAppleLAnguageTo(lang: language)
        }
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        
        if let language = LanguagesViewController.getSelectedLanguage() {
            Language.setAppleLAnguageTo(lang: language)
        }
        
        let searchBarTextColor = [NSForegroundColorAttributeName:UIColor.black,NSFontAttributeName:UIFont.systemFont(ofSize: 15.0)] as [String : Any]
        if #available(iOS 9.0, *) {
            let sBar = UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.classForCoder() as! UIAppearanceContainer.Type])
            sBar.defaultTextAttributes=searchBarTextColor
        } else {
            // Fallback on earlier versions
        }
        
        if locaationSearch.text?.isEmpty == true {
            locaationSearch.placeholder="Search location"
        }
        dismiss(animated: true, completion: nil)
    }
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //MARK: Apologies in advance, this is an outcome of poor planning. please forgive.....
    
    var onnetViewCtrl:ONNetListViewController? = nil
    
    func ShowCheckPricePopupView(){
        CreateInstanceOfOnNetVctrl(true)
    }
    
    override func openLeftView(_ sender: Any?) {
        
        AnalyticsHelper().analyticsEventsAction(category: "Navigation Menu", action: "navigationMenu_click", label: "Navigation Menu")
        if(self.onnetViewCtrl != nil){
            self.onnetViewCtrl!.prepareForActualShowUp()
        }
        super.openLeftView(sender)
    }
    
    func CreateInstanceOfOnNetVctrl(_ isSilent:Bool){
        let noONNETlistavailable = NSLocalizedString("No ONNET list available", comment: "")
        if onnetListArray?.count == 0 {
            
            if(!isSilent){
                
//                let alert = UIAlertController(title: "", message: "No ONNET list available", preferredStyle: UIAlertControllerStyle.alert)
//                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
                UIHelper.showAlert(message: noONNETlistavailable, inViewController: self)
            }
        }else if abArray?.count == nil {
            if(!isSilent){
//                let alert = UIAlertController(title: "", message: "No ONNET list available", preferredStyle: UIAlertControllerStyle.alert)
//                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
                UIHelper.showAlert(message: noONNETlistavailable, inViewController: self)
            }
        }else{
            
            if(onnetViewCtrl == nil){
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                onnetViewCtrl = storyBoard.instantiateViewController(withIdentifier: "ONNetListViewController") as? ONNetListViewController
            }
            // passing only ONNET array
            onnetViewCtrl?.abArray = self.onnetListArray
            onnetViewCtrl?.selectedMarker = selectedSearchedAddress
            if(!isSilent){
                self.onnetViewCtrl!.prepareForActualShowUp()
                self.navigationController?.pushViewController(onnetViewCtrl!, animated: true)
            }
            else{
                onnetViewCtrl!.showUpSilently(self.navigationController!){ [weak self] in
                    if(self?.onnetViewCtrl != nil){
                        self?.onnetViewCtrl!.view.removeFromSuperview()
                    }
                }
                self.view.addSubview(onnetViewCtrl!.view)
            }
            FactoryResetOfOnNetViewCtrl(isSilent)
        }
    }
    
    func FactoryResetOfOnNetViewCtrl(_ isSilent:Bool){
        
        if(onnetViewCtrl == nil) {
            return
        }
        
        if(isSilent) {

        }
        else{
            onnetViewCtrl?.navigationController?.setNavigationBarHidden(true, animated: true)
            if(onnetViewCtrl?.mainTableView != nil){
                onnetViewCtrl?.mainTableView.reloadData() //Did this to update the Selected Building....
            }
        }
    }
    
    static func ClearAllStaticData(){
        s_gmsPlace = nil
        s_longitude = nil
        s_latitude = nil
        s_slectedBuildingLatitude = nil
        s_selectedAddress = nil
    
    }
    
    func getLocationAddres(lattitude:CLLocationDegrees,longitude:CLLocationDegrees) {
        
        DashBoardViewController.s_premissesNumber = nil
        DashBoardViewController.s_streetName = nil
        DashBoardViewController.s_cityName = nil
        DashBoardViewController.s_country = nil
        DashBoardViewController.s_postcode = nil
        
        // clearing SavedSearch Data before adding
        clearSavedSearchCommonHelperData()
        
        DispatchQueue.global().async {
            let locationString = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&key=\(self.apiKey)"
            let locationUrl:URL = URL(string: locationString)!
            do {
                let locationResponseData:Data = try Data.init(contentsOf: locationUrl)
                let dict = try JSONSerialization.jsonObject(with: locationResponseData, options: .allowFragments) as? Dictionary<String, Any>
                //            print(dict ?? "No value")
                let locationComponents = dict!["results"] as! [Any]
                if locationComponents.count > 0 {
                    let sinleDict = locationComponents[0] as! [String:Any]
                    self.locationAddress = sinleDict["formatted_address"] as? String
                    DashBoardViewController.s_selectedAddress = self.locationAddress
                    
                    let placeAddressComponents = sinleDict["address_components"] as? [[String:Any]]
                    
                    for components in placeAddressComponents! {
                        let items = components as! [String:Any]
                        let keysArray = items["types"] as! [String]
                        
                        for kKey in keysArray {
                            print(kKey)
                            print(items["long_name"])
                            if kKey == "street_number" {
                                let value = items["long_name"]
                                DashBoardViewController.s_premissesNumber = value as? String
                               
                            }else if kKey == "route" ||  kKey == "political" {
                                let value = items["long_name"]
                                if DashBoardViewController.s_streetName == nil{
                                DashBoardViewController.s_streetName = value as? String
                                }
                                
                            }else if kKey == "locality" {
                                let value = items["long_name"]
                                DashBoardViewController.s_cityName = value as? String
                                
                            }else if kKey == "country" {
                                let value = items["long_name"]
                                DashBoardViewController.s_country = value as? String
                                
                            }else if kKey == "postal_code" {
                                let value = items["long_name"]
                                DashBoardViewController.s_postcode = value as? String
                                
                            }
                        }
                    }
                    
                    
                    
                 DispatchQueue.main.async {
                    if DashBoardViewController.s_selectedAddress != nil{
                     self.locaationSearch.text = DashBoardViewController.s_selectedAddress
                    }else{
                        DashBoardViewController.s_selectedAddress = self.locationAddress
                    }
                self.locaationSearch.text = self.locationAddress
                DashBoardViewController.s_selectedAddress = self.locationAddress
                DashBoardViewController.s_longitude = longitude
                DashBoardViewController.s_latitude = lattitude
                    }
                    
                 self.addingSavedSearchCommonHelperData() // Assigning SavedSearch Data to commonhelper variables
                    
                self.getActiveAndInactiveBuildings(lattitudePlace: lattitude, longitudePlace: longitude, addressPlace: self.locationAddress!)
                }
                
            } catch  {
                print(error)
            }
            
        }
    }
    
    // Calling this method to Get Place_ID from google API
    
    func getPlace_IDfromSelectedAddress(premissesNo:String,streetName:String,cityName:String,country:String,postcode:String) {
       
       
        let appDel = UIApplication.shared.delegate as! AppDelegate
        
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
        }else{
            
        DispatchQueue.main.async {
            SwiftOverlays.showBlockingWaitOverlayWithText(self.Please_wait)
        }
        
        var place_ID:String?
        DashBoardViewController.s_premissesNumber = premissesNo
        DashBoardViewController.s_streetName = streetName
        DashBoardViewController.s_cityName = cityName
        DashBoardViewController.s_postcode = postcode
        DashBoardViewController.s_country = country
        
//        var address = "\(premissesNo), \(streetName), \(cityName),\(country), \(postcode)"
//        locaationSearch.text = "\(premissesNo),\(streetName),\(cityName),\(country),\(postcode)"
        
        var aEndAddress = ""
        if !(premissesNo.isEmpty) {
            aEndAddress = "\(aEndAddress) \(premissesNo),"
        }
        if !(streetName.isEmpty) {
            aEndAddress = "\(aEndAddress) \(streetName),"
        }
        if !(cityName.isEmpty) {
            aEndAddress = "\(aEndAddress) \(cityName),"
        }
        if !(country.isEmpty) {
            aEndAddress = "\(aEndAddress) \(country),"
        }
        if !(postcode.isEmpty) {
            aEndAddress = "\(aEndAddress) \(postcode),"
        }
        
        if !(aEndAddress.isEmpty) {
            aEndAddress = aEndAddress.substring(to: aEndAddress.index(before: aEndAddress.endIndex))
        }
        aEndAddress = aEndAddress.trimmingCharacters(in: CharacterSet(charactersIn: " "))
        
        locaationSearch.text = aEndAddress
        
        let urlString  =  "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(aEndAddress)&key=AIzaSyAYP3NIanhDTOXdf347RwkUQRrfcy-R1tg&sensor=false"
            let url = URL(string:(urlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed))!)
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                if (error != nil) {
                    print("error")
                } else {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                        let responseList = json["predictions"] as? [[String:Any]]
                        
                        for item in responseList! {
                            if place_ID == nil || place_ID == ""{
                                place_ID = item["place_id"] as? String
                                print("place_ID = \(place_ID)")
                            }
                        }
                        print(json as NSDictionary)

                    } catch let error as NSError{
                        print(error)
                    }
                    // Api calling to get Lat and Long values
                    if place_ID == nil {
                        DispatchQueue.main.async {
                        SwiftOverlays.removeAllBlockingOverlays()
                        }
                        let pleaseProvideValidAddress = NSLocalizedString("Please provide valid address.", comment: "")
                        UIHelper.showAlert(message: pleaseProvideValidAddress, inViewController: self
                        )
                    }else{
                      self.getLatitudeandLongitudeValuesfromPlace_ID(place_ID: place_ID!,address:aEndAddress)
                    }
                }
            }).resume()
        }
    }
    func getLatitudeandLongitudeValuesfromPlace_ID(place_ID:String,address:String){
        
     
        let appDel = UIApplication.shared.delegate as! AppDelegate
        
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
            
        }else{
        
        var latitude:Double?
        var longitude:Double?
        
        let urlString  = "https://maps.googleapis.com/maps/api/geocode/json?place_id=\(place_ID)&key=AIzaSyAYP3NIanhDTOXdf347RwkUQRrfcy-R1tg&sensor=false"
        let url = URL(string:urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if (error != nil) {
                print("error")
            } else {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                    let responseList = json["results"] as? [[String:Any]]
                    let tempVal = responseList![0] as? [String:Any]
                    let value = tempVal!["geometry"] as? [String:Any]
                    let value2 = value!["location"] as? [String:Double]
                     latitude = value2!["lat"]
                     longitude = value2!["lng"]
                    print(json as NSDictionary)
                    
                    
                } catch let error as NSError{
                    print(error)
                }
                // Calling ONNET api 
                self.getActiveAndInactiveBuildings(lattitudePlace: latitude!, longitudePlace: longitude!, addressPlace: address)
                
                CommonDataHelper.sharedInstance.isFromSavedsearch = false
            }
        }).resume()
        
      }
    }
        
 }
extension DashBoardViewController: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
//        struct StaticHolder{
//            static var locationCord:CLLocationCoordinate2D? = nil
//        }
        
        if isCurrentLocationAdded==false {
            isCurrentLocationAdded=true
            var locationAddress:String?
            let location: CLLocation = locations.last!
            //        print("Location: \(location)")
            
            // https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=YOUR_API_KEY
            
            let appDel = UIApplication.shared.delegate as! AppDelegate
            
            if appDel.networkStatus! == NetworkStatus.NotReachable {
                
                DispatchQueue.main.async {
                    
                    UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
                }
                
            }else{
                
                DispatchQueue.global().async {
                    let locationString = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(location.coordinate.latitude),\(location.coordinate.longitude)&key=\(self.apiKey)"
                    let locationUrl:URL = URL(string: locationString)!
                    do {
                        let locationResponseData:Data = try Data.init(contentsOf: locationUrl)
                        let dict = try JSONSerialization.jsonObject(with: locationResponseData, options: .allowFragments) as? Dictionary<String, Any>
                        //            print(dict ?? "No value")
                        let locationComponents = dict!["results"] as! [Any]
                        
                        if locationComponents.count > 0 {
                            let sinleDict = locationComponents[1] as! [String:Any]
                            locationAddress = sinleDict["formatted_address"] as? String
                            print(locationAddress ?? "No address")
                            self.currenPlaceAddressComponents = sinleDict["address_components"] as? [[String:Any]]
                            
                            for components in self.currenPlaceAddressComponents! {
                                let items = components as! [String:Any]
                               let keysArray = items["types"] as! [String]
                                
                                for kKey in keysArray {
                                    print(kKey)
                                    if kKey == "street_number" {
                                        let value = items["long_name"]
                                        DashBoardViewController.s_premissesNumber = value as? String
                                    }else if kKey == "political" {
                                        let value = items["long_name"]
                                        DashBoardViewController.s_streetName = value as? String
                                    }else if kKey == "locality" {
                                        let value = items["long_name"]
                                        DashBoardViewController.s_cityName = value as? String
                                    }else if kKey == "country" {
                                        let value = items["long_name"]
                                        DashBoardViewController.s_country = value as? String
                                    }else if kKey == "postal_code" {
                                        let value = items["long_name"]
                                        DashBoardViewController.s_postcode = value as? String
                                    }
                                }
                            }
                        }
                        
//                        if(StaticHolder.locationCord == nil){
//                            StaticHolder.locationCord = location.coordinate
//                            self.getActiveAndInactiveBuildings(lattitudePlace: location.coordinate.latitude, longitudePlace: location.coordinate.longitude, addressPlace: locationAddress!)
//                            DispatchQueue.main.async {
//                                self.currentLocationMarker!.title = locationAddress
//                                self.mapView.selectedMarker=self.currentLocationMarker!
//                            }
//                        }
//                        
//                        var latdiff = abs((StaticHolder.locationCord?.latitude)! - location.coordinate.latitude)
//                        var longdiff = abs((StaticHolder.locationCord?.longitude)! - location.coordinate.longitude)
//
//                        
//                        if(latdiff > 0.00001 && longdiff > 0.00001){
                        
                        // Showing current address in Searchbar
                         DispatchQueue.main.async {
                            if let addr = locationAddress {
                                self.locaationSearch.text = addr
                            } else {
                                self.locaationSearch.text = ""
                            }
                        }
                        // Calling GetBuilding list API
                            DispatchQueue.main.async {
                              self.getActiveAndInactiveBuildings(lattitudePlace: location.coordinate.latitude, longitudePlace: location.coordinate.longitude, addressPlace: locationAddress ?? "")
//                                self.currentLocationMarker!.title = locationAddress
                              self.mapView.selectedMarker=self.currentLocationMarker!
                            }
//                        }
                        
                    } catch  {
                        print(error)
                    }
                }
            }
            
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                                  longitude: location.coordinate.longitude,
                                                  zoom: 15.0)
            mapView.camera = camera
            currentLocationMarker = GMSMarker(position: location.coordinate)
            currentLocationMarker!.map = mapView
            currentLocationMarker?.isTappable=false
            //                    currentLocationMarker!.title="My location"
            currentLocationMarker!.icon=UIImage(named: "CurrentLocation")
            locationManager.stopUpdatingLocation()
            
        }
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
//            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
            locationManager.requestWhenInUseAuthorization()
        case .authorizedAlways:
            print("Location status is OK. authorizedAlways")
            locationManager.startUpdatingLocation()
        case .authorizedWhenInUse:
            print("Location status is OK. authorizedWhenInUse")
            locationManager.startUpdatingLocation()
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    
//    func showError(_ msg: String) {
//        UIHelper.showAlert(message: msg, inViewController: self)
//        let alertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
//        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//        alertController.addAction(defaultAction)
//        self.present(alertController, animated: true, completion: nil)
//    }
    func showAlert(notification: NSNotification) {
        if notification.object != nil{
            let alertMsg = notification.object as? String
            UIHelper.showAlert(message: alertMsg, inViewController: self)
//            let alertController = UIAlertController(title: "", message: alertMsg, preferredStyle: .alert)
//            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//            alertController.addAction(defaultAction)
//            self.present(alertController, animated: true, completion: nil)
        }
    }
    
}

