//
//  ProductsModel.swift
//  FeetOnStreet
//
//  Created by admin on 3/14/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import CoreData

class ProductsModel: NSObject {
    
    var productName:String
    var productCode:String
    
    override init(){
        productName = ""
        productCode = ""
    }
    
    
    init(name: String, code:String){
        productName = name
        productCode = code
    }
    
    
    class func getProductListWithCode(results:[Any]) -> [ProductsModel] {
        
        var productsModel = [ProductsModel]()
        var productsModelList:ProductsModel?
        
        for row in results {
            productsModelList=ProductsModel()
            productsModelList?.productName=(row as! NSManagedObject).value(forKey: "name") as! String
            productsModelList?.productCode=(row as! NSManagedObject).value(forKey: "code") as! String
            
            productsModel.append((productsModelList)!)
        }
        
        return productsModel
    }
    
}

