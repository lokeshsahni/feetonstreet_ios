//
//  TermsOfUseVC.swift
//  FeetOnStreet
//
//  Created by admin on 8/7/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import SwiftOverlays

class TermsOfUseVC: UIViewController , UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = UIColor.c_Teal
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        
//            UIHelper.sharedInstance.showLoadingOnViewController(loadVC: self)
        let Please_wait = NSLocalizedString("Please wait", comment: "")
        SwiftOverlays.showBlockingWaitOverlayWithText(Please_wait)
        let lang = LanguagesViewController.getSelectedLanguage()
        var url:URL!
        if lang == "fr" {
            url = Bundle.main.url(forResource: "French_legal", withExtension:"html")
        }
        else if lang == "de" {
            url = Bundle.main.url(forResource: "German_legal", withExtension:"html")
        }
        else if lang == "it" {
            url = Bundle.main.url(forResource: "Italian_legal", withExtension:"html")
        }
        else if lang == "ja" {
            url = Bundle.main.url(forResource: "Japanese_legal", withExtension:"html")
        }
        else if lang == "pt-PT" {
            url = Bundle.main.url(forResource: "Portugese_legal", withExtension:"html")
        }
        else if lang == "es-ES" {
            url = Bundle.main.url(forResource: "Spanish_legal", withExtension:"html")
        }else{
            url = Bundle.main.url(forResource: "LegalNotice", withExtension:"html")
        }
        
        DispatchQueue.main.async {
            SwiftOverlays.removeAllBlockingOverlays()
            self.webView.loadRequest(URLRequest(url: url!))
//            UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        AnalyticsHelper().analyticLogScreen(screen: "Terms of use Screen")
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked {
    //            UIApplication.shared.openURL(request.url!)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }

            return false
        }
        return true
    }

    @IBAction func sideMenuBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
