//
//  EMFandDSLPopUP.swift
//  FeetOnStreet
//
//  Created by admin on 9/6/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class EMFandDSLPopUP: UIView , UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var eMFDSLlistArray:[Any] = []
    var oLOlistArray:[Any] = []
    var addresslist:[Any] = []
    var citiesMatchedList:[Any]? = []
    var sectionsArray = ["---ONNET---","---EMF/DSL---","---OLO---"]
    var singleSectionsArray = ["---ONNET---"]
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
        tableView.estimatedRowHeight = 157
        tableView.rowHeight = UITableViewAutomaticDimension
    
        
        //   tableView.register(UINib(nibName: "EMFandOLOPopUpCell", bundle: nil), forCellReuseIdentifier: "FandOLOPopUpCell")
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        
        if CommonDataHelper.sharedInstance.selectedProduct == "Colt Private Ethernet" || CommonDataHelper.sharedInstance.selectedProduct == "Colt Wave" {
           return singleSectionsArray.count
        }else{
        return sectionsArray.count
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return addresslist.count
        }else if section == 1 {
            return eMFDSLlistArray.count
        }else{
            return oLOlistArray.count
            
        }
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            var cell = tableView.dequeueReusableCell(withIdentifier: "BEndCell") as? BEndCell
            cell=Bundle.main.loadNibNamed("BEndCell", owner: self, options: nil)?[0] as? BEndCell
            let activeAndInactiveBuildings = addresslist[indexPath.row] as? ActiveAndInactiveBuildings
            let permisesNumber = activeAndInactiveBuildings!.permisesNumber
            let streetName = activeAndInactiveBuildings!.streetName
            let cityName = activeAndInactiveBuildings!.englishCityName
            let postCode = activeAndInactiveBuildings!.postCode
            let coltOperatingCountry = activeAndInactiveBuildings!.coltOperatingCountry
            
            cell?.addressLbl.text = "\(permisesNumber!), \(streetName!), \(cityName!), \(postCode!), \(coltOperatingCountry!)"
            cell?.supplierNameLbl.text = "Colt Fibre"
            
            return cell!
            
        }else if indexPath.section == 1 {
            var cell = tableView.dequeueReusableCell(withIdentifier: "EmfCell") as? EmfCell
            cell=Bundle.main.loadNibNamed("EmfCell", owner: self, options: nil)?[0] as? EmfCell
            let dSLModel = eMFDSLlistArray[indexPath.row] as! DSLModel
            if dSLModel.streetName == nil {
                cell?.addressLbl.text =  dSLModel.cityName! + " , " + dSLModel.postCode! + " , " + dSLModel.coltOperatingCountry!
            }else{
                cell?.addressLbl.text = dSLModel.streetName! + " , " + dSLModel.cityName! + " , " + dSLModel.postCode! + " , " + dSLModel.coltOperatingCountry!
            }
            cell?.addressLbl.textAlignment = .center
            cell?.supplierNameLbl.text = dSLModel.supplierName
            cell?.accessTypeLbl.text = dSLModel.accessType
            
            return cell!
            
        }else{
            var cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? EMFandOLOPopUpCell
            cell=Bundle.main.loadNibNamed("EMFandOLOPopUpCell", owner: self, options: nil)?[0] as? EMFandOLOPopUpCell
            let oLOModel = oLOlistArray[indexPath.row] as! OLOModel
            cell?.supplierNameLbl.text = oLOModel.supplierName
            cell?.accessTypeLbl.text = "Leased Line"
            cell?.addressLbl.text = oLOModel.premisesNumber! + " , " + oLOModel.streetName! + " , " + oLOModel.cityName! + " , "  + oLOModel.cityName! + " , " + oLOModel.postCode! + oLOModel.coltOperatingCountry!
            cell?.supplierProctLbl.text = oLOModel.supplierProduct
            
            return cell!
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let activeAndInactiveBuildings = addresslist[indexPath.row] as? ActiveAndInactiveBuildings
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "EMF_DSL_Popup"), object: activeAndInactiveBuildings)
        }
        else if indexPath.section == 1 {
            let dSLModel = eMFDSLlistArray[indexPath.row] as! DSLModel
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "EMF_DSL_Popup_DSLResult"), object: dSLModel)
        }else{
            let oLOModel = oLOlistArray[indexPath.row] as! OLOModel
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "EMF_DSL_Popup_OLOResult"), object: oLOModel)
        }
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let oNNET = NSLocalizedString("ONNET", comment: "")
        let sectionsTitlesKeys = ["---\(oNNET)---","---EMF/DSL---","---OLO---"]
        let key = sectionsTitlesKeys[section]
        let localizedString = NSLocalizedString(key,tableName: nil, bundle: Bundle.main, value: "", comment: "")
        return sectionsTitlesKeys[section]
        
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
        if let headerTitle = view as? UITableViewHeaderFooterView {
            headerTitle.backgroundColor = UIColor.clear
            headerTitle.textLabel?.textColor = UIColor(colorLiteralRed: 0, green: 165.0/255.0, blue: 156.0/255.0, alpha: 1.0)
            headerTitle.textLabel?.font = UIFont.systemFont(ofSize: 17)
            headerTitle.textLabel?.textAlignment = NSTextAlignment.center
            
        }
    }
    
    @IBAction func closeViewBtnAction(_ sender: Any) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "EMF_DSL_Popup"), object: nil)
        
        
    }
    
    
}

