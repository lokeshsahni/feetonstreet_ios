//
//  WebHelper.swift
//  FeetOnStreet
//
//  Created by admin on 7/28/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class WebHelper: NSObject {
    
    static let sharedInstance = WebHelper()
    
    fileprivate override init() {
        super.init()
    }
    
    
    func callDirectService(_ url: String, body: String, soapAction: String?, completionHandler: @escaping (_ err_msg: String?, _ resp: String?) -> Void) {
        
        let request = NSMutableURLRequest(url: URL(string: url)!)
        
        request.httpMethod = "POST"
        request.httpBody = body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        request.addValue(String(describing: request.httpBody?.count), forHTTPHeaderField: "Content-Length")
        request.addValue("text/xml", forHTTPHeaderField: "Content-Type")
        if(soapAction != nil)
        {
            request.addValue(soapAction!, forHTTPHeaderField: "SOAPAction")
        }
        //request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            if error == nil
            {
                if let data = data, let result = String(data: data, encoding: String.Encoding.utf8)
                {
                    completionHandler(nil, result)
                }
                else
                {
                    completionHandler("Failed to get valid response from server.",nil)
                }
            }
            else
            {
                completionHandler(error?.localizedDescription,nil)
            }
            
            }
            .resume()
        
    }
    
    
    func getURLContentsViaGateway(_ url: String, headers: Dictionary<String,String>, completionHandler:@escaping (_ err_msg: String?, _ resp: String?) -> Void) {
    
        let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/connectnew")!)
        
        request.httpMethod = "GET"
        request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
        
        var err_msg : String?
        
        request.addValue(CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "token")!, forHTTPHeaderField:"X-GATEWAY-A")
        
        request.addValue(((url).data(using: String.Encoding.utf8))!.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0)), forHTTPHeaderField:"X-CONNECT-U")
        
        request.addValue(CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "accountType")!, forHTTPHeaderField:"X-GATEWAY-F")
        
        for header in headers
        {
            request.addValue(header.1, forHTTPHeaderField: header.0)
        }
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            if(error == nil)
            {
                if let data = data, let result = String(data: data, encoding: String.Encoding.utf8)
                {
                    completionHandler(nil, result)
                }
                else
                {
                    completionHandler("Failed to get valid response from server.",nil)
                }
            }
            else
            {
                completionHandler(error.debugDescription,nil)
            }
            
            }
            .resume()
        
    }
    
    
    func callServiceViaGateway(_ url: String, body: String, soapAction: String?, basicAuth: String?,completionHandler: @escaping (_ err_msg:String?, _ resp: String?) -> Void) {
    
        let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/connectnew")!)
        
        request.httpMethod = "POST"
        request.httpBody = body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        request.addValue(String(describing: request.httpBody?.count), forHTTPHeaderField: "Content-Length")
        request.addValue("text/xml;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue(soapAction!, forHTTPHeaderField: "SOAPAction")
        request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
        
        var err_msg : String?
        
        request.addValue(CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "token")!, forHTTPHeaderField:"X-GATEWAY-A")
        
        //print(((url).dataUsingEncoding(NSUTF8StringEncoding))!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.init(rawValue: 0)) + " : X-CONNECT-U")
        request.addValue(((url).data(using: String.Encoding.utf8))!.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0)), forHTTPHeaderField:"X-CONNECT-U")
        
        //print(CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "accountType")! + " : X-GATEWAY-F")
        
        request.addValue(CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "accountType")!, forHTTPHeaderField:"X-GATEWAY-F")
        
        if(basicAuth != nil)
        {
            if(basicAuth!.contains("Basic "))
            {
                request.addValue(basicAuth!, forHTTPHeaderField:"Authorization")
            }
            else
            {
                request.addValue(basicAuth!, forHTTPHeaderField:"X-BASIC-AUTH")
            }
        }
        
        URLSession.shared.configuration.timeoutIntervalForRequest = 500
        URLSession.shared.configuration.timeoutIntervalForResource = 500
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            if error == nil
            {
                if let data = data, let result = String(data: data, encoding: String.Encoding.utf8)
                {
                    completionHandler(nil, result)
                }
                else
                {
                    completionHandler("Failed to get valid response from server.",nil)
                }
            }
            else
            {
                completionHandler(error?.localizedDescription,nil)
            }
            
            
            }
            .resume()
        
    }

}
