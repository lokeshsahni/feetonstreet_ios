//
//  UpdatesTableViewCell.swift
//  FeetOnStreet
//
//  Created by admin on 3/2/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class UpdatesTableViewCell: UITableViewCell {
    @IBOutlet weak var fibreDistanceLbl: UILabel!
    @IBOutlet weak var thearticalLatencyLbl: UILabel!
    @IBOutlet weak var thearticalRDSlbl: UILabel!
    @IBOutlet weak var connectedSiteNameLbl: UILabel!
    @IBOutlet weak var cellCountLbl: UILabel!
    @IBOutlet weak var shortesrOrdivereseRouteLbl: UILabel!
    @IBOutlet weak var backGroundBottomView: UIView!
    @IBOutlet weak var view_xchng: UIView!
    @IBOutlet weak var layoutConstraintHeight_viewXchng: NSLayoutConstraint!
    @IBOutlet weak var btn_quickLook: UIButton!
    @IBOutlet weak var longliningIsRequiredLbl: UILabel!
    
    @IBOutlet weak var constraintHeight_subSeaLink: NSLayoutConstraint!
    @IBOutlet weak var lbl_subSeaLink: UILabel!
    weak var vcReference:RTDtoolViewController? = nil
    
    var routeModel:RouteDetailModel?{
        didSet{
            cellCountLbl.layer.cornerRadius = cellCountLbl.frame.width/2
            cellCountLbl.layer.masksToBounds = true
            let fibreDistance = Double((routeModel?.distance)!) // fibreDistance
            let fibreDstnce = Double(round(1000*fibreDistance)/1000)
            fibreDistanceLbl.text = String(fibreDstnce)
            let thearticalLatency = Double((routeModel?.totalLatency100G)!)
            let thearticLatency = Double(round(1000*thearticalLatency)/1000)
            thearticalLatencyLbl.text = String(thearticLatency)
            let thearticalRDS = Double((routeModel?.totalLatency100G)!)
            let theartical = thearticalRDS*2
//            let thearticalRDS = Double((routeModel?.totalPhotonicLatency100G)!)
            let thearticRDS = Double(round(1000*theartical)/1000)
            thearticalRDSlbl.text = String(thearticRDS)
            connectedSiteNameLbl.text = routeModel?.connectedSiteName
            if let virtualPaths = (routeModel?.path.filter({ (p) -> Bool in
                return (p.isVirtual)
            })) {
                longliningIsRequiredLbl.isHidden = (virtualPaths.count == 0)
            }else{
                longliningIsRequiredLbl.isHidden = true
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backGroundBottomView.layer.borderWidth = 1
        self.backGroundBottomView.layer.borderColor = UIColor.darkGray.cgColor
        let quickViewToSeeRouteOnMap = NSLocalizedString("Quick View-see route on a map", comment: "")
        btn_quickLook.setTitle(quickViewToSeeRouteOnMap, for: .normal)
        self.btn_quickLook.addTarget(self, action: #selector(self.quickLookAction), for: .touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func quickLookAction(_ sender:Any) {
        AnalyticsHelper().analyticsEventsAction(category: "Quick RootMap View", action: "Quick_RootMap_click", label: "RootMap_View")
        vcReference?.quickLookAction(with: routeModel!)
    }
}
