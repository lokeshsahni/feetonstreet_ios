//
//  HelpTour1.swift
//  FeetOnStreet
//
//  Created by admin on 5/17/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class HelpTour1: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        AnalyticsHelper().analyticLogScreen(screen: "Help Tour Screen")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func nextBtnAction(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let helpTour2 = storyBoard.instantiateViewController(withIdentifier: "HelpTour2") as! HelpTour2
        self.navigationController?.pushViewController(helpTour2, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
