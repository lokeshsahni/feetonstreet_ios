//
//  MainViewController.swift
//  FeetOnStreet
//
//  Created by admin on 7/28/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class MainViewController: LGSideMenuController {
    
    var alertController : UIAlertController!
    
    override func viewDidLoad() {
        
//        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
//        let statusBarColor = UIColor(red: 0/255, green: 105/255, blue: 95/255, alpha: 1.0)
//        statusBarView.backgroundColor = statusBarColor
//        view.addSubview(statusBarView)
        
        let leftViewController: LeftMenuViewController = UIStoryboard.main().instantiateViewController(withIdentifier: "LeftMenu") as! LeftMenuViewController
        let  rootViewController : UINavigationController = UIStoryboard.main().instantiateViewController(withIdentifier: "DashBoardNav") as! UINavigationController
        
        self.rootViewController = rootViewController
        self.leftViewController = leftViewController
        let greenCoverColor = UIColor(red: 0.0, green: 0.1, blue: 0.0, alpha: 0.3)
        let purpleCoverColor = UIColor(red: 0.1, green: 0.0, blue: 0.1, alpha: 0.3)
        let regularStyle: UIBlurEffectStyle
        
        if #available(iOS 10.0, *) {
            regularStyle = .regular
        }
        else {
            regularStyle = .light
        }
        
        leftViewPresentationStyle = .slideAbove
        rootViewCoverColorForLeftView = greenCoverColor
        
        rightViewPresentationStyle = .slideAbove
        rootViewCoverColorForRightView = purpleCoverColor
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.setCentreView), name: Notification.Name("SetCenterView"), object: nil)
    }
    
    func setCentreView(sender : NSNotification)  {
        
//        print(navigationController?.viewControllers)
        
//        let userInfo = sender.userInfo as! [String: Any] // or use if you know the type  [AnyHashable : Any]

        let indexPath = sender.object as! Int
        
        if indexPath == 0 {
             AnalyticsHelper().analyticsEventsAction(category: "Navigation Menu", action: "nav_connectivityChecker_click", label: "Connectivity Checker")
            self.hideLeftView(nil)
            let dashBoardViewController : UINavigationController = UIStoryboard.main().instantiateViewController(withIdentifier: "DashBoardNav") as! UINavigationController
            self.rootViewController = dashBoardViewController
            
        }else if indexPath == 1 {
            AnalyticsHelper().analyticsEventsAction(category: "Navigation Menu", action: "nav_SavedSearch_click", label: "SavedSearch")
            self.hideLeftView(nil)
            let  saveSearchVC : UINavigationController = UIStoryboard.main().instantiateViewController(withIdentifier: "SavedSearchNav") as! UINavigationController
            self.rootViewController = saveSearchVC
        }
        else if indexPath == 2 {
             AnalyticsHelper().analyticsEventsAction(category: "Navigation Menu", action: "nav_accountSettings_click", label: "Account Settings")
            self.hideLeftView(nil)
            let accountSettingsViewController : UINavigationController = UIStoryboard.main().instantiateViewController(withIdentifier: "AccountSettingsNav") as! UINavigationController
            self.rootViewController = accountSettingsViewController
        }
        else if indexPath == 3 {
            AnalyticsHelper().analyticsEventsAction(category: "Navigation Menu", action: "nav_LanguageSettings_click", label: "Language Settings")
            self.hideLeftView(nil)
            let languageSettingsViewController : UINavigationController = UIStoryboard.main().instantiateViewController(withIdentifier: "LanguageSettingsNav") as! UINavigationController
            self.rootViewController = languageSettingsViewController
        }
        else if indexPath == 4 {
             AnalyticsHelper().analyticsEventsAction(category: "Navigation Menu", action: "nav_dataPrivacyStatement_click", label: "Data Privacy Statement")
            self.hideLeftView(nil)
            let dataPrivacyStatementVC : UINavigationController = UIStoryboard.main().instantiateViewController(withIdentifier: "DataPrivacyStatementNav") as! UINavigationController
            self.rootViewController = dataPrivacyStatementVC
        }
        else if indexPath == 5 {            
            AnalyticsHelper().analyticsEventsAction(category: "Navigation Menu", action: "nav_termsOfUse_click", label: "Terms of Use")
            self.hideLeftView(nil)
            let termsOfUseVC : UINavigationController = UIStoryboard.main().instantiateViewController(withIdentifier: "TermsOfUseNav") as! UINavigationController
            self.rootViewController = termsOfUseVC
        }
        else if indexPath == 6 {
            AnalyticsHelper().analyticsEventsAction(category: "Navigation Menu", action: "nav_RTD_click", label: "RTD Screen")
            self.hideLeftView(nil)
            
            //for testing
            //            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            //            let rTDtoolViewController = storyBoard.instantiateViewController(withIdentifier: "MapDirectionVC") as! MapDirectionVC
            //            self.rootViewController = rTDtoolViewController
            //            return
            //testing code ends
            
            let cityandSiteVC : UINavigationController = UIStoryboard.main().instantiateViewController(withIdentifier: "CityandSiteNav") as! UINavigationController
            self.rootViewController = cityandSiteVC
            
        }
        else if indexPath == 7 {
            AnalyticsHelper().analyticsEventsAction(category: "Navigation Menu", action: "nav_Help Tour_click", label: "Take a Tour")
            self.hideLeftView(nil)
            let  takeAtourVC : UINavigationController = UIStoryboard.main().instantiateViewController(withIdentifier: "HelpTourNav") as! UINavigationController
            self.rootViewController = takeAtourVC
        }
        else{
            AnalyticsHelper().analyticsEventsAction(category: "Navigation Menu", action: "nav_logout_click", label: "Logout")
            self.hideLeftView(nil)
            showLogoutAlert()
//            let appDel = UIApplication.shared.delegate as! AppDelegate
//            if appDel.isAlertShown! {return}
//            let Logout = NSLocalizedString("Logout", comment: "")
//            let areyousureyouwant = NSLocalizedString("Are you sure you want to log out?", comment: "")
//            let yES = NSLocalizedString("YES", comment: "")
//            let nO = NSLocalizedString("NO", comment: "")
//
//            alertController = UIAlertController(title: Logout, message: areyousureyouwant, preferredStyle: .alert)
//            let okAction = UIAlertAction(title: yES, style: UIAlertActionStyle.default) {
//                UIAlertAction in
//               NotificationCenter.default.post(name: Notification.Name(rawValue: "RemoveAllObservers"), object: nil)
//                NotificationCenter.default.removeObserver(self, name: Notification.Name("SetCenterView"), object: nil)
////                NotificationCenter.default.removeObserver(self)
//                self.alertController = nil
//                appDel.isAlertShown = false
//                appDel.window?.rootViewController = nil
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
//                appDel.window?.rootViewController = initialViewController
//                UserDefaults.standard.set(false, forKey: "isAlreadyLoggedIn")
//
//            }
//            let cancelAction = UIAlertAction(title: nO, style: UIAlertActionStyle.default) {
//                UIAlertAction in
//                self.alertController = nil
//                appDel.isAlertShown = false
//            }
//            alertController.addAction(okAction)
//            alertController.addAction(cancelAction)
//            //            DispatchQueue.main.async {
//            //                let navigationController = UIApplication.shared.windows[0].rootViewController as! UINavigationController
//            //                if !(activeViewCont?.isKind(of: UIAlertController.self))! {
//            appDel.isAlertShown = true
////            DispatchQueue.main.async {
////                self.present(self.alertController, animated: true, completion: nil)
////            }
//            self.present(alertController, animated: true, completion: nil)
        
        }
    }
    
    func showLogoutAlert () {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        let logout = NSLocalizedString("Logout", comment: "")
        let areyousureyouwant = NSLocalizedString("Are you sure you want to log out?", comment: "")
        let yES = NSLocalizedString("YES", comment: "")
        let nO = NSLocalizedString("NO", comment: "")
        
        alertController = UIAlertController(title: logout, message: areyousureyouwant, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: yES, style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            NotificationCenter.default.removeObserver(self, name: Notification.Name("SetCenterView"), object: nil)
            //                NotificationCenter.default.removeObserver(self)
            appDel.window?.rootViewController = nil
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
            appDel.window?.rootViewController = initialViewController
            UserDefaults.standard.set(false, forKey: "isAlreadyLoggedIn")
            self.alertController = nil
        }
        let cancelAction = UIAlertAction(title: nO, style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            self.alertController = nil
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func timer() {
        print(UIApplication.shared.windows[0].rootViewController)
    }
    
    override func leftViewWillLayoutSubviews(with size: CGSize) {
        super.leftViewWillLayoutSubviews(with: size)
        
        if !isLeftViewStatusBarHidden {
            leftView?.frame = CGRect(x: 0.0, y: 20.0, width: size.width, height: size.height - 20.0)
        }
    }
    
    override var isLeftViewStatusBarHidden: Bool {
        get {
            return super.isLeftViewStatusBarHidden
        }
        
        set {
            super.isLeftViewStatusBarHidden = newValue
        }
    }
    
    deinit {
    }
    
}
