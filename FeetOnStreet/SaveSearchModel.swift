//
//  SaveSearchModel.swift
//  FeetOnStreet
//
//  Created by admin on 3/12/18.
//  Copyright © 2018 admin. All rights reserved.
//


import ObjectMapper

class SaveSearchModel: Mappable  {
    
    var access_type:String?
    var assembly_code:Int?
    var bandwidthcode:String?
    var channel_attribute:String?
    var channel_id:Int?
    var countrycode:Int?
    var customer_name:String?
    var email_id:String?
    var explore_request_flg:String?
    var featurecode:String?
    var hubname:String?
    var locationa_buildingname:String?
    var locationa_cityname:String?
    var locationa_countrycode:String?
    var locationa_countryname:String?
    var locationa_housenumber:String?
    var locationa_postcode:String?
    var locationa_streetname:String?
    var locationa_telephonenumber:String?
    var locationb_buildingname:String?
    var locationb_cityname:String?
    var locationb_countrycode:String?
    var locationb_countryname:String?
    var locationb_housenumber:String?
    var locationb_postcode:String?
    var locationb_streetname:String?
    var locationb_telephonenumber:String?
    var networkpointcode:String?
    var pribriQuantities:String?
    var remarks:String?
    var routetype: Int?
    var search_name:String?
    var search_savetime:String?
    var searchid: Int?
    var searchmode_id:Int?
    var servicecode: String?
    var user_searchcount:Int?
    var userid:String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        access_type <- map["access_type"]
        assembly_code <- map["assembly_code"]
        bandwidthcode <- map["bandwidthcode"]
        channel_attribute <- map["channel_attribute"]
        channel_id <- map["channel_id"]
        countrycode <- map["countrycode"]
        customer_name <- map["customer_name"]
        email_id <- map["email_id"]
        explore_request_flg <- map["explore_request_flg"]
        featurecode <- map["featurecode"]
        hubname <- map["hubname"]
        locationa_buildingname <- map["locationa_buildingname"]
        locationa_cityname <- map["locationa_cityname"]
        locationa_countrycode <- map["locationa_countrycode"]
        locationa_countryname <- map["locationa_countryname"]
        locationa_housenumber <- map["locationa_housenumber"]
        locationa_postcode <- map["locationa_postcode"]
        locationa_streetname <- map["locationa_streetname"]
        locationa_telephonenumber <- map["locationa_telephonenumber"]
        locationb_buildingname <- map["locationb_buildingname"]
        locationb_cityname <- map["locationb_cityname"]
        locationb_countrycode <- map["locationb_countrycode"]
        locationb_countryname <- map["locationb_countryname"]
        locationb_housenumber <- map["locationb_housenumber"]
        locationb_postcode <- map["locationb_postcode"]
        locationb_streetname <- map["locationb_streetname"]
        locationb_telephonenumber <- map["locationb_telephonenumber"]
        networkpointcode <- map["networkpointcode"]
        pribriQuantities <- map["pribriQuantities"]
        remarks <- map["remarks"]
        routetype <- map["routetype"]
        search_name <- map["search_name"]
        search_savetime <- map["search_savetime"]
        searchid <- map["searchid"]
        searchmode_id <- map["searchmode_id"]
        servicecode <- map["servicecode"]
        user_searchcount <- map["user_searchcount"]
        userid <- map["userid"]
        
    }
}
class SavedSearchModel: NSObject {
    
    var access_type:String?
    var assembly_code:Int?
    var bandwidthcode:String?
    var channel_attribute:String?
    var channel_id:Int?
    var countrycode:Int?
    var customer_name:String?
    var email_id:String?
    var explore_request_flg:String?
    var featurecode:String?
    var hubname:String?
    var locationa_buildingname:String?
    var locationa_cityname:String?
    var locationa_countrycode:String?
    var locationa_countryname:String?
    var locationa_housenumber:String?
    var locationa_postcode:String?
    var locationa_streetname:String?
    var locationa_telephonenumber:String?
    var locationb_buildingname:String?
    var locationb_cityname:String?
    var locationb_countrycode:String?
    var locationb_countryname:String?
    var locationb_housenumber:String?
    var locationb_postcode:String?
    var locationb_streetname:String?
    var locationb_telephonenumber:String?
    var networkpointcode:String?
    var pribriQuantities:String?
    var remarks:String?
    var routetype: Int?
    var search_name:String?
    var search_savetime:String?
    var searchid: Int?
    var searchmode_id:Int?
    var servicecode: String?
    var user_searchcount:Int?
    var userid:String?
    
    
    class func getSaveSearchResponse(responseArray:Any)-> [SavedSearchModel] {
        
        let responseArr = responseArray as? [[String:Any]]
        
         var locationa_cityname:String?
        var saveSearchModel = [SavedSearchModel]()
        var saveModel:SavedSearchModel?

        for child in responseArr! {
           let tempModel = SavedSearchModel()
            tempModel.access_type = child["access_type"] as? String
            tempModel.assembly_code = child["assembly_code"] as? Int
            tempModel.bandwidthcode = child["bandwidthcode"] as? String
            tempModel.channel_attribute = child["channel_attribute"] as? String
            tempModel.channel_id = child["channel_id"] as? Int
            tempModel.countrycode = child["countrycode"] as? Int
            tempModel.customer_name = child["customer_name"] as? String
            tempModel.email_id = child["email_id"] as? String
            tempModel.explore_request_flg = child["explore_request_flg"] as? String
            tempModel.featurecode = child["featurecode"] as? String
            tempModel.hubname = child["hubname"] as? String
            tempModel.locationa_buildingname = child["locationa_buildingname"] as? String
            tempModel.locationa_cityname = child["locationa_cityname"] as? String
            tempModel.locationa_countrycode = child["locationa_countrycode"] as? String
            tempModel.locationa_countryname = child["locationa_countryname"] as? String
            tempModel.locationa_housenumber = child["locationa_housenumber"] as? String
            tempModel.locationa_postcode = child["locationa_postcode"] as? String
            tempModel.locationa_streetname = child["locationa_streetname"] as? String
            tempModel.locationa_telephonenumber = child["locationa_telephonenumber"] as? String
            tempModel.locationb_buildingname = child["locationb_buildingname"] as? String
            tempModel.locationb_cityname = child["locationb_cityname"] as? String
            tempModel.locationb_countrycode = child["locationb_countrycode"] as? String
            tempModel.locationb_countryname = child["locationb_countryname"] as? String
            tempModel.locationb_housenumber = child["locationb_housenumber"] as? String
            tempModel.locationb_postcode = child["locationb_postcode"] as? String
            tempModel.locationb_streetname = child["locationb_streetname"] as? String
            tempModel.locationb_telephonenumber = child["locationb_telephonenumber"] as? String
            tempModel.networkpointcode = child["networkpointcode"] as? String
            tempModel.pribriQuantities = child["pribriQuantities"] as? String
            tempModel.remarks = child["remarks"] as? String
            tempModel.routetype = child["routetype"] as? Int
            tempModel.search_name = child["search_name"] as? String
            tempModel.search_savetime = child["search_savetime"] as? String
            tempModel.searchid = child["searchid"] as? Int
            tempModel.searchmode_id = child["searchmode_id"] as? Int
            tempModel.servicecode = child["servicecode"] as? String
            tempModel.user_searchcount = child["user_searchcount"] as? Int
            tempModel.userid = child["userid"] as? String
            
           saveSearchModel.append(tempModel)
    }
         return saveSearchModel
  }
}
