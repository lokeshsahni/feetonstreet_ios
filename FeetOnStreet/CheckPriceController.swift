//
//  CheckPriceController.swift
//  FeetOnStreet
//
//  Created by Suresh Murugaiyan on 8/31/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import SwiftOverlays

enum ViewType {
    case BuildingType
    case AddressType
}

class CheckPriceController: UIViewController,UIPageViewControllerDataSource,UIPageViewControllerDelegate {
    
    @IBOutlet weak var view_Btns: UIView!
    @IBOutlet weak var btn_Sla: UIButton!
    @IBOutlet weak var btn_Price: UIButton!
    @IBOutlet weak var btn_Input: UIButton!
    @IBOutlet weak var lbl_location: UILabel!
    @IBOutlet weak var gmailBtn: UIButton!
    
    var inputVC:InputController?
    var priceVC:PriceController?
    var slaVC:SLAController?
    var vcArr:[UIViewController]?
    var pageVC:UIPageViewController?
    var currentVCIndex:Int?
    var locationStr:String?
    var productType:String?
    var oCnValue:String?
    var bandWidthType:String?
    var aEndAddress_id:String?
    var bEndAddress_id:String?
    var bendAddress:String?
    var multiPriceList:[Any]=[]
    
    var isPageControllerAdded:Bool?
    
    var fromOnnetListVc:Bool?
    
    var eMailpopupView:EMailPopUpView?
    var apiCallCount:Int = 0
    let priceArr:NSMutableArray = NSMutableArray()
    let slaListArr:NSMutableArray = NSMutableArray()
    var viewType:ViewType = ViewType.BuildingType
    
    // BackgroundApiCall
    var backgroundTask:UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        isPageControllerAdded=false
        lbl_location.text=locationStr
        //        gmailBtn.isHidden = true
        //        if productType == "Colt Ethernet Line" {
        //            productType = "Colt LANLink Point to Point (Ethernet Point to Point)"
        //        }
        
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.dismissEmailPopupView), name: Notification.Name("Close_Email_Popup"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(eMailSuccess), name: Notification.Name("EMail_Success"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(removeAllObservers), name: Notification.Name("RemoveAllObservers"), object: nil)
        
        // BackgroundApiCall
        NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        
        // BackgroundApiCall
        registerForBackgroundTask()
        
        if self.viewType == ViewType.BuildingType {
            aEndAddress_id = bEndAddress_id
            getPriceAndSlaList()
            //            DispatchQueue.main.async {
            //            self.priceVC?.tbl_Price.reloadData()
            //            }
        }else{
            callPriceApi()
            DispatchQueue.main.async {
                self.priceVC?.tbl_Price.reloadData()
            }
        }
    }
    
    // BackgroundApiCall
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // BackgroundApiCall
    @objc func reinstateBackgroundTask() {
        if backgroundTask == UIBackgroundTaskInvalid {
            registerForBackgroundTask()
        }
    }
    // BackgroundApiCall
    func registerForBackgroundTask() {
        if backgroundTask == UIBackgroundTaskInvalid {
            backgroundTask=UIApplication.shared.beginBackgroundTask(expirationHandler: {
                self.endBackGroundTask()
            })
        }
    }
    
    // BackgroundApiCall
    func endBackGroundTask() {
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask=UIBackgroundTaskInvalid
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
      self.navigationController?.navigationBar.isHidden=true
      AnalyticsHelper().analyticLogScreen(screen: "Price List Screen")
    }
    
    private func callPriceApi() {
        if self.multiPriceList.count != 0 {
            if self.multiPriceList.count > self.apiCallCount  {
                let price = self.multiPriceList[self.apiCallCount] as? OLOModel
                aEndAddress_id = price?.pointID
                //                CommonDataHelper.sharedInstance.npc = aEndAddress_id
                print("aEndAddress_id - ",aEndAddress_id)
                getPriceAndSlaList()
            }else{
                self.priceVC?.priceList = self.priceArr as! [Price]
                self.slaVC?.slaList=(self.slaListArr as? [SLA])!
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden=false
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isPageControllerAdded == false {
            isPageControllerAdded=true
            createPageViewController()
            
            let appDel = UIApplication.shared.delegate as! AppDelegate
            
            if appDel.networkStatus! == NetworkStatus.NotReachable {
                
                DispatchQueue.main.async {
                    let no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
                    UIHelper.sharedInstance.showReachabilityWarning(no_internet_connection, _onViewController: self)
                }
                
            }else{
                //             getPriceAndSlaList()
            }
        }
    }
    
    private func getPriceAndSlaList(){
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            // BackgroundApiCall
            endBackGroundTask()
            DispatchQueue.main.async {
                let no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
                UIHelper.sharedInstance.showReachabilityWarning(no_internet_connection, _onViewController: self)
            }
            
        }else{
            DispatchQueue.main.async {
                let Please_wait = NSLocalizedString("Please wait", comment: "")
                SwiftOverlays.showBlockingWaitOverlayWithText(Please_wait)
//                UIHelper.sharedInstance.showLoadingOnViewController(loadVC: self)
            }
            var err_msg : String?
            let selected_ocn = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "selected_ocn")
            if(selected_ocn != nil) {
                let ocn_parts = selected_ocn?.components(separatedBy: "|BREAK|")
                oCnValue = ocn_parts?.last!
            }
            if oCnValue == nil {
                oCnValue = ""
            }
            var requestBody:String?
            if productType == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
                requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pric=\"http://cp.colt.com/priceservice\"> <soapenv:Header/> <soapenv:Body> <pric:checkPriceRequest> <pric:sequenceID>4495</pric:sequenceID> <pric:userID>FeetOnStreet</pric:userID> <pric:source>FeetOnStreet</pric:source> <pric:requestPrice> <pric:requestID>1</pric:requestID> <pric:ocn>\(oCnValue!)</pric:ocn> <pric:requestType>SERVICE</pric:requestType> <pric:service>\(productType!)</pric:service> <pric:serviceType>HNS</pric:serviceType> <pric:networkPoints><pric:aEndNetworkPoint>\(CommonDataHelper.sharedInstance.npc!)</pric:aEndNetworkPoint><pric:bEndNetworkPoint>\(aEndAddress_id!)</pric:bEndNetworkPoint></pric:networkPoints> <pric:bandwidth>\(bandWidthType!)</pric:bandwidth> <pric:isSLAInfoRequired>true</pric:isSLAInfoRequired> <pric:isPriceLevelGPCNInfoRequired>true</pric:isPriceLevelGPCNInfoRequired> <pric:isSupplierInfoRequired>true</pric:isSupplierInfoRequired> </pric:requestPrice></pric:checkPriceRequest> </soapenv:Body> </soapenv:Envelope>"
                
            }else if productType == "Colt LANLink Point to Point (Ethernet Point to Point)" {
                requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pric=\"http://cp.colt.com/priceservice\"> <soapenv:Header/> <soapenv:Body> <pric:checkPriceRequest> <pric:sequenceID>4495</pric:sequenceID> <pric:userID>FeetOnStreet</pric:userID> <pric:source>FeetOnStreet</pric:source> <pric:requestPrice> <pric:requestID>1</pric:requestID> <pric:ocn>\(oCnValue!)</pric:ocn> <pric:requestType>SERVICE</pric:requestType> <pric:service>\(productType!)</pric:service> <pric:serviceType>P2P</pric:serviceType> <pric:networkPoints><pric:aEndNetworkPoint>\(CommonDataHelper.sharedInstance.npc!)</pric:aEndNetworkPoint><pric:bEndNetworkPoint>\(aEndAddress_id!)</pric:bEndNetworkPoint></pric:networkPoints> <pric:bandwidth>\(bandWidthType!)</pric:bandwidth> <pric:isSLAInfoRequired>true</pric:isSLAInfoRequired> <pric:isPriceLevelGPCNInfoRequired>true</pric:isPriceLevelGPCNInfoRequired> <pric:isSupplierInfoRequired>true</pric:isSupplierInfoRequired> </pric:requestPrice></pric:checkPriceRequest> </soapenv:Body> </soapenv:Envelope>"
                
            }else if productType == "Colt Private Ethernet" || productType == "Colt Wave" {
                //                requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pric=\"http://cp.colt.com/priceservice\"><soapenv:Header /><soapenv:Body><pric:checkPriceRequest><pric:sequenceID>4495</pric:sequenceID><pric:userID>FeetOnStreet</pric:userID><pric:source>FeetOnStreet</pric:source><pric:requestPrice><pric:requestID>1</pric:requestID><pric:ocn>\(oCnValue!)</pric:ocn><pric:requestType>SERVICE</pric:requestType><pric:service>\(productType!)</pric:service><pric:serviceType>P2P</pric:serviceType><pric:networkPoints><pric:aEndNetworkPoint>16447</pric:aEndNetworkPoint><pric:bEndNetworkPoint>3969</pric:bEndNetworkPoint></pric:networkPoints><pric:bandwidth>1Gbps</pric:bandwidth><pric:isSLAInfoRequired>true</pric:isSLAInfoRequired><pric:isPriceLevelGPCNInfoRequired>true</pric:isPriceLevelGPCNInfoRequired><pric:isSupplierInfoRequired>true</pric:isSupplierInfoRequired></pric:requestPrice></pric:checkPriceRequest></soapenv:Body></soapenv:Envelope>"
                
                requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pric=\"http://cp.colt.com/priceservice\"> <soapenv:Header/> <soapenv:Body> <pric:checkPriceRequest> <pric:sequenceID>4495</pric:sequenceID> <pric:userID>FeetOnStreet</pric:userID> <pric:source>FeetOnStreet</pric:source> <pric:requestPrice> <pric:requestID>1</pric:requestID> <pric:ocn>\(oCnValue!)</pric:ocn> <pric:requestType>SERVICE</pric:requestType> <pric:service>\(productType!)</pric:service> <pric:serviceType>P2P</pric:serviceType> <pric:networkPoints><pric:aEndNetworkPoint>\(CommonDataHelper.sharedInstance.npc!)</pric:aEndNetworkPoint><pric:bEndNetworkPoint>\(aEndAddress_id!)</pric:bEndNetworkPoint></pric:networkPoints> <pric:bandwidth>\(bandWidthType!)</pric:bandwidth> <pric:isSLAInfoRequired>true</pric:isSLAInfoRequired> <pric:isPriceLevelGPCNInfoRequired>true</pric:isPriceLevelGPCNInfoRequired> <pric:isSupplierInfoRequired>true</pric:isSupplierInfoRequired> </pric:requestPrice></pric:checkPriceRequest> </soapenv:Body> </soapenv:Envelope>"
                
            }else {
                requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pric=\"http://cp.colt.com/priceservice\"> <soapenv:Header/> <soapenv:Body> <pric:checkPriceRequest> <pric:sequenceID>4495</pric:sequenceID> <pric:userID>FeetOnStreet</pric:userID> <pric:source>FeetOnStreet</pric:source> <pric:requestPrice> <pric:requestID>1</pric:requestID> <pric:ocn>\(oCnValue!)</pric:ocn> <pric:requestType>SERVICE</pric:requestType> <pric:service>\(productType!)</pric:service> <pric:serviceType>SITE</pric:serviceType> <pric:networkPoints><pric:bEndNetworkPoint>\(aEndAddress_id!)</pric:bEndNetworkPoint></pric:networkPoints> <pric:bandwidth>\(bandWidthType!)</pric:bandwidth> <pric:isSLAInfoRequired>true</pric:isSLAInfoRequired> <pric:isPriceLevelGPCNInfoRequired>true</pric:isPriceLevelGPCNInfoRequired> <pric:isSupplierInfoRequired>true</pric:isSupplierInfoRequired> </pric:requestPrice></pric:checkPriceRequest> </soapenv:Body> </soapenv:Envelope>"
            }
            
            var errMsg:String?
            let tokenString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "token")
            let accountTypeString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "accountType")
            let urlString = "http://lonbpm03:8181/checkPrice/priceService"
            let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
            let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/connectnew")!)
            request.httpMethod = "POST"
            let requestData = requestBody?.data(using: String.Encoding.utf8)
            request.httpBody = requestData
            request.addValue("text/xml", forHTTPHeaderField: "Content-Type")
            request.addValue("check_price", forHTTPHeaderField: "SOAPAction")
            request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
            request.addValue(base64String!, forHTTPHeaderField: "X-CONNECT-U")
            request.addValue(tokenString!, forHTTPHeaderField: "X-GATEWAY-A")
            request.addValue(accountTypeString!, forHTTPHeaderField: "X-GATEWAY-F")
            request.timeoutInterval = 600.0 // TimeoutInterval in Second
            
            let session = URLSession.shared
            //        session.configuration.timeoutIntervalForRequest = 600.0
            _ = session.dataTask(with: request as URLRequest) { (data, response, error) in
                
                if error == nil
                {
                    if let data = data, let result = String(data: data, encoding: String.Encoding.utf8)
                    {
                        let xml = SWXMLHash.parse(result)
                        //                                        print(xml)
                        let priceAndSlaList=PriceAndSLA.getPriceAndSlaList(xmlResponseData: xml)
                        var tempPriceCount:String?
                        var tempSlaCount:String?
                        
                        DispatchQueue.main.async {
                            self.priceArr.addObjects(from: priceAndSlaList.priceArray! as [Price])
                            self.slaListArr.addObjects(from: priceAndSlaList.slaArray! as [SLA])
                            
                            if self.priceArr.count>0 {
                                //                        self.gmailBtn.isHidden = false
                                let price = NSLocalizedString("PRICE", comment: "")
                                tempPriceCount="\(price)(\(self.priceArr.count))"
                                tempSlaCount="SLA(\(self.slaListArr.count))"
                                self.gmailBtn.isHidden = false
                                self.priceVC?.noDataAvailableLbl.isHidden = true
                            }else{
                                tempPriceCount="PRICE"
                                tempSlaCount="SLA"
                                self.priceVC?.noDataAvailableLbl.isHidden = false
                            }
                            self.btn_Price.setTitle(tempPriceCount, for: .normal)
                            self.btn_Sla.setTitle(tempSlaCount, for: .normal)
                            self.priceVC?.priceList = self.priceArr as! [Price]
                            self.slaVC?.slaList=self.slaListArr as! [SLA]
                            //                        self.gmailBtn.isHidden = false
                            self.priceVC?.tbl_Price.reloadData()
                            
                            // BackgroundApiCall
                            self.endBackGroundTask()
                            
                            if self.viewType == ViewType.BuildingType {
//                                UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                                SwiftOverlays.removeAllBlockingOverlays()
                            }
                            //                            if priceAndSlaList.priceArray!.count<=0 {
                            //                                self.priceVC?.noDataAvailableLbl.isHidden = false
                            //                            }else{
                            //                                self.priceVC?.noDataAvailableLbl.isHidden = true
                            //                            }
                        }
                        
                        if self.viewType == ViewType.BuildingType {
                            self.priceVC?.priceList = self.priceArr as! [Price]
                            self.slaVC?.slaList=self.slaListArr as! [SLA]
                            DispatchQueue.main.async {
                                self.priceVC?.tbl_Price.reloadData()
                            }
                        }else{
                            self.apiCallCount += 1
                            self.callPriceApi()
                            
                            if self.multiPriceList.count == self.apiCallCount {
                                
                                // BackgroundApiCall
                                self.endBackGroundTask()
                                
//                                UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                                 DispatchQueue.main.async {
                                SwiftOverlays.removeAllBlockingOverlays()
                                }
                            }
                        }
                    }
                    else
                    {
                        // BackgroundApiCall
                        self.endBackGroundTask()
                        print("Failed to get valid response from server.")
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        SwiftOverlays.removeAllBlockingOverlays()
//                        UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                        self.showError("Server error. Please try again.")
                        
                        //                    let alert=UIAlertController(title: "", message: "The network connection was lost", preferredStyle: .alert)
                        //                    let okAction=UIAlertAction(title: "Ok", style: .default, handler: nil)
                        //                    alert.addAction(okAction)
                        //                    self.present(alert, animated: true, completion: nil)
                    }
                    print(error?.localizedDescription ?? "Error")
                }
                }
                .resume()
        }
    }
    private func createPageViewController(){
        
        let storyboard = UIStoryboard.init(name: "Secondary", bundle: nil)
        inputVC = storyboard.instantiateViewController(withIdentifier: "InputController") as? InputController
        priceVC = storyboard.instantiateViewController(withIdentifier: "PriceController") as? PriceController
        slaVC = storyboard.instantiateViewController(withIdentifier: "SLAController") as? SLAController
        vcArr = [inputVC!,priceVC!,slaVC!]
        
        inputVC?.locationStr=locationStr
        inputVC?.productType=productType
        inputVC?.bandWidthType=bandWidthType
        inputVC?.bendAddress = bendAddress
        
        pageVC=UIPageViewController.init(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
        pageVC?.dataSource=self
        pageVC?.delegate=self
        
        pageVC?.setViewControllers([priceVC!], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
        self.addChildViewController(pageVC!)
        let tempVal = view_Btns.frame.origin.y+view_Btns.frame.size.height
        pageVC?.view.frame=CGRect(x: 0, y: tempVal, width: self.view.frame.size.width, height: self.view.frame.size.height-tempVal)
        self.view.addSubview((pageVC?.view)!)
        currentVCIndex=1
        btn_Price.setTitleColor(UIColor.white, for: .normal)
        self.view.bringSubview(toFront: gmailBtn)
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?{
        
        let vCS = pageViewController.viewControllers
        let topVC:UIViewController? = vCS?[0]
        
        var index = vcArr?.index(of: topVC!)
        if index!>=1 && index!<=2 {
            index! -= 1
            let nextVC:UIViewController? = vcArr?[index!]
            return nextVC!
        }
        return nil
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?{
        
        let vCS = pageViewController.viewControllers
        let topVC:UIViewController? = vCS?[0]
        
        
        var index = vcArr?.index(of: topVC!)
        if index!>=0 && index!<2 {
            index! += 1
            let nextVC:UIViewController? = vcArr?[index!]
            return nextVC!
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let tempVC = pageViewController.viewControllers?[0]
        let tempIndex = vcArr?.index(of: tempVC!)
        currentVCIndex=tempIndex
        
        if tempVC is InputController {
            btn_Input.setTitleColor(UIColor.white, for: .normal)
            btn_Price.setTitleColor(UIColor.black, for: .normal)
            btn_Sla.setTitleColor(UIColor.black, for: .normal)
        }else if tempVC is PriceController {
            btn_Input.setTitleColor(UIColor.black, for: .normal)
            btn_Price.setTitleColor(UIColor.white, for: .normal)
            btn_Sla.setTitleColor(UIColor.black, for: .normal)
        }else if tempVC is SLAController {
            btn_Input.setTitleColor(UIColor.black, for: .normal)
            btn_Price.setTitleColor(UIColor.black, for: .normal)
            btn_Sla.setTitleColor(UIColor.white, for: .normal)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func inputAction(_ sender: Any) {
        btn_Input.setTitleColor(UIColor.white, for: .normal)
        btn_Price.setTitleColor(UIColor.black, for: .normal)
        btn_Sla.setTitleColor(UIColor.black, for: .normal)
        if currentVCIndex != 0 {
            pageVC?.setViewControllers([inputVC!], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: nil)
            currentVCIndex=0
        }
    }
    @IBAction func priceAction(_ sender: Any) {
        btn_Input.setTitleColor(UIColor.black, for: .normal)
        btn_Price.setTitleColor(UIColor.white, for: .normal)
        btn_Sla.setTitleColor(UIColor.black, for: .normal)
        if currentVCIndex != 1 {
            if currentVCIndex! < 1 {
                pageVC?.setViewControllers([priceVC!], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
            }else{
                pageVC?.setViewControllers([priceVC!], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: nil)
            }
            currentVCIndex=1
        }
    }
    @IBAction func slaAction(_ sender: Any) {
        btn_Input.setTitleColor(UIColor.black, for: .normal)
        btn_Price.setTitleColor(UIColor.black, for: .normal)
        btn_Sla.setTitleColor(UIColor.white, for: .normal)
        if currentVCIndex != 2 {
            pageVC?.setViewControllers([slaVC!], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
            currentVCIndex=2
        }
    }
    @IBAction func eMailBtnAction(_ sender: Any) {
        
        AnalyticsHelper().analyticsEventsAction(category: "Email", action: "priceList_emailicon_click", label: "Email")
        if (priceVC?.priceList.count)!>0 {
            eMailpopupView = Bundle.main.loadNibNamed("EMailPopUpView", owner: self, options: nil)?[0] as? EMailPopUpView
            eMailpopupView?.priceList = (self.priceVC?.priceList)!
            eMailpopupView?.frame = CGRect(x: 0, y: 20, width: self.view.frame.size.width, height: self.view.frame.size.height-20)
            eMailpopupView?.productType = productType
            eMailpopupView?.a_endAddress = locationStr
            self.view.addSubview(eMailpopupView!)
        }else{
            //            UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Price list not available")
            let priceListNotAvailable = NSLocalizedString("Price list not available", comment: "")
            self.showError(priceListNotAvailable)
        }
        
    }
    
    public func dismissEmailPopupView() {
        DispatchQueue.main.async {
            self.eMailpopupView?.removeFromSuperview()
        }
    }
    public func eMailSuccess() {
        dismissEmailPopupView()
        //     UIHelper.sharedInstance.showError("Email queued for delivery.")
        let EmailQueuedForDelivery = NSLocalizedString("Email queued for delivery", comment: "")
        self.showError(EmailQueuedForDelivery)
    }
    
    func showError(_ msg: String) {
        //        UIHelper.showAlert(message: msg, inViewController: self)
        let alertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let ok = NSLocalizedString("OK", comment: "")
        let defaultAction = UIAlertAction(title: ok, style: .default, handler: nil)
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func removeAllObservers()  {
        NotificationCenter.default.removeObserver(self)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

