//
//  BEndAddressCell.swift
//  FeetOnStreet
//
//  Created by Suresh Murugaiyan on 07/09/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class BEndAddressCell: UITableViewCell {

    @IBOutlet weak var lbl_accessTyp: UILabel!
    @IBOutlet weak var lbl_supplierName: UILabel!
    @IBOutlet weak var btn_Onnet: UIButton!
    @IBOutlet weak var view_selectedeBendAddress: UIView!
    @IBOutlet weak var btn_search: UIButton!
    @IBOutlet weak var btn_Olo: UIButton!
    @IBOutlet weak var btn_selectEfm: UIButton!
    @IBOutlet weak var txtFld_BendAddress: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
