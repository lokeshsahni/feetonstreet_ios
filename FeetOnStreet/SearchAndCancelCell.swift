//
//  SearchAndCancelCell.swift
//  FeetOnStreet
//
//  Created by Suresh Murugaiyan on 08/09/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class SearchAndCancelCell: UITableViewCell {

    @IBOutlet weak var btn_search: UIButton!
    @IBOutlet weak var btn_cancel: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
