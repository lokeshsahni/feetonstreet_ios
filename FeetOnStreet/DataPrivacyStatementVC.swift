//
//  DataPrivacyStatementVC.swift
//  FeetOnStreet
//
//  Created by admin on 8/7/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import SwiftOverlays

class DataPrivacyStatementVC: UIViewController , UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

//        UIHelper.sharedInstance.showLoading()
        let Please_wait = NSLocalizedString("Please wait", comment: "")
        SwiftOverlays.showBlockingWaitOverlayWithText(Please_wait)
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = UIColor(red: 0/255, green: 105/255, blue: 95/255, alpha: 1.0)
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        let lang = LanguagesViewController.getSelectedLanguage()
        var url:URL!
        if lang == "fr" {
          url = Bundle.main.url(forResource: "French_privacy", withExtension:"html")
        }
        else if lang == "de" {
           url = Bundle.main.url(forResource: "German_privacy", withExtension:"html")
        }
        else if lang == "it" {
           url = Bundle.main.url(forResource: "Italian_privacy", withExtension:"html")
        }
        else if lang == "ja" {
          url = Bundle.main.url(forResource: "Japanese_privacy", withExtension:"html")
        }
        else if lang == "pt-PT" {
           url = Bundle.main.url(forResource: "Portugese_privacy", withExtension:"html")
        }
        else if lang == "es-ES" {
            url = Bundle.main.url(forResource: "Spanish_privacy", withExtension:"html")
        }else{
          url = Bundle.main.url(forResource: "privacy", withExtension:"html")
        }
        webView.contentMode = UIViewContentMode.scaleAspectFit
        self.webView.loadRequest(URLRequest(url: url))
//            UIHelper.sharedInstance.hideLoading()
        SwiftOverlays.removeAllBlockingOverlays()
              
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
       AnalyticsHelper().analyticLogScreen(screen: "Data Privacy Statement Screen")
        
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked {
//            UIApplication.shared.openURL(request.url!)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }

            return false
        }
        return true
    }
    
    @IBAction func sideMenuBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
