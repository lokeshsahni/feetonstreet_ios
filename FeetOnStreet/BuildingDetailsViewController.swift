//
//  BuildingDetailsViewController.swift
//  FeetOnStreet
//
//  Created by admin on 8/31/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class BuildingDetailsViewController: UIViewController {
    @IBOutlet weak var buildingIDLbl: UILabel!
    @IBOutlet weak var buildingTypeLbl: UILabel!
    var buildingDetails : NSMutableArray!
    static fileprivate let kTableViewCellReuseIdentifier = "TableViewCellReuseIdentifier"
    @IBOutlet fileprivate weak var tableView: FZAccordionTableView!
    
//    var sectionsArray = ["Customers","Sites","Equipments","Circuites"]
    var sectionsArray:[Any]?
    
    var sites : NSMutableSet = NSMutableSet()
    var customers : NSMutableSet = NSMutableSet()
    var circuits : NSMutableSet = NSMutableSet()
    var equipments : NSMutableSet = NSMutableSet()
    
    var sites_list:[Any]?
    var customers_list:[Any]?
    var circuits_list:[Any]?
    var equipments_list:[Any]?
    
    var bEndAddress_id:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
//        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
//        let statusBarColor = UIColor(red: 0/255, green: 105/255, blue: 95/255, alpha: 1.0)
//        statusBarView.backgroundColor = statusBarColor
//        view.addSubview(statusBarView)
        let customers = NSLocalizedString("Customers", comment: "")
        let sites = NSLocalizedString("Sites", comment: "")
        let equipments = NSLocalizedString("Equipments", comment: "")
        let circuits = NSLocalizedString("Circuites", comment: "")
        
        sectionsArray = ["\(customers)","\(sites)","\(equipments)","\(circuits)"]
        
        
        
        buildingIDLbl.adjustsFontSizeToFitWidth = true
        buildingTypeLbl.adjustsFontSizeToFitWidth = true
        DispatchQueue.main.async {
        UIHelper.sharedInstance.showLoadingOnViewController(loadVC: self)
        }
//        CommonDataHelper.sharedInstance.asset_building_category = ""
        buildingIDLbl.text = ""
        buildingTypeLbl.text = ""
        
        getBuildingDetails()
        
        NotificationCenter.default.addObserver(self, selector: #selector(dataAvilable), name: Notification.Name("reload_table_data_asset"), object: nil)
        tableView.allowMultipleSectionsOpen = true
        tableView.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: BuildingDetailsViewController.kTableViewCellReuseIdentifier)
        tableView.register(UINib(nibName: "AccordionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: AccordionHeaderView.kAccordionHeaderViewReuseIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        AnalyticsHelper().analyticLogScreen(screen: "Building Details screen")
    }
    
    
    func getBuildingDetails() {
        clearBuildingData()
        CommonDataHelper.sharedInstance.npc = bEndAddress_id
        if(CommonDataHelper.sharedInstance.npc != nil){
            let asset_service = AssetService()
            CommonDataHelper.sharedInstance.search_in_progress_asset = true
            asset_service.getAssetDetail(CommonDataHelper.sharedInstance.npc!)
        }
    }
    
    @objc func dataAvilable() {
        
//        print("buildId = ",    CommonDataHelper.sharedInstance.asset_building_id)
        buildingIDLbl.text = CommonDataHelper.sharedInstance.asset_building_id
//        print("build catagory = ",    CommonDataHelper.sharedInstance.asset_building_category)
        buildingTypeLbl.text = CommonDataHelper.sharedInstance.asset_building_category
        print("build sites = ",    CommonDataHelper.sharedInstance.asset_sites)
        sites = CommonDataHelper.sharedInstance.asset_sites
        sites_list = sites.allObjects
        print("build customer = ",    CommonDataHelper.sharedInstance.asset_customer)
        sites_list = sites.allObjects
        customers = CommonDataHelper.sharedInstance.asset_customer
        customers_list = customers.allObjects
        print("build circuit = ",    CommonDataHelper.sharedInstance.asset_circuit)
        circuits = CommonDataHelper.sharedInstance.asset_circuit
        circuits_list = circuits.allObjects
        print("build equipment = ",    CommonDataHelper.sharedInstance.asset_equipment)
        equipments = CommonDataHelper.sharedInstance.asset_equipment
        equipments_list = equipments.allObjects
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
        
        
    }
    func clearBuildingData() {
        CommonDataHelper.sharedInstance.asset_building_id = nil
        CommonDataHelper.sharedInstance.asset_building_category = nil
    }
        
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


// MARK: - Extra Overrides -

extension BuildingDetailsViewController {
    override var prefersStatusBarHidden : Bool {
        return true
    }
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate> -

extension BuildingDetailsViewController : UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsArray!.count;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count:Int!
        
        if section == 0 {
            count = customers_list?.count
        }
        else if section == 1 {
            count = sites_list?.count
        }
        else if section == 2{
            count = equipments_list?.count
        }
        else {
            count = circuits_list?.count
        }
        
        return count
    }
 
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return AccordionHeaderView.kDefaultAccordionHeaderViewHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView(tableView, heightForRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return self.tableView(tableView, heightForHeaderInSection:section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BuildingDetailsViewController.kTableViewCellReuseIdentifier, for: indexPath)
          cell.selectionStyle = .none
        if indexPath.section == 0 {
            cell.textLabel?.numberOfLines = 0
            cell.contentView.backgroundColor = UIColor (colorLiteralRed: 241/255, green: 241/255, blue: 241/255, alpha: 1.0)
            cell.textLabel!.backgroundColor = UIColor.clear
            cell.textLabel!.text = customers_list?[indexPath.row] as? String
        }
        else if indexPath.section == 1 {
            cell.textLabel?.numberOfLines = 0
            cell.contentView.backgroundColor = UIColor (colorLiteralRed: 241/255, green: 241/255, blue: 241/255, alpha: 1.0)
            cell.textLabel!.backgroundColor = UIColor.clear
            cell.textLabel!.text = sites_list?[indexPath.row] as? String
        }
        else if indexPath.section == 2{
            cell.textLabel?.numberOfLines = 0
            cell.contentView.backgroundColor = UIColor (colorLiteralRed: 241/255, green: 241/255, blue: 241/255, alpha: 1.0)
            cell.textLabel!.backgroundColor = UIColor.clear
            cell.textLabel!.text = equipments_list?[indexPath.row] as? String
        }
        else {
            cell.textLabel?.numberOfLines = 0
            cell.contentView.backgroundColor = UIColor (colorLiteralRed: 241/255, green: 241/255, blue: 241/255, alpha: 1.0)
            cell.textLabel!.backgroundColor = UIColor.clear
            cell.textLabel!.text = circuits_list?[indexPath.row] as? String
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: AccordionHeaderView.kAccordionHeaderViewReuseIdentifier) as!AccordionHeaderView
//        let title1 = sectionsArray[section]
        let customers = NSLocalizedString("Customers", comment: "")
        let sites = NSLocalizedString("Sites", comment: "")
        let equipments = NSLocalizedString("Equipments", comment: "")
        let circuits = NSLocalizedString("Circuites", comment: "")
        var title:String?
        if section == 0 {
            if customers_list?.count == 0 {
                title = customers
            }else{
             title = "\(customers)(\(customers_list!.count))"
            }
        }
        else if section == 1 {
            
            if sites_list?.count == 0 {
                title = sites
            }else{
                title = "\(sites)(\(sites_list!.count))"
            }
        }
        else if section == 2 {
            if equipments_list?.count == 0 {
                title = equipments
            }else{
                title = "\(equipments)(\(equipments_list!.count))"
            }
        }
        else {
            if circuits_list?.count == 0 {
                title = circuits
            }else{
                title = "\(circuits)(\(circuits_list!.count))"
            }
        }
      view.configure(value: title!)
        return view
    }
}

// MARK: - <FZAccordionTableViewDelegate> -

extension BuildingDetailsViewController : FZAccordionTableViewDelegate {
    
    func tableView(_ tableView: FZAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}

