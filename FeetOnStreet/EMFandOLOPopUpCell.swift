//
//  EMFandOLOPopUpCell.swift
//  FeetOnStreet
//
//  Created by admin on 9/6/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class EMFandOLOPopUpCell: UITableViewCell {
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var supplierNameLbl: UILabel!
    @IBOutlet weak var accessTypeLbl: UILabel!
    @IBOutlet weak var accessTypeTitleLbl: UILabel!
    @IBOutlet weak var supplierProctTitleLbl: UILabel!
    @IBOutlet weak var supplierProctLbl: UILabel!

    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
