//
//  ONNetListViewController.swift
//  FeetOnStreet
//
//  Created by admin on 8/18/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CoreLocation


enum TableType {
    case ProductType
    case BandwidthType
    case HubListType
}

class ONNetListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,GMSMapViewDelegate,ProductBandwithPopupDelegate  {
    
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var fakeNavigationBar: UIView!
    @IBOutlet weak var eMFandOLOview: UIView!
    @IBOutlet weak var eMFandOLOviewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var oLObtn: UIButton!
    @IBOutlet weak var eMF_DSLbtn: UIButton!
    @IBOutlet weak var addressTxtFld: UITextField!
    @IBOutlet weak var selectedBEndBtn: UIButton!
    @IBOutlet weak var eMF_DSLImgView: UIImageView!
    @IBOutlet weak var oLOimgView: UIImageView!
    var isOLObtnBtnselected = false
    var isEMF_DSLbtnselected = false
    var oLOlist:[Any] = []
    var eMFDSLlist:[Any] = []
    var addressMatchedList:[ActiveAndInactiveBuildings]? = []
    var streetsArray:[Any] = []
    var selectedMarker:ActiveAndInactiveBuildings?
    
    var eMFpopupView:EMFandDSLPopUP?
    
    var abArray:[ActiveAndInactiveBuildings]?
    var activeINactiveBuildngs:[ActiveAndInactiveBuildings]?
    var selectedBendAdderess:ActiveAndInactiveBuildings?
    var permisesNumber:String?
    
    var product_list:[String]?
    var bandWidth_list:[String] = []
    var countries_list:[String] = []
    var product_bandwidth_list:[String] = []
    var selectedProduct:String?
    var selectedBandwidth:String?
    var selectedHub:[String:Any] = [:]
    var productBandWidth_list:[String] = []
    var hub_list:[String] = []
    var hubs_list: NSMutableArray = NSMutableArray()
    var hubList:[Dictionary<String, String>]?
    var aEndAddress_id:String?
    var isHubSelected : Bool = false
    var point_id:String = ""
    var formattedAddress:String = ""
    var selectedPlace:GMSPlace?
    var selectedB_EndAddress:String?
    var selectedPlaceArray:[ActiveAndInactiveBuildings]?
    var dSLmodel:DSLModel?
    var oLOmOdel:OLOModel?
    var filter_places:[Any] = []
    var abiBuildings:[String] = []
    
    var permisesnum:String?
    var streetName:String?
    var cityTown:String?
    var postCode:String?
    var country:String?
    var selectedaddressLatitude:Double?
    var selectedAddressLongitude:Double?
    
    var point_ID:String?
    var englishCityName:String?
    var address1:String?
    
    var isProductBtnselected = true
    
    var tableType : TableType = TableType.ProductType
    var selectMainIndexPath : IndexPath?
    
    static var selectedProductIndex = 0
    static var selectedBandwidthIndex = 0
    
    var productBandwithpopupView:ProductBandwithPopUpView?
    var bend_Address:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
//        backBtn.titleLabel!.adjustsFontSizeToFitWidth = true
        
        if let npc = CommonDataHelper.sharedInstance.npc {
            
            print("Npc = ",npc)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(CloseProductBandwidthPopUp), name: Notification.Name("CloseProductBandwidthPopUp"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GoToBuildingDetailsViewController), name: Notification.Name("GoToBuildingDetailsViewController"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GoToCheckPriceController), name: Notification.Name("GoToCheckPriceController"), object: nil)
        
//        NotificationCenter.default.removeObserver(self, name: Notification.Name("ShowAlert"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showAlert), name: Notification.Name("ShowAlert"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeAllObservers), name: Notification.Name("RemoveAllObservers"), object: nil)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        AnalyticsHelper().analyticLogScreen(screen: "ONNET List Screen")
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count:Int?
        
            count = self.abArray?.count
            
        return count!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let oNNETCell = tableView.dequeueReusableCell(withIdentifier: "ONNETCell") as? ONNETCell
            let aiBuilding = self.abArray?[indexPath.row]
            let permisesNumber =  aiBuilding?.permisesNumber
            let streetName =  aiBuilding?.streetName
            englishCityName =  aiBuilding?.englishCityName
            let postCode =  aiBuilding?.postCode
            point_ID = aiBuilding?.pointID
            let coltOperatingCountry =  aiBuilding?.coltOperatingCountry
            let address1 = "\(permisesNumber!), \(streetName!), \(englishCityName!), \(postCode!), \(coltOperatingCountry!)"
            print(address1)
            oNNETCell?.addressLbl.text = address1
            
            if aiBuilding?.buildingStatus == "ACTIVE" {
//                let aCTIVE = NSLocalizedString("ACTIVE", comment: "")
                oNNETCell?.inActiveBtn.setTitle("ACTIVE", for: .normal)
                oNNETCell?.inActiveBtn.backgroundColor = UIColor (colorLiteralRed: 0/255, green: 130/255, blue: 70/255, alpha: 1.0)
            }else if aiBuilding?.buildingStatus == "Pre approved On Net" {
                oNNETCell?.inActiveBtn .setTitle("Pre approved On Net", for: .normal)
                oNNETCell?.inActiveBtn.backgroundColor = UIColor.purple
                
            }else if aiBuilding?.buildingStatus == "IN PROGRESS" {
                oNNETCell?.inActiveBtn .setTitle("IN PROGRESS", for: .normal)
                oNNETCell?.inActiveBtn.backgroundColor = UIColor (colorLiteralRed: 189/255, green: 122/255, blue: 0/255, alpha: 1.0)
                
            }else{
//                let inActive = NSLocalizedString("INACTIVE", comment: "")
                oNNETCell?.inActiveBtn .setTitle("INACTIVE", for: .normal)
                oNNETCell?.inActiveBtn.backgroundColor = UIColor.red
            }
                     if aiBuilding?.inhouseCablingviaCOLTpossible == true {
                     oNNETCell?.inHouseCablingBtn.isHidden = false
                      }else{
                            oNNETCell?.inHouseCablingBtn.isHidden = true
                        }

            if indexPath == selectMainIndexPath {
                oNNETCell?.bottomView.backgroundColor = UIColor (colorLiteralRed: 0/255, green: 165/255, blue: 155/255, alpha: 1.0)
            }else{
                oNNETCell?.bottomView.backgroundColor = UIColor (colorLiteralRed: 241/255, green: 241/255, blue: 241/255, alpha: 1.0)
            }
            if aiBuilding == selectedMarker {
                oNNETCell?.bottomView.backgroundColor = UIColor (colorLiteralRed: 0/255, green: 165/255, blue: 155/255, alpha: 1.0)
            }
            
            oNNETCell?.locationBtn.addTarget(self, action: #selector(self.mapsButton(button:)), for: .touchUpInside)
            oNNETCell?.locationBtn.tag=indexPath.row
            return oNNETCell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        AnalyticsHelper().analyticsEventsAction(category: "ONNET List", action: "onnetList_item_click", label: "ONNET Item Click")
            self.sideMenuController?.isLeftViewDisabled = true
            tableView.reloadData()
            selectMainIndexPath = indexPath
            
            //Making the legend jump...
            selectMarkerInmapview(sMarker: indexPath.row)
            
            PerformSelectInMainTable()
    }
    
    func PerformSelectInMainTable(){
        
        selectedMarker=nil
        let aiBuilding = self.abArray?[(selectMainIndexPath?.row)!]
        let permisesNumber =  aiBuilding?.permisesNumber
        let streetName =  aiBuilding?.streetName
        let englishCityName =  aiBuilding?.englishCityName
        let postCode =  aiBuilding?.postCode
        let coltOperatingCountry =  aiBuilding?.coltOperatingCountry
        aEndAddress_id = aiBuilding?.pointID
        address1 = "\(permisesNumber!), \(streetName!), \(englishCityName!), \(postCode!), \(coltOperatingCountry!)"
//        print(address1)
        CommonDataHelper.sharedInstance.npc = nil
        
        // To Open Product And Bandwidth PopUp
        OpenProductAndBandwidthPopUp()
        
    }
    
    func OpenProductAndBandwidthPopUp() {
        productBandwithpopupView = Bundle.main.loadNibNamed("ProductBandwithPopUp", owner: self, options: nil)?[0] as? ProductBandwithPopUpView
        let viewHeight = productBandwithpopupView?.frame.size.height
        productBandwithpopupView?.viewHeight = viewHeight
        productBandwithpopupView?.productBandwithpopupViewFrame = productBandwithpopupView?.frame
        productBandwithpopupView?.viewController = self
        productBandwithpopupView?.frame = CGRect(x: 0, y: 20, width: self.view.frame.size.width, height: self.view.frame.size.height-20)
        productBandwithpopupView?.englishCityName = englishCityName
        productBandwithpopupView?.address1 = address1
        productBandwithpopupView?.delegate = self
        productBandwithpopupView?.popupController = self
        self.view.addSubview(productBandwithpopupView!)

   }
   func CloseProductBandwidthPopUp() {
        self.sideMenuController?.isLeftViewDisabled = false
         DispatchQueue.main.async {
        self.productBandwithpopupView?.removeFromSuperview()
        }
     NotificationCenter.default.removeObserver(self, name: Notification.Name("ShowAlert"), object: nil)

    }
    
   func mapsButton(button: UIButton) {
        selectMarkerInmapview( sMarker: button.tag)
        self.CheckAndPopViewController(animated: true)
    }
    
   func selectMarkerInmapview(sMarker:Int) {
    
    AnalyticsHelper().analyticsEventsAction(category: "ONNET List", action: "onnetList_locationIcon_click", label: "Location marker click")
        let vcArray = self.navigationController?.viewControllers
        for vcInContainer in vcArray! {
            if vcInContainer is DashBoardViewController {
                let locationMarker = abArray?[sMarker]
                let vcDashBoard = vcInContainer as! DashBoardViewController
                if(locationMarker != nil){
                    vcDashBoard.selectMarker(locationMarker: locationMarker!)
                }
                break
            }
        }
    }

    func dismissProductBandwithPopup(selectionType:String,productBandwithPopUpView:ProductBandwithPopUpView) {
        if selectionType == "CheckPrice" {
            selectedProduct = productBandwithPopUpView.selectedProduct
            selectedBandwidth = productBandwithPopUpView.selectedBandwidth
//            aEndAddress_id = productBandwithPopUpView.aEndAddress_id
            bend_Address = productBandwithPopUpView.selectedBEndBtn.currentTitle
            selectedPlace = productBandwithPopUpView.selectedPlace
            selectedB_EndAddress = productBandwithPopUpView.selectedB_EndAddress
            GoToCheckPriceController()
        }
    }
    
    func GoToCheckPriceController() {
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            DispatchQueue.main.async {
                let no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
                UIHelper.sharedInstance.showReachabilityWarning(no_internet_connection, _onViewController: self)
            }
            
        }else{
        selectedProduct = CommonDataHelper.sharedInstance.selectedProduct
        selectedBandwidth = CommonDataHelper.sharedInstance.selectedBandwidth
        
            if selectedProduct == "Colt LANLink Point to Point (Ethernet Point to Point)" || selectedProduct == "Colt Wave" || selectedProduct == "Colt Private Ethernet"  {
                if selectedPlace == nil {
//                    self.showError("Please Search B-End Address")
                    let please_search_BEndaddress = NSLocalizedString("Please search B-End address", comment: "")
                    UIHelper.showAlert(message: please_search_BEndaddress, inViewController: self)
                }
                else if selectedB_EndAddress == nil {
                    if selectedProduct == "Colt Private Ethernet" {
//                         self.showError("The Colt Ethernet Private service is only available within the same metro city.  You're A-B address combination is not a Metro service.  Please consider our Ethernet Line service.")
                        UIHelper.showAlert(message: "The Colt Ethernet Private service is only available within the same metro city.  You're A-B address combination is not a Metro service.  Please consider our Ethernet Line service.", inViewController: self)
                    }else{
//                    self.showError("Please select B-End Address")
                        let PleaseSelectBendAddress = NSLocalizedString("Please select B-End address", comment: "")
                        UIHelper.showAlert(message:PleaseSelectBendAddress, inViewController: self)
                    }
                }else{
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Secondary", bundle:nil)
                    let checkPriceVC = storyBoard.instantiateViewController(withIdentifier: "CheckPriceController") as! CheckPriceController
                    checkPriceVC.locationStr = address1
                    checkPriceVC.productType = selectedProduct
                    checkPriceVC.bandWidthType = selectedBandwidth
                    checkPriceVC.bEndAddress_id = aEndAddress_id
                    checkPriceVC.bendAddress = bend_Address
                    CloseProductBandwidthPopUp()
                    self.CheckAndPushViewController(checkPriceVC, animated: true)
                }
            }else if selectedProduct == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
                if CommonDataHelper.sharedInstance.npc == nil {
//                    self.showError("Please select Hub")
                    let please_select_Hub = NSLocalizedString("Please_select_hub", comment: "")
                    UIHelper.showAlert(message: please_select_Hub, inViewController: self)
                }else{
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Secondary", bundle:nil)
                    let checkPriceVC = storyBoard.instantiateViewController(withIdentifier: "CheckPriceController") as! CheckPriceController
                    checkPriceVC.locationStr = address1
                    checkPriceVC.productType = selectedProduct
                    checkPriceVC.bandWidthType = selectedBandwidth
                    checkPriceVC.bEndAddress_id = aEndAddress_id
                    CloseProductBandwidthPopUp()
                    self.CheckAndPushViewController(checkPriceVC, animated: true)
                }
            }
            else{
                let storyBoard : UIStoryboard = UIStoryboard(name: "Secondary", bundle:nil)
                let checkPriceVC = storyBoard.instantiateViewController(withIdentifier: "CheckPriceController") as! CheckPriceController
                checkPriceVC.locationStr = address1
                checkPriceVC.productType = selectedProduct
                checkPriceVC.bandWidthType = selectedBandwidth
                checkPriceVC.bEndAddress_id = aEndAddress_id
                CloseProductBandwidthPopUp()
                self.CheckAndPushViewController(checkPriceVC, animated: true)
            }
        }
    }
    
    func GoToBuildingDetailsViewController() {
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            DispatchQueue.main.async {
                let no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
                UIHelper.sharedInstance.showReachabilityWarning(no_internet_connection, _onViewController: self)
            }
            
        }else{
        
        CloseProductBandwidthPopUp()
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let buildingDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "BuildingDetailsViewController") as! BuildingDetailsViewController
            buildingDetailsViewController.bEndAddress_id = aEndAddress_id
        self.navigationController?.pushViewController(buildingDetailsViewController, animated: true)
            
        }
    }
    
    func ShowLoading_internal(){
        //Showing up the LoadingView.....
        DispatchQueue.main.async {
            //UIHelper.sharedInstance.isLoading = true
            UIHelper.sharedInstance.showLoadingOnViewController(loadVC: self)
        }
    }
    
    func HideLoading_internal(){
        UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
    }
    
    func clearEfmDslOloAndBuilding() {
        self.activeINactiveBuildngs = nil
        CommonDataHelper.sharedInstance.eMF_DSL_results = NSMutableArray()
        CommonDataHelper.sharedInstance.oLO_results = NSMutableArray()
        eMFDSLlist = []
        oLOlist = []
    }
    
    @objc func dataAvilable() {
        
        eMFDSLlist = CommonDataHelper.sharedInstance.eMF_DSL_results as! [Any]
        print(eMFDSLlist)
//      UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
    }
    @objc func oLOdataAvilable() {
        
        oLOlist = CommonDataHelper.sharedInstance.oLO_results as! [Any]
//      UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
//      UIHelper.sharedInstance.isLoading = false
    }
   
    @IBAction func backBtnAction(_ sender: Any) {
        self.CheckAndPopViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    //TODO: Remove this flow....
    
    var isSilent = false;
    var unloadClosure = { () in  return}
    var dboardNavigationController: UINavigationController? = nil

    public func prepareForActualShowUp(){

        selectMainIndexPath = nil

        if(isSilent){
//            closePopUp()
        }
    }
    
    public func showUpSilently(_ dboardNavContr:UINavigationController, onUnload:@escaping( ()->Void)){
        isSilent = true;
        unloadClosure = onUnload
        dboardNavigationController = dboardNavContr
        if #available(iOS 9.0, *) {
            self.loadViewIfNeeded()
        } else {
            // Fallback on earlier versions
        }
        self.mainTableView.isHidden = true
        self.fakeNavigationBar.isHidden = true

        let rowToSelect = self.abArray!.index { (a:ActiveAndInactiveBuildings) -> Bool in
            return (a==selectedMarker)
        }

        if(rowToSelect != nil){
            selectMainIndexPath = IndexPath(row: rowToSelect!, section: 0)
            PerformSelectInMainTable()
        }
    }

    public func unloadSilentMode(){

        isSilent = false
        self.mainTableView.isHidden = false
        self.fakeNavigationBar.isHidden = false
        self.dboardNavigationController = nil

        unloadClosure()
        self.unloadClosure = {() in return}
    }
    
    public func CheckAndPushViewController(_ viewController: UIViewController, animated: Bool){
        if(isSilent){
            self.dboardNavigationController?.pushViewController(viewController, animated: animated)
        }
        self.navigationController?.pushViewController(viewController, animated: animated)
    }

    public func CheckAndPopViewController(animated: Bool){
        if(isSilent){
            self.dboardNavigationController?.popViewController(animated: animated)
        }
        self.navigationController?.popViewController(animated: animated)
    }
    func showAlert(notification: NSNotification) {
        if notification.object != nil{
        let alertMsg = notification.object as? String
            UIHelper.showAlert(message: alertMsg, inViewController: self)
//        let alertController = UIAlertController(title: "", message: alertMsg, preferredStyle: .alert)
//        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//        alertController.addAction(defaultAction)
//        self.present(alertController, animated: true, completion: nil)
      }
    }
    
//    func showError(_ msg: String) {
//        UIHelper.showAlert(message: msg, inViewController: self)
//        let alertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
//        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//        alertController.addAction(defaultAction)
//        self.present(alertController, animated: true, completion: nil)
//    }
    
    func removeAllObservers()  {
        NotificationCenter.default.removeObserver(self)
    }
}
