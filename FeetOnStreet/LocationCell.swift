//
//  LocationCell.swift
//  FeetOnStreet
//
//  Created by Suresh Murugaiyan on 08/09/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {

    @IBOutlet weak var lbl_longitude: UILabel!
    @IBOutlet weak var lbl_latitude: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
