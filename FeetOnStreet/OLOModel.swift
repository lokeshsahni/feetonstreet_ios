//
//  OLOModel.swift
//  FeetOnStreet
//
//  Created by admin on 9/5/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class OLOModel: NSObject {
    
    var coltOperatingCountry:String?
    var cityName:String?
    var postCode:String?
    var premisesNumber:String?
    var streetName:String?
    var latitude:Double?
    var longitude:Double?
    var coltProduct:String?
    var coltBandwidth:String?
    var supplierName:String?
    var supplierProduct:String?
    var pointID:String?
    var accesType:String?
    
    
    class func getOLOresponse(xmlResponseData:XMLIndexer) -> [OLOModel] {
        
        var oLOModel = [OLOModel]()
        
        let tempArray = xmlResponseData["SOAP-ENV:Envelope"]["SOAP-ENV:Body"]["ns2:checkConnectivityResponse"]["ns2:localSiteAddress"]["ns2:oloOptionsStatus"]["ns2:oloOptionAEndResult"]
        
        var oLOModelList:OLOModel?
        
        for child1 in tempArray.children {
            
            oLOModelList=OLOModel()
            if child1.element?.name == "ns2:supplierOffnetOption" {
                for child in child1.children {
                    if child.element?.name == "ns2:coltOperatingCountry" {
                        oLOModelList?.coltOperatingCountry=child.element?.text
                    }
                    if child.element?.name == "ns2:cityName" {
                        oLOModelList?.cityName=child.element?.text
                    }
                    if child.element?.name == "ns2:postCode" {
                        oLOModelList?.postCode=child.element?.text
                    }
                    if child.element?.name == "ns2:premisesNumber" {
                        oLOModelList?.premisesNumber=child.element?.text
                    }
                    if child.element?.name == "ns2:streetName" {
                        oLOModelList?.streetName=child.element?.text
                    }
                    if child.element?.name == "ns2:latitude" {
                        oLOModelList?.latitude=Double((child.element?.text)!)
                    }
                    if child.element?.name == "ns2:longitude" {
                        oLOModelList?.longitude=Double((child.element?.text)!)
                    }
                    if child.element?.name == "ns2:coltProduct" {
                        oLOModelList?.coltProduct=child.element?.text
                    }
                    if child.element?.name == "ns2:bandwidth" {
                        oLOModelList?.coltBandwidth=child.element?.text
                    }
//                    if child.element?.name == "ns2:bandwidthName" {
//                        oLOModelList?.coltBandwidth=child.element?.text
//                    }
                    if child.element?.name == "ns2:supplierName" {
                        oLOModelList?.supplierName=child.element?.text
                    }
//                    if child.element?.name == "ns2:supplierName" {
//                        oLOModelList?.supplierName=child.element?.text
//                    }
                    if child.element?.name == "ns2:pointID" {
                        oLOModelList?.pointID=child.element?.text
                    }
                    if child.element?.name == "ns2:supplierProduct" {
                        oLOModelList?.supplierProduct=child.element?.text
                    }
                    
                }
                oLOModel.append((oLOModelList)!)
            }            
            
        }
        return oLOModel
    }
    
    
}
