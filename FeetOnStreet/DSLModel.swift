//
//  DSLModel.swift
//  FeetOnStreet
//
//  Created by admin on 9/5/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class DSLModel: NSObject {

    var sequenceId:String?
    var bandwidthName:String?
    var accessType:String?
    var coltOperatingCountry:String?
    var supplierName:String?
    var coltProduct:String?
    var pointID:String?
    var postCode:String?
    var streetName:String?
    var cityName:String?
   
    
    class func getEMF_DSLResponse(xmlResponseData:XMLIndexer) -> [DSLModel] {
        
        var eMFDsl = [DSLModel]()
        
        let tempArray = xmlResponseData["SOAP-ENV:Envelope"]["SOAP-ENV:Body"]["ns2:checkConnectivityResponse"]["ns2:localSiteAddress"]["ns2:offNetOptionStatus"]["ns2:offNetOptionAEndResult"]
        
        var tempEMFDsl:DSLModel?
        
        for child1 in tempArray.children {
            
//            if child.element?.name == "ns2:sequenceId" {
//                tempBuilding=DSLModel()
//                tempBuilding?.sequenceId=child.element?.text
//            }
            tempEMFDsl=DSLModel()
            if child1.element?.name == "ns2:offNetOptionResult" {
                for child in child1.children {
                    if child.element?.name == "ns2:bandwidth" {
                        tempEMFDsl?.bandwidthName=child.element?.text
                    }
                    if child.element?.name == "ns2:accessType" {
                        tempEMFDsl?.accessType=child.element?.text
                    }
                    if child.element?.name == "ns2:coltOperatingCountry" {
                        tempEMFDsl?.coltOperatingCountry=child.element?.text
                    }
                    if child.element?.name == "ns2:supplierName" {
                        tempEMFDsl?.supplierName=child.element?.text
                    }
                    if child.element?.name == "ns2:coltProduct" {
                        tempEMFDsl?.coltProduct=child.element?.text
                    }
                    if child.element?.name == "ns2:pointID" {
                        tempEMFDsl?.pointID=child.element?.text
                    }
                    if child.element?.name == "ns2:postCode" {
                        tempEMFDsl?.postCode=child.element?.text
                    }
                    if child.element?.name == "ns2:streetName" {
                        tempEMFDsl?.streetName=child.element?.text
                    }
                    if child.element?.name == "ns2:cityName" {
                        tempEMFDsl?.cityName=child.element?.text
                    }
                }
                eMFDsl.append((tempEMFDsl)!)
            }            
        }
        return eMFDsl
    }


}
