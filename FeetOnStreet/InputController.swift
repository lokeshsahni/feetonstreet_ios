//
//  InputController.swift
//  FeetOnStreet
//
//  Created by Suresh Murugaiyan on 9/1/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import SwiftOverlays

class InputController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate  {
    
    @IBOutlet weak var tbl_Input: UITableView!
    @IBOutlet weak var popUpBackgroundBtn: UIButton!
    @IBOutlet weak var saveSearchPopUp: UIView!
    @IBOutlet weak var saveSearchTxtFld: UITextField!
    
    
    var locationStr:String?
    var productType:String?
    var bandWidthType:String?
    var bendAddress:String?
    var err_msg : String?
    
    var serverErrorPleaseTryAgain = ""
    
    // BackgroundApiCall
    var backgroundTask:UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tbl_Input.estimatedRowHeight=74.0
        tbl_Input.rowHeight=UITableViewAutomaticDimension
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(InputController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        // BackgroundApiCall
        NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        
        // BackgroundApiCall
        registerForBackgroundTask()
    }
    override func viewWillAppear(_ animated: Bool) {
       serverErrorPleaseTryAgain  = NSLocalizedString("Server error. Please try again.", comment: "")
    }
    
    // BackgroundApiCall
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // BackgroundApiCall
    @objc func reinstateBackgroundTask() {
        if backgroundTask == UIBackgroundTaskInvalid {
            registerForBackgroundTask()
        }
    }
    // BackgroundApiCall
    func registerForBackgroundTask() {
        if backgroundTask == UIBackgroundTaskInvalid {
            backgroundTask=UIApplication.shared.beginBackgroundTask(expirationHandler: {
                self.endBackGroundTask()
            })
        }
    }
    
    // BackgroundApiCall
    func endBackGroundTask() {
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask=UIBackgroundTaskInvalid
    }

    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier)
        if cell==nil {
            cell=UITableViewCell(style: .default, reuseIdentifier: identifier)
        }
        let lbl_Header = cell?.contentView.viewWithTag(1) as! UILabel
        let lbl_Info = cell?.contentView.viewWithTag(2) as! UILabel
        if indexPath.row==0 {
            let selectedProduct = NSLocalizedString("Selected Product", comment: "")
            lbl_Header.text=selectedProduct
            // changing product names for user
            setProductNames()
            lbl_Info.text=productType
        }else if indexPath.row==1 {
            let selectedBandwidth = NSLocalizedString("Selected bandwidth", comment: "")
            lbl_Header.text=selectedBandwidth
            lbl_Info.text=bandWidthType
        }else if indexPath.row==2 {
            let selectedAEndAddress = NSLocalizedString("Selected A-End address", comment: "")
            lbl_Header.text=selectedAEndAddress
            lbl_Info.text=locationStr
        }else if indexPath.row==3 {
            let selectedBEndAddress = NSLocalizedString("Selected B-End address", comment: "")
            lbl_Header.text=selectedBEndAddress
            if bendAddress == nil {
                let notApplicable = NSLocalizedString("Not applicable", comment: "")
                lbl_Info.text=notApplicable
            }else{
                lbl_Info.text=bendAddress
            }
        }
        return cell!
    }
    func setProductNames() {
        if productType == "Colt VoIP Access" {
            productType = "Colt SIP Trunking"
        }else if productType == "Colt LANLink Point to Point (Ethernet Point to Point)" {
            productType = "Colt Ethernet Line"
        }else if productType == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
            productType = "Colt Ethernet Hub and Spoke"
        }else if productType == "Colt Ethernet Private Network (EPN)"{
            productType = "ColT Ethernet VPN"
        }
    }
    
    @IBAction func saveSearchedAddressBtnAction(_ sender: Any) {
        popUpBackgroundBtn.isHidden = false
        saveSearchPopUp.isHidden = false
    }
    @IBAction func saveBtnAction(_ sender: Any) {
        view.endEditing(true)
        let tempUsrName = saveSearchTxtFld.text
        let trimmedtempUsrName=tempUsrName?.replacingOccurrences(of: " ", with: "")
        if (trimmedtempUsrName?.isEmpty)! {
            saveSearchTxtFld.clearsOnBeginEditing = true
            let saveSearchNameIsRequired = NSLocalizedString("Save search name is required.", comment: "")
            UIHelper.sharedInstance.showError(saveSearchNameIsRequired)
            return
        }
        callSaveSearchAPI(userName: tempUsrName!)
        self.closeSaveSearchPopUp()
    }
    @IBAction func cancelBtnAction(_ sender: Any) {
        popUpBackgroundBtn.isHidden = true
        closeSaveSearchPopUp()
    }
    
    func callSaveSearchAPI(userName:String) {
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
            // BackgroundApiCall
               endBackGroundTask()
            DispatchQueue.main.async {
                let no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
                UIHelper.sharedInstance.showReachabilityWarning(no_internet_connection, _onViewController: self)
            }
            
        }else{
        DispatchQueue.main.async {
//        UIHelper.sharedInstance.showLoadingOnViewController(loadVC: self)
            let please_wait = NSLocalizedString("Please wait", comment: "")
             SwiftOverlays.showBlockingWaitOverlayWithText(please_wait)
            }
            
            if CommonDataHelper.sharedInstance.saveSearchPermisesNumber == nil{
                CommonDataHelper.sharedInstance.saveSearchPermisesNumber = ""
            }
            if CommonDataHelper.sharedInstance.saveSearchStreetName == nil{
                CommonDataHelper.sharedInstance.saveSearchStreetName = ""
            }
            if CommonDataHelper.sharedInstance.saveSearchCityName == nil{
                CommonDataHelper.sharedInstance.saveSearchCityName = ""
            }
            if CommonDataHelper.sharedInstance.saveSearchColtOperatingCountry == nil{
                CommonDataHelper.sharedInstance.saveSearchColtOperatingCountry = ""
            }
            if CommonDataHelper.sharedInstance.saveSearchPostCode == nil{
                CommonDataHelper.sharedInstance.saveSearchPostCode = ""
            }
            if CommonDataHelper.sharedInstance.selectedProductCode == nil{
                CommonDataHelper.sharedInstance.selectedProductCode = ""
            }
            let remarksText = "\(CommonDataHelper.sharedInstance.saveSearchPermisesNumber!) \(CommonDataHelper.sharedInstance.saveSearchStreetName!) \(CommonDataHelper.sharedInstance.saveSearchCityName!) \(CommonDataHelper.sharedInstance.saveSearchColtOperatingCountry!)"
            
            
        let dict = ["locationa_buildingname": "",
                    "locationa_cityname": CommonDataHelper.sharedInstance.saveSearchCityName!,
                    "locationa_countryname": CommonDataHelper.sharedInstance.saveSearchColtOperatingCountry!,
                    "locationa_housenumber": CommonDataHelper.sharedInstance.saveSearchPermisesNumber!,
                    "locationa_streetname": CommonDataHelper.sharedInstance.saveSearchStreetName!,
                    "locationa_postcode": CommonDataHelper.sharedInstance.saveSearchPostCode!,
                    "locationa_telephonenumber": "",
                    "locationb_buildingname": "",
                    "locationb_cityname": "",
                    "locationb_countryname": "",
                    "locationb_housenumber": "",
                    "locationb_postcode": "",
                    "locationb_streetname": "",
                    "locationb_telephonenumber": "",
                    "bandwidthcode": CommonDataHelper.sharedInstance.selectedBandwidth!,
                    "access_type": "",
                    "customer_name": "COLT TEST CUSTOMER",
                    "search_name": userName,
                    "userid": CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "username")!,
                    "servicecode": CommonDataHelper.sharedInstance.selectedProductCode!,
                    "assembly_code": 0,
                    "channel_id": 0,
                    "countrycode": 0,
                    "email_id": "",
                    "explore_request_flg": "",
                    "group_id": 0,
                    "remarks": remarksText,
                    "routetype": 0,
                    "search_savetime": "",
                    "searchid": 0,
                    "searchmode_id": 0,
                    "user_searchcount": 0] as [String: Any]
        
        if let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: []) {
            
//            let url = NSURL(string: "http://172.30.241.174:4002/saveSearchCriteria")!
            let url = NSURL(string: "https://rfsdcp.colt.net/aptApi/saveSearchCriteria")!

            //            let url = NSURL(string: "https://rfsdcp.colt.net/aptApi/saveSearchCriteria")!
//            let url = URL(string:(urlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed))!)
            let request = NSMutableURLRequest(url: url as URL)
            request.httpMethod = "POST"
            request.httpBody = jsonData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")  // the request is JSON
            request.setValue("Basic YXB0Y2xpZW50OmFwdDEyJDU2", forHTTPHeaderField: "Authorization")  // the expected response is also JSON
            request.timeoutInterval = 600
//            request.timeoutInterval=TimeInterval(MAXFLOAT)
            
            let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
                if error != nil{
                    print(error?.localizedDescription)
                    // BackgroundApiCall
                    self.endBackGroundTask()
                    DispatchQueue.main.async(execute: {
//                        UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                        SwiftOverlays.removeAllBlockingOverlays()
                    })
                    self.showAlert(self.serverErrorPleaseTryAgain, messageValue: "")
                    return
                }
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                    
                    if let parseJSON = json {
                        var srtatusValue:String = parseJSON["status"] as! String
                        var messageValue:String = parseJSON["message"] as! String
                        print("result: \(srtatusValue)")
                        print(parseJSON)
                        // BackgroundApiCall
                        self.endBackGroundTask()
                        
                        if srtatusValue == "SUCCESS"{
                           let savedSearchedCriteriaSuccessfully  = NSLocalizedString("Saved Searched Criteria Successfully.", comment: "")
                            srtatusValue = savedSearchedCriteriaSuccessfully
                            messageValue = ""
                        }
                      
                        DispatchQueue.main.async(execute: {
//                            UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                            SwiftOverlays.removeAllBlockingOverlays()
                        })
                        
                        self.showAlert(srtatusValue, messageValue: "")
                        
                    }
                } catch let error as NSError {
                    print(error)
                    // BackgroundApiCall
                    self.endBackGroundTask()
                    DispatchQueue.main.async(execute: {
//                        UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                        SwiftOverlays.removeAllBlockingOverlays()
                    })
                    self.showAlert(self.serverErrorPleaseTryAgain, messageValue: "")
                }
            }
            task.resume()
        }
      }
    }
    
    func closeSaveSearchPopUp() {
        popUpBackgroundBtn.isHidden = true
        saveSearchPopUp.isHidden = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let maxLength = 50
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return false to ignore.
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    func showAlert(_ srtatusValue: String,messageValue: String ) {
        
        let parentViewController: UIViewController = UIApplication.shared.windows[1].rootViewController!
        let alert = UIAlertController(title: messageValue, message:srtatusValue , preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(action: UIAlertAction!) in
        }))
        DispatchQueue.main.async() {
            parentViewController.present(alert, animated: true, completion: nil)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

