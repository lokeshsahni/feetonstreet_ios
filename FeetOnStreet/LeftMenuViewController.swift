//
//  LeftMenuViewController.swift
//  FeetOnStreet
//
//  Created by admin on 7/28/17.
//  Copyright © 2017 admin. All rights reserved.
//
import UIKit

class LeftMenuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var mainTableView : UITableView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var versionLbl: UILabel!

    var menuListArray:[Any]?
    var menuIconsArray = ["ic_map_view","ic_saved_serach","ic_account_settings","ic_Language","ic_data_privacy","ic_terms_of_use1x","ic_rtd","ic_tour","ic_logout"]
    var isRDTAvailable = false

    override func viewDidLoad() {
        super.viewDidLoad()
        let connectivityChecker = NSLocalizedString("Connectivity Checker", comment: "")
        let savedSearch = NSLocalizedString("Saved Search", comment: "")
        let accountSettings = NSLocalizedString("Account Settings", comment: "")
        let languageSettings = NSLocalizedString("Language Settings", comment: "")
        let dataPrivacyStatement = NSLocalizedString("Data Privacy Statement", comment: "")
        let termsofUse = NSLocalizedString("Terms of Use", comment: "")
        let rTD = NSLocalizedString("RTD", comment: "")
        let takeAtour = "Take a Tour"
        let logout = NSLocalizedString("Logout", comment: "")
        
        
        menuListArray = ["\(connectivityChecker)","\(savedSearch)","\(accountSettings)","\(languageSettings)","\(dataPrivacyStatement)","\(termsofUse)","\(rTD)","\(takeAtour)","\(logout)"]
        var err_msg : String?
        let tempVal1 =  CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "isRTDAvailable")
        let accountType =  CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "accountType")

        if tempVal1 == "1" || accountType == "WINDOWS" {
//            let tempVal2 =  CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "employeeType")
//            if tempVal2 == "Colt Employee" {
               isRDTAvailable = true
//            }
        }

//        isRDTAvailable = false // for test

        if !isRDTAvailable {
            menuListArray?.remove(at: 6)
            menuIconsArray.remove(at: 6)
        }

        self.mainTableView.estimatedRowHeight = 100
        self.mainTableView.rowHeight = UITableViewAutomaticDimension
        self.mainTableView.tableFooterView = UIView()
        
        self.mainTableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        self.mainTableView.separatorColor = UIColor.c_DarkTeal
        self.mainTableView.separatorInset = UIEdgeInsets.zero
        let version = NSLocalizedString("Version", comment: "")
        let date = NSLocalizedString("Date", comment: "")
        let appVersionObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject
        versionLbl.text = "\(version): \(String(describing: appVersionObject!))"
//        versionLbl.text = "\(version): 2.1.2"
        let curtrentdate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let todayDate = formatter.string(from: curtrentdate)
        dateLbl.text = "\(date): \(todayDate)"
//        dateLbl.text = "\(date): 06/09/2018"
        
        let indexPath = IndexPath(row: 0, section: 0)
        mainTableView.selectRow(at: indexPath, animated: false, scrollPosition: .top)

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        AnalyticsHelper().analyticLogScreen(screen: "Navigation Menu Screen")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return menuListArray!.count;
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell", for: indexPath) as! SideMenuTableViewCell
            if indexPath.row == 6 {
                if isRDTAvailable {
                  cell.iQNET_Lbl.text = "IQNet"
                }
            }
            else{
                cell.iQNET_Lbl.text = ""
                
        }
            cell.titleLbl.text = menuListArray?[indexPath.row] as? String
            let menuIcons = menuIconsArray[indexPath.row]
            cell.iconImgView.image = UIImage(named: menuIcons)
            
            let bgColorView = UIView()
            bgColorView.backgroundColor = UIColor.c_DarkTeal
            cell.selectedBackgroundView = bgColorView
            
            return cell
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

            var indexRow = indexPath.row
            if !isRDTAvailable {
                if indexPath.row > 5 {
                    indexRow = indexRow+1
                }
            }
            NotificationCenter.default.post(name: Notification.Name("SetCenterView"), object: indexRow, userInfo: nil)
//            self.hideLeftView(nil)
        }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
