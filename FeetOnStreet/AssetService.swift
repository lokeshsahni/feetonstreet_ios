//
//  AssetService.swift
//  FeetOnStreet
//
//  Created by admin on 7/28/17.
//  Copyright © 2017 admin. All rights reserved.
//
import Foundation


class AssetService : NSObject {
    
    let empty_string = ""

    func getAssetDetail(_ npc:String) {
            
        CommonDataHelper.sharedInstance.asset_sites = NSMutableSet()
        CommonDataHelper.sharedInstance.asset_customer = NSMutableSet()
        CommonDataHelper.sharedInstance.asset_circuit = NSMutableSet()
        CommonDataHelper.sharedInstance.asset_equipment = NSMutableSet()
        CommonDataHelper.sharedInstance.asset_building_id = nil
        CommonDataHelper.sharedInstance.asset_building_status = nil

        
        var err_msg : String?
        
        let token = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "token")
        
        if(token != nil)
        {
                let authFrom = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "accountType")
                var soapBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dgat=\"http://dgateway2.web.colt.net\"> <soapenv:Header/> <soapenv:Body> <dgat:getAssetsDetails> <dgat:npc>"
                soapBody += npc + "</dgat:npc> <dgat:token>"
                soapBody += token! + "</dgat:token> <dgat:authFrom>"
                soapBody += authFrom! + "</dgat:authFrom> </dgat:getAssetsDetails> </soapenv:Body> </soapenv:Envelope>"
            
                WebHelper.sharedInstance.callDirectService("https://dcp.colt.net/dgateway/services/AssetService", body: soapBody, soapAction: "", completionHandler: { (err_msg, resp) in
                
                    if(err_msg != nil)
                    {
                        DispatchQueue.main.async(execute: {
                            UIHelper.sharedInstance.hideLoading()
                            UIHelper.sharedInstance.showError(err_msg!)
                        })
                    }
                    
                    if(resp != nil)
                    {                        
                        let xml = SWXMLHash.parse(resp!)
                        
                        let asset_list = xml["soapenv:Envelope"]["soapenv:Body"]["getAssetsDetailsResponse"]
                        
                        for entry in asset_list.children
                        {
                            if(entry.element!.name == "getAssetsDetailsReturn")
                            {
                                var _building_id = self.empty_string
                                var _building_category = self.empty_string
                                var _site_id = self.empty_string
                                var _circuit_id = self.empty_string
                                var _equipment_category = self.empty_string
                                var _legal_party_id = self.empty_string
                                var _legal_party_name = self.empty_string
                                //var _service_party_Name = self.empty_string
                                //var _service_description = self.empty_string
                                
                                for attr in entry.children
                                {
                                    if(attr.element!.name == "buildingId")
                                    {
                                        _building_id = attr.element!.text
                                        
                                    }
                                    
                                    if(attr.element!.name == "buildingCategory")
                                    {
                                        _building_category = attr.element!.text

                                    }
                                    
                                    if(attr.element!.name == "circuitId")
                                    {
                                        _circuit_id = attr.element!.text
                                    }
                                    
                                    if(attr.element!.name == "equimentCategory")
                                    {
                                        _equipment_category = attr.element!.text
                                    }
                                    
                                    if(attr.element!.name == "legalPartyId")
                                    {
                                        _legal_party_id = attr.element!.text
                                    }
                                    
                                    if(attr.element!.name == "legalPartyName")
                                    {
                                        _legal_party_name = attr.element!.text
                                    }
                                    
                                    /*if(attr.element!.name == "serviceDescription")
                                    {
                                        _service_description = attr.element!.text!
                                    }*/
                                    
                                    /*if(attr.element!.name == "servicePartyName")
                                    {
                                        _service_party_Name = attr.element!.text!
                                    }*/
                                    
                                    if(attr.element!.name == "siteId")
                                    {
                                        _site_id = attr.element!.text
                                    }
                                }
                                
                                /*var row_data = Dictionary<String,NSObject>()
                                
                                row_data.updateValue(_building_id, forKey: "building_id")
                                row_data.updateValue(_building_category, forKey: "building_category")
                                row_data.updateValue(_circuit_id, forKey: "circuit_id")
                                row_data.updateValue(_equipment_category, forKey: "equipment_category")
                                row_data.updateValue(_legal_party_id, forKey: "legal_party_id")
                                row_data.updateValue(_legal_party_name, forKey: "legal_party_name")
                                row_data.updateValue(_service_description, forKey: "service_description")
                                row_data.updateValue(_service_party_Name, forKey: "service_party_Name")
                                row_data.updateValue(_site_id, forKey: "site_id")*/
                                
                                if(CommonDataHelper.sharedInstance.asset_building_id == nil)
                                {
                                    CommonDataHelper.sharedInstance.asset_building_id = _building_id
                                }
                                if(CommonDataHelper.sharedInstance.asset_building_category == nil)
                                {
                                    CommonDataHelper.sharedInstance.asset_building_category = _building_category
                                }

                                if(!(_site_id.isEmpty))
                                {
                                    CommonDataHelper.sharedInstance.asset_sites.add(_site_id)
                                }
                                
                                if(!(_circuit_id.isEmpty))
                                {
                                    CommonDataHelper.sharedInstance.asset_circuit.add(_circuit_id)
                                }
                                
                                if(!(_legal_party_name.isEmpty))
                                {
                                    let consolidated_name = _legal_party_name + (_legal_party_id.isEmpty ? "" : (" ( " + _legal_party_id + " )"))
                                    
                                    CommonDataHelper.sharedInstance.asset_customer.add(consolidated_name)
                                }
                                
                                if(!(_equipment_category.isEmpty))
                                {
                                    CommonDataHelper.sharedInstance.asset_equipment.add(_equipment_category)
                                }
                                
//                                CommonDataHelper.sharedInstance.asset_data.addObject(row_data)
                                
                            }
                        }
                    }
                    
                    DispatchQueue.main.async(execute: {
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "reload_table_data_asset"), object: nil)
                        CommonDataHelper.sharedInstance.search_in_progress_asset = false
                    })
                    
                })
        }
        else
        {
            if(err_msg == nil)
            {
                DispatchQueue.main.async(execute: {
                    CommonDataHelper.sharedInstance.search_in_progress_asset = false
                    UIHelper.sharedInstance.showError("Failed to get asset information.")
                })
            }
            else
            {
                DispatchQueue.main.async(execute: {
                    CommonDataHelper.sharedInstance.search_in_progress_asset = false
                    UIHelper.sharedInstance.showError(err_msg!)
                })
            }
        }
    }
    
}
