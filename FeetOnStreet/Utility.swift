//
//  Utility.swift
//  FeetOnStreet
//
//  Created by admin on 7/28/17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation

extension UIStoryboard{
    static func main() -> UIStoryboard{
        return UIStoryboard.init(name: "Main", bundle: Bundle.main)
    }
}
