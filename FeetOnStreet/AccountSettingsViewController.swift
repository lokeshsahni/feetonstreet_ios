//
//  AccountSettingsViewController.swift
//  FeetOnStreet
//
//  Created by admin on 8/7/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class AccountSettingsViewController: UIViewController , UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var eMailIdLbl: UILabel!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var selectedOCNLbl: UILabel!
    @IBOutlet weak var selectOCNbtn: UIButton!
    @IBOutlet weak var popUpBackgroundBtn: UIButton!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var doneButton:UIButton?
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView:UITableView!

    
    var listArray = ["1:user","2:eMail","3:country","4:selected","5:popUp","6:scrol","7:done","8:search","9:Cont","10:status","11:hare","12:arCo","13:olor","14:odata","15:serN","16:Backg","17:sView","18:elect","19:ilase","20:err"]
    var ocn_data:[String] = []
    var filter_ocn_data:[String] = []
    var selectedOCN : String?
    var searchActive : Bool = false
    var selected_ocn : String?

    

    override func viewDidLoad() {
        super.viewDidLoad()
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = UIColor(red: 0/255, green: 105/255, blue: 95/255, alpha: 1.0)
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        popUpView.layer.cornerRadius  = 5
        var err_msg : String?
        userNameLbl.text = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "username")
        eMailIdLbl.text = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "email")
        countryLbl.text = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "country")        
        let selected_ocn = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "selected_ocn")
        let dataArr = CoreDataHelper.sharedInstance.getKeyValues(&err_msg, key: "ocn")
        
        // server unrachable messeage
        if let dataArray = dataArr {
            if dataArray.count > 0 {
                for item in dataArray {
                    let componetArr = item.components(separatedBy: "|")
                    let name = componetArr[2]+":"+componetArr[0]
                    ocn_data.append(name)
                }
            } else {

                let serverErrorPleaseTryTgain = NSLocalizedString("Server error. Please try again.", comment: "")
                UIHelper.showAlert(message: serverErrorPleaseTryTgain, inViewController: self)
            }
      }

        
        if(selected_ocn != nil)
        {
            let ocn_parts = selected_ocn?.components(separatedBy: "|BREAK|")
                selectedOCNLbl.text = ocn_parts?.last!
        }
        else
        {
//            selectDefaultCustomer.isHidden = true
            selectedOCNLbl.text = ""
        }
    }

    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return searchActive ? filter_ocn_data.count : ocn_data.count
       
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath ) as UITableViewCell?
        
//        let oCNName = ocn_data[indexPath.row]
//        let componetArr = oCNName.components(separatedBy: "|")
//        let name = componetArr[2]+":"+componetArr[0]
        cell?.textLabel?.text  = searchActive ? filter_ocn_data[indexPath.row] : ocn_data[indexPath.row]
//               cell?.textLabel?.text  = oCNName
        
        return cell!

    }
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        popUpBackgroundBtn.isHidden = true
        popUpView.isHidden = true
        view.endEditing(true)
        let oCNName  = searchActive ? filter_ocn_data[indexPath.row] : ocn_data[indexPath.row]
        let oCNtext = oCNName.components(separatedBy:":")
        let oCNnumber: String = oCNtext [0]
        selectedOCNLbl.text = oCNnumber
        selected_ocn = oCNName
        
//        let oCNName = ocn_data[indexPath.row]
//        let componetArr = oCNName.components(separatedBy: "|")
//        let name = componetArr[2]+":"+componetArr[0]
        searchBar.text = oCNName
    }

    @IBAction func sideMenuBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func selectOCNbtnAction(_ sender: Any) {
        popUpBackgroundBtn.isHidden = false
        popUpView.isHidden = false
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
      })
    }
    @IBAction func popUpBackgroundBtnAction(_ sender: Any) {
        popUpBackgroundBtn.isHidden = true
        popUpView.isHidden = true
        view.endEditing(true)
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        })
    }
    @IBAction func clickedOnDoneBtn(_ sender: Any) {
        AnalyticsHelper().analyticsEventsAction(category: "Account Settings", action: "acoountSettingsDone_click", label: "Done")
        
        var err_msg : String?
//        CoreDataHelper.sharedInstance.addToConfig(&err_msg, key: "selected_ocn", value: self.selected_ocn!)

        if searchBar.text == "" {
            
        }else{
           let oCNtext = searchBar.text?.components(separatedBy:":")
            let oCNnumber: String = oCNtext! [0]
            selected_ocn = oCNnumber
            CoreDataHelper.sharedInstance.addToConfig(&err_msg, key: "selected_ocn", value: self.selected_ocn!, viewController: self)
            searchBar.text = ""
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController")
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.window?.rootViewController = initialViewController
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        registerForKeyboardNotifications()

    AnalyticsHelper().analyticLogScreen(screen: "Account Setting Screen")
        
    }
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView?.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView?.contentInset = contentInsets
        self.scrollView?.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.bounds
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.doneButton {
            let btnRect = self.view.convert(activeField.bounds, from: activeField)
            if (!aRect.contains(CGPoint.init(x: btnRect.maxX, y: btnRect.maxY))){
                self.scrollView?.scrollRectToVisible(btnRect, animated: true)
            }
        }
    }
    func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        self.scrollView?.contentInset = contentInsets
        self.scrollView?.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView?.isScrollEnabled = false
    }
    func searchBar(_ searchBar: UISearchBar,
                   textDidChange searchText: String) {
        filter_ocn_data = ocn_data.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        
        if(filter_ocn_data.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }

        
        print(filter_ocn_data)
        self.tableView.reloadData()
        popUpBackgroundBtn.isHidden = false
        popUpView.isHidden = false
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        })
    }
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
//        popUpBackgroundBtn.isHidden = false
//        popUpView.isHidden = false
//        UIView.animate(withDuration: 0.4, animations: {
//            self.view.layoutIfNeeded()
//        })
        return true
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        AnalyticsHelper().analyticsEventsAction(category: "Account Settings", action: "selectOCN_click", label: "OCN")
        popUpBackgroundBtn.isHidden = false
        popUpView.isHidden = false
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
