//
//  SLAController.swift
//  FeetOnStreet
//
//  Created by Suresh Murugaiyan on 9/1/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class SLAController: UIViewController,UITableViewDataSource {

    @IBOutlet weak var tbl_Sla: UITableView!
    @IBOutlet weak var noDataAvailableLbl: UILabel!
    
    var slaList:[SLA]=[]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tbl_Sla.estimatedRowHeight = 1000
        tbl_Sla.rowHeight = UITableViewAutomaticDimension
        if slaList.count<=0 {
            noDataAvailableLbl.isHidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return slaList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? SlaCell
        if cell == nil {
            cell=SlaCell(style: .default, reuseIdentifier: identifier)
        }
        cell?.view_Price.layer.borderWidth=1.0
        cell?.view_Price.layer.borderColor=UIColor(colorLiteralRed: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0).cgColor
        let tempSla = slaList[indexPath.row]
        cell?.lbl_Presentation.text=tempSla.servicePresentation
        let deliveryTime = (tempSla.deliverTimeInDays)! + " Day(s)"
        cell?.lbl_DeliveryTime.text=deliveryTime
        if tempSla.targetRepairTimeInHours == nil {
            cell?.lbl_TotalRepairTime.text = ""
        }else{
        let totalRepairTime = (tempSla.targetRepairTimeInHours)! + " Hour(s)"
        cell?.lbl_TotalRepairTime.text=totalRepairTime
        }
        let availability = (tempSla.availabilityInPercentage)! + " %"
        cell?.lbl_Availability.text=availability
        cell?.lbl_remarks.text=tempSla.remarksSla
        return cell!
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
