//
//  RTDtoolViewController.swift
//  FeetOnStreet
//
//  Created by admin on 3/1/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import SwiftOverlays


class RTDtoolViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UIWebViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var shareBtn: UIButton!
    var mainArray = NSArray()
    var listArr = NSArray()
    var listArr2 = NSArray()
    var xNGlistArr = NSArray()
    var xNGlistArr2 = NSArray()
    
    var connectedSiteName:String?
    var fibreDistance:String?
    var thearticalLatency:String?
    var thearticalRDS:String?
    
    var isShortestRouteSelected:Bool?
    var isDiverseRouteSelected:Bool?
    var isSubSeaRouteSelected:Bool?

    
    var routes = [RouteDetailModel]()
    var isColtEmployee = false
    
    var tempPathModel:PathModel?
    
    var serverErrorPleaseTryTgain = ""
    
    var eMailPopUp:RTDEMailPopUpView?
    
    var selectedName:String = ""{
        didSet {
        }
    }
    
    var searchType: String?
    var siteA_ID: Int?
    var siteB_ID: Int?
    var subSeaLink: String?
    var subSeaLinkID: Any?

        
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
     self.navigationController?.setNavigationBarHidden(true, animated: true)
        AnalyticsHelper().analyticLogScreen(screen: "RTD Tool Lost Screen")
    let share = NSLocalizedString("SHARE", comment: "")
//        shareBtn.setTitle(share, for: UIControlState.normal)
        shareBtn.setTitle(share, for: .normal)
        serverErrorPleaseTryTgain = NSLocalizedString("Server error. Please try again.", comment: "")
    
        var err_msg : String?
        let tempVal2 =  CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "employeeType")
        if tempVal2 == "Colt Employee" {
            isColtEmployee = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    func numberOfSections(in tableView: UITableView) -> Int {
        return routes.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isColtEmployee {
            return 1 + routes[section].path.count
        }
        return 1
    }
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 0 {
            let updatesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "UpdatesTableViewCell", for: indexPath) as! UpdatesTableViewCell
            updatesTableViewCell.routeModel = routes[indexPath.section]
           
            if isShortestRouteSelected == true {
                updatesTableViewCell.shortesrOrdivereseRouteLbl.text = "Unprotected Route"
            }
            if isDiverseRouteSelected == true {
                updatesTableViewCell.shortesrOrdivereseRouteLbl.text = "Protected Route"
            }
            if isSubSeaRouteSelected == true {
                updatesTableViewCell.shortesrOrdivereseRouteLbl.text = "Unprotected Route with Custom Subsea Link"
            }
            updatesTableViewCell.cellCountLbl.text = String(indexPath.section + 1)
            if isColtEmployee == false {
                updatesTableViewCell.view_xchng.isHidden = true
                updatesTableViewCell.layoutConstraintHeight_viewXchng.constant = 0
            }else{
                updatesTableViewCell.view_xchng.isHidden = false
                updatesTableViewCell.layoutConstraintHeight_viewXchng.constant = 38.0
            }
            if let kSubSeaName = subSeaLink  {
                if !(kSubSeaName.isEmpty) {
                    updatesTableViewCell.lbl_subSeaLink.text = "(Subsea Link: \(kSubSeaName))"
                }else{
                    updatesTableViewCell.constraintHeight_subSeaLink.constant = 0
                    updatesTableViewCell.lbl_subSeaLink.isHidden = true
                }
            }else{
                updatesTableViewCell.constraintHeight_subSeaLink.constant = 0
                updatesTableViewCell.lbl_subSeaLink.isHidden = true
            }
            updatesTableViewCell.vcReference = self
            return updatesTableViewCell
        }else {
            let kMZlibLinkCell = tableView.dequeueReusableCell(withIdentifier: "KMZlibLinkCell", for: indexPath) as! KMZlibLinkCell
            kMZlibLinkCell.pathModel = routes[indexPath.section].path[indexPath.row - 1]
            return kMZlibLinkCell
        }
        
    }

    func quickLookAction(with model:RouteDetailModel) {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            DispatchQueue.main.async {
                let no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
                UIHelper.sharedInstance.showReachabilityWarning(no_internet_connection, _onViewController: self)
            }
        }else{
            DispatchQueue.main.async {
                let please_wait = NSLocalizedString("Please wait", comment: "")
                SwiftOverlays.showBlockingWaitOverlayWithText(please_wait)
            }
            
            var errMsg:String?

            var siteAString = "\((model.siteAId+10000))"
            if(model.siteAId < 0 && siteA_ID != nil){
                siteAString = "\((siteA_ID!+10000))"
            }

            var siteBString = "\((model.siteBId+10000))"
            if(model.siteBId < 0 && siteB_ID != nil){
                siteBString = "\((siteB_ID!+10000))"
            }

            var routesPath = siteAString
            for path in model.path{
                routesPath = "\(routesPath),\(path.kMZLibNo)"
            }
            routesPath = "\(routesPath),\(siteBString)"
            routesPath = routesPath.trimmingCharacters(in: CharacterSet.init(charactersIn: ","))
            
            var routeName = ""
            
            let routeEndPoints = model.connectedSiteName
            if(routeEndPoints.count > 0){
                if let startPoint = routeEndPoints.split(separator: Character.init("=")).first{
                    if let startPointStr = String(startPoint.trimmingCharacters(in: CharacterSet(charactersIn: "> "))){
                        if let startPointStrEncoded = startPointStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics){
                            routeName = startPointStrEncoded
                        }
                    }
                }
                if let endPoint = routeEndPoints.split(separator: Character.init("=")).last{
                    if let endPointStr = String(endPoint.trimmingCharacters(in: CharacterSet(charactersIn: "> "))){
                        if let endPointStrEncoded = endPointStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics){
                            routeName = "\(routeName)--\(endPointStrEncoded)%20Shortest"
                        }
                    }
                }
            }
            
            //let urlString = "https://dcp.colt.net/rtdKmlService/download/route.kmz?routes=1008,10,33,13,1006&name=COLT%20BER%20BWS--COLT%20AMS%20MAF%20Shortest&color=ff00ff00"
            let urlString = "https://dcp.colt.net/rtdKmlService/download/route.kmz?routes=\(routesPath)&name=\(routeName)&color=#bd0000"
//            let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
//            let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
            let request = NSMutableURLRequest(url: URL(string: urlString)!)
            request.httpMethod = "GET"
            request.addValue("Basic cnRkY2xpZW50OnJ0ZCRhY2Nlc3MybWU=", forHTTPHeaderField: "Authorization")
            request.timeoutInterval=600
            let session = URLSession.shared
            weak var weakSelf = self
            let downloadSession = session.downloadTask(with: request as URLRequest, completionHandler: { (localURl, responce, error) in
                if error == nil
                {
                    if let localURL = localURl {
                        
                        let fileManager = FileManager()
                        
                        if var documentsPathURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                            //This gives you the URL of the path
                            documentsPathURL.appendPathComponent("KMLDirectory")
                            do {
                                if fileManager.fileExists(atPath: documentsPathURL.path){
                                    try fileManager.removeItem(at: documentsPathURL)

                                }
                                try fileManager.createDirectory(at: documentsPathURL, withIntermediateDirectories: true, attributes: nil)
                                //try fileManager.unzipItem(at: localURL, to: documentsPathURL)
                                SSZipArchive.unzipFile(atPath: localURL.path, toDestination: documentsPathURL.path)
                                if let file =  try fileManager.contentsOfDirectory(atPath: documentsPathURL.path).first{
                                    let data = try Data.init(contentsOf: documentsPathURL.appendingPathComponent(file))
                                    DispatchQueue.main.async {
                                        
                                        SwiftOverlays.removeAllBlockingOverlays()
                                        
                                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                        let rTDtoolViewController = storyBoard.instantiateViewController(withIdentifier: "MapDirectionVC") as! MapDirectionVC
                                        rTDtoolViewController.kmlData = data
                                        weakSelf?.navigationController?.pushViewController(rTDtoolViewController, animated: true)
                                    }
                                }
                                
                            } catch {
                                DispatchQueue.main.async {
                                    SwiftOverlays.removeAllBlockingOverlays()
                                    
                                    UIHelper.showAlert(message: self.serverErrorPleaseTryTgain, inViewController: self)
//                                    UIHelper.showAlert(message: "Unable to extract map information. Please try again.", inViewController: self)
                                }

                                print("Extraction of ZIP archive failed with error:\(error)")
                            }

                        }
                        
                        
                        
//                        let currentWorkingPath = fileManager.currentDirectoryPath
//                        var sourceURL = URL(fileURLWithPath: currentWorkingPath)
//                        sourceURL.appendPathComponent("archive.zip")
//                        var destinationURL = URL(fileURLWithPath: currentWorkingPath)
//                        destinationURL.appendPathComponent("directory")
//                        do {
//                            try fileManager.createDirectory(at: destinationURL, withIntermediateDirectories: true, attributes: nil)
////                            try fileManager.unzipItem(at: sourceURL, to: destinationURL)
//                        } catch {
//                            print("Extraction of ZIP archive failed with error:\(error)")
//                        }
//                        
                        
                        

//                        if let string = try? String(contentsOf: localURL) {
//                            print(string)
//                        }
                    }
                    
                }else{
                    // BackgroundApiCall
                    print(error?.localizedDescription ?? "Error")
                    
                    DispatchQueue.main.async {
                        //                    UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                        SwiftOverlays.removeAllBlockingOverlays()
                    }
                    
                    UIHelper.showAlert(message: self.serverErrorPleaseTryTgain, inViewController: self)
                }
                
            })
            downloadSession.resume()
        
//            _ = session.dataTask(with: request as URLRequest) { (data, response, error) in
//
//                if error == nil
//                {
//                    if let data = data
//                    {
//                        DispatchQueue.main.async {
//                            SwiftOverlays.removeAllBlockingOverlays()
//
////                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
////                            let rTDtoolViewController = storyBoard.instantiateViewController(withIdentifier: "MapDirectionVC") as! MapDirectionVC
////                            rTDtoolViewController.kmlData = data
////                            weakSelf?.navigationController?.pushViewController(rTDtoolViewController, animated: true)
//                        }
//
//                    }
//                }else{
//                    // BackgroundApiCall
//                    print(error?.localizedDescription ?? "Error")
//
//                    DispatchQueue.main.async {
//                        //                    UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
//                        SwiftOverlays.removeAllBlockingOverlays()
//                    }
//                    UIHelper.showAlert(message: "Server error. Please try again.", inViewController: self)
//                }
//                }
//                .resume()
        }
    }

    @IBAction func gMailBtnAction(_ sender: Any) {
//        AnalyticsHelper().analyticsEventsAction(category: "Email", action: "RTD_emailicon_click", label: "Email")
        eMailPopUp = Bundle.main.loadNibNamed("RTDEMailPopUpView", owner: self, options: nil)?[0] as? RTDEMailPopUpView
        eMailPopUp?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        eMailPopUp?.rTDtoolViewController = self
        self.view.addSubview(eMailPopUp!)
    }
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//    let rTDtoolViewController = storyBoard.instantiateViewController(withIdentifier: "MapDirectionVC") as! MapDirectionVC
//    self.navigationController?.pushViewController(rTDtoolViewController, animated: true)
//    }

    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
