//
//  KMZlibLinkCell.swift
//  FeetOnStreet
//
//  Created by admin on 3/5/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class KMZlibLinkCell: UITableViewCell {

    @IBOutlet weak var kMZlibLinkNoLbl: UILabel!
    @IBOutlet weak var xNGroutingNoLbl: UILabel!
    @IBOutlet weak var backGroundBottomView: UIView!
    
    var pathModel:PathModel?{
        didSet{
           // kMZlibLinkNoLbl.text = String((pathModel?.kMZLibNo)!)
            xNGroutingNoLbl.text = pathModel?.xNGWMSId
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backGroundBottomView.layer.borderWidth = 1
        self.backGroundBottomView.layer.borderColor = UIColor.darkGray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
