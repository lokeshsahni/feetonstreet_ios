//
//  EFMViewController.swift
//  FeetOnStreet
//
//  Created by admin on 9/8/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class EFMViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,PopupDelegate {
    
    
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var popUpTableView: UITableView!
    @IBOutlet weak var popUpBackgroundBtn: UIButton!
    @IBOutlet weak var filterTypeBtn: UIButton!
    @IBOutlet weak var nodataAvailableLbl: UILabel!
    
    var selectedProduct:String?
    var supplierName:String?
    var eMFDSLlist:[DSLModel] = []
    var eMFDSLlistTable:[DSLModel] = []
    var emfFiltersupplierList:[Any] = []
    var fosPopUpEmf:Fos_Popup?
    var onnetViewCtrl:ONNetListViewController? = nil
    var address1:String?
    var point_ID:String?
    var isIntialLoded:Bool = true
    var productName:String?
    
    var bEndAddress_id:String?
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        eMFDSLlist = CommonDataHelper.sharedInstance.eMF_DSL_results as! [Any] as! [DSLModel]
        print(eMFDSLlist)
        emfFiltersupplierList = eMFDSLlist
        filterTypeBtn.contentHorizontalAlignment = .left
        filterTypeBtn.layer.borderWidth = 1
        listTableView.estimatedRowHeight = 1000
        listTableView.rowHeight = UITableViewAutomaticDimension
        popUpTableView.layer.borderWidth = 1.0
        removeDuplicateSupplierName()
        if (CommonDataHelper.sharedInstance.eMF_DSL_results.count)>0 {
            filterTypeBtn.isHidden = false
             nodataAvailableLbl.isHidden=true
        }
        if isIntialLoded == false {
            nodataAvailableLbl.isHidden=false
        }
    }
    
    func removeDuplicateSupplierName() {
        var femfData:[String]=[]
        for emfData1 in eMFDSLlist {
            if !femfData.contains(emfData1.supplierName!) {
                femfData.append(emfData1.supplierName!)
                eMFDSLlistTable.append(emfData1)
            }
        }
    }
    
    func reloadTablevieWithResponse() {
        emfFiltersupplierList = CommonDataHelper.sharedInstance.eMF_DSL_results as! [Any]
        eMFDSLlist = CommonDataHelper.sharedInstance.eMF_DSL_results as! [DSLModel]
        filterTypeBtn.isHidden = false
        listTableView.reloadData()
        popUpTableView.reloadData()
        removeDuplicateSupplierName()
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        var count:Int?
        if tableView == popUpTableView {
            count =  2
        }else{
            count = 1
        }
        return count!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == popUpTableView {
            if section==0 {
                return 1
            }else{
                return eMFDSLlistTable.count
            }
        }else{
            return emfFiltersupplierList.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == popUpTableView {
            let popUpCell = tableView.dequeueReusableCell(withIdentifier: "popUpCell")
            if indexPath.section == 0 {
                popUpCell?.textLabel?.text = "All"
            }else{
                let dSLModel = eMFDSLlistTable[indexPath.row]
                popUpCell?.textLabel?.text = dSLModel.supplierName
            }
            return popUpCell!
        }
        else{
            let identifier = "OffNetEMFCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OffNetEMFCell
            cell?.backGroundView.layer.borderWidth = 0.5
            cell?.backGroundView.layer.borderColor = UIColor.lightGray.cgColor
            let dSLModel = emfFiltersupplierList[indexPath.row] as? DSLModel
            address1 = "\(dSLModel!.cityName!),\(dSLModel!.postCode!), \(dSLModel!.coltOperatingCountry!)"
            if dSLModel?.streetName != nil {
                address1 = "\(dSLModel!.streetName!), " + address1!
            }
            print(address1)
            cell?.addressLbl.text = address1
            cell?.suplierNameLbl.text = dSLModel?.supplierName
            cell?.accessTypeLbl.text = dSLModel?.accessType
            selectedProduct = dSLModel?.coltProduct
            // changing product names for user
            self.setProductNames()
            cell?.coltProductLbl.text = selectedProduct
            return cell!
            
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == popUpTableView {
            if indexPath.section == 0 {
                filterTypeBtn.setTitle("All", for: .normal)
                popUpBackgroundBtn.isHidden = true
                popUpTableView.isHidden = true
                emfFiltersupplierList = eMFDSLlist
                listTableView.reloadData()
            }else{
                let dSLModel = eMFDSLlistTable[indexPath.row]
                supplierName = dSLModel.supplierName
                filterTypeBtn.setTitle(supplierName, for: .normal)
                popUpBackgroundBtn.isHidden = true
                popUpTableView.isHidden = true
                filterBySupplierName()
            }
            
        }else{
            CommonDataHelper.sharedInstance.npc = nil
            let dSLModel = emfFiltersupplierList[indexPath.row] as? DSLModel
            bEndAddress_id = dSLModel?.pointID
            address1 = "\(dSLModel!.cityName!),\(dSLModel!.postCode!), \(dSLModel!.coltOperatingCountry!)"
            if dSLModel?.streetName != nil {
                address1 = "\(dSLModel!.streetName!), " + address1!
            }
            let objectsDict: NSDictionary = [
                "bEndAddress_id" : bEndAddress_id,
                "viewType" : ViewType.BuildingType,
                "address1" : address1!,
                "notificationType" : "EFM",
                "bEndAddressId" : "EFM"
            ]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "OpenProductBandwidthPopUp"), object: objectsDict)
            
            /*
             tableView.deselectRow(at: indexPath, animated: false)
             if fosPopUpEmf?.popupFosDetails?.selectedProductType == "Colt LANLink Point to Point (Ethernet Point to Point)" {
             
             let cellTypes = [PopupcellTypes.AddressCell,PopupcellTypes.ProductCell,PopupcellTypes.BandWidthCell,PopupcellTypes.BEndAddressCell,PopupcellTypes.CheckPriceCell,PopupcellTypes.CancelCell]
             let pDetails = PopupDetails()
             pDetails.pType=PopupType.MapviewPopup4
             pDetails.popupController=self
             pDetails.locationAddress=fosPopUpEmf?.popupFosDetails?.locationAddress
             pDetails.lattitudeLocation = fosPopUpEmf?.popupFosDetails?.lattitudeLocation
             pDetails.longitudeLocation = fosPopUpEmf?.popupFosDetails?.longitudeLocation
             pDetails.isPopupMutable=false
             pDetails.productName=fosPopUpEmf?.popupFosDetails?.productName
             pDetails.selectedProductType=fosPopUpEmf?.popupFosDetails?.selectedProductType
             pDetails.selectedBandWidth=fosPopUpEmf?.popupFosDetails?.selectedBandWidth
             let fosPopup_emf = Fos_Popup.initWithPopupElements(cells: cellTypes, popupDetails: pDetails)
             fosPopup_emf.delegate=self
             presentPopupView(fosPopup_emf)
             
             }else if fosPopUpEmf?.popupFosDetails?.selectedProductType == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
             
             let cellTypes = [PopupcellTypes.AddressCell,PopupcellTypes.ProductCell,PopupcellTypes.BandWidthCell,PopupcellTypes.HubCell,PopupcellTypes.CheckPriceCell,PopupcellTypes.CancelCell]
             let pDetails = PopupDetails()
             pDetails.pType=PopupType.MapviewPopup3
             pDetails.popupController=self
             pDetails.locationAddress=fosPopUpEmf?.popupFosDetails?.locationAddress
             pDetails.lattitudeLocation = fosPopUpEmf?.popupFosDetails?.lattitudeLocation
             pDetails.longitudeLocation = fosPopUpEmf?.popupFosDetails?.longitudeLocation
             pDetails.isPopupMutable=false
             pDetails.productName=fosPopUpEmf?.popupFosDetails?.productName
             pDetails.selectedProductType=fosPopUpEmf?.popupFosDetails?.selectedProductType
             pDetails.selectedBandWidth=fosPopUpEmf?.popupFosDetails?.selectedBandWidth
             let fosPopup_emf = Fos_Popup.initWithPopupElements(cells: cellTypes, popupDetails: pDetails)
             fosPopup_emf.delegate=self
             presentPopupView(fosPopup_emf)
             }else{
             let cellTypes = [PopupcellTypes.AddressCell,PopupcellTypes.ProductCell,PopupcellTypes.BandWidthCell,PopupcellTypes.CheckPriceCell,PopupcellTypes.CancelCell]
             let pDetails = PopupDetails()
             pDetails.pType=PopupType.MapviewPopup2
             pDetails.popupController=self
             pDetails.locationAddress=fosPopUpEmf?.popupFosDetails?.locationAddress
             pDetails.lattitudeLocation = fosPopUpEmf?.popupFosDetails?.lattitudeLocation
             pDetails.longitudeLocation = fosPopUpEmf?.popupFosDetails?.longitudeLocation
             pDetails.isPopupMutable=false
             pDetails.productName=fosPopUpEmf?.popupFosDetails?.productName
             pDetails.selectedProductType=fosPopUpEmf?.popupFosDetails?.selectedProductType
             pDetails.selectedBandWidth=fosPopUpEmf?.popupFosDetails?.selectedBandWidth
             let fosPopup_emf = Fos_Popup.initWithPopupElements(cells: cellTypes, popupDetails: pDetails)
             fosPopup_emf.delegate=self
             presentPopupView(fosPopup_emf)
             }
             */
        }
    }
    
    func setProductNames() {
        if productName == "Colt VoIP Access" {
            productName = "Colt SIP Trunking"
        }else if productName == "Colt LANLink Point to Point (Ethernet Point to Point)" {
            productName = "Colt Ethernet Line"
            
        }else if productName == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
            productName = "Colt Ethernet Hub and Spoke"
        }else if productName == "Colt Ethernet Private Network (EPN)"{
            productName = "ColT Ethernet VPN"
        }
    }
    
    func filterBySupplierName() {
        emfFiltersupplierList = []
        for supplier in eMFDSLlist{
            if supplier.supplierName!.lowercased() == supplierName?.lowercased() {
                emfFiltersupplierList.append(supplier)
                print(emfFiltersupplierList)
                print("supplierName = ",(supplier as AnyObject).supplierName)
            }
            print("supplierName111111 = ",(supplier as AnyObject).supplierName)
        }
        listTableView.reloadData()
    }
    
    func dismissPopup(selectionType:String,popupFos:Fos_Popup) {
        if selectionType == "cancel" {
            dismissPopupView()
        }else if selectionType == "checkprice" {
            
            if popupFos.popupFosDetails?.selectedProductType == nil {
                UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select product")
                return
            }
            if popupFos.popupFosDetails?.selectedBandWidth == nil {
                if popupFos.popupFosDetails?.productName == "Colt SIP Trunking" {
                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select voice channel")
                }else{
                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select bandwidth")
                }
                
                return
            }
            if popupFos.popupFosDetails?.pType == PopupType.MapviewPopup3 && ((popupFos.popupFosDetails?.selectedHubName) == nil) {
                UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select hub name")
                return
            }
            if popupFos.popupFosDetails?.pType == PopupType.MapviewPopup4 {
                if popupFos.popupFosDetails?.bEndAddress == nil {
                    let please_select_BEndaddress = NSLocalizedString("Please select B-End address", comment: "")
                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: please_select_BEndaddress)
                    return
                }
                if popupFos.popupFosDetails?.isBendAddressSearched == false {
                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please search B-End address")
                    return
                }
                
            }
            dismissPopupView()
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Secondary", bundle:nil)
            let checkPriceVC = storyBoard.instantiateViewController(withIdentifier: "CheckPriceController") as! CheckPriceController
            checkPriceVC.locationStr=popupFos.popupFosDetails?.locationAddress as String!
            checkPriceVC.productType=popupFos.popupFosDetails?.selectedProductType
            checkPriceVC.bandWidthType=popupFos.popupFosDetails?.selectedBandWidth
            self.navigationController?.pushViewController(checkPriceVC, animated: true)
        }else if selectionType == "buildingDetails" {
            dismissPopupView()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let buildingDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "BuildingDetailsViewController") as! BuildingDetailsViewController
            
            self.navigationController?.pushViewController(buildingDetailsViewController, animated: true)
        }
    }
    
    @IBAction func filterBtnAction(_ sender: Any) {
        AnalyticsHelper().analyticsEventsAction(category: "OFFNET List", action: "EFM/DSL_supplierNameFilter_click", label: "EFM/DSL supplier filter")
        popUpBackgroundBtn.isHidden = false
        popUpTableView.isHidden = false
        popUpTableView.reloadData()
    }
    @IBAction func popUpBacgroundBtnAction(_ sender: Any) {
        popUpBackgroundBtn.isHidden = true
        popUpTableView.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

