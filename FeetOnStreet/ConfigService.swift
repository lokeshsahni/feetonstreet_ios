//
//  ConfigService.swift
//  FeetOnStreet
//
//  Created by admin on 7/28/17.
//  Copyright © 2017 admin. All rights reserved.
//


import Foundation
import SwiftOverlays



class ConfigService : NSObject {
    
    let empty_string = ""
    let untraceableErrorString = "Something Went Wrong!!"

    func getHubs(_ ocn : String, product: String) {
    
        CommonDataHelper.sharedInstance.search_in_progress_hublist = true
        
        DispatchQueue.main.async {
            SwiftOverlays.showBlockingWaitOverlayWithText("Logging In...")
        }
        let url = "https://dcp.colt.net/dgateway/services/ConfigService"
        let body = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dgat=\"http://dgateway2.web.colt.net\"> <soapenv:Header/> <soapenv:Body> <dgat:getHubForOcn> <dgat:ocn>" + ocn + "</dgat:ocn> <dgat:productName>" + product + "</dgat:productName> </dgat:getHubForOcn> </soapenv:Body> </soapenv:Envelope>"
         
        CommonDataHelper.sharedInstance.hub_list.removeAllObjects()  // clear previous results before next call
        
        WebHelper.sharedInstance.callDirectService(url, body: body, soapAction: "") { (err_msg, resp) in
            
            if(err_msg != nil)
            {
                DispatchQueue.main.async {
                    SwiftOverlays.removeAllBlockingOverlays()
                }
                DispatchQueue.main.async(execute: {
                    UIHelper.sharedInstance.showError(err_msg!)
                })
            }
            
            if(resp != nil)
            {
                DispatchQueue.main.async {
                 SwiftOverlays.removeAllBlockingOverlays()
                }
                
                let xml = SWXMLHash.parse(resp!)
                
                let hubs = xml["soapenv:Envelope"]["soapenv:Body"]["getHubForOcnResponse"]
                
                for entry in hubs.children
                {
                    if entry.element!.name == "getHubForOcnReturn"
                    {
                        //TODO: insted of defining optionals. use nonoptional string and assign them to empty - ""
                        var _country : String?
                        var _city : String?
                        var _streetname : String?
                        var _postcode : String?
                        var _house_number : String?
                        var _building_name : String?
                        var _point_id : String?
                        var _hubname : String?
                        
                        for record in entry.children
                        {
                            //Check for record.element != nil insted of forced unwrapping the optional element. use guard-let-else statement.
                            //TODO: record.element!.text == nil is redundent. This will never be true. As text is non-optional. Do you mean record.element == nil.
                            if(record.element!.name == "buildingName")
                            {
                                _building_name = record.element!.text
                                if _building_name == nil {
                                    _building_name=""
                                }
                            }
                            else if(record.element!.name == "city")
                            {
                                _city = record.element!.text
                                if _city == nil {
                                    _city=""
                                }
                            }
                            else if(record.element!.name == "country")
                            {
                                _country = record.element!.text
                                if _country == nil {
                                    _country=""
                                }
                            }
                            else if(record.element!.name == "houseNumber")
                            {
                                _house_number = record.element!.text
                                if _house_number == nil {
                                    _house_number=""
                                }
                            }
                            else if(record.element!.name == "hubName")
                            {
                                _hubname = record.element!.text
                                if _hubname == nil {
                                    _hubname=""
                                }
                            }
                            else if(record.element!.name == "networkPointCode")
                            {
                                _point_id = record.element!.text
                                if _point_id == nil {
                                    _point_id=""
                                }
                            }
                            else if(record.element!.name == "postcode")
                            {
                                _postcode = record.element!.text
                                if _postcode == nil {
                                    _postcode=""
                                }
                            }
                            else if(record.element!.name == "streetName")
                            {
                                _streetname = record.element!.text
                                if _streetname == nil {
                                    _streetname=""
                                }
                            }
                        }
                        
                        if _point_id != nil
                        {
                            
                            var row_data = Dictionary<String,String>()
                            
                            row_data.updateValue(_point_id!, forKey: "point_id")
                            
                            var formattedAddress = _house_number! + ", " + _building_name! + ", " + _streetname! + ", " + _postcode! + ", " + _city! + ", " + _country!
                            
                            formattedAddress = formattedAddress.replacingOccurrences(of: ", ,", with: ", ")
                            formattedAddress = formattedAddress.replacingOccurrences(of: "  ", with: " ")
                            formattedAddress = formattedAddress.trimmingCharacters(in: CharacterSet.init(charactersIn: ", "))
                            
                            row_data.updateValue(formattedAddress, forKey: "formatted_address")
                            row_data.updateValue(_hubname!, forKey: "hub_name")
                            
                            CommonDataHelper.sharedInstance.hub_list.add(row_data)
                            
                        }
                    }
                }
//                NotificationCenter.default.post(name: Notification.Name(rawValue: "finish_loading_hub_list"), object: nil)
            }
            
            CommonDataHelper.sharedInstance.search_in_progress_hublist = false
            
        }
    }
    
    func getBandwidthForAllProducts(onComplete : @escaping ((Bool, String) -> Void)) {
    
        
        let url = "https://dcp.colt.net/dgateway/services/ConfigService"
        let body = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dgat=\"http://dgateway2.web.colt.net\"> <soapenv:Header/> <soapenv:Body> <dgat:getBandwidthForAllProducts/> </soapenv:Body> </soapenv:Envelope>"
        
        
        WebHelper.sharedInstance.callDirectService(url, body: body, soapAction: "") { (err_msg, resp) in
            
            if(err_msg != nil)
            {
                onComplete(false, err_msg!);
                return;
//                DispatchQueue.main.async(execute: {
//                    UIHelper.sharedInstance.showError(err_msg!)
//                })
            }
            
            var _err_msg : String?
            
            if(resp != nil)
            {
                let xml = SWXMLHash.parse(resp!)
                
                let bandwidths = xml["soapenv:Envelope"]["soapenv:Body"]["getBandwidthForAllProductsResponse"]
                
                
                var successStatus = true
                var allErrors = ""
                
                for entry in bandwidths.children
                {
                    if(entry.element!.name == "getBandwidthForAllProductsReturn")
                    {
                        //TODO: See above function...
                        var _product_name : String?
                        var _bandwidth : String?
                        var _sort_order : String?
                        
                        for record in entry.children
                        {
                            //TODO: See above function...
                            if(record.element!.name == "serviceInternalName")
                            {
                                _product_name = record.element!.text == nil ? self.empty_string : record.element!.text
                            }
                            else if(record.element!.name == "bandwidthDesc")
                            {
                                _bandwidth = record.element!.text == nil ? self.empty_string : record.element!.text
                            }
                            else if(record.element!.name == "bandwidthOrder")
                            {
                                _sort_order = record.element!.text == nil ? self.empty_string : record.element!.text
                            }
                        }
                        
                        if(_product_name != nil && !(_product_name!.isEmpty) && _bandwidth != nil && !(_bandwidth!.isEmpty) && _sort_order != nil)
                        {
                            
                            CoreDataHelper.sharedInstance.add_product_bandwidth(&_err_msg, product_name: _product_name!, bandwidth: _bandwidth!, sort_order: _sort_order!);
                            
                            if(_err_msg != nil){
                                //Failed...
                                successStatus = false
                                allErrors = allErrors + _err_msg!
                            }
                            
                        }
                        
                    }
                }
                
                onComplete(successStatus, allErrors)
                return;
            }
            onComplete(false, self.untraceableErrorString)
        }
        
        
    }
    
    
    func getBandwidthForProduct(_ product_name : String) {
        
        CommonDataHelper.sharedInstance.bandwidth_list_for_product = [String]()
        
        let url = "https://dcp.colt.net/dgateway/services/ConfigService"
        let body = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dgat=\"http://dgateway2.web.colt.net\"> <soapenv:Header/> <soapenv:Body> <dgat:getBandwidthForProduct> <dgat:productName>" + product_name + "</dgat:productName> </dgat:getBandwidthForProduct> </soapenv:Body> </soapenv:Envelope>"
        
        WebHelper.sharedInstance.callDirectService(url, body: body, soapAction: "") { (err_msg, resp) in
            
            if(err_msg != nil)
            {
                DispatchQueue.main.async(execute: {
                    UIHelper.sharedInstance.showError(err_msg!)
                })
            }
            
            if(resp != nil)
            {                
                let xml = SWXMLHash.parse(resp!)
                
                let bandwidths = xml["soapenv:Envelope"]["soapenv:Body"]["getBandwidthForProductResponse"]
                
                for entry in bandwidths.children
                {
                    if(entry.element!.name == "getBandwidthForProductReturn")
                    {
                        if(entry.element!.text != nil)
                        {
                            let _bandwidth_name = entry.element!.text
                            
                            if(!(_bandwidth_name.isEmpty))
                            {
                                CommonDataHelper.sharedInstance.bandwidth_list_for_product?.append(_bandwidth_name)
                            }
                        }
                    }
                }
            }
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "finish_loading_bandwidth"), object: nil)
        }
        
    }
    
    
    
    func getBandwidthFromEIX(onComplete : @escaping ((Bool, String) -> Void)) {
        
        
        let url = "https://dcp.colt.net/dgateway/services/ConfigService"
        let body = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dgat=\"http://dgateway2.web.colt.net\"> <soapenv:Header/> <soapenv:Body> <dgat:getAllBandwidths/> </soapenv:Body> </soapenv:Envelope>"
        
        CommonDataHelper.sharedInstance.bandwidth_list = NSMutableArray()
        
        WebHelper.sharedInstance.callDirectService(url, body: body, soapAction: "") { (err_msg, resp) in
            
            var _err_msg : String?
            
            if(err_msg != nil)
            {
                onComplete(false, err_msg!) //Failed....
                return
//                DispatchQueue.main.async(execute: {
//                    UIHelper.sharedInstance.showError(err_msg!)
//                })
            }
            
            if(resp != nil)
            {
                let xml = SWXMLHash.parse(resp!)
                
                let bandwidths = xml["soapenv:Envelope"]["soapenv:Body"]["getAllBandwidthsResponse"]
                
                var _sort_order = 1
                
                var successStatus = true
                var allErrors = ""
                
                for entry in bandwidths.children
                {
                    if(entry.element!.name == "getAllBandwidthsReturn")
                    {
                        if(entry.element!.text != nil)
                        {
                            let _bandwidth_name = entry.element!.text
                            
                            if(!(_bandwidth_name.isEmpty))
                            {
                                CoreDataHelper.sharedInstance.add_bandwidth(&_err_msg, name: _bandwidth_name, sort_order: String(_sort_order))
                                                            
                                _sort_order += 1
                            
                                if(_err_msg != nil){
                                    //Failed...
                                    
                                    //DispatchQueue.main.async(execute: {
                                    //  UIHelper.sharedInstance.showError(_err_msg!)
                                    //})
                                    
                                    successStatus = false
                                    allErrors = allErrors + _err_msg!
                                }
                            }
                        }
                    }
                }
                
                onComplete(successStatus, allErrors)
                return;
                
            }
            onComplete(false, self.untraceableErrorString)
            
        }
        
    }
    
    
    func getProductsFromEIX(onComplete : @escaping ((Bool, String) -> Void)) {
        
        
        let url = "https://dcp.colt.net/dgateway/services/ConfigService"
        let body = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dgat=\"http://dgateway2.web.colt.net\"> <soapenv:Header/> <soapenv:Body> <dgat:getAllProducts/> </soapenv:Body> </soapenv:Envelope>"
        
        CommonDataHelper.sharedInstance.product_list = NSMutableArray()
        
        WebHelper.sharedInstance.callDirectService(url, body: body, soapAction: "") { (err_msg, resp) in
            
            var _err_msg : String?
            
            if(err_msg != nil)
            {
                onComplete(false, err_msg!) //Failed....
                return
//                DispatchQueue.main.async(execute: {
//                    UIHelper.sharedInstance.showError(err_msg!)
//                })
            }
            
            if(resp != nil)
            {
                let xml = SWXMLHash.parse(resp!)
                
                let products = xml["soapenv:Envelope"]["soapenv:Body"]["getAllProductsResponse"]
                
                
                var successStatus = true
                var allErrors = ""
                
                for entry in products.children
                {
                    if(entry.element!.name == "getAllProductsReturn")
                    {
                        var _product_name = self.empty_string
                        var _product_code = self.empty_string
                        var break_now = false
                        for attr in entry.children
                        {
                            if(attr.element!.name == "internalServiceName")
                            {
                                _product_name = attr.element!.text
                                if(break_now)
                                {
                                    break
                                }
                                else
                                {
                                    break_now = true
                                }
                            }
                            if(attr.element!.name == "serviceCode")
                            {
                                _product_code = attr.element!.text
                                if(break_now)
                                {
                                    break
                                }
                                else
                                {
                                    break_now = true
                                }
                            }
                        }
                        
                        if(!(_product_name.isEmpty))
                        {
                            CoreDataHelper.sharedInstance.add_product(&_err_msg, name: _product_name, code: _product_code)
                            
                            if(_err_msg != nil)
                            {
                                //Failed....
                                successStatus = false
                                allErrors = allErrors + _err_msg!
                                
//                                DispatchQueue.main.async(execute: {
//                                    
//                                    UIHelper.sharedInstance.showError(_err_msg!)
//                                })
                            }
                        }
                    }
                }
                
                onComplete(successStatus, allErrors)
                return;
            }
            
            onComplete(false, self.untraceableErrorString)

        }
        
    }
    
    
    
    func getCountriesFromEIX(onComplete : @escaping ((Bool, String) -> Void)) {
    
        
        let url = "https://dcp.colt.net/dgateway/services/ConfigService"
        let body = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dgat=\"http://dgateway2.web.colt.net\"> <soapenv:Header/> <soapenv:Body> <dgat:getAllCountries/> </soapenv:Body> </soapenv:Envelope>"
        
        CommonDataHelper.sharedInstance.country_list = NSMutableArray()
        
        WebHelper.sharedInstance.callDirectService(url, body: body, soapAction: "") { (err_msg, resp) in
            
            var _err_msg : String?
            
            if(err_msg != nil)
            {
                onComplete(false, err_msg!) //Failed....
                return
//                DispatchQueue.main.async(execute: {
//                    UIHelper.sharedInstance.showError(err_msg!)
//                })
            }
            
            if(resp != nil)
            {
                    let xml = SWXMLHash.parse(resp!)
                    
                    let countries = xml["soapenv:Envelope"]["soapenv:Body"]["getAllCountriesResponse"]
                
                    var successStatus = true
                    var allErrors = ""
                
                    for entry in countries.children
                    {
                            if(entry.element!.name == "getAllCountriesReturn")
                            {
                                    var _country_name = self.empty_string
                                    var _isocode = self.empty_string
                                    var break_now = false
                                    for attr in entry.children
                                    {
                                        if(attr.element!.name == "countryIsoCode")
                                        {
                                            _isocode = attr.element!.text
                                            if(break_now)
                                            {
                                                break
                                            }
                                            else
                                            {
                                                break_now = true
                                            }
                                        }
                                        if(attr.element!.name == "countryName")
                                        {
                                            _country_name = attr.element!.text
                                            if(break_now)
                                            {
                                                break
                                            }
                                            else
                                            {
                                                break_now = true
                                            }
                                        }
                                    }
                                
                                    if(!(_country_name.isEmpty))
                                    {
                                       CoreDataHelper.sharedInstance.add_country(&_err_msg, name: _country_name, isocode: _isocode)
                                        
                                        if(_err_msg != nil)
                                        {
//                                            DispatchQueue.main.async(execute: {
//                                                
//                                                UIHelper.sharedInstance.showError(_err_msg!)
//                                            })
                                            //Failed....
                                            successStatus = false
                                            allErrors = allErrors + _err_msg!
                                            
                                        }
                                    }
                            }
                    }
                    onComplete(successStatus, allErrors)
                    return;
            }
            onComplete(false, self.untraceableErrorString)
            
        }
                
    }
    
}
