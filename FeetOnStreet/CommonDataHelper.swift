//
//  CommonDataHelper.swift
//  FeetOnStreet
//
//  Created by admin on 7/28/17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation


class CommonDataHelper : NSObject {

    static var sharedInstance = CommonDataHelper()
    
    var search_type : String?
    
    var onnet_results : NSMutableArray = NSMutableArray()
    
    var olo_results : NSMutableDictionary = NSMutableDictionary()
    var olo_pointids : NSMutableArray = NSMutableArray()
    
    var dsl_results : NSMutableDictionary = NSMutableDictionary()
    var dsl_pointids : NSMutableArray = NSMutableArray()
    var dsl_check_results : NSMutableArray = NSMutableArray()
    
    var latitude : String?
    var longitude : String?
    
    //var asset_data : NSMutableArray = NSMutableArray()
    var asset_sites : NSMutableSet = NSMutableSet()
    var asset_customer : NSMutableSet = NSMutableSet()
    var asset_circuit : NSMutableSet = NSMutableSet()
    var asset_equipment : NSMutableSet = NSMutableSet()
    var asset_building_id : String?
    var asset_building_status: String?
    var asset_building_category: String?
    
    var npc : String?
    var formattedAddress : String?
    
    var do_not_show_loading : Bool = false
    
    var folder_list : [Dictionary<String,String>] = []
    var breadcrumb : [String] = []
    var breadcrumb_text : String = "ROOT"
    var current_folder : String?
        
    var search_in_progress_on : Bool = false
    var search_in_progress_off : Bool = false
    var search_in_progress_olo : Bool = false
    var search_in_progress_asset : Bool = false
    var search_in_progress_hublist : Bool = false
    var search_in_progress_price : Bool = false
    
    // Config
    
    var country_list : NSMutableArray = NSMutableArray()
    var product_list : NSMutableArray = NSMutableArray()
    var bandwidth_list : NSMutableArray = NSMutableArray()
    
    var selected_product : String?
    var bandwidth_list_for_product : [String]?
    
//    var common_vc : UIViewController?
    
    
    // Hub list
    
    var hub_list: NSMutableArray = NSMutableArray()
    
    // Price Check
    
    var selected_bandwidth: String?
    
    var bend_list: Dictionary<String,String> = Dictionary<String,String>()
    var bend_status: Dictionary<String,String> = Dictionary<String,String>()
    var aend_list: Dictionary<String,String> = Dictionary<String,String>()
    var aend_icon: String?
    
     // Price Details
    var price_results: NSMutableArray = NSMutableArray()
    
    
     // EMF/DSL Details
    
    var eMF_DSL_results: NSMutableArray = NSMutableArray()
    
    
    // OLO Details
    
    var oLO_results: NSMutableArray = NSMutableArray()
    
    // Selected Product
    
    var selectedProduct: String?
    
    var selectedProductCode: String?
    
    // Selected product type
    
    var selectedBandwidth: String?
    
    //employeeType
    
    var employeeType: String?
    
    // Offnet EFM btn serlected
    
    var efmSelected:Bool = false
  
    // Offnet OLO btn serlected
    
    var oLOSelected:Bool = false
    
    // price list for ONNET
    var isOnnetPrice:Bool = true

    // popup EMF OLO
    var isPopUpEFM_OLO:Bool = false
    
    // popup Alert 1st time
    var isAlertShown:Bool = false
    
     // For Nearnet marker click
    var isNearnetMarkerClicked = false
    
    // For Save Search
    var saveSearchPermisesNumber: String?
    var saveSearchStreetName: String?
    var saveSearchCityName: String?
    var saveSearchPostCode: String?
    var saveSearchPoint_ID: String?
    var saveSearchColtOperatingCountry: String?
    
    var isFromSavedsearch = false
    
    var savedSearchModelList:SavedSearchModel?
    
    // To show offnet popup list comparing
    var isLongPressedOnMap = false
    
    // Using while saving data in common heleper for saved search addres list
    var savedSearchAddresONNETModel:ActiveAndInactiveBuildings?
    
    
    
    
}
