//
//  CoreDataHelper.swift
//  FeetOnStreet
//
//  Created by admin on 7/28/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CoreData
import SwiftOverlays

var productsModelArray:[ProductsModel]?

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


class CoreDataHelper: NSObject {
    
    static var sharedInstance = CoreDataHelper()
    var managedContext: NSManagedObjectContext?
    var entityDesc: NSEntityDescription?
    var entityCountries: NSEntityDescription?
    var entityProducts: NSEntityDescription?
    var entityBandwidth: NSEntityDescription?
    var entityProductBandwidth: NSEntityDescription?
    var entityUserCrentials: NSEntityDescription?
    var entityEmployeeType: NSEntityDescription?
    
    fileprivate override init() {
        
        if #available(iOS 10.0, *) {
            self.managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        } else {
            self.managedContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
        }
        self.entityDesc = NSEntityDescription.entity(forEntityName: "Config", in: managedContext!)!
        self.entityCountries = NSEntityDescription.entity(forEntityName: "Countries", in: managedContext!)!
        self.entityProducts = NSEntityDescription.entity(forEntityName: "Products", in: managedContext!)!
        self.entityBandwidth = NSEntityDescription.entity(forEntityName: "Bandwidth", in: managedContext!)!
        self.entityProductBandwidth = NSEntityDescription.entity(forEntityName: "ProductBandwidth", in: managedContext!)!
        self.entityUserCrentials = NSEntityDescription.entity(forEntityName: "UserCredentials", in: managedContext!)!
        self.entityEmployeeType = NSEntityDescription.entity(forEntityName: "EmployeeType", in: managedContext!)!
    }
    
    
    
    func add_user_Credentials(_ err_msg: inout String?, username: String, password: String) {
        
        do
        {
            let c = NSManagedObject(entity: self.entityUserCrentials!,insertInto: managedContext)
            c.setValue(username, forKey: "username");
            c.setValue(password, forKey: "password")
            
            try managedContext?.save()
            
            err_msg = nil
        }
        catch let error as NSError
        {
            err_msg = error.description
        }
    }
    
    
    func getUserCrentials() -> [String]? {
        
        do
        {
            var list = [String]()
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = self.entityUserCrentials
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count < 1)
            {
                throw(NSError(domain: "No userDetails found.", code: -4, userInfo: nil))
            }
            
            for row in results! {
                
                list.append((row as! NSManagedObject).value(forKey: "username") as! String)
                
            }
            
            return list
        }
        catch _ as NSError
        {
            return nil
        }
    }
    
    
    
    
    func is_static_data_populated() -> Bool {
        
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = self.entityCountries
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count < 1)
            {
                throw(NSError(domain: "static data not found.", code: -4, userInfo: nil))
            }
            
            return true
        }
        catch _ as NSError
        {
            return false
        }
    }
    
    
    func clear_all_static_data() -> Bool {
        
        if(clear_static_data_countries())
        {
            if(clear_static_data_products())
            {
                if(clear_static_data_product_bandwidth())
                {
                    
                    if(clear_static_data_config()){
                        
                        return(clear_static_data_bandwidth())
                        
                    }else{
                        
                        return false
                    }
                }
                else
                {
                    return false
                }
            }
            else
            {
                return false
            }
        }
        else
        {
            return false
        }
    }
    
    
    func clear_static_data_countries() -> Bool {
        
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = self.entityCountries
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count>0)
            {
                for row in results! {
                    
                    managedContext?.delete(row as! NSManagedObject)
                    
                }
                
                try managedContext?.save()
            }
            
            return true
        }
        catch _ as NSError
        {
            return false
        }
    }
    
    //    func clear_static_data_usercredientials() -> Bool {
    //
    //        do
    //        {
    //            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
    //            fetchRequest.entity = self.entityUserCrentials
    //
    //            let results = try managedContext?.fetch(fetchRequest)
    //
    //            if(results?.count>0)
    //            {
    //                for row in results! {
    //
    //                    managedContext?.delete(row as! NSManagedObject)
    //
    //                }
    //
    //                try managedContext?.save()
    //            }
    //
    //            return true
    //        }
    //        catch _ as NSError
    //        {
    //            return false
    //        }
    //    }
    //
    //    func clear_static_data_employeeType() -> Bool {
    //
    //        do
    //        {
    //            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
    //            fetchRequest.entity = self.entityEmployeeType
    //
    //            let results = try managedContext?.fetch(fetchRequest)
    //
    //            if(results?.count>0)
    //            {
    //                for row in results! {
    //
    //                    managedContext?.delete(row as! NSManagedObject)
    //
    //                }
    //
    //                try managedContext?.save()
    //            }
    //
    //            return true
    //        }
    //        catch _ as NSError
    //        {
    //            return false
    //        }
    //    }
    
    func clear_static_data_products() -> Bool {
        
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = self.entityProducts
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count>0)
            {
                for row in results! {
                    
                    managedContext?.delete(row as! NSManagedObject)
                    
                }
                
                try managedContext?.save()
            }
            
            return true
        }
        catch _ as NSError
        {
            return false
        }
    }
    
    
    func clear_static_data_bandwidth() -> Bool {
        
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = self.entityBandwidth
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count>0)
            {
                for row in results! {
                    
                    managedContext?.delete(row as! NSManagedObject)
                    
                }
                
                try managedContext?.save()
            }
            
            return true
        }
        catch _ as NSError
        {
            return false
        }
    }
    
    func clear_static_data_config() -> Bool {
        
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = self.entityDesc
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count>0)
            {
                for row in results! {
                    
                    managedContext?.delete(row as! NSManagedObject)
                    
                }
                
                try managedContext?.save()
            }
            
            return true
        }
        catch _ as NSError
        {
            return false
        }
    }
    
    
    func clear_static_data_product_bandwidth() -> Bool {
        
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = self.entityProductBandwidth
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count>0)
            {
                for row in results! {
                    
                    managedContext?.delete(row as! NSManagedObject)
                    
                }
                
                try managedContext?.save()
            }
            
            return true
        }
        catch _ as NSError
        {
            return false
        }
    }
    
    
    func add_country(_ err_msg: inout String?, name: String, isocode: String) {
        DispatchQueue.main.sync {
            
            do
            {
                let c = NSManagedObject(entity: self.entityCountries!,insertInto: managedContext)
                c.setValue(name, forKey: "name");
                c.setValue(isocode, forKey: "isocode")
                
                try managedContext?.save()
                
                err_msg = nil
            }
            catch let error as NSError
            {
                err_msg = error.description
            }
        }
    }
    
    
    func add_product(_ err_msg: inout String?, name: String, code: String) {
        DispatchQueue.main.sync {
            
            do
            {
                let c = NSManagedObject(entity: self.entityProducts!,insertInto: managedContext)
                c.setValue(name, forKey: "name");
                c.setValue(code, forKey: "code")
                
                try managedContext?.save()
                
                err_msg = nil
            }
            catch let error as NSError
            {
                err_msg = error.description
            }
        }
    }
    
    
    func add_bandwidth(_ err_msg: inout String?, name: String, sort_order: String) {
        DispatchQueue.main.sync {
            do
            {
                let c = NSManagedObject(entity: self.entityBandwidth!,insertInto: managedContext)
                c.setValue(sort_order, forKey: "sort_order");
                c.setValue(name, forKey: "name")
                
                try managedContext?.save()
                
                err_msg = nil
            }
            catch let error as NSError
            {
                err_msg = error.description
            }
        }
    }
    
    
    func add_product_bandwidth(_ err_msg: inout String?, product_name: String, bandwidth: String, sort_order: String) {
        DispatchQueue.main.sync {
            do
            {
                let c = NSManagedObject(entity: self.entityProductBandwidth!,insertInto: managedContext)
                c.setValue(product_name, forKey: "product_name");
                c.setValue(bandwidth, forKey: "bandwidth")
                c.setValue(Int(sort_order), forKey: "sort_order")
                
                try managedContext?.save()
                
                err_msg = nil
            }
            catch let error as NSError
            {
                err_msg = error.description
            }
        }
    }
    func add_EmployeeType(_ err_msg: inout String?, employeeType: String) {
        DispatchQueue.main.sync {
            
            do
            {
                let c = NSManagedObject(entity: self.entityEmployeeType!,insertInto: managedContext)
                c.setValue(employeeType, forKey: "name");
                
                
                try managedContext?.save()
                
                err_msg = nil
            }
            catch let error as NSError
            {
                err_msg = error.description
            }
        }
    }
    
    
    
    func getAllProductBandwidth() -> [String]? {
        
        do
        {
            var list = [String]()
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = self.entityProductBandwidth
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count < 1)
            {
                throw(NSError(domain: "No bandwidth found.", code: -4, userInfo: nil))
            }
            
            for row in results! {
                
                list.append((row as! NSManagedObject).value(forKey: "bandwidth") as! String)
                
            }
            
            return list
        }
        catch _ as NSError
        {
            return nil
        }
    }
    
    
    
    func getAllCountries() -> [String]? {
        
        do
        {
            var list = [String]()
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = self.entityCountries
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count < 1)
            {
                throw(NSError(domain: "No country found.", code: -4, userInfo: nil))
            }
            
            for row in results! {
                
                list.append((row as! NSManagedObject).value(forKey: "name") as! String)
                
            }
            
            return list
        }
        catch _ as NSError
        {
            return nil
        }
    }
    
    
    func getAllProducts() -> [String]? {
        
        do
        {
            var list = [String]()
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = self.entityProducts
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count < 1)
            {
                throw(NSError(domain: "No product found.", code: -4, userInfo: nil))
            }
            
            for row in results! {
                
                list.append((row as! NSManagedObject).value(forKey: "name") as! String)
                
            }
            
            return list
        }
        catch _ as NSError
        {
            return nil
        }
    }
    
    func getAllProductsWithProductCode() -> [ProductsModel]? {
        
        do
        {
            
            var list = [String]()
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = self.entityProducts
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count < 1)
            {
                throw(NSError(domain: "No product found.", code: -4, userInfo: nil))
            }
            productsModelArray = ProductsModel.getProductListWithCode(results:results!)
            //            for row in results! {
            //
            //                list.append((row as! NSManagedObject).value(forKey: "name") as! String)
            //
            //            }
            
            return productsModelArray
        }
        catch _ as NSError
        {
            return nil
        }
    }
    
    func getAllBandwidths() -> [String]? {
        
        do
        {
            var list = [String]()
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = self.entityBandwidth
            
            let sortDescriptor = NSSortDescriptor(key: "sort_order", ascending: true)
            let sortDescriptors = [sortDescriptor]
            fetchRequest.sortDescriptors = sortDescriptors
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count < 1)
            {
                throw(NSError(domain: "No product found.", code: -4, userInfo: nil))
            }
            for row in results! {
                
                list.append((row as! NSManagedObject).value(forKey: "name") as! String)
                
            }
            
            return list
        }
        catch _ as NSError
        {
            return nil
        }
    }
    
    func getBandwidthForProduct(_ product_name: String) -> [String]? {
        
        do
        {
            var list = [String]()
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = self.entityProductBandwidth
            fetchRequest.predicate = NSPredicate(format: "product_name == %@", product_name)
            
            fetchRequest.resultType = NSFetchRequestResultType.dictionaryResultType
            fetchRequest.propertiesToFetch = ["bandwidth"]
            fetchRequest.returnsDistinctResults = true
            
            let sortDescriptor = NSSortDescriptor(key: "sort_order", ascending: true)
            let sortDescriptors = [sortDescriptor]
            fetchRequest.sortDescriptors = sortDescriptors
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count < 1)
            {
                throw(NSError(domain: "No bandwidth found.", code: -4, userInfo: nil))
            }
            
            for row in results! {
                
                //list.append((row as! NSManagedObject).valueForKey("bandwidth") as! String)
                list.append((row as! [String:String])["bandwidth"]!)
                
                
            }
            
            return list
        }
        catch _ as NSError
        {
            return nil
        }
    }
    func getEmployeeType() -> [String]? {
        
        do
        {
            var list = [String]()
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = self.entityEmployeeType
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count < 1)
            {
                throw(NSError(domain: "No employeeType found.", code: -4, userInfo: nil))
            }
            for row in results! {
                
                list.append((row as! [String:String])["employeeType"]!)
            }
            return list
        }
        catch _ as NSError
        {
            return nil
        }
    }
    
    
    
    
    /*func purge_product_data() -> Bool {
     
     do
     {
     let fetchRequest = NSFetchRequest()
     fetchRequest.entity = productDesc
     
     let results = try managedContext?.executeFetchRequest(fetchRequest)
     
     if(results?.count>0)
     {
     for row in results! {
     
     managedContext?.deleteObject(row as! NSManagedObject)
     
     }
     
     try managedContext?.save()
     }
     
     return true
     }
     catch _ as NSError
     {
     return false
     }
     }
     
     
     func purge_onnet_data() -> Bool {
     
     do
     {
     purge_product_data()
     
     let fetchRequest = NSFetchRequest()
     fetchRequest.entity = onnetDesc
     
     let results = try managedContext?.executeFetchRequest(fetchRequest)
     
     if(results?.count>0)
     {
     for row in results! {
     
     managedContext?.deleteObject(row as! NSManagedObject)
     
     }
     
     try managedContext?.save()
     }
     
     return true
     }
     catch _ as NSError
     {
     return false
     }
     }
     
     
     func get_onnet_data(inout err_msg: String?) -> NSMutableArray {
     
     let results:NSMutableArray = NSMutableArray()
     
     do
     {
     let fetchRequest = NSFetchRequest()
     fetchRequest.entity = onnetDesc
     
     let rows = try managedContext?.executeFetchRequest(fetchRequest)
     
     for row in rows!
     {
     var data = Dictionary<String,String>()
     data.updateValue(row, forKey: <#T##Hashable#>)
     results.addObject(row)
     }
     }
     catch let error as NSError
     {
     err_msg = error.description
     }
     
     return (results)
     }
     
     
     func add_onnet_data(building_status:String, colt_operating_country:String, english_city_name:String, postcode:String, premise_number:String, building_name: String, street_name:String, latitude: String, longitude:String, building_connectivity_type: String, distance:String, colt_product:[String],point_id: String, floor_of_cea:String, cea:String, cea_floor_text:String, in_house_cable:String, dual_entry:String) -> Bool {
     
     do
     {
     let entry = NSManagedObject(entity: onnetDesc!,insertIntoManagedObjectContext: managedContext)
     
     entry.setValue(building_status, forKey: "building_status");
     entry.setValue(colt_operating_country, forKey: "colt_operating_country");
     entry.setValue(english_city_name, forKey: "english_city_name");
     entry.setValue(postcode, forKey: "postcode");
     entry.setValue(premise_number, forKey: "premise_number");
     entry.setValue(building_name, forKey: "building_name");
     entry.setValue(street_name, forKey: "street_name");
     entry.setValue(latitude, forKey: "latitude");
     entry.setValue(longitude, forKey: "longitude");
     entry.setValue(building_connectivity_type, forKey: "building_connectivity_type");
     entry.setValue(distance, forKey: "distance");
     entry.setValue(point_id, forKey: "point_id");
     entry.setValue(floor_of_cea, forKey: "floor_of_cea");
     entry.setValue(cea, forKey: "cea");
     
     entry.setValue(cea_floor_text, forKey: "cea_floor_text");
     entry.setValue(in_house_cable, forKey: "in_house_cable");
     entry.setValue(dual_entry, forKey: "dual_entry");
     
     
     for product in colt_product {
     
     let product_entry = NSManagedObject(entity: productDesc!, insertIntoManagedObjectContext: managedContext)
     
     product_entry.setValue(point_id, forKey: "point_id")
     product_entry.setValue(product, forKey: "colt_product")
     }
     
     try managedContext?.save()
     
     return true
     }
     catch _ as NSError
     {
     return false
     }
     
     }*/
    
    
    func purgeData(_ err_msg: inout String?) -> Bool {
        
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = entityDesc
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count>0)
            {
                for row in results! {
                    
                    managedContext?.delete(row as! NSManagedObject)
                    
                }
                
                try managedContext?.save()
            }
            
            return true
        }
        catch let error as NSError
        {
            err_msg = error.description
            return false
        }
        
    }
    
    
    func addToConfig(_ err_msg: inout String?, key: String, value: String, viewController: UIViewController ) -> Bool {
     
        do
        {
            let no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
            let appDel = UIApplication.shared.delegate as! AppDelegate
            if appDel.networkStatus! == NetworkStatus.NotReachable {
            DispatchQueue.main.async {
                SwiftOverlays.removeAllBlockingOverlays()
                UIHelper.sharedInstance.showReachabilityWarning(no_internet_connection, _onViewController: viewController)
             }
            }else{
                var config = findKey(&err_msg, key: key)
                
                if(config == nil)
                {
                config = NSManagedObject(entity: entityDesc!,insertInto: managedContext)
                config!.setValue(key, forKey: "key");
                config!.setValue(value, forKey: "value")
                  
                }
                else
                {
                    config!.setValue(key, forKey: "key");
                    config!.setValue(value, forKey: "value")
                }
                
                try managedContext?.save()
                
                err_msg = nil
         }
            return true
        }
        catch let error as NSError
        {
            err_msg = error.description
            return false
        }
    }

    func appendToConfig(_ err_msg: inout String?, key: String, value: String) -> Bool {
        
        do
        {
            let config = NSManagedObject(entity: entityDesc!,insertInto: managedContext)
            config.setValue(key, forKey: "key");
            config.setValue(value, forKey: "value")
            
            try managedContext?.save()
            
            err_msg = nil
            
            return true
        }
        catch let error as NSError
        {
            err_msg = error.description
            return false
        }
        
    }

    
    func deleteAllFromConfig(_ err_msg: inout String?, key: String) -> Bool {
        
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.predicate = NSPredicate(format: "key == %@", key)
            fetchRequest.entity = entityDesc
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count>0)
            {
                for row in results! {
                    
                    managedContext?.delete(row as! NSManagedObject)
                }
                
                try managedContext?.save()
            }
            
            return true
        }
        catch let error as NSError
        {
            err_msg = error.description
            return false
        }
    }
    
    
    func deleteFromConfig(_ err_msg: inout String?, key: String) -> Bool {
        
        do
        {
            let config = findKey(&err_msg, key: key)
            
            if(config == nil)
            {
                if(err_msg == nil)
                {
                    throw(NSError(domain: "Key not found.", code: -2, userInfo: nil))
                }
                else
                {
                    throw(NSError(domain: err_msg!, code: -3, userInfo: nil))
                }
            }
            else
            {
                managedContext?.delete(config!)
            }
            
            try managedContext?.save()
            
            return true
        }
        catch let error as NSError
        {
            err_msg = error.description
            return false
        }
        
    }
    
    
    func getKeyValue(_ err_msg: inout String?, key: String) -> String? {
        
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.predicate = NSPredicate(format: "key == %@", key)
            fetchRequest.entity = entityDesc
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count < 1)
            {
                throw(NSError(domain: "Key not found.", code: -4, userInfo: nil))
            }
            
            err_msg = nil
            
            return (results![0] as! NSManagedObject).value(forKey: "value") as? String
        }
        catch let error as NSError
        {
            err_msg = error.description
            return nil
        }
    }
    
    
    
    func getKeyValues(_ err_msg: inout String?, key: String) -> [String]? {
        
        do
        {
            var list = [String]()
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.predicate = NSPredicate(format: "key == %@", key)
            fetchRequest.entity = entityDesc
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count < 1)
            {
                throw(NSError(domain: "Key not found.", code: -4, userInfo: nil))
            }
            
            for row in results! {
                
                list.append((row as! NSManagedObject).value(forKey: "value") as! String)
                
            }
            
            err_msg = nil
            
            return list
        }
        catch let error as NSError
        {
            err_msg = error.description
            return nil
        }
    }
    
    
    
    func findKey(_ err_msg: inout String?, key: String) -> NSManagedObject? {
        
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.predicate = NSPredicate(format: "key == %@", key)
            fetchRequest.entity = entityDesc
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count < 1)
            {
                throw(NSError(domain: "Key not found.", code: -1, userInfo: nil))
            }
            
            err_msg = nil
            
            return(results![0] as! NSManagedObject)
        }
        catch let error as NSError
        {
            err_msg = error.description
            return nil
        }
    }
    
    
    func clearForm(_ err_msg: inout String?) -> Bool {
        
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.predicate = NSPredicate(format: "key beginswith %@", "form_")
            fetchRequest.entity = entityDesc
            
            let results = try managedContext?.fetch(fetchRequest)
            
            if(results?.count>0)
            {
                for row in results! {
                    
                    managedContext?.delete(row as! NSManagedObject)
                    
                }
                
                try managedContext?.save()
            }
            
            return true
        }
        catch let error as NSError
        {
            err_msg = error.description
            return false
        }
    }
    
}

