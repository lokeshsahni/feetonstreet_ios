//
//  Fos_Popup.swift
//  FeetOnStreet
//
//  Created by Suresh Murugaiyan on 8/22/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

protocol PopupDelegate{
    func dismissPopup(selectionType:String,popupFos:Fos_Popup)
}

class Fos_Popup: UIView,UITableViewDataSource,UITableViewDelegate,GMSAutocompleteViewControllerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var constrintTopDropdown: NSLayoutConstraint!
    @IBOutlet weak var constrintHeightDropDown: NSLayoutConstraint!
    @IBOutlet weak var tbl_dropDown: UITableView!
    @IBOutlet weak var constriantHeightPopup: NSLayoutConstraint!
    @IBOutlet weak var tbl_Popup: UITableView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var viewTap: UIView!
    
    var cellsForPopup:[Any]?
    var cellsType:[PopupcellTypes]?
    var delegate:PopupDelegate?
    var popupFosDetails:PopupDetails?
    var productOrBandWidth_List:[ProductsModel]?
    var productOrBandWidth_List_Nearnet:[ProductsModel]?
    var selectedButton:UIButton?
    var isSelectedProductType:Bool?
    var bandWidths:[String]?
//    var selectedProductType,selectedBandWidth:String?
    var hubList:[Dictionary<String, String>]?
    var bEndAddressTxtFld:UITextField?
    var abArray:[ActiveAndInactiveBuildings]?
    var aEndAddress_id:String?
    var productName:String?
    var bandwidtName:String?
    
    
    
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    class func initWithPopupElements( cells:[PopupcellTypes],popupDetails:PopupDetails) -> Fos_Popup {
        
        var fosPopup = Bundle.main.loadNibNamed("Fos_Popup", owner: self, options: nil)?[0] as! Fos_Popup
        fosPopup.frame=UIScreen.main.bounds
        fosPopup.popupFosDetails=popupDetails
        fosPopup.tbl_dropDown.isHidden=true
        fosPopup.isSelectedProductType=false
        fosPopup=fosPopup .reloadWithCells(cells: cells, popupDetails: popupDetails, fosPopup: fosPopup)
        fosPopup.popupFosDetails?.tapGes=UITapGestureRecognizer(target: fosPopup, action: #selector(viewTappedInPopup))
        fosPopup.popupFosDetails?.tapGes?.numberOfTouchesRequired=1
        fosPopup.popupFosDetails?.tapGes?.numberOfTapsRequired=1
        return fosPopup
    }
    
    func reloadWithCells(cells:[PopupcellTypes],popupDetails:PopupDetails,fosPopup:Fos_Popup) -> Fos_Popup {
        var cellsToTable=[UITableViewCell]()
        var viewHeight:CGFloat = 0
        fosPopup.cellsType=cells
        for cellType in cells {
            
            if cellType == PopupcellTypes.AddressCell {
                let aCell = Bundle.main.loadNibNamed("AddressCell", owner: self, options: nil)?[0] as! AddressCell
                aCell.lbl_address.text=popupDetails.locationAddress as String?
                cellsToTable.append(aCell)
                viewHeight=viewHeight+PopupCellHeight.AddressCellHeight
            }else if cellType == PopupcellTypes.CheckPriceCell {
                
                let aCell = Bundle.main.loadNibNamed("CheckPriceCell", owner: self, options: nil)?[0] as! CheckPriceCell
                var err_msg : String?
                let tempKey = UserDefaults.standard.value(forKey: "eType")
                let employeeType =  CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: tempKey as! String)
                
                if employeeType == "Non Employee" {
                    aCell.btn_BuildingDetail.backgroundColor = UIColor.lightGray
                    aCell.btn_BuildingDetail.isUserInteractionEnabled = false
                    aCell.btn_CheckPrice.addTarget(fosPopup, action: #selector(checkPriceAction), for: .touchUpInside)
                }else{
                    aCell.btn_BuildingDetail.addTarget(fosPopup, action: #selector(buildingDetailAction), for: .touchUpInside)
                    aCell.btn_CheckPrice.addTarget(fosPopup, action: #selector(checkPriceAction), for: .touchUpInside)
                }
                if popupDetails.isBuildingDetailEnable == false {
                    aCell.btn_BuildingDetail.isEnabled=popupDetails.isBuildingDetailEnable
                    aCell.btn_BuildingDetail.backgroundColor = UIColor.lightGray
                }
                cellsToTable.append(aCell)
                viewHeight=viewHeight+PopupCellHeight.CheckPriceCellHeight
                
            }else if cellType == PopupcellTypes.CheckOffnetConnectivityCell {
                let aCell = Bundle.main.loadNibNamed("CheckOffnetConnectivityCell", owner: self, options: nil)?[0] as! CheckOffnetConnectivityCell
                aCell.checkOffnetConnectivityBtn.addTarget(fosPopup, action: #selector(checkOffenetConnectivty_Action(sender:)), for: UIControlEvents.touchUpInside)
                cellsToTable.append(aCell)
                viewHeight=viewHeight+PopupCellHeight.CheckOffnetConnectivityCellHeight
            }else if cellType == PopupcellTypes.CancelCell {
                let aCell = Bundle.main.loadNibNamed("CancelCell", owner: self, options: nil)?[0] as! CancelCell
                cellsToTable.append(aCell)
                viewHeight=viewHeight+PopupCellHeight.CancelCellHeight
                aCell.btn_Cancel.addTarget(fosPopup, action: #selector(cancelPopup), for: UIControlEvents.touchUpInside)
            }else if cellType == PopupcellTypes.ProductCell {
                let aCell = Bundle.main.loadNibNamed("ProductCell", owner: self, options: nil)?[0] as! ProductCell
                aCell.btn_Product.layer.borderWidth=1.0
                aCell.btn_Product.layer.borderColor=UIColor.black.cgColor
                if CommonDataHelper.sharedInstance.selectedProduct != nil {
                    productName = CommonDataHelper.sharedInstance.selectedProduct
                    if productName == "Select Product" {
                      let selectProduct = NSLocalizedString("Select Product", comment: "")
                        productName = selectProduct
                    }
                    setProductNames()
                   aCell.btn_Product.setTitle(productName, for: .normal)
                }
                aCell.btn_Product.addTarget(fosPopup, action: #selector(showProductList(sender:)), for: UIControlEvents.touchUpInside)
                
                if popupFosDetails?.pType == PopupType.MapviewPopup3 {
                    aCell.btn_Product.setTitle("    \((popupFosDetails?.selectedProductType)!)", for: .normal)
                }else if popupFosDetails?.pType == PopupType.MapviewPopup4 {
                    aCell.btn_Product.setTitle("Colt Ethernet Line", for: .normal)
                }else if popupFosDetails?.pType == PopupType.MapviewPopup2 {
                    if popupDetails.productName != nil {
                        aCell.btn_Product.setTitle("\((popupDetails.productName)!)", for: .normal)
                    }
                }
                cellsToTable.append(aCell)
                viewHeight=viewHeight+PopupCellHeight.ProductCellHeight
                if popupFosDetails?.isPopupMutable == false {
                    aCell.btn_Product.isEnabled=false
                    aCell.btn_Product.setTitle("\((popupFosDetails?.selectedProductType)!)", for: .normal)
                    aCell.btn_Product.setTitleColor(UIColor.darkGray, for: .normal)
                }
            }else if cellType == PopupcellTypes.BandWidthCell {
                let aCell = Bundle.main.loadNibNamed("BandWidthCell", owner: self, options: nil)?[0] as! BandWidthCell
                if CommonDataHelper.sharedInstance.selectedBandwidth != nil {
                    bandwidtName = CommonDataHelper.sharedInstance.selectedBandwidth
                    if bandwidtName == "Select Bandwidth" {
                        let selectBandwidth = NSLocalizedString("Select Product", comment: "")
                        bandwidtName = selectBandwidth
                    }
                    aCell.btn_Bandwidth.setTitle(bandwidtName, for: .normal)
                }
                if popupDetails.productName == "Colt SIP Trunking" {
                    let voiceChannels = NSLocalizedString("Voice Channels", comment: "")
                    aCell.btn_Bandwidth.setTitle(voiceChannels, for: .normal)
                    aCell.lbl_ProductOrVoiceChannel.text=voiceChannels
                }
                
                aCell.btn_Bandwidth.layer.borderWidth=1.0
                aCell.btn_Bandwidth.layer.borderColor=UIColor.black.cgColor
                aCell.btn_Bandwidth.addTarget(fosPopup, action: #selector(showBandWidthList(sender:)), for: UIControlEvents.touchUpInside)
                aCell.btn_Bandwidth.tag=5
                cellsToTable.append(aCell)
                viewHeight=viewHeight+PopupCellHeight.BandWidthCellHeight
                if popupFosDetails?.isPopupMutable == false {
                    aCell.btn_Bandwidth.isEnabled=false
                    aCell.btn_Bandwidth.setTitle("\((popupFosDetails?.selectedBandWidth)!)", for: .normal)
                    aCell.btn_Bandwidth.setTitleColor(UIColor.darkGray, for: .normal)
                }
            }else if cellType == PopupcellTypes.HubCell {
                let aCell = Bundle.main.loadNibNamed("HubCell", owner: self, options: nil)?[0] as! HubCell
                aCell.view_Outline.layer.borderWidth=1.0
                aCell.view_Outline.layer.borderColor=UIColor.black.cgColor
                aCell.btn_SelectHub.layer.borderWidth=1.0
                aCell.btn_SelectHub.layer.borderColor=UIColor.black.cgColor
                aCell.btn_SelectHub.addTarget(fosPopup, action: #selector(showHubList(sender:)), for: UIControlEvents.touchUpInside)
                cellsToTable.append(aCell)
                viewHeight=viewHeight+PopupCellHeight.HubCellHeight
            }else if cellType == PopupcellTypes.BEndAddressCell {
                let aCell = Bundle.main.loadNibNamed("BEndAddressCell", owner: self, options: nil)?[0] as! BEndAddressCell
                aCell.txtFld_BendAddress.layer.borderWidth=1.0
                aCell.txtFld_BendAddress.layer.borderColor=UIColor.black.cgColor
                aCell.view_selectedeBendAddress.layer.borderWidth=1.0
                aCell.view_selectedeBendAddress.layer.borderColor=UIColor.black.cgColor
                aCell.btn_selectEfm.addTarget(fosPopup, action: #selector(selectEfmAction(sender:)), for: UIControlEvents.touchUpInside)
                aCell.btn_Olo.addTarget(fosPopup, action: #selector(selectOloAction(sender:)), for: UIControlEvents.touchUpInside)
                aCell.btn_search.addTarget(fosPopup, action: #selector(searchEfmAndOlo(sender:)), for: UIControlEvents.touchUpInside)
                aCell.btn_Onnet.addTarget(fosPopup, action: #selector(onnetAction(sender:)), for: UIControlEvents.touchUpInside)
                aCell.btn_Onnet.isHidden=true
                aCell.lbl_supplierName.isHidden=true
                aCell.lbl_accessTyp.isHidden=true
                aCell.txtFld_BendAddress.delegate=fosPopup
                cellsToTable.append(aCell)
                viewHeight=viewHeight+PopupCellHeight.BEndAddressCellHeight
            }else if cellType == PopupcellTypes.LocationCell {
                let aCell = Bundle.main.loadNibNamed("LocationCell", owner: self, options: nil)?[0] as! LocationCell
                aCell.lbl_latitude.layer.borderWidth=1.0
                aCell.lbl_latitude.layer.borderColor=UIColor.black.cgColor
                aCell.lbl_longitude.layer.borderWidth=1.0
                aCell.lbl_longitude.layer.borderColor=UIColor.black.cgColor
                aCell.lbl_latitude.text="  \((popupFosDetails?.lattitudeLocation)!)"
                aCell.lbl_longitude.text="  \((popupFosDetails?.longitudeLocation)!)"
                cellsToTable.append(aCell)
                viewHeight=viewHeight+PopupCellHeight.LocationCellHeight
            }else if cellType == PopupcellTypes.EmfDslCell {
                let aCell = Bundle.main.loadNibNamed("EfmDslCell", owner: self, options: nil)?[0] as! EfmDslCell
                if CommonDataHelper.sharedInstance.selectedProduct == "Colt Private Ethernet" || CommonDataHelper.sharedInstance.selectedProduct == "Colt Wave" {
                    aCell.efm_OloSubView.isHidden = true
                }else{
                    
                }
                if CommonDataHelper.sharedInstance.efmSelected == true {
                   aCell.btn_efm.setImage(UIImage(named:"ic_check_box"), for: .normal)
                   aCell.btn_efm.isSelected = true
                   popupFosDetails?.isEfmSelected = true
                }
                if CommonDataHelper.sharedInstance.oLOSelected == true {
                    aCell.btn_olo.setImage(UIImage(named:"ic_check_box"), for: .normal)
                    aCell.btn_olo.isSelected = true
                    popupFosDetails?.isOloSelected=true

                }
                
                aCell.btn_efm.addTarget(fosPopup, action: #selector(selectEfmAction(sender:)), for: UIControlEvents.touchUpInside)
                aCell.btn_olo.addTarget(fosPopup, action: #selector(selectOloAction(sender:)), for: UIControlEvents.touchUpInside)
                cellsToTable.append(aCell)
                viewHeight=viewHeight+PopupCellHeight.EmfDslCellHeight
            }else if cellType == PopupcellTypes.SearchAndCancelCell {
                let aCell = Bundle.main.loadNibNamed("SearchAndCancelCell", owner: self, options: nil)?[0] as! SearchAndCancelCell
                aCell.btn_search.addTarget(fosPopup, action: #selector(searchBuildingAction), for: UIControlEvents.touchUpInside)

//                if CommonDataHelper.sharedInstance.selectedProduct == "Colt Private Ethernet" || CommonDataHelper.sharedInstance.selectedProduct == "Colt Wave" {
//                    aCell.btn_search.backgroundColor = UIColor.gray
//                }else{
//                    aCell.btn_search.backgroundColor = UIColor(red: 0/255, green: 165/255, blue: 155/255, alpha: 1.0)
                    
//                }
                
                aCell.btn_cancel.addTarget(fosPopup, action: #selector(cancelPopup), for: UIControlEvents.touchUpInside)
                cellsToTable.append(aCell)
                viewHeight=viewHeight+PopupCellHeight.SearchAndCancelCellHeight
            }
        }
        fosPopup.cellsForPopup=cellsToTable
        //        fosPopup.constriantHeightPopup.constant=viewHeight+CGFloat(16.0)
        let tempHeight = UIScreen.main.bounds.size.height-40.0
        if viewHeight+40 >= UIScreen.main.bounds.size.height {
            fosPopup.constriantHeightPopup.constant=tempHeight
        }else{
            fosPopup.constriantHeightPopup.constant=viewHeight+CGFloat(16.0)
        }
        
        if viewHeight>tempHeight {  // Change for scrolling
            fosPopup.tbl_Popup.isScrollEnabled=true
        }else{
            fosPopup.tbl_Popup.isScrollEnabled=false
        }
        return fosPopup
    }
    
    func viewTappedInPopup () {
        tbl_dropDown.isHidden=true
    }
    func setProductNames() {
        if productName == "Colt VoIP Access" {
            productName = "Colt SIP Trunking"
        }else if productName == "Colt LANLink Point to Point (Ethernet Point to Point)" {
            productName = "Colt Ethernet Line"
            
        }else if productName == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
            productName = "Colt Ethernet Hub and Spoke"
            
        }else if productName == "Colt Ethernet Private Network (EPN)"{
            productName = "ColT Ethernet VPN"
        }
    }

    func checkOffenetConnectivty_Action(sender:UIButton) {
        
        delegate?.dismissPopup(selectionType: "CheckOffnetConnectivity", popupFos: self)
        
        AnalyticsHelper().analyticsEventsAction(category: "Connectivity Checker", action: "connectivityChecker_offnetSearch_click", label: "Check offnet connectivity")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        bEndAddressTxtFld=textField
        let autocompleteController = GMSAutocompleteViewController()
        UINavigationBar.appearance().tintColor=UIColor.white
        //        UINavigationBar.appearance().barTintColor = UIColor(colorLiteralRed: 0, green: 165.0/255.0, blue: 156.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().barTintColor = UIColor.c_Teal
        let searchBarTextColor = [NSForegroundColorAttributeName:UIColor.white,NSFontAttributeName:UIFont.systemFont(ofSize: 15.0)] as [String : Any]
        if #available(iOS 9.0, *) {
            let sBar = UITextField.appearance(whenContainedInInstancesOf: [UINavigationBar.classForCoder() as! UIAppearanceContainer.Type])
            sBar.defaultTextAttributes=searchBarTextColor
        } else {
            //            let searchTextField: UITextField? = searchBar.value(forKey: "searchField") as? UITextField
            //            autocompleteController.
            //            if searchTextField!.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
            //                let attributeDict = [NSAttributedStringKey.foregroundColor: UIColor.white]
            //                searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search", attributes: attributeDict)
            //            }
        }
        
        autocompleteController.delegate = self
        //        sel(autocompleteController, animated: true, completion: nil)
        self.popupFosDetails?.popupController?.present(autocompleteController, animated: true, completion: nil)
        
        return false
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        bEndAddressTxtFld?.text=place.formattedAddress
        popupFosDetails?.bEndAddress=place.formattedAddress
        //        locaationSearch.text=place.formattedAddress
        //        let searchBarTextColor = [NSForegroundColorAttributeName:UIColor.black,NSFontAttributeName:UIFont.systemFont(ofSize: 15.0)] as [String : Any]
        //        let sBar = UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.classForCoder() as! UIAppearanceContainer.Type])
        //        sBar.defaultTextAttributes=searchBarTextColor
        //        getActiveAndInactiveBuildings(place: place)
        //        dismiss(animated: true, completion: nil)
        
        for item11 in place.addressComponents! {
            print(item11.name)
            print(item11.type)
            if item11.type == "street_number" {
                popupFosDetails?.permissionNumber = item11.name
            }else if item11.type == "route" {
                popupFosDetails?.streetName = item11.name
            }else if item11.type == "locality" {
                popupFosDetails?.cityName = item11.name
            }else if item11.type == "postal_code" {
                popupFosDetails?.postcode = item11.name
            }else if item11.type == "country" {
                popupFosDetails?.country = item11.name
            }
        }
        self.popupFosDetails?.popupController?.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        //        dismiss(animated: true, completion: nil)
        self.popupFosDetails?.popupController?.dismiss(animated: true, completion: nil)
    }
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func selectEfmAction(sender:UIButton)  {
        
        if sender.isSelected == false {
            sender.isSelected=true
            popupFosDetails?.isEfmSelected=true
            sender.setImage(UIImage(named:"ic_check_box"), for: .normal)
            CommonDataHelper.sharedInstance.efmSelected = true
        }else{
            popupFosDetails?.isEfmSelected=false
            sender.isSelected=false
            sender.setImage(UIImage(named:"ic_uncheck_box"), for: .normal)
            CommonDataHelper.sharedInstance.efmSelected = false
        }
    }
    
    func selectOloAction(sender:UIButton)  {
        if sender.isSelected == false {
            popupFosDetails?.isOloSelected=true
            sender.isSelected=true
            sender.setImage(UIImage(named:"ic_check_box"), for: .normal)
            CommonDataHelper.sharedInstance.oLOSelected = true
        }else{
            popupFosDetails?.isOloSelected=false
            sender.isSelected=false
            sender.setImage(UIImage(named:"ic_uncheck_box"), for: .normal)
            CommonDataHelper.sharedInstance.oLOSelected = false
        }
    }
    
    func searchEfmAndOlo(sender:UIButton)  {
        
        if popupFosDetails?.selectedProductType == nil {
            let pleaseSelectProduct = NSLocalizedString("Please select product", comment: "")
            UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: pleaseSelectProduct)
            return
        }
        if popupFosDetails?.selectedBandWidth == nil {
            let pleaseSelectbandwidth = NSLocalizedString("Please select bandwidth", comment: "")
            UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: pleaseSelectbandwidth)
            return
        }
        if bEndAddressTxtFld?.text == nil {
            
            let PleaseSelectBendAddress = NSLocalizedString("Please select B-End address", comment: "")
            UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: PleaseSelectBendAddress)
        }else{
            
            let appDel = UIApplication.shared.delegate as! AppDelegate
            
            if appDel.networkStatus! == NetworkStatus.NotReachable {
                
                DispatchQueue.main.async {
                    let noInternetConnection = NSLocalizedString("No_internet_connection", comment: "")
                    UIHelper.sharedInstance.showReachabilityWarning(noInternetConnection, _onViewController: (self.popupFosDetails?.popupController)!)
                }
                
            }else{
                popupFosDetails?.isBendAddressSearched=true
                UIHelper.sharedInstance.showLoading()
                popupFosDetails?.numberOfServiceToCall=(popupFosDetails?.numberOfServiceToCall)!+1
                if popupFosDetails?.isEfmSelected == true {
                    popupFosDetails?.numberOfServiceToCall=(popupFosDetails?.numberOfServiceToCall)!+1
                }
                if popupFosDetails?.isOloSelected == true{
                    popupFosDetails?.numberOfServiceToCall=(popupFosDetails?.numberOfServiceToCall)!+1
                }
                getActiveAndInactiveBuildings()
                NotificationCenter.default.addObserver(self, selector: #selector(hideLoadingAfterGettingAllRespone(notification:)), name: Notification.Name("getActiveAndInactiveBuildings"), object: nil)
                
                if popupFosDetails?.isEfmSelected == true {
                    
                    EmfDslService().getEFM_DSLdetails((popupFosDetails?.permissionNumber)!, streetName: (popupFosDetails?.streetName)!, cityTown: (popupFosDetails?.cityName)!, postCode: (popupFosDetails?.postcode)!, country: (popupFosDetails?.country)!, selectedProduct: (popupFosDetails?.selectedProductType!)!, selectedBandwidth: (popupFosDetails?.selectedBandWidth!)!, onComplete: {(Any) in
                        //TODO: Remove Notification and use this....
                    })
                    
                    NotificationCenter.default.addObserver(self, selector: #selector(hideLoadingAfterGettingAllRespone(notification:)), name: Notification.Name("EMF_DSL_data"), object: nil)//
                }
                
                if popupFosDetails?.isOloSelected == true{
                    OLOService().getOLOdetails((popupFosDetails?.permissionNumber)!, streetName: (popupFosDetails?.streetName)!, cityTown: (popupFosDetails?.cityName)!, postCode: (popupFosDetails?.postcode)!, country: (popupFosDetails?.country)!, selectedProduct: (popupFosDetails?.selectedProductType!)!, selectedBandwidth: (popupFosDetails?.selectedBandWidth!)!, latitude: "\((popupFosDetails?.lattitudeLocation)!)", longitude: "\((popupFosDetails?.longitudeLocation)!)", onComplete: {(Any) in
                        //TODO: Remove Notification and use this....
                    })
                    
                    NotificationCenter.default.addObserver(self, selector: #selector(hideLoadingAfterGettingAllRespone(notification:)), name: Notification.Name("OLO_data"), object: nil)
                }
            }
        }
    }
    
    func hideLoadingAfterGettingAllRespone(notification:Notification) {
        let message = notification.object as? String
        if message == "failure" {
            if popupFosDetails?.emfServerResponse == "failure" {
                popupFosDetails?.emfServerResponse="failure"
                UIHelper.sharedInstance.showReachabilityWarning("Server is not responing.Try again", _onViewController: (self.popupFosDetails?.popupController)!)
                DispatchQueue.main.async {
                    UIHelper.sharedInstance.hideLoading()
                }
            }
            
        }else{
            popupFosDetails?.numberOfResponeGot=(popupFosDetails?.numberOfResponeGot)!+1
            if popupFosDetails?.numberOfServiceToCall == popupFosDetails?.numberOfResponeGot {
                DispatchQueue.main.async {
                    UIHelper.sharedInstance.hideLoading()
                }
                print("got all response")
            }
        }
    }
    
    func getActiveAndInactiveBuildings() {
        
        let requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"> <soapenv:Body> <ax:checkConnectivityRequest ax:schemaVersion=\"3.0\" xmlns:ax=\"http://aat.colt.com/connectivityservice\"> <ax:sequenceId>1</ax:sequenceId> <ax:requestType>ALL</ax:requestType> <ax:requestMode> <ax:requestID>4495</ax:requestID> <ax:siteAddress> <ax:latitude>\((popupFosDetails?.lattitudeLocation)!)</ax:latitude> <ax:longitude>\((popupFosDetails?.longitudeLocation)!)</ax:longitude> <ax:radius>500</ax:radius> <ax:connectivityType>COLT Fibre</ax:connectivityType> </ax:siteAddress> </ax:requestMode> </ax:checkConnectivityRequest> </soapenv:Body> </soapenv:Envelope>"
        
        var errMsg:String?
        let tokenString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "token")
        let accountTypeString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "accountType")
        let urlString = "http://loncis01/checkConnectivity/connectivity.wsdl"
        let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
        let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
        let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/connectnew")!)
        request.httpMethod = "POST"
        let requestData = requestBody.data(using: String.Encoding.utf8)
        request.httpBody = requestData
        request.addValue("text/xml", forHTTPHeaderField: "Content-Type")
        request.addValue("radius_search", forHTTPHeaderField: "SOAPAction")
        request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
        request.addValue(base64String!, forHTTPHeaderField: "X-CONNECT-U")
        request.addValue(tokenString!, forHTTPHeaderField: "X-GATEWAY-A")
        request.addValue(accountTypeString!, forHTTPHeaderField: "X-GATEWAY-F")
        
        let session = URLSession.shared
        _ = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            if error == nil
            {
                if let data = data, let result = String(data: data, encoding: String.Encoding.utf8)
                {
                    let xml = SWXMLHash.parse(result)
                    //                    print(xml)
                    self.abArray = ActiveAndInactiveBuildings.getOnnetLocations(xmlResponseData: xml)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "getActiveAndInactiveBuildings"), object: nil) //
                    DispatchQueue.main.async {
                        
                        if (self.abArray?.count)!>0 {
                            
                            self.popupFosDetails?.streetArray=[ActiveAndInactiveBuildings]()
                            for aiBuilding in self.abArray!{
                                
                                //                                let cordna2d = CLLocationCoordinate2D(latitude: aiBuilding.lattitude!, longitude: aiBuilding.longitude!)
                                //                                let marker = GMSMarker(position: cordna2d)
                                
                                //                                marker.accessibilityHint=aiBuilding.buildingStatus
                                //                                if aiBuilding.buildingStatus == "ACTIVE" {
                                //                                    marker.icon=UIImage(named: "ActiveBuilding")
                                //                                }else{
                                //                                    marker.icon=UIImage(named: "InActiveBuilding")
                                //                                }
                                
                                //                                let permisesNumber =  aiBuilding.permisesNumber
                                var streetName =  aiBuilding.streetName
                                streetName=streetName?.replacingOccurrences(of: " ", with: "")
                                print(streetName!)
                                print((self.popupFosDetails?.mapViewStreetName)!)
                                if streetName==self.popupFosDetails?.mapViewStreetName
                                {
                                    self.popupFosDetails?.streetArray?.append(aiBuilding)
                                }
                                
                                //                                let englishCityName =  aiBuilding.englishCityName
                                //                                let postCode =  aiBuilding.postCode
                                //                                let coltOperatingCountry =  aiBuilding.coltOperatingCountry
                                //                                let address1 = "\(permisesNumber!), \(streetName!), \(englishCityName!), \(postCode!), \(coltOperatingCountry!)"
                                //                                marker.accessibilityValue=address1
                            }
                            
                            if (self.popupFosDetails?.streetArray?.count)! > 0 {
                                let tempCell = self.cellsForPopup?[3] as! BEndAddressCell
                                tempCell.btn_Onnet.isHidden=false
                            }
                            
                            
                            
                        }else{
                            let lOCAIONDOSENTPROVIDEONNET = NSLocalizedString("MAPS_LOCAION_DOSENT_PROVIDE_ONNET", comment: "")
                            UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: lOCAIONDOSENTPROVIDEONNET)
                        }
                        
                        //                        UIHelper.sharedInstance.hideLoading()
                        
                        
                    }
                    
                }
                else
                {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "getActiveAndInactiveBuildings"), object: "failure")
                    print("Failed to get valid response from server.")
                }
            }
            else
            {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "getActiveAndInactiveBuildings"), object: "failure")
                print(error?.localizedDescription ?? "Error")
            }
            
            }
            .resume()
    }
    
    func onnetAction(sender:UIButton)  {
        
    }
    func showProductList(sender:UIButton)  {
        
        innerView.addGestureRecognizer((popupFosDetails?.tapGes)!)
        viewTap.addGestureRecognizer((popupFosDetails?.tapGes)!)
        
        selectedButton=sender
        tbl_dropDown.tag=10
        let kFrame = sender.superview?.convert(sender.frame, to: innerView)
        constrintTopDropdown.constant=(kFrame?.origin.y)!
        constrintHeightDropDown.constant=constriantHeightPopup.constant-constrintTopDropdown.constant
        tbl_dropDown.isHidden=false
        productOrBandWidth_List = CoreDataHelper.sharedInstance.getAllProductsWithProductCode()
        
        //        print(productOrBandWidth_List ?? "List empty")
        
        // To Remove products which is not there as of now
        productOrBandWidth_List=productOrBandWidth_List?.filter{$0.productName != "Colt Business Access Pack (with VoIP)"}
        productOrBandWidth_List=productOrBandWidth_List?.filter{$0.productName != "Colt Business Access Pack (with IP Voice Line)"}
        productOrBandWidth_List=productOrBandWidth_List?.filter{$0.productName != "Colt Link Point to Multi Point (SDH Point to Multi Point)"}
        productOrBandWidth_List=productOrBandWidth_List?.filter{$0.productName != "Colt Link Point to Point (SDH Point to Point)"}
        productOrBandWidth_List=productOrBandWidth_List?.filter{$0.productName != "Colt Voice Line (v)"}
        productOrBandWidth_List_Nearnet = productOrBandWidth_List
        
//        if CommonDataHelper.sharedInstance.isNearnetMarkerClicked == true {
//          productOrBandWidth_List=productOrBandWidth_List?.filter{$0 != "Colt Wave"}
//          productOrBandWidth_List=productOrBandWidth_List?.filter{$0 != "Colt Private Ethernet"}
//        }else{
//            productOrBandWidth_List = productOrBandWidth_List_Nearnet
//        }
        let selectProduct = NSLocalizedString("Select Product", comment: "")
        let selectProductModel = ProductsModel(name: selectProduct, code:"-1")
        
        productOrBandWidth_List?.insert( selectProductModel, at: 0)
        tbl_dropDown.dataSource=self
        tbl_dropDown.delegate=self
        tbl_dropDown.reloadData()
    }
    func showBandWidthList(sender:UIButton)  {
        
        innerView.addGestureRecognizer((popupFosDetails?.tapGes)!)
        viewTap.addGestureRecognizer((popupFosDetails?.tapGes)!)
        
        if (CommonDataHelper.sharedInstance.selectedProduct == nil) || (CommonDataHelper.sharedInstance.selectedProduct == "Select Product") {
            let pleaseSelectProduct = NSLocalizedString("Please select Product", comment: "")
            UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: pleaseSelectProduct)

        }else {
            selectedButton=sender
            tbl_dropDown.tag=20
            let kFrame = sender.superview?.convert(sender.frame, to: innerView)
            constrintTopDropdown.constant=(kFrame?.origin.y)!
            constrintHeightDropDown.constant=constriantHeightPopup.constant-constrintTopDropdown.constant
            tbl_dropDown.isHidden=false
            if bandWidths == nil {
                bandWidths=CoreDataHelper.sharedInstance.getBandwidthForProduct(CommonDataHelper.sharedInstance.selectedProduct!)
                if (CommonDataHelper.sharedInstance.selectedProduct! == "Colt SIP Trunking") || (CommonDataHelper.sharedInstance.selectedProduct! == "Colt VoIP Access") {
                    let voicechannels = NSLocalizedString("Voice Channels", comment: "")
                   bandWidths?.insert(voicechannels, at: 0)
                }else{
                    let selectBandwith = NSLocalizedString("Select Bandwidth", comment: "")
                  bandWidths?.insert(selectBandwith, at: 0)
                }
                tbl_dropDown.dataSource=self
                tbl_dropDown.delegate=self
            }
            
            var tempBandWidthList = [ProductsModel]()
            
            if let bandwidthNotNil = bandWidths {
                for bandwith in bandwidthNotNil {
                    tempBandWidthList.append(ProductsModel(name: bandwith, code: ""))
                }
            }
            
            productOrBandWidth_List =  tempBandWidthList
            tbl_dropDown.reloadData()
        }
    }
    func showHubList(sender:UIButton)  {
        
        //        if hubList == nil {
        var err_msg : String?
        
        let ocn = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "selected_ocn")
        
        if ocn != nil
        {
            CommonDataHelper.sharedInstance.search_in_progress_hublist = true
            
            let ocn_parts = ocn!.components(separatedBy: "|BREAK|")
            
            if ocn_parts.last != nil
            {
                //                    DispatchQueue.main.async {
                UIHelper.sharedInstance.showLoading()
                NotificationCenter.default.addObserver(self, selector: #selector(getHubListFromServer), name: NSNotification.Name(rawValue: "finish_loading_hub_list"), object: nil)   //  Fix bug here
                let tempOcnPart = ocn_parts.last!
                //                    let selectedPrdct = CommonDataHelper.sharedInstance.selected_product
                
                ConfigService().getHubs(tempOcnPart , product: (popupFosDetails?.selectedProductType!)!)
                //                    }
            }
        }
        //        }
    }
    
    func getHubListFromServer() {
        DispatchQueue.main.async {
            print("getHubListFromServer")
            UIHelper.sharedInstance.hideLoading()
            self.hubList=CommonDataHelper.sharedInstance.hub_list as? [Dictionary<String, String>]
            self.constrintTopDropdown.constant=0
            //            self.constrintHeightDropDown.constant=self.constriantHeightPopup.constant-PopupCellHeight.CheckPriceCellHeight+PopupCellHeight.CancelCellHeight
            self.constrintHeightDropDown.constant=self.constriantHeightPopup.constant
            self.tbl_dropDown.isHidden=false
            self.tbl_dropDown.tag=30
            self.tbl_dropDown.reloadData()
        }
    }
    func replacePopupCells(cells:[PopupcellTypes],popupDetails:PopupDetails, fosPopup:Fos_Popup)
    {
        fosPopup.cellsForPopup?.removeAll()
        let  fosPopup1=fosPopup.reloadWithCells(cells: cells, popupDetails: popupDetails, fosPopup: fosPopup)
        fosPopup1.tbl_Popup.reloadData()
    }
    
    //    func numberOfSections(in tableView: UITableView) -> Int {
    //        if tableView == tbl_dropDown && tableView.tag == 30 {
    //            return 1
    //        }
    //        return 0
    //    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tbl_dropDown && tableView.tag == 30 {
            return 50.0
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == tbl_dropDown && tableView.tag == 30 {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tbl_dropDown.frame.size.width, height: 50.0))
            headerView.backgroundColor=UIColor.white
            let titleHeader = UILabel(frame: CGRect(x: 8.0, y: 8.0, width: 100.0, height: 30.0))
            titleHeader.text="Select hub"
            headerView.addSubview(titleHeader)
            return headerView
        }else{
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tbl_Popup {
            return (cellsForPopup?.count)!
        }else{
            if tableView.tag == 30 {
                return (hubList?.count)!
            }else{
                return (productOrBandWidth_List?.count)!
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tbl_Popup {
            let cell = cellsForPopup![indexPath.row]
            return cell as! UITableViewCell
        }else{
            if tableView.tag == 30 {
                let identifier = "cell"
                var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HubListCell
                if cell == nil {
                    cell=Bundle.main.loadNibNamed("HubListCell", owner: self, options: nil)?[0] as? HubListCell
                }
                let tempListVal = hubList?[indexPath.row]
                cell?.lbl_Address.text=tempListVal?["formatted_address"]
                cell?.lbl_SubAddress.text=tempListVal?["hub_name"]
                return cell!
            }else if tableView.tag == 20 {
                let identifier = "cell"
                var cell = tableView.dequeueReusableCell(withIdentifier: identifier)
                if cell==nil {
                    cell=UITableViewCell(style: .default, reuseIdentifier: identifier)
                }
                let productName = productOrBandWidth_List?[indexPath.row].productName
                cell?.textLabel?.text=productName
                return cell!
            }else{
                let identifier = "cell"
                var cell = tableView.dequeueReusableCell(withIdentifier: identifier)
                if cell==nil {
                    cell=UITableViewCell(style: .default, reuseIdentifier: identifier)
                }
                productName = productOrBandWidth_List?[indexPath.row].productName
                if productName == "Colt VoIP Access" {
                    productName = "Colt SIP Trunking"
                }else if productName == "Colt LANLink Point to Point (Ethernet Point to Point)" {
                    productName = "Colt Ethernet Line"
                    
                }else if productName == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
                    productName = "Colt Ethernet Hub and Spoke"
                    
                }else if productName == "Colt Ethernet Private Network (EPN)"{
                    productName = "ColT Ethernet VPN"
                }
                cell?.textLabel?.text=productName
                return cell!
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tbl_Popup {
            let cType = cellsType![indexPath.row]
            var cHeicht:CGFloat?
            
            if cType == PopupcellTypes.AddressCell {
                cHeicht = PopupCellHeight.AddressCellHeight
            }else if cType == PopupcellTypes.CheckOffnetConnectivityCell {
                cHeicht = PopupCellHeight.CheckOffnetConnectivityCellHeight
            }else if cType == PopupcellTypes.CheckPriceCell {
                cHeicht = PopupCellHeight.CheckPriceCellHeight
            }else if cType == PopupcellTypes.CancelCell {
                cHeicht = PopupCellHeight.CancelCellHeight
            }else if cType == PopupcellTypes.ProductCell {
                cHeicht = PopupCellHeight.ProductCellHeight
            }else if cType == PopupcellTypes.BandWidthCell {
                cHeicht = PopupCellHeight.BandWidthCellHeight
            }else if cType == PopupcellTypes.HubCell {
                cHeicht = PopupCellHeight.HubCellHeight
            }else if cType == PopupcellTypes.BEndAddressCell {
                cHeicht = PopupCellHeight.BEndAddressCellHeight
            }else if cType == PopupcellTypes.LocationCell {
                cHeicht = PopupCellHeight.LocationCellHeight
            }else if cType == PopupcellTypes.EmfDslCell {
                cHeicht = PopupCellHeight.EmfDslCellHeight
            }else if cType == PopupcellTypes.SearchAndCancelCell {
                cHeicht = PopupCellHeight.SearchAndCancelCellHeight
            }
            return cHeicht!
            
        }else{
            if tableView.tag == 30 {
                return 62.0
            }else{
                return 44.0
            }
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tbl_dropDown {
            
            if tableView.tag == 10 {
                
                if indexPath.row>0 {
                    
                    if popupFosDetails?.selectedProductType != productOrBandWidth_List?[indexPath.row].productName{
                        //                    let tempSelectedProductType = selectedProductType
                        
                        popupFosDetails?.selectedHubName=nil
                        isSelectedProductType=true
                        
                        var selectTitle = ""
                        productName = productOrBandWidth_List?[indexPath.row].productName
                        if productName == "Colt VoIP Access" || productName == "Colt SIP Trunking" {
                            let voiceChannel = NSLocalizedString("Voice Channels", comment: "")
                               selectTitle = voiceChannel
                        }else {
                             //                        let selectTitle = "Select bandwidth"
                            let selectBandwith = NSLocalizedString("Select Bandwidth", comment: "")
                             selectTitle = selectBandwith
                        }
                       
                        
                        CommonDataHelper.sharedInstance.selectedProductCode = productOrBandWidth_List?[indexPath.row].productCode
                        CommonDataHelper.sharedInstance.selectedProduct = productName
                        if productName == "Colt VoIP Access" {
                            productName = "Colt SIP Trunking"
                            
                        }else if productName == "Colt LANLink Point to Point (Ethernet Point to Point)" {
                            productName = "Colt Ethernet Line"
                            
                        }else if productName == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
                            productName = "Colt Ethernet Hub and Spoke"
                            popupFosDetails?.selectedProductType=productName
                            
                        }else if productName == "Colt Ethernet Private Network (EPN)"{
                            productName = "ColT Ethernet VPN"
                        }
                        
                        popupFosDetails?.productName=productName
                        popupFosDetails?.selectedProductType=productOrBandWidth_List?[indexPath.row].productName
                        selectedButton?.setTitle("\(productName!)", for: .normal)
                        CommonDataHelper.sharedInstance.selectedProduct = productName
                        CommonDataHelper.sharedInstance.selectedProduct=productOrBandWidth_List?[indexPath.row].productName
                        popupFosDetails?.selectedBandWidth=nil
                        
                        let tempBandWidth = CoreDataHelper.sharedInstance.getBandwidthForProduct((productOrBandWidth_List?[indexPath.row].productName)!)
                        bandWidths=tempBandWidth
                        bandWidths?.insert(selectTitle, at: 0)
                        CommonDataHelper.sharedInstance.selectedBandwidth = nil
                        
                        if popupFosDetails?.pType == PopupType.MapviewPopup5 {
                            tbl_dropDown.isHidden=true
                            innerView.removeGestureRecognizer((popupFosDetails?.tapGes)!)
                            viewTap.removeGestureRecognizer((popupFosDetails?.tapGes)!)
                            let tempCell = cellsForPopup?[3] as? BandWidthCell
                            
                    // if prodct not Colt Private Ethernet giving actions and showing
                            let efmDslCell = cellsForPopup?[4] as? EfmDslCell
                            let searchAndCancelCell = cellsForPopup?[5] as? SearchAndCancelCell
                            let bandwith = NSLocalizedString("Bandwidth", comment: "")
                            if productName == "Colt SIP Trunking" || productName == "Colt VoIP Access"{
                                let voiceChannels = NSLocalizedString("Voice Channels", comment: "")
                                tempCell?.lbl_ProductOrVoiceChannel.text=voiceChannels
                                tempCell?.btn_Bandwidth.setTitle(voiceChannels, for: .normal)
                                efmDslCell?.efm_OloSubView.isHidden = false
                         
                            }else if productName == "Colt Private Ethernet" || productName == "Colt Wave"{
                                tempCell?.lbl_ProductOrVoiceChannel.text=bandwith
                                let selectbandwidth = NSLocalizedString("Select Bandwidth", comment: "")
                                tempCell?.btn_Bandwidth.setTitle(selectbandwidth, for: .normal)

                                efmDslCell?.efm_OloSubView.isHidden = true
                                
//                                searchAndCancelCell?.btn_search.backgroundColor = UIColor.gray

                            }else{
                                tempCell?.lbl_ProductOrVoiceChannel.text=bandwith
                                let selectbandwidth = NSLocalizedString("Select Bandwidth", comment: "")
                                tempCell?.btn_Bandwidth.setTitle(selectbandwidth, for: .normal)

                                efmDslCell?.efm_OloSubView.isHidden = false
//                                searchAndCancelCell?.btn_search.backgroundColor = UIColor(red: 0/255, green: 165/255, blue: 155/255, alpha: 1.0)
                            }
                            return
                        }
                        
                        //                    var tempCell:BandWidthCell?
                        //                    if popupFosDetails?.pType == PopupType.MapviewPopup5 {
                        //                        tempCell = cellsForPopup?[3] as? BandWidthCell
                        //                    }else{
                        //                        tempCell = cellsForPopup?[2] as? BandWidthCell
                        //                    }
                        
                        //                    if tempSelectedProductType == "Colt LANLink Point to Point (Ethernet Point to Point)" {
                        ////                        if tempSelectedProductType == "Colt LANLink Point to Point (Ethernet Point to Point)" || tempSelectedProductType == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
                        //
                        //                            delegate?.dismissPopup(selectionType: "Normalpopup", popupFos: self)
                        //
                        ////                        }else{
                        ////                            tempCell?.lbl_ProductOrVoiceChannel.text="Voice channel"
                        ////                            tempCell?.btn_Bandwidth.setTitle("   Select a voice channel", for: .normal)
                        ////                            selectTitle="Select a voice channel"
                        ////                        }
                        //
                        //                    }else
                        
                        //                    if tempSelectedProductType == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
                        //                        delegate?.dismissPopup(selectionType: "Hub", popupFos: self)
                        //                    }else
                        
                        if productName == "Colt Ethernet Hub and Spoke" {
                            if popupFosDetails?.pType != PopupType.MapviewPopup5 {
                                delegate?.dismissPopup(selectionType: "Hub", popupFos: self)
                            }
                        }else if productName == "Colt Ethernet Line" {
                            if popupFosDetails?.pType != PopupType.MapviewPopup5 {
                                delegate?.dismissPopup(selectionType: "B-End address", popupFos: self)
                            }
                        }
                        else{
                            delegate?.dismissPopup(selectionType: "Normalpopup", popupFos: self)
                            
                            //                        if productName == "Colt SIP Trunking" {
                            //                            if tempSelectedProductType == "Colt LANLink Point to Point (Ethernet Point to Point)" || tempSelectedProductType == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
                            //                                delegate?.dismissPopup(selectionType: "Normalpopup", popupFos: self)
                            //                            }else{
                            //                                tempCell?.lbl_ProductOrVoiceChannel.text="Voice channel"
                            //                                tempCell?.btn_Bandwidth.setTitle("   Select a voice channel", for: .normal)
                            //                                selectTitle="Select a voice channel"
                            //                            }
                            //
                            //                        }else{
                            //                            tempCell?.lbl_ProductOrVoiceChannel.text="Bandwith"
                            //                            tempCell?.btn_Bandwidth.setTitle("   Select a bandwidth", for: .normal)
                            //                            selectTitle="Select a bandwidth"
                            //                        }
                            
                        }
                        
                        
                        //                    print(tempBandWidth ?? "Empty bandwidth")
                    }
                    
                }else{
                    isSelectedProductType=false
                    popupFosDetails?.productName=nil
                    popupFosDetails?.selectedHubName=nil
                    popupFosDetails?.selectedProductType=nil
                    popupFosDetails?.selectedBandWidth=nil
                    CommonDataHelper.sharedInstance.selectedProduct = "Select Product"
                    if popupFosDetails?.pType == PopupType.MapviewPopup5 {
                        tbl_dropDown.isHidden=true
                        innerView.removeGestureRecognizer((popupFosDetails?.tapGes)!)
                        viewTap.removeGestureRecognizer((popupFosDetails?.tapGes)!)
                        let tempCell = cellsForPopup?[3] as? BandWidthCell
                        tempCell?.lbl_ProductOrVoiceChannel.text="Bandwith"
                        let selectbandwidth = NSLocalizedString("Select Bandwidth", comment: "")
                        tempCell?.btn_Bandwidth.setTitle(selectbandwidth, for: .normal)
                        let tempCell2 = cellsForPopup?[2] as? ProductCell
                        let selectProduct = NSLocalizedString("Select Product", comment: "")
                        tempCell2?.btn_Product.setTitle(selectProduct, for: .normal)
                        CommonDataHelper.sharedInstance.selectedProduct = "Select Product"

                        return
                        
                    }
                    delegate?.dismissPopup(selectionType: "Normalpopup", popupFos: self)
                    
                    //                    if selectedProductType == "Colt LANLink Point to Point (Ethernet Point to Point)" || selectedProductType == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
                    //                        delegate?.dismissPopup(selectionType: "ReloadInitial", popupFos: self)
                    //                    }else{
                    //                        selectedButton?.setTitle("    Select a product", for: .normal)
                    //                        var cell:BandWidthCell?
                    //                        if popupFosDetails?.pType == PopupType.MapviewPopup5 {
                    //                            cell = cellsForPopup?[3] as? BandWidthCell
                    //                        }else{
                    //                            cell = cellsForPopup?[indexPath.row+2] as? BandWidthCell
                    //                        }
                    //                        let tempButton = cell?.viewWithTag(5) as! UIButton
                    //                        tempButton.setTitle("    Select a bandwidth", for: .normal)
                    //                        let lbl_banwdth = cell?.lbl_ProductOrVoiceChannel
                    //                        lbl_banwdth?.text="Bandwith"
                    //
                    //                    }
                    
                }
            }else if tableView.tag == 30 {
                let tempCell = cellsForPopup?[3] as! HubCell
                tempCell.lbl_Hub.isHidden=true
                tempCell.lbl_Address.isHidden=false
                tempCell.lbl_SubName.isHidden=false
                tempCell.lbl_Level.isHidden=false
                let tempListVal = hubList?[indexPath.row]
                tempCell.lbl_Address.text=tempListVal?["formatted_address"]
                tempCell.lbl_SubName.text=tempListVal?["hub_name"]
                popupFosDetails?.selectedHubName=(tempListVal?["hub_name"])!
                popupFosDetails?.aEndAddress_id = tempListVal?["point_id"]
            }else{
                
                if indexPath.row>0 {
                    selectedButton?.setTitle("\((productOrBandWidth_List?[indexPath.row].productName)!)", for: .normal)
                    popupFosDetails?.selectedBandWidth=(productOrBandWidth_List?[indexPath.row].productName)!
                    CommonDataHelper.sharedInstance.selectedBandwidth = popupFosDetails?.selectedBandWidth
                }else{
                    if popupFosDetails?.selectedProductType == "Colt VoIP Access" || popupFosDetails?.selectedProductType == "Colt SIP Trunking" {
                        let voicechannels = NSLocalizedString("Voice Channels", comment: "")
                        selectedButton?.setTitle(voicechannels, for: .normal)
                    }else{
                        let selectbandwidth = NSLocalizedString("Select Bandwidth", comment: "")
                        selectedButton?.setTitle(selectbandwidth, for: .normal)
                    }
                    popupFosDetails?.selectedBandWidth=nil
                    CommonDataHelper.sharedInstance.selectedBandwidth = nil
                    
                }
            }
            tbl_dropDown.isHidden=true
            innerView.removeGestureRecognizer((popupFosDetails?.tapGes)!)
            viewTap.removeGestureRecognizer((popupFosDetails?.tapGes)!)
        }
    }
    
    func cancelPopup() {
        AnalyticsHelper().analyticsEventsAction(category: "Connectivity Checker", action: "connectivityChecker_cancel_click", label: "Cancel")
        delegate?.dismissPopup(selectionType: "cancel", popupFos: self)
    }
    func checkPriceAction() {
        
        AnalyticsHelper().analyticsEventsAction(category: "Connectivity Checker", action: "connectivityChecker_checkPrice_click", label: "Check Price")
        
        var err_msg : String?
        let tempKey = UserDefaults.standard.value(forKey: "eType")
        let employeeType =  CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: tempKey as! String)

        if employeeType == "Non Employee" {
//            UIHelper.sharedInstance.showError("You are not eligible to check price")
//            showAlet("You are not eligible to check price")
//            NotificationCenter.default.post(name: Notification.Name(rawValue: "ShowAlert"), object: "You are not eligible to check price")
            let youAreNotEligible = NSLocalizedString("You are not eligible to check price", comment: "")
            UIHelper.showAlert(message: youAreNotEligible, inViewController: popupFosDetails?.popupController)
        }else{
             delegate?.dismissPopup(selectionType: "checkprice", popupFos: self)
        }       
    }
    func showAlet(_ msg: String) {
        let alert = UIAlertController(title: "",message:msg,
                                      preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK",style:UIAlertActionStyle.default,handler: nil))
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func searchBuildingAction()  {
        if CommonDataHelper.sharedInstance.selectedProduct == "Colt Private Ethernet" || CommonDataHelper.sharedInstance.selectedProduct == "Colt Wave" {
            let ForThisProductDontHaveOFFNET = NSLocalizedString("For this Product we don't have OFFNET Results!", comment: "")
             UIHelper.showAlert(message: ForThisProductDontHaveOFFNET, inViewController: popupFosDetails?.popupController)
            
        }else{
        CommonDataHelper.sharedInstance.isPopUpEFM_OLO = false
        delegate?.dismissPopup(selectionType: "search", popupFos: self)
        }
    }
    func buildingDetailAction() {
        delegate?.dismissPopup(selectionType: "buildingDetails", popupFos: self)
        
        AnalyticsHelper().analyticsEventsAction(category: "Connectivity Checker", action: "connectivityChecker_buildingDetail_click", label: "Building detail")
    }
}

enum PopupcellTypes {
    case AddressCell,CancelCell,CheckOffnetConnectivityCell,CheckPriceCell,ProductCell,BandWidthCell,HubCell,BEndAddressCell,LocationCell,EmfDslCell,SearchAndCancelCell
}
struct PopupCellHeight {
    static let AddressCellHeight:CGFloat=44.0
    static let CancelCellHeight:CGFloat=45.0
    static let CheckOffnetConnectivityCellHeight:CGFloat=50.0
    static let CheckPriceCellHeight:CGFloat=60.0
    static let ProductCellHeight:CGFloat=72.0
    static let BandWidthCellHeight:CGFloat=72.0
    static let HubCellHeight:CGFloat=124.0
    static let BEndAddressCellHeight:CGFloat=263.0
    static let LocationCellHeight:CGFloat=66.0
    static let EmfDslCellHeight:CGFloat=44.0
    static let SearchAndCancelCellHeight:CGFloat=60.0
}

enum PopupType {
    case MapviewPopup1,MapviewPopup2,MapviewPopup3,MapviewPopup4,MapviewPopup5
}
class PopupDetails: NSObject {
    var selectedProductType:String?
    var selectedBandWidth:String?
    var pType:PopupType?
    var isPopupMutable:Bool = true
    var mapViewStreetName:String?
    var locationAddress:NSString?
    var selectedHubName:String?
    var popupController:UIViewController?
    var lattitudeLocation:Double?
    var longitudeLocation:Double?
    var permissionNumber:String?
    var streetName:String?
    var cityName:String?
    var postcode:String?
    var country:String?
    var isEfmSelected:Bool?=false
    var isOloSelected:Bool?=false
    var streetArray:[ActiveAndInactiveBuildings]?
    var numberOfServiceToCall:Int = 0
    var numberOfResponeGot:Int=0
    var emfServerResponse:String?
    var productName:String?
    var bEndAddress:String?
    var aEndAddress_id:String?
    var isBendAddressSearched:Bool?=false
    var isBuildingDetailEnable:Bool = false
    var tapGes:UITapGestureRecognizer?
    
}
