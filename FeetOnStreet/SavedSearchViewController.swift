//
//  SavedSearchViewController.swift
//  FeetOnStreet
//
//  Created by admin on 3/9/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import SwiftOverlays

class SavedSearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate {
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var noDataAvilableLbl: UILabel!
    
    var dashBoardViewController: DashBoardViewController?
    var savedSearchModel:SavedSearchModel?
    
    var err_msg : String?
    var saveSearchArray:[SavedSearchModel] = []
    var filter_List_data:[SavedSearchModel] = []
    var searchActive : Bool = false
    var Please_wait = ""
    var no_internet_connection = ""
    var ServerErrorPleaseTryTgain = ""
    var noDataAvailable = ""
    
    
    // BackgroundApiCall
    var backgroundTask:UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // BackgroundApiCall
        NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        // BackgroundApiCall
        registerForBackgroundTask()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
        AnalyticsHelper().analyticLogScreen(screen: "SavedSearch Screen")
        
        CommonDataHelper.sharedInstance.savedSearchModelList = nil // making model nill data
        noDataAvilableLbl.isHidden = true
        Please_wait = NSLocalizedString("Please wait", comment: "")
        no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
        ServerErrorPleaseTryTgain = NSLocalizedString("Server error. Please try again.", comment: "")
        noDataAvailable = NSLocalizedString("No data available", comment: "")
        noDataAvilableLbl.text = self.noDataAvailable
        getApiMethod ()  // calling SavedSearch API
        
    }
    
    // BackgroundApiCall
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // BackgroundApiCall
    @objc func reinstateBackgroundTask() {
        if backgroundTask == UIBackgroundTaskInvalid {
            registerForBackgroundTask()
        }
    }
    // BackgroundApiCall
    func registerForBackgroundTask() {
        if backgroundTask == UIBackgroundTaskInvalid {
            backgroundTask=UIApplication.shared.beginBackgroundTask(expirationHandler: {
                self.endBackGroundTask()
            })
        }
    }
    
    // BackgroundApiCall
    func endBackGroundTask() {
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask=UIBackgroundTaskInvalid
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return filter_List_data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let savedSearchCell = tableView.dequeueReusableCell(withIdentifier: "savedSearchCell", for: indexPath)
        let savedSearchModel = filter_List_data[indexPath.row]
        savedSearchCell.textLabel?.text = savedSearchModel.search_name
        savedSearchCell.textLabel?.numberOfLines = 0
 
        return savedSearchCell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        view.endEditing(true) // close keyboard
        
        savedSearchModel = saveSearchArray[indexPath.row]
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let savedSearchDetailsVC = storyBoard.instantiateViewController(withIdentifier: "SavedSearchDetailsVC") as! SavedSearchDetailsVC
        savedSearchDetailsVC.savedSearchModel = savedSearchModel
        self.navigationController?.pushViewController(savedSearchDetailsVC, animated: true)
        
        
        //        nameLbl.text = savedSearchModel?.search_name
        //        productLbl.text = savedSearchModel?.servicecode
        //        bandwidthLbl.text = savedSearchModel?.bandwidthcode
        //        if savedSearchModel?.locationa_housenumber == nil{
        //            savedSearchModel?.locationa_housenumber = ""
        //        }
        //        if savedSearchModel?.locationa_streetname == nil{
        //            savedSearchModel?.locationa_streetname = ""
        //        }
        //        if savedSearchModel?.locationa_housenumber == nil{
        //            savedSearchModel?.locationa_housenumber = ""
        //        }
        //        if savedSearchModel?.locationa_cityname == nil{
        //            savedSearchModel?.locationa_cityname = ""
        //        }
        //        if savedSearchModel?.locationa_countryname == nil{
        //            savedSearchModel?.locationa_countryname = ""
        //        }
        //        if savedSearchModel?.locationb_postcode == nil{
        //            savedSearchModel?.locationb_postcode = ""
        //        }
        //        let aEndAddress = "\(savedSearchModel?.locationa_housenumber!),\(savedSearchModel?.locationa_streetname!),\(savedSearchModel?.locationa_cityname!),\(savedSearchModel?.locationa_countryname!),\(savedSearchModel?.locationb_postcode!)"
        //        aEndAddressLbl.text = aEndAddress
        
    }
    
    func searchBar(_ searchBar: UISearchBar,
                   textDidChange searchText: String) {
        AnalyticsHelper().analyticsEventsAction(category: "SavedSearch Search", action: "SEARCH_click", label: "SavedSearch_list_search")
        if searchText.isEmpty {
            filter_List_data = saveSearchArray
        }else{
            filter_List_data.removeAll()
            for listName in saveSearchArray {
                if listName.search_name == nil {
                   listName.search_name = ""
                }
                let tmp: NSString = listName.search_name! as NSString
                let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                if range.location != NSNotFound {
                    filter_List_data.append(listName)
                    //                DispatchQueue.main.async {
                    //                    self.tableView.reloadData()
                    //                }
                }
            }
        }
        //        filter_List_data = saveSearchArray.filter({ (text) -> Bool in
        //            let tmp: NSString = text as NSString
        //            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
        //            return range.location != NSNotFound
        //        })
        
        //        if(filter_List_data.count == 0){
        //            searchActive = false;
        //        } else {
        //            searchActive = true;
        //        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func getApiMethod () {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
            // BackgroundApiCall
            endBackGroundTask()
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
        }else{
            DispatchQueue.main.async {
//            UIHelper.sharedInstance.showLoadingOnViewController(loadVC: self)
                SwiftOverlays.showBlockingWaitOverlayWithText(self.Please_wait)
            }
            
//            SwiftOverlays.showBlockingWaitOverlayWithText("Please wait...")
            
            var userName = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "username")!
            userName = userName.replacingOccurrences(of: " ", with: "")
            let url = NSURL(string: "https://rfsdcp.colt.net/aptApi/getSavedSearchCriteria?coltAccount=\(userName)")!
//            let url = NSURL(string: "https://rfsdcp.colt.net/aptApi/getSavedSearchCriteria?coltAccount=\(userName)")!
            let request = NSMutableURLRequest(url: url as URL, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 500.0)
            //        let request = NSMutableURLRequest(url: url as URL)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")  // the request is JSON
            request.setValue("Basic YXB0Y2xpZW50OmFwdDEyJDU2", forHTTPHeaderField: "Authorization")        // the expected response is also JSON
            request.httpMethod = "POST"
            request.timeoutInterval = 600
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard let data = data, error == nil else {
                    print(error) // some fundamental network error
                    // BackgroundApiCall
                    self.endBackGroundTask()
                    DispatchQueue.main.async {
//                    UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                        SwiftOverlays.removeAllBlockingOverlays()
                    }
                    UIHelper.showAlert(message: self.ServerErrorPleaseTryTgain, inViewController: self)
                    return
                }
                do {
                    let responseObject = try JSONSerialization.jsonObject(with: data, options: [])
                    
                    //                let response: [String: Any] = ["response": responseObject]
                    //                let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    //                let modifiedJSON = try JSONSerialization.jsonObject(with: jsonData, options: [])
                    //                let saveSearchList = SaveSearchRootModel(JSONString: modifiedJSONZZZString())
                    
                    self.saveSearchArray = SavedSearchModel.getSaveSearchResponse(responseArray: responseObject)
                    self.filter_List_data = self.saveSearchArray
                    
                    if self.filter_List_data.count==0 {
                        DispatchQueue.main.async {
                           self.noDataAvilableLbl.isHidden = false
                        }
                    }
                    
                    // BackgroundApiCall
                    self.endBackGroundTask()
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    DispatchQueue.main.async {
                        //                        UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                        SwiftOverlays.removeAllBlockingOverlays()
                    }
                } catch let jsonError {
                    print(jsonError)
                    // BackgroundApiCall
                    self.endBackGroundTask()
                   DispatchQueue.main.async {
//                        UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                    SwiftOverlays.removeAllBlockingOverlays()
                    }
                    SwiftOverlays.removeAllBlockingOverlays()
                    UIHelper.showAlert(message: self.ServerErrorPleaseTryTgain, inViewController: self)
                }
                
            }
            task.resume()
        }
    }
    @IBAction func searchBtnAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.window?.rootViewController = initialViewController
        
        CommonDataHelper.sharedInstance.isFromSavedsearch = true
        CommonDataHelper.sharedInstance.savedSearchModelList = savedSearchModel
        
        getProductNamewithProductCode(productCode:(savedSearchModel?.servicecode)!)
    }
    
    func getProductNamewithProductCode(productCode:String) {
        if let products = CoreDataHelper.sharedInstance.getAllProductsWithProductCode() {
            for product in products {
                if product.productCode == productCode {
                    CommonDataHelper.sharedInstance.selectedProduct = product.productName
                    CommonDataHelper.sharedInstance.selectedBandwidth = savedSearchModel?.bandwidthcode
                }
            }
        }
    }
    
//    @IBAction func savedDetailsPopUpBacgrounBtn_Action(_ sender: Any) {
//        savedDetailsPopUp.isHidden = true
//        savedDetailsPopUpBacgrounBtn.isHidden = true
//    }
    
//    func showAlert(_ srtatusValue: String,messageValue: String ) {
//
//        let parentViewController: UIViewController = UIApplication.shared.windows[1].rootViewController!
//        let alert = UIAlertController(title: messageValue, message:srtatusValue , preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(action: UIAlertAction!) in
//        }))
//        DispatchQueue.main.async() {
//            parentViewController.present(alert, animated: true, completion: nil)
//        }
//    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

