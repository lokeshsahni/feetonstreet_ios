//
//  RTDEMailPopUpView.swift
//  FeetOnStreet
//
//  Created by admin on 6/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import SwiftOverlays

class RTDEMailPopUpView: UIView, UITextFieldDelegate {
    
    var rTDtoolViewController: RTDtoolViewController?
    var isSubseaLink = false
    var customLink_ID: Int?
    
    @IBOutlet weak var txtView_email: UITextField!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
   var ok = NSLocalizedString("OK", comment: "")
   var serverErrorPleaseTryTgain = NSLocalizedString("Server error. Please try again.", comment: "")
   var somethingWentWrongPleaseTryAgain =  NSLocalizedString("Something went wrong. Please try again", comment: "")

//    override func layoutSubviews() {
//        super.layoutSubviews()
//        txtView_email.layer.borderWidth = 1.0
//        txtView_email.layer.borderColor = UIColor.black.cgColor
//        txtView_email.textContainerInset = .init(top: 4.0, left: 4.0, bottom: 4.0, right: 4.0)
//    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        
        AnalyticsHelper().analyticsEventsAction(category: "RTDEmail", action: "RTDEmailCancel_click", label: "Cancel")
        rTDtoolViewController?.eMailPopUp?.removeFromSuperview()
    }

    @IBAction func sendEmailAction(_ sender: Any) {
        
        AnalyticsHelper().analyticsEventsAction(category: "RTD E-mail Share(Mobile)", action: "EmailSend_Click", label: "Send")
        txtView_email.resignFirstResponder()
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            let no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
            showAlertForEmail(isSuccess: false, message: no_internet_connection)
        }else{
            if txtView_email.text == "" {
                let PleaseEnteremailiD = NSLocalizedString("Please enter email iD", comment: "")
                    showAlert(message: PleaseEnteremailiD)
            }else {
                
                let isValidEmail = isValidEmailAddress(emailAddressString: txtView_email.text!)
              if isValidEmail {
                    let please_wait = NSLocalizedString("Please wait", comment: "")
                SwiftOverlays.showBlockingWaitOverlayWithText(please_wait)
                var err_msg : String?
                #if DEBUG
                let userName = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "email")!
                #else
                let userName = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "email")!
                #endif
                let requestBody:[String : Any]?
                if rTDtoolViewController?.isSubSeaRouteSelected == true {
                    requestBody = ["customLinkId": "\((rTDtoolViewController?.subSeaLinkID)!)","emailFrom":userName,"emailTo":txtView_email.text!,"searchType":(rTDtoolViewController?.searchType)!,"siteA":(rTDtoolViewController?.siteA_ID)!,"siteB":(rTDtoolViewController?.siteB_ID)!]
                }else {
                    requestBody = ["emailFrom":userName,"emailTo":txtView_email.text!,"searchType":(rTDtoolViewController?.searchType)!,"siteA":(rTDtoolViewController?.siteA_ID)!,"siteB":(rTDtoolViewController?.siteB_ID)!]
                }
                var errMsg:String?
                let tokenString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "token")
                let accountTypeString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "accountType")
//                let urlString = "http://lonrtd02:8080/rtdapi/sm/sendMailAttachment"
                let urlString = "http://loncis01/rtdapi/sm/sendMailAttachment"
                let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
                let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
                let urlStr = URL(string: "https://dcp.colt.net/dgateway/connectnew")

                let request = NSMutableURLRequest(url: urlStr!)
                    request.httpMethod = "POST"
                let requestData = try! JSONSerialization.data(withJSONObject: requestBody, options: .prettyPrinted)
                    request.httpBody = requestData
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.addValue("radius_search", forHTTPHeaderField: "SOAPAction")
                    request.addValue("Apache-HttpClient/4.1.1", forHTTPHeaderField: "User-Agent")
                    request.addValue(base64String!, forHTTPHeaderField: "X-CONNECT-U")
                    request.addValue(tokenString!, forHTTPHeaderField: "X-GATEWAY-A")
                    request.addValue(accountTypeString!, forHTTPHeaderField: "X-GATEWAY-F")
                let authKey = "rtdapiuser:zAq1#mLp0"
                let encodedAuthKey = authKey.data(using: String.Encoding.utf8, allowLossyConversion: true)
                let base64AuthKey = encodedAuthKey?.base64EncodedString(options: .init(rawValue: 0))
//                    request.addValue("Basic \(base64AuthKey!)", forHTTPHeaderField: "Authorization")

                let session = URLSession.shared
                _ = session.dataTask(with: request as URLRequest) { (data, response, error) in
                    DispatchQueue.main.async {
                        SwiftOverlays.removeAllBlockingOverlays()
                    }
                    if error == nil
                    {
                        if let httpResponse = response as? HTTPURLResponse {
                            print("code \(httpResponse.statusCode)")
                            if httpResponse.statusCode == 200 {

                                if let data = data
                                {
                                    do {
                                        if let resultDict = try JSONSerialization.jsonObject(with: data, options: []) as? [String:String] {

                                            if resultDict["result"] == "success" {

                                                let EmailQueuedForDelivery = NSLocalizedString("Email queued for delivery", comment: "")
                                                let alert = UIAlertController(title: nil, message: "Email sent successfully", preferredStyle: .alert)
                                                let okAction = UIAlertAction(title: self.ok, style: .default, handler: { _ in
                                                    DispatchQueue.main.async {
                                                        self.removeFromSuperview()
                                                        self.rTDtoolViewController?.navigationController?.popViewController(animated: true)
                                                    }
                                                })
                                                alert.addAction(okAction)
                                                DispatchQueue.main.async {
                                                    self.rTDtoolViewController?.present(alert, animated: true, completion: nil)
                                                }
                                            }else{
                                                DispatchQueue.main.async {

                                                    self.showAlertForEmail(isSuccess: false, message: self.somethingWentWrongPleaseTryAgain)
                                                }
                                            }

                                        }
                                    }
                                    catch {
                                        self.showAlertForEmail(isSuccess: false, message: self.somethingWentWrongPleaseTryAgain)
                                    }
                                    DispatchQueue.main.async {
                                        SwiftOverlays.removeAllBlockingOverlays()
                                    }
                                }

                            }else{
                                DispatchQueue.main.async {
                                   
                                    self.showAlertForEmail(isSuccess: false, message: self.somethingWentWrongPleaseTryAgain)
                                }

                            }
                        }
                     }
                    else
                    {
                        DispatchQueue.main.async {
                            self.showAlertForEmail(isSuccess: false, message: self.somethingWentWrongPleaseTryAgain)
                        }
                    }

                    }
                    .resume()


            }else{
                let emailAddressisNotValid = NSLocalizedString("Email address is not valid", comment: "")
                showAlert(message: emailAddressisNotValid)
            }
          }
        }

    }
    func showAlert(message:String) {
        
        let alert = UIAlertController(title: nil, message:message , preferredStyle: .alert)
        let okAction = UIAlertAction(title: ok, style: .default, handler: nil)
        alert.addAction(okAction)
        rTDtoolViewController?.present(alert, animated: true, completion: nil)
    }

    func showAlertForEmail(isSuccess: Bool, message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        if isSuccess {
            let okAction = UIAlertAction(title: ok, style: .default, handler: { _ in
                self.removeFromSuperview()
                self.rTDtoolViewController?.navigationController?.popViewController(animated: true)
            })
            alert.addAction(okAction)
            self.rTDtoolViewController?.present(alert, animated: true, completion: nil)
        }else{
            let okAction = UIAlertAction(title: ok, style: .default, handler: { _ in
                self.removeFromSuperview()
            })
            alert.addAction(okAction)
        }
       self.rTDtoolViewController?.present(alert, animated: true, completion: nil)
    }

    @IBAction func dismissKB(_ sender: Any) {
        txtView_email.resignFirstResponder()
    }
//    func isValidEmailAddress(emailAddressString: String) -> Bool {
//
//        var returnValue = true
//        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
//
//        let emailsArray = emailAddressString.components(separatedBy: ",")
//
//        do {
//            for kItem in emailsArray {
//                let regex = try NSRegularExpression(pattern: emailRegEx)
//                let nsString = kItem as NSString
//                let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
//
//                if results.count == 0
//                {
//                     return false
//                }
//            }
//
//
//        } catch let error as NSError {
//            print("invalid regex: \(error.localizedDescription)")
//            returnValue = false
//        }
//
//        return  returnValue
//    }
    
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
}
