//
//  BEndCell.swift
//  FeetOnStreet
//
//  Created by Ramesh on 18/09/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class BEndCell: UITableViewCell {
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var supplierNameLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
