//
//  PriceAndSLA.swift
//  FeetOnStreet
//
//  Created by Suresh Murugaiyan on 9/1/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class PriceAndSLA: NSObject {
    var priceArray:[Price]?
    var slaArray:[SLA]?
    
    class func getPriceAndSlaList(xmlResponseData:XMLIndexer)->(PriceAndSLA){
        let pSList = PriceAndSLA()
        pSList.priceArray=[Price]()
        pSList.slaArray=[SLA]()
        
        do{
         let keys = try xmlResponseData.findAll("ns2:pricePoints");
//        }catch{}
//        let tempArray = xmlResponseData["SOAP-ENV:Envelope"]["SOAP-ENV:Body"]["ns2:checkPriceResponse"]["ns2:responsePrice"]["ns2:prices"]["ns2:onnetPrice"]["ns2:onnetNetworkPointPrices"]
        
//        for child in key.children {
            
//            if child.element?.name == "ns2:pricePoints" {
            
            for key in keys{
                let tempPrice=Price()
                pSList.priceArray?.append(tempPrice)
                for subChild in key.children {
                    if subChild.element?.name == "ns2:bEndDeliverySupplier" {
                        tempPrice.bEndDeliverySupplier=subChild.element?.text
                    }else if subChild.element?.name == "ns2:aEndDeliverySupplier" {
                        tempPrice.aEndDeliverySupplier=subChild.element?.text
                    }else if subChild.element?.name == "ns2:priceNrc1" {
                        tempPrice.priceNrc1=subChild.element?.text
                    }else if subChild.element?.name == "ns2:priceMrc1" {
                        tempPrice.priceMrc1=subChild.element?.text
                    }else if subChild.element?.name == "ns2:totalPrice1" {
                        tempPrice.totalPrice1=subChild.element?.text
                    }else if subChild.element?.name == "ns2:priceNrc2" {
                        tempPrice.priceNrc2=subChild.element?.text
                    }else if subChild.element?.name == "ns2:priceMrc2" {
                        tempPrice.priceMrc2=subChild.element?.text
                    }else if subChild.element?.name == "ns2:totalPrice2" {
                        tempPrice.totalPrice2=subChild.element?.text
                    }else if subChild.element?.name == "ns2:priceNrc3" {
                        tempPrice.priceNrc3=subChild.element?.text
                    }else if subChild.element?.name == "ns2:priceMrc3" {
                        tempPrice.priceMrc3=subChild.element?.text
                    }else if subChild.element?.name == "ns2:totalPrice3" {
                        tempPrice.totalPrice3=subChild.element?.text
                    }else if subChild.element?.name == "ns2:currency" {
                        tempPrice.currency=subChild.element?.text
                    }else if subChild.element?.name == "ns2:remarks" {
                        tempPrice.remarksPrice=subChild.element?.text
                    }else if subChild.element?.name == "ns2:accessTypeAEnd" {
                        tempPrice.accessTypeAEnd=subChild.element?.text
                    }else if subChild.element?.name == "ns2:bandwidthDesc" {
                        tempPrice.bandwidthDesc=subChild.element?.text
                        

                    }
                }
            }
                
//            }else if child.element?.name == "ns2:SLA" {
//               let tempSla=SLA()
//                for subChild in child.children {
//                    if subChild.element?.name == "ns2:deliverTimeInDays" {
//                        tempSla.deliverTimeInDays=subChild.element?.text
//                    }else if subChild.element?.name == "ns2:servicePresentation" {
//                        tempSla.servicePresentation=subChild.element?.text
//                    }else if subChild.element?.name == "ns2:targetRepairTimeInHours" {
//                        tempSla.targetRepairTimeInHours=subChild.element?.text
//                    }else if subChild.element?.name == "ns2:availabilityInPercentage" {
//                        tempSla.availabilityInPercentage=subChild.element?.text
//                        pSList.slaArray?.append(tempSla)
//                    }else if subChild.element?.name == "ns2:remarks" {
//                        tempSla.remarksSla=subChild.element?.text
//                    }
//                }
//            }
//        }
//        print(pSList)
      }catch{}
        
        do{
           let keys = try xmlResponseData.findAll("ns2:SLA")
            for key in keys{
                let tempSla=SLA()
                pSList.slaArray?.append(tempSla)
            for subChild in key.children {
                if subChild.element?.name == "ns2:deliverTimeInDays" {
                   tempSla.deliverTimeInDays=subChild.element?.text
                }else if subChild.element?.name == "ns2:servicePresentation" {
                   tempSla.servicePresentation=subChild.element?.text
                }else if subChild.element?.name == "ns2:targetRepairTimeInHours" {
                   tempSla.targetRepairTimeInHours=subChild.element?.text
                }else if subChild.element?.name == "ns2:availabilityInPercentage" {
                   tempSla.availabilityInPercentage=subChild.element?.text
                }else if subChild.element?.name == "ns2:remarks" {
                  tempSla.remarksSla=subChild.element?.text
              }
            }
          }
        }catch{}
    
     return pSList
            
    }
}

class Price: NSObject {
    var bEndDeliverySupplier:String?
    var aEndDeliverySupplier:String?
    var priceNrc1:String?
    var priceMrc1:String?
    var totalPrice1:String?
    var priceNrc2:String?
    var priceMrc2:String?
    var totalPrice2:String?
    var priceNrc3:String?
    var priceMrc3:String?
    var totalPrice3:String?
    var currency:String?
    var bandwidthDesc:String?
    var accessTypeAEnd:String?
    var remarksPrice:String?
}

class SLA: NSObject {
    var deliverTimeInDays:String?
    var servicePresentation:String?
    var targetRepairTimeInHours:String?
    var availabilityInPercentage:String?
    var remarks:String?
    var remarksSla:String?
}
