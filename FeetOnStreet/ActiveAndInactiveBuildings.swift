//
//  ActiveAndInactiveBuildings.swift
//  FeetOnStreet
//
//  Created by Suresh Murugaiyan on 8/11/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
enum buildingType {
    case Onnet
    case Nearnet
    
}

class ActiveAndInactiveBuildings: NSObject{

    var buildingStatus:String?
    var coltOperatingCountry:String?
    var englishCityName:String?
    var postCode:String?
    var permisesNumber:String?
    var streetName:String?
    var lattitude:Double?
    var longitude:Double?
    var pointID:String?
    var inhouseCablingviaCOLTpossible:Bool?
    var dualEntryAvailability:Bool?
    var messageCode:Double?
    
    var nearnetDistance:String?
    var isNearNet:String?
    var indicativeLeadTime:String?
    var notes:String?
    
    
//    var nearnetCityName:String?
//    var nearnetPremisesNumber:String?
//    var nearnetStreetName:String?
//    var nearnetPostCode:String?
//    var nearnetLattitude:Double?
//    var nearnetLongitude:Double?
//    var nearnetColtOperatingCountry:String?
//    var nearnetPointID:String?
    
    
    
    var bType:buildingType?
    
    
   //TODO: Fix indentation
   //TODO: Documentation
   //TODO: Good Practice. If you are using class func instead of static. Use "class final" if you are not overriding it.
//   class func getActiceAndInactiveBuildingsFromResponse(xmlResponseData:XMLIndexer) -> [ActiveAndInactiveBuildings] {
//
//
//
//
//
//
////    var nearNetBuilding:ActiveAndInactiveBuildings?
//
//
//
//
//        return aiBuildings
//    }
    
   class func getNearnrtLocations(xmlResponseData:XMLIndexer)-> [ActiveAndInactiveBuildings] {
        var aiBuildings = [ActiveAndInactiveBuildings]()
        let nearNetArray = xmlResponseData["SOAP-ENV:Envelope"]["SOAP-ENV:Body"]["ns2:checkConnectivityResponse"]["ns2:localSiteAddress"]["ns2:nearNetStatus"]["ns2:nearNetAEndResult"]["ns2:nearNetResult"]
    
    var tempBuilding:ActiveAndInactiveBuildings?
    
        for child in nearNetArray.children {
            
//            print(child.element?.name)
            if child.element?.name == "ns2:premisesNumber" {
                tempBuilding = ActiveAndInactiveBuildings()
                tempBuilding?.bType = buildingType.Nearnet
                tempBuilding?.permisesNumber=child.element?.text
            }
            if child.element?.name == "ns2:streetName" {
                tempBuilding?.streetName=child.element?.text
            }
            if child.element?.name == "ns2:cityName" {
                tempBuilding?.englishCityName=child.element?.text
            }
            if child.element?.name == "ns2:postCode" {
                tempBuilding?.postCode=child.element?.text
            }
            if child.element?.name == "ns2:latitude" {
                tempBuilding?.lattitude=Double((child.element?.text)!)
            }
            if child.element?.name == "ns2:longitude" {
                tempBuilding?.longitude=Double((child.element?.text)!)
            }
            if child.element?.name == "ns2:coltOperatingCountry" {
                tempBuilding?.coltOperatingCountry=child.element?.text
            }
            if child.element?.name == "ns2:isNearNet" {
                tempBuilding?.isNearNet=child.element?.text
            }
            if child.element?.name == "ns2:pointID" {
                tempBuilding?.pointID=child.element?.text
            }
            if child.element?.name == "ns2:nearnetDistance" {
                tempBuilding?.nearnetDistance=child.element?.text
            }
            if child.element?.name == "ns2:indicativeLeadTime" {
                tempBuilding?.indicativeLeadTime=child.element?.text
            }
            if child.element?.name == "ns2:notes" {
                tempBuilding?.notes=child.element?.text
                aiBuildings.append((tempBuilding)!)
            }
        }
      return aiBuildings
    }
   class func getOnnetLocations(xmlResponseData:XMLIndexer)-> [ActiveAndInactiveBuildings] {
    var aiBuildings: [ActiveAndInactiveBuildings] = []
        let tempArray = xmlResponseData["SOAP-ENV:Envelope"]["SOAP-ENV:Body"]["ns2:checkConnectivityResponse"]["ns2:localSiteAddress"]["ns2:onNetStatus"]["ns2:onNetAEndResult"]["ns2:onNetResult"]
        
    var tempBuilding:ActiveAndInactiveBuildings?
        for child in tempArray.children {
            
            
//            print(child.element?.name)
            if child.element?.name == "ns2:buildingStatus" {
                tempBuilding=ActiveAndInactiveBuildings()
                tempBuilding?.bType = buildingType.Onnet
                tempBuilding?.buildingStatus=child.element?.text
            }
            if child.element?.name == "ns2:coltOperatingCountry" {
                tempBuilding?.coltOperatingCountry=child.element?.text
            }
            if child.element?.name == "ns2:englishCityName" {
                tempBuilding?.englishCityName=child.element?.text
            }
            if child.element?.name == "ns2:postCode" {
                tempBuilding?.postCode=child.element?.text
            }
            if child.element?.name == "ns2:premisesNumber" {
                tempBuilding?.permisesNumber=child.element?.text
            }
            if child.element?.name == "ns2:streetName" {
                tempBuilding?.streetName=child.element?.text
            }
            if child.element?.name == "ns2:latitude" {
                tempBuilding?.lattitude=Double((child.element?.text)!)
            }
            if child.element?.name == "ns2:longitude" {
                tempBuilding?.longitude=Double((child.element?.text)!)
            }
            if child.element?.name == "ns2:inhouseCablingviaCOLTpossible" {
                let tempInhouse = child.element?.text
                
                if(tempInhouse?.lowercased() == "yes"){
                    tempBuilding?.inhouseCablingviaCOLTpossible=true
                }else{
                    tempBuilding?.inhouseCablingviaCOLTpossible=false
                }
            }
            if child.element?.name == "ns2:dualEntryAvailability" {
                tempBuilding?.dualEntryAvailability=Bool((child.element?.text)!)
            }
            if child.element?.name == "ns2:messageCode" {
                tempBuilding?.messageCode=Double((child.element?.text)!)
            }
            if child.element?.name == "ns2:pointID" {
                tempBuilding?.pointID=child.element?.text
                aiBuildings.append((tempBuilding)!)
            }
            
        }
       return aiBuildings
    }
}




