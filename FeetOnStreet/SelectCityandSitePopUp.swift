//
//  SelectCityandSitePopUp.swift
//  FeetOnStreet
//
//  Created by admin on 3/1/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class SelectCityandSitePopUp: UIView,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var viewHeight:CGFloat?
    var viewController: CityandSiteViewController?
    var list = NSArray()
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return list.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "CityAndSitePopUpCell", bundle: nil), forCellReuseIdentifier: "CityAndSitePopUpCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityAndSitePopUpCell", for: indexPath) as! CityAndSitePopUpCell
        cell.textLabel?.text = list[indexPath.row] as? String
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let city = list[indexPath.row]
        viewController?.selectedName = city as! String
        viewController?.selectedIndex = indexPath.row
    }

}
