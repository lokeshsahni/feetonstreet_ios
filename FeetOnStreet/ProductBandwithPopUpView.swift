//
//  ProductBandwithPopUpView.swift
//  FeetOnStreet
//
//  Created by admin on 9/28/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import SwiftOverlays

protocol ProductBandwithPopupDelegate : class{
    func dismissProductBandwithPopup(selectionType:String,productBandwithPopUpView:ProductBandwithPopUpView)
}

enum TableViewType {
    case ProductType
    case BandwidthType
    case HubListType
}

class ProductBandwithPopUpView: UIView, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate,GMSMapViewDelegate,GMSAutocompleteViewControllerDelegate {
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var scroolView: UIScrollView!
    @IBOutlet weak var popUpTableVIEW: UITableView!
    
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var selectProduView: UIView!
    @IBOutlet weak var selectHubView: UIView!
    @IBOutlet weak var eMFandOLOview: UIView!
    @IBOutlet weak var checkPriceView: UIView!
    
    @IBOutlet weak var popUpAddressLbl: UILabel!
    
    @IBOutlet weak var selectAproductBtn: UIButton!
    @IBOutlet weak var selectBandwidthBtn: UIButton!
    @IBOutlet weak var bandWidthTitleLbl: UILabel!
    @IBOutlet weak var productBandwidthPopupBackgroundBtn: UIButton!
    @IBOutlet weak var productAndBandwidthPopUpView: UIView!
    @IBOutlet weak var popUpBackgroundBtn: UIButton!
    
    
    @IBOutlet weak var hubAddressView: UIView!
    @IBOutlet weak var hubAdressLbl: UILabel!
    @IBOutlet weak var hubNameLbl: UILabel!
    @IBOutlet weak var hubLevelLbl: UILabel!
    @IBOutlet weak var selectHubBtn: UIButton!
    
    
    @IBOutlet weak var addressTxtFld: UITextField!
    @IBOutlet weak var eMF_DSLImgView: UIImageView!
    @IBOutlet weak var eMF_DSLbtn: UIButton!
    @IBOutlet weak var oLOimgView: UIImageView!
    @IBOutlet weak var oLObtn: UIButton!
    @IBOutlet weak var selectedBEndBtn: UIButton!
    @IBOutlet weak var buildingDetailsBtn: UIButton!
    
    
    @IBOutlet weak var productAndBandwidthPopUpViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var hubViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var eMFandOLOviewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var popUpviewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var popUpviewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var priceAndBuildingViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var efmOloBackroundSubView: UIView!
    
    
    var isOLObtnBtnselected = false
    var isEMF_DSLbtnselected = false
    var oLOlist:[Any] = []
    var eMFDSLlist:[Any] = []
    var addressMatchedList:[ActiveAndInactiveBuildings]? = []
    var citiesMatchedList:[ActiveAndInactiveBuildings]? = []
    var streetsArray:[Any] = []
    var selectedMarker:ActiveAndInactiveBuildings?
    
    
    var eMFpopupView:EMFandDSLPopUP?
    
    var abArray:[ActiveAndInactiveBuildings] = []
    var activeINactiveBuildngs:[ActiveAndInactiveBuildings]?
    var selectedBendAdderess:ActiveAndInactiveBuildings?
    var permisesNumber:String?
    
    var product_list:[ProductsModel]?
    
    var bandWidth_list:[String] = []
    
    var countries_list:[String] = []
    var product_bandwidth_list:[ProductsModel] = []
    
    var selectedProduct:String?
    var selectedProductCode:String?
    
    var selectedBandwidth:String?
    var selectedHub:[String:Any] = [:]
    var productBandWidth_list:[String] = []
    var hub_list:[String] = []
    var hubs_list: NSMutableArray = NSMutableArray()
    var hubList:[Dictionary<String, String>]?
    var aEndAddress_id:String?
    var isHubSelected : Bool = false
    var point_id:String = ""
    var formattedAddress:String = ""
    var selectedPlace:GMSPlace?
    var selectedB_EndAddress:String?
    var selectedPlaceArray:[ActiveAndInactiveBuildings]?
    var dSLmodel:DSLModel?
    var oLOmOdel:OLOModel?
    var filter_places:[Any] = []
    var abiBuildings:[String] = []
    
    var permisesnum:String?
    var streetName:String?
    var cityTown:String?
    var postCode:String?
    var country:String?
    var selectedaddressLatitude:Double?
    var selectedAddressLongitude:Double?
    
    var point_ID:String?
    var address1:String?
    var englishCityName:String?
    var isEfmOloVC = false
    
    var isProductBtnselected = true
    
    var tableType : TableViewType = TableViewType.ProductType
    var selectMainIndexPath : IndexPath?
    
    static var selectedProductIndex = 0
    static var selectedBandwidthIndex = 0
    
    
    var productBandwithPopUpView : ProductBandwithPopUpView!
    var viewHeight:CGFloat?
    var productBandwithpopupViewFrame:CGRect?
    var viewController: UIViewController?
    var productName:String?
    weak var delegate:ProductBandwithPopupDelegate?
    var isBuildiningDeytailsBtnHidden = false
    
    var multiPriceList:[Any]=[]
    var viewType:ViewType?
    var employeeType:String?
    
    var popupController:UIViewController?
    
    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
        
        // Drawing code
        
        
        popUpTableVIEW.rowHeight = UITableViewAutomaticDimension
        popUpTableVIEW.estimatedRowHeight = 1000
        
        selectAproductBtn.layer.borderWidth = 1
        selectBandwidthBtn.layer.borderWidth = 1
        selectHubView.layer.borderWidth = 1
        hubAddressView.layer.borderWidth = 1
        selectAproductBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        selectBandwidthBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        selectHubBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        hubLevelLbl.text = ""
        hubNameLbl.text = ""
        hubAdressLbl.text = ""
        popUpAddressLbl.text = address1
        eMF_DSLImgView.image = UIImage(named: "ic_uncheck_box.png")
        oLOimgView.image = UIImage(named: "ic_uncheck_box.png")
        selectedBEndBtn.layer.borderWidth = 1
        //      selectedBEndBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        scroolView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        scroolView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        
        if let products = CoreDataHelper.sharedInstance.getAllProductsWithProductCode() {
     
            var productsTempArray:[ProductsModel] = []
            for product in products {
                if product.productName == "Colt Business Access Pack (with VoIP)" || product.productName == "Colt Business Access Pack (with IP Voice Line)" || product.productName == "Colt Link Point to Multi Point (SDH Point to Multi Point)" || product.productName == "Colt Link Point to Point (SDH Point to Point)" || product.productName == "Colt Voice Line (v)" {
                    
                }else{
                    productsTempArray.append(product)
                }
            }
            product_list = productsTempArray

        }
        
        if let countries = CoreDataHelper.sharedInstance.getAllCountries() {
            countries_list = countries
        }
        print(countries_list)
        
        if let bandwidth = CoreDataHelper.sharedInstance.getAllBandwidths() {
            bandWidth_list = bandwidth
        }
        print(bandWidth_list)
        
        if let npc = CommonDataHelper.sharedInstance.npc {
            
            print("Npc = ",npc)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.dismissEmfPopupView), name: Notification.Name("EMF_DSL_Popup"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.dismissEmfPopupViewWithDSL), name: Notification.Name("EMF_DSL_Popup_DSLResult"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.dismissEmfPopupViewWithOLO), name: Notification.Name("EMF_DSL_Popup_OLOResult"), object: nil)
        
        if isBuildiningDeytailsBtnHidden == true {
            buildingDetailsBtn.backgroundColor = UIColor.lightGray
            buildingDetailsBtn.isUserInteractionEnabled = false
        }
        if isEfmOloVC == true {
            buildingDetailsBtn.backgroundColor = UIColor.lightGray
            buildingDetailsBtn.isUserInteractionEnabled = false
            selectAproductBtn.setTitleColor(UIColor.lightGray, for: .normal)
            selectAproductBtn.isUserInteractionEnabled = false
            selectBandwidthBtn.setTitleColor(UIColor.lightGray, for: .normal)
            selectBandwidthBtn.isUserInteractionEnabled = false
        }
        
        self.setFrameBasedOnSelectedProduct()
        
        // Popuup for Normal products
        showPopupRoutine()
        var err_msg : String?
        let tempKey = UserDefaults.standard.value(forKey: "eType")
        employeeType =  CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: tempKey as! String)
        if employeeType == "Non Employee" {
            buildingDetailsBtn.backgroundColor  = UIColor.lightGray
            buildingDetailsBtn.isUserInteractionEnabled = false
        }
        if CommonDataHelper.sharedInstance.selectedProduct == "Colt Ethernet Hub and Spoke" || CommonDataHelper.sharedInstance.selectedProduct == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
            callHublistAPI()
        }
        
        
    }
    
    
    public func dismissEmfPopupView(notification:Notification) {
        if notification.object == nil{
            eMFpopupView?.removeFromSuperview()
            
        }else{
            selectedBendAdderess = notification.object as? ActiveAndInactiveBuildings
            permisesNumber = selectedBendAdderess!.permisesNumber
            let streetName = selectedBendAdderess!.streetName
            let cityName = selectedBendAdderess!.englishCityName
            let postCode = selectedBendAdderess!.postCode
            let coltOperatingCountry = selectedBendAdderess!.coltOperatingCountry
            selectedB_EndAddress = "\(permisesNumber!), \(streetName!), \(cityName!), \(postCode!), \(coltOperatingCountry!)"
            selectedBEndBtn.setTitle(selectedB_EndAddress,for: .normal)
            
            //        NotificationCenter.default.removeObserver(self, name: Notification.Name("EMF_DSL_Popup"), object: nil)
            eMFpopupView?.removeFromSuperview()
        }
    }
    public func dismissEmfPopupViewWithDSL(notification:Notification) {
        if notification.object == nil{
            eMFpopupView?.removeFromSuperview()
            
        }else{
            dSLmodel = notification.object as? DSLModel
            aEndAddress_id = dSLmodel?.pointID
            CommonDataHelper.sharedInstance.npc = aEndAddress_id
            //        if selectedBendAdderess!.permisesNumber != nil {
            //            permisesNumber = dSLmodel!.permisesNumber
            //        }
            let streetName = dSLmodel!.streetName
            let cityName = dSLmodel!.cityName
            let postCode = dSLmodel!.postCode
            let coltOperatingCountry = dSLmodel!.coltOperatingCountry
            if dSLmodel!.streetName == nil {
                selectedB_EndAddress = "\(cityName!), \(postCode!), \(coltOperatingCountry!)"
            }else {
                selectedB_EndAddress = "\(streetName!), \(cityName!), \(postCode!), \(coltOperatingCountry!)"
            }
            
            selectedBEndBtn.setTitle(selectedB_EndAddress,for: .normal)
            
            //        NotificationCenter.default.removeObserver(self, name: Notification.Name("EMF_DSL_Popup"), object: nil)
            eMFpopupView?.removeFromSuperview()
        }
    }
    public func dismissEmfPopupViewWithOLO(notification:Notification) {
        if notification.object == nil{
            eMFpopupView?.removeFromSuperview()
            
        }else{
            
            oLOmOdel = notification.object as? OLOModel
            aEndAddress_id = oLOmOdel?.pointID
            CommonDataHelper.sharedInstance.npc = aEndAddress_id
            //        if selectedBendAdderess!.permisesNumber != nil {
            permisesNumber = oLOmOdel?.premisesNumber
            //        }
            let streetName = oLOmOdel?.streetName
            let cityName = oLOmOdel?.cityName
            let postCode = oLOmOdel?.postCode
            let coltOperatingCountry = oLOmOdel?.coltOperatingCountry
            
            if oLOmOdel!.streetName == nil {
                selectedB_EndAddress = "\(cityName!), \(postCode!), \(coltOperatingCountry!)"
            }else {
                selectedB_EndAddress = "\(streetName!), \(cityName!), \(postCode!), \(coltOperatingCountry!)"
            }
            selectedBEndBtn.setTitle(selectedB_EndAddress,for: .normal)
            
            //        NotificationCenter.default.removeObserver(self, name: Notification.Name("EMF_DSL_Popup"), object: nil)
            eMFpopupView?.removeFromSuperview()
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count:Int?
        
        if tableType == TableViewType.ProductType {
            count = product_bandwidth_list.count+1
        }else if tableType == TableViewType.BandwidthType {
            count = bandWidth_list.count+1
        }
        else if tableType == TableViewType.HubListType {
            count = (self.hubList?.count)!+1
            
        }
        
        return count!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableType == TableViewType.BandwidthType || tableType == TableViewType.ProductType {
            let selectProduct = NSLocalizedString("Select Product", comment: "")
            let selectBandwidth = NSLocalizedString("Select Bandwidth", comment: "")
            
            popUpTableVIEW.register(UINib(nibName: "ProductBandwidthCell", bundle: nil), forCellReuseIdentifier: "ProductBandwidthCell")
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductBandwidthCell", for: indexPath) as! ProductBandwidthCell
            
            selectedProduct = CommonDataHelper.sharedInstance.selectedProduct
            selectedBandwidth = CommonDataHelper.sharedInstance.selectedBandwidth
            
            if selectedProduct != nil {
                productName = selectedProduct
                
                //Change product names for response
                self.setProductNames()
//                if selectedProduct == "Select Product"{
//                    selectedProduct = selectProduct
//                }
                if productName == "Select Product"{
                    productName = selectProduct
                }
                selectAproductBtn .setTitle(productName, for: .normal)
                
            }else{
                
                selectAproductBtn.setTitle(selectProduct, for: .normal)
            }
            if selectedBandwidth != nil {
                var bandwidthName = selectedBandwidth
                if bandwidthName == "Select Bandwidth"{
                    bandwidthName = selectBandwidth
                }
                
                selectBandwidthBtn .setTitle(bandwidthName, for: .normal)
            }else{
                if selectedProduct == "Colt VoIP Access" {
                    let voiceChannels = NSLocalizedString("Voice Channels", comment: "")
                    selectBandwidthBtn.setTitle(voiceChannels, for: .normal)
                }else{
                    
                    selectBandwidthBtn.setTitle(selectBandwidth, for: .normal)
                }
            }
            
            if indexPath.row == 0 {
                if tableType == TableViewType.ProductType {
                    cell.textLabel?.text = selectProduct
                }else{
                    if selectedProduct == "Colt VoIP Access" {
                        let voiceChannels = NSLocalizedString("Voice Channels", comment: "")
                        cell.textLabel?.text = voiceChannels
                        bandWidthTitleLbl.text = voiceChannels
                        selectBandwidthBtn.setTitle(voiceChannels, for: .normal)
                        
                    }else{
                        cell.textLabel?.text = selectBandwidth
                    }
                }
                return cell
            }else{
                if tableType == TableViewType.ProductType {
                    let product_list = product_bandwidth_list[indexPath.row-1]
                    productName = product_list.productName
                }else{
                    productName = bandWidth_list[indexPath.row-1]
                }
                
                if tableType == .ProductType{
                    //Change product names for response
                    setProductNames()
                }
                cell.textLabel?.text = productName
                return cell
            }
        }else{

            popUpTableVIEW.register(UINib(nibName: "SelectHubCell", bundle: nil), forCellReuseIdentifier: "SelectHubCell")
            let selectHubCell = tableView.dequeueReusableCell(withIdentifier: "SelectHubCell", for: indexPath) as! SelectHubCell
            if indexPath.row == 0 {
                let selectHub = NSLocalizedString("Select Hub", comment: "")
                selectHubCell.adressLbl?.text = selectHub
                selectHubCell.levelLbl.isHidden = true
                selectHubCell.countryLbl.isHidden = true
                return selectHubCell
            }else{
                selectHubCell.levelLbl.isHidden = false
                selectHubCell.countryLbl.isHidden = false
                //                  var hub = hubs_list[indexPath.row-1] as! [String:Any]
                //                  cell.adressLbl.text = hub["formatted_address"] as? String
                //                  cell.countryLbl.text = hub["hub_name"] as? String
                
                let tempListVal = hubList?[indexPath.row-1]
                selectHubCell.adressLbl.text=tempListVal?["formatted_address"]
                selectHubCell.countryLbl.text=tempListVal?["hub_name"]
                selectHubCell.levelLbl.text = "Level 3"
                
                return selectHubCell
                //                }
                
            }
            /*
             }else{
             let oNNETCell = tableView.dequeueReusableCell(withIdentifier: "ONNETCell", for: indexPath) as? ONNETCell
             let aiBuilding = self.abArray?[indexPath.row]
             let permisesNumber =  aiBuilding?.permisesNumber
             let streetName =  aiBuilding?.streetName
             let englishCityName =  aiBuilding?.englishCityName
             let postCode =  aiBuilding?.postCode
             point_ID = aiBuilding?.pointID
             let coltOperatingCountry =  aiBuilding?.coltOperatingCountry
             let address1 = "\(permisesNumber!), \(streetName!), \(englishCityName!), \(postCode!), \(coltOperatingCountry!)"
             print(address1)
             oNNETCell?.addressLbl.text = address1
             
             if aiBuilding?.buildingStatus == "ACTIVE" {
             oNNETCell?.inActiveBtn.setTitle("ACTIVE", for: .normal)
             oNNETCell?.inActiveBtn.backgroundColor = UIColor (colorLiteralRed: 0/255, green: 130/255, blue: 70/255, alpha: 1.0)
             }else{
             oNNETCell?.inActiveBtn .setTitle("INACTIVE", for: .normal)
             oNNETCell?.inActiveBtn.backgroundColor = UIColor.red
             }
             if aiBuilding?.inhouseCablingviaCOLTpossible == true {
             oNNETCell?.inHouseCablingBtn.isHidden = false
             }else{
             oNNETCell?.inHouseCablingBtn.isHidden = true
             }
             
             
             if indexPath == selectMainIndexPath {
             oNNETCell?.bottomView.backgroundColor = UIColor (colorLiteralRed: 0/255, green: 165/255, blue: 155/255, alpha: 1.0)
             }else{
             oNNETCell?.bottomView.backgroundColor = UIColor (colorLiteralRed: 241/255, green: 241/255, blue: 241/255, alpha: 1.0)
             }
             if aiBuilding == selectedMarker {
             oNNETCell?.bottomView.backgroundColor = UIColor (colorLiteralRed: 0/255, green: 165/255, blue: 155/255, alpha: 1.0)
             }
             
             oNNETCell?.locationBtn.addTarget(self, action: #selector(self.mapsButton(button:)), for: .touchUpInside)
             oNNETCell?.locationBtn.tag=indexPath.row
             oNNETCell?.inActiveBtn.addTarget(self, action: #selector(self.inActiveBtn(button:)), for: .touchUpInside)
             oNNETCell?.inHouseCablingBtn.addTarget(self, action: #selector(self.inHouseCablingBtn(button:)), for: .touchUpInside)
             
             return oNNETCell!
             }
             */
        }
    }
    func refreshPopupTable(){
        
        self.popUpTableVIEW.reloadData()
        self.popUpTableVIEW.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: true)
    }
    
    func CleanUIOnProductChange(selectRowAt indexPath: IndexPath) {
        //to fake static variable.
        struct staticHolder {
            static var row = 0
        }
        
        if(indexPath.row != staticHolder.row){
            
            staticHolder.row = indexPath.row
            
            //UI.....
            self.hubLevelLbl.text = ""
            self.hubNameLbl.text = ""
            self.hubAdressLbl.text = ""
            let selectHub = NSLocalizedString("Select Hub", comment: "")
            self.selectHubBtn.setTitle(selectHub, for: .normal)
            
            self.addressTxtFld.text = ""
            let selectBandwidth = NSLocalizedString("Select Bandwidth", comment: "")
            self.selectBandwidthBtn.setTitle(selectBandwidth, for: .normal)
            
            //UI and Model.....
            if(self.isEMF_DSLbtnselected) {
                self.eMF_DSLbtnAction(self.eMF_DSLImgView)
            }
            if(self.isOLObtnBtnselected){
                self.oLObtnAction(self.oLOimgView)
            }
            
            //Model...
            self.selectedBandwidth = nil
            self.selectedPlace = nil
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableType == TableViewType.ProductType {
            //                isHubSelected = false
            
            CleanUIOnProductChange(selectRowAt: indexPath)
            
            if indexPath.row == 0 {
                selectedProduct = "Select Product"
                CommonDataHelper.sharedInstance.selectedProduct = selectedProduct
                CommonDataHelper.sharedInstance.selectedBandwidth = nil
            }else{
                let selectProduct = product_bandwidth_list[indexPath.row-1]
                selectedProduct = selectProduct.productName
                selectedProductCode = selectProduct.productCode
                CommonDataHelper.sharedInstance.selectedProduct = selectedProduct
                CommonDataHelper.sharedInstance.selectedProductCode = selectedProductCode
                CommonDataHelper.sharedInstance.selectedBandwidth = nil
            }
            if selectedProduct == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
                selectHubView.isHidden  = false
                eMFandOLOview.isHidden  = true
                CommonDataHelper.sharedInstance.npc = nil
                callHublistAPI()
                setPopUpFrameWithHubView()
                priceAndBuildingViewTopConstraint.constant = selectHubView.frame.size.height+20
            }
            else if selectedProduct == "Colt LANLink Point to Point (Ethernet Point to Point)" {
                efmOloBackroundSubView.isHidden = false
                eMFandOLOview.isHidden  = false
                selectHubView.isHidden  = true
                // Making nil B_End address popup array
                self.addressMatchedList = nil
                self.selectedBEndBtn.setTitle(nil, for: .normal)
                priceAndBuildingViewTopConstraint.constant = eMFandOLOview.frame.size.height+20
                eMFandOLOviewTopConstraint.constant = 10
                setPopUpFrameWithEMFandOLOView()
            }
            else if selectedProduct == "Colt Wave" || selectedProduct == "Colt Private Ethernet" {
                efmOloBackroundSubView.isHidden = true
                eMFandOLOview.isHidden  = false
                selectHubView.isHidden  = true
                // Making nil B_End address popup array
                self.addressMatchedList = nil
                self.selectedBEndBtn.setTitle(nil, for: .normal)
                priceAndBuildingViewTopConstraint.constant = eMFandOLOview.frame.size.height+20
                eMFandOLOviewTopConstraint.constant = 10
                setPopUpFrameWithEMFandOLOView()
            }
            else{
                priceAndBuildingViewTopConstraint.constant = 20
                selectHubView.isHidden  = true
                eMFandOLOview.isHidden  = true
                setPopUpFrame()
            }
            if selectedProduct == "Colt VoIP Access" {
                selectedBandwidth = "Voice Channels"
                let voiceChannels = NSLocalizedString("Voice Channels", comment: "")
                selectBandwidthBtn.setTitle(voiceChannels, for: .normal)
                bandWidthTitleLbl.text = voiceChannels
                selectedProduct = "Colt SIP Trunking"
                selectedBandwidth = "Voice Channels"
            }else if selectedProduct == "Colt LANLink Point to Point (Ethernet Point to Point)" {
                selectedProduct = "Colt Ethernet Line"
                
            }else if selectedProduct == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
                selectedProduct = "Colt Ethernet Hub and Spoke"
                
            }else if selectedProduct == "Colt Ethernet Private Network (EPN)"{
                selectedProduct = "ColT Ethernet VPN"
            }
            else{
                let selectBandwidth = NSLocalizedString("Select Bandwidth", comment: "")
                selectBandwidthBtn.setTitle(selectBandwidth, for: .normal)
                let bandwidth = NSLocalizedString("Bandwidth", comment: "")
                bandWidthTitleLbl.text = bandwidth
            }
            selectAproductBtn .setTitle(selectedProduct, for: .normal)
            productBandwidthPopupBackgroundBtn.isHidden = true
            productAndBandwidthPopUpView.isHidden = true
            isProductBtnselected = false
        }else if tableType == TableViewType.BandwidthType {
            if indexPath.row == 0 {
                if selectedProduct == "Colt VoIP Access" {
                    selectedBandwidth = "Voice Channels"
                    CommonDataHelper.sharedInstance.selectedBandwidth = selectedBandwidth
                    
                }else{
                    selectedBandwidth = "Select Bandwidth"
                    CommonDataHelper.sharedInstance.selectedBandwidth = selectedBandwidth
                }
                
            }else{
                selectedBandwidth = bandWidth_list[indexPath.row-1]
                CommonDataHelper.sharedInstance.selectedBandwidth = selectedBandwidth
                
            }
            //                CommonDataHelper.sharedInstance.selectedBandwidth = selectedBandwidth
            selectBandwidthBtn.setTitle(selectedBandwidth, for: .normal)
            productBandwidthPopupBackgroundBtn.isHidden = true
            productAndBandwidthPopUpView.isHidden = true
        }else{
            if indexPath.row == 0 {
                selectedHub = ["formatted_address":"Select Hub","hub_name":""]
                let selectHub = NSLocalizedString("Select Hub", comment: "")
                selectHubBtn.setTitle(selectHub, for: .normal)
            }else{
                selectedHub = hubList![indexPath.row-1]
                hubAdressLbl.text = selectedHub["formatted_address"] as? String
                hubNameLbl.text = selectedHub["hub_name"] as? String
                //                    hubNameLbl.backgroundColor = UIColor (colorLiteralRed: 0/255, green: 165/255, blue: 155/255, alpha: 1.0)
                hubNameLbl.backgroundColor = UIColor.c_Teal
                hubLevelLbl.text = "Level 3"
                aEndAddress_id = selectedHub["point_id"] as? String
                CommonDataHelper.sharedInstance.npc = aEndAddress_id
                productBandwidthPopupBackgroundBtn.isHidden = true
                productAndBandwidthPopUpView.isHidden = true
                selectHubBtn.setTitle("", for: .normal)
            }
        }
    }
    
    func setProductNames() {
        if productName == "Colt VoIP Access" {
            productName = "Colt SIP Trunking"
        }else if productName == "Colt LANLink Point to Point (Ethernet Point to Point)" {
            productName = "Colt Ethernet Line"
            
        }else if productName == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
            productName = "Colt Ethernet Hub and Spoke"
        }else if productName == "Colt Ethernet Private Network (EPN)"{
            productName = "ColT Ethernet VPN"
        }
    }
    
    func showPopupRoutine(){
        scroolView.isHidden = false
        popUpView.isHidden = false
        popUpBackgroundBtn.isHidden = false
        priceAndBuildingViewTopConstraint.constant = 20
        setFrameBasedOnSelectedProduct()
        //        setPopUpFrame()
    }
    
    func setPopUpFrame() {
        //        let viewHeight = productBandwithPopUpView?.frame.size.height
        let popupHeight = self.frame.size.height - (selectProduView.frame.size.height+addressView.frame.size.height+checkPriceView.frame.size.height+30)
        popUpviewTopConstraint.constant  = popupHeight/2
        popUpviewBottomConstraint.constant  = popupHeight/2
        self.refreshPopupTable()
    }
    func setPopUpFrameWithHubView() {
        //        let viewHeight = productBandwithPopUpView?.frame.size.height
        let popupHeight = self.frame.size.height - (selectProduView.frame.size.height+addressView.frame.size.height+checkPriceView.frame.size.height+selectHubView.frame.size.height+30)
        popUpviewTopConstraint.constant  = popupHeight/2
        popUpviewBottomConstraint.constant  = popupHeight/2
    }
    func setPopUpFrameWithEMFandOLOView() {
        //        let viewHeight = productBandwithPopUpView?.frame.size.height
        let popupHeight = self.frame.size.height - (selectProduView.frame.size.height+addressView.frame.size.height+checkPriceView.frame.size.height+eMFandOLOview.frame.size.height+30)
        popUpviewTopConstraint.constant  = popupHeight/2
        popUpviewBottomConstraint.constant  = popupHeight/2
    }
    func setFrameBasedOnSelectedProduct()  {
        
        if selectedProduct == "Colt LANLink Spoke (Ethernet Hub and Spoke)" || selectedProduct == "Colt Ethernet Hub and Spoke" {
            selectHubView.isHidden  = false
            eMFandOLOview.isHidden  = true
            setPopUpFrameWithHubView()
            priceAndBuildingViewTopConstraint.constant = selectHubView.frame.size.height+20
            //            isHubSelected = true
        }
        else if selectedProduct == "Colt Ethernet Line" || selectedProduct == "Colt LANLink Point to Point (Ethernet Point to Point)" {
            efmOloBackroundSubView.isHidden = false
            setFrameForColtEthernetProduct()
        }
        else if selectedProduct == "Colt Wave" || selectedProduct == "Colt Private Ethernet" {
            efmOloBackroundSubView.isHidden = true
            setFrameForColtEthernetProduct()
        }
        else{
            priceAndBuildingViewTopConstraint.constant = 20
            selectHubView.isHidden  = true
            eMFandOLOview.isHidden  = true
            setPopUpFrame()
        }
    }
    func setFrameForColtEthernetProduct() {
        eMFandOLOview.isHidden  = false
        selectHubView.isHidden  = true
        priceAndBuildingViewTopConstraint.constant = eMFandOLOview.frame.size.height+20
        eMFandOLOviewTopConstraint.constant = 10
        setPopUpFrameWithEMFandOLOView()
    }
    @IBAction func eMF_DSLbtnAction(_ sender: Any) {
        if isEMF_DSLbtnselected == false {
            eMF_DSLImgView.image = UIImage(named: "ic_check_box.png")
            isEMF_DSLbtnselected = true
            AnalyticsHelper().analyticsEventsAction(category: "ONNET List", action: "onnetList_BEnd_EFM/DSL_checked", label: "checked")
        }else{
            eMF_DSLImgView.image = UIImage(named: "ic_uncheck_box.png")
            isEMF_DSLbtnselected = false
            AnalyticsHelper().analyticsEventsAction(category: "ONNET List", action: "onnetList_BEnd_EFM/DSL_unchecked", label: "unchecked")
        }
    }
    @IBAction func selectedBEndBtnAction(_ sender: Any) {
        
        if (self.activeINactiveBuildngs == nil || (self.activeINactiveBuildngs?.count)! == 0)  {
            //            UIHelper.sharedInstance.showError("Please select B-End Address")
            //            showAlertWithNotification("Please select B-End Address")
            let please_select_BEndaddress = NSLocalizedString("Please select B-End address", comment: "")
            UIHelper.showAlert(message: please_select_BEndaddress, inViewController: popupController)
            
            return
        }else if (selectedBEndBtn.currentTitle == nil) || (selectedBEndBtn.currentTitle == ""){
            //            showAlertWithNotification("Please select B-End Address")
            let please_select_BEndaddress = NSLocalizedString("Please select B-End address", comment: "")
            UIHelper.showAlert(message: please_select_BEndaddress, inViewController: popupController)
        }
        else{
            eMFpopupView = Bundle.main.loadNibNamed("EMFandDSLPopUp", owner: self, options: nil)?[0] as? EMFandDSLPopUP
            eMFpopupView?.eMFDSLlistArray = eMFDSLlist
            eMFpopupView?.oLOlistArray = oLOlist
            //            if CommonDataHelper.sharedInstance.selectedProduct == "Colt Private Ethernet" {
            //                eMFpopupView?.addresslist = self.citiesMatchedList!
            //            }else{
            eMFpopupView?.addresslist = self.addressMatchedList!
            //            }
            eMFpopupView?.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.size.width, height:  self.frame.size.height)
            self.addSubview(eMFpopupView!)
        }
    }
    
    @IBAction func oLObtnAction(_ sender: Any) {
        if isOLObtnBtnselected == false {
            oLOimgView.image = UIImage(named: "ic_check_box.png")
            isOLObtnBtnselected = true
            AnalyticsHelper().analyticsEventsAction(category: "ONNET List", action: "onnetList_BEnd_OLO_checked", label: "checked")
        }else{
            oLOimgView.image = UIImage(named: "ic_uncheck_box.png")
            isOLObtnBtnselected = false
            AnalyticsHelper().analyticsEventsAction(category: "ONNET List", action: "onnetList_BEnd_OLO_unchecked", label: "unchecked")
        }
    }
    @IBAction func selectAproductBtnAction(_ sender: Any) {
        
        productAndBandwidthPopUpView.isHidden = false
        productBandwidthPopupBackgroundBtn.isHidden = false
        productAndBandwidthPopUpViewTopConstraint.constant = 0
        product_bandwidth_list = product_list!
        tableType = TableViewType.ProductType
        refreshPopupTable()
    }
    @IBAction func selectBandwidthBtnAction(_ sender: Any) {
        
        let pleaseSelectProduct = NSLocalizedString("Please select Product", comment: "")
        
        if selectedProduct == nil {
            
            //            showAlertWithNotification("Please select Product")
            UIHelper.showAlert(message: pleaseSelectProduct, inViewController: popupController)
        }else if selectedProduct == "Select Product" {
            
            //            showAlertWithNotification("Please select Product")
            UIHelper.showAlert(message: pleaseSelectProduct, inViewController: popupController)
        }else{
            if selectedProduct == "Colt SIP Trunking" {
                selectedProduct = "Colt VoIP Access"
            }else if selectedProduct == "Colt Ethernet Line" {
                selectedProduct = "Colt LANLink Point to Point (Ethernet Point to Point)"
                
            }else if selectedProduct == "Colt Ethernet Hub and Spoke" {
                selectedProduct = "Colt LANLink Spoke (Ethernet Hub and Spoke)"
                
            }else if selectedProduct == "ColT Ethernet VPN"{
                selectedProduct = "Colt Ethernet Private Network (EPN)"
            }
            productBandwidthPopupBackgroundBtn.isHidden = false
            productAndBandwidthPopUpView.isHidden = false
            productAndBandwidthPopUpViewTopConstraint.constant = 64
            
            // To select Bandwidth Based on Select Product
            if let bandwidthList = CoreDataHelper.sharedInstance.getBandwidthForProduct(selectedProduct!) {
                bandWidth_list = bandwidthList
            }
            //            product_bandwidth_list = productBandWidth_list
            tableType = TableViewType.BandwidthType
            refreshPopupTable()
        }
    }
    @IBAction func selectHUBbtnAction(_ sender: Any) {
        
        //        var err_msg : String?
        //
        //        let ocn = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "selected_ocn")
        //
        //        if ocn != nil
        //        {
        //            CommonDataHelper.sharedInstance.search_in_progress_hublist = true
        //
        //            let ocn_parts = ocn!.components(separatedBy: "|BREAK|")
        //
        //            if ocn_parts.last != nil
        //            {
        //                //                UIHelper.sharedInstance.showLoading()
        //                SwiftOverlays.showBlockingWaitOverlayWithText("Please wait...")
        //                NotificationCenter.default.addObserver(self, selector: #selector(getHubListFromServer), name: NSNotification.Name(rawValue: "finish_loading_hub_list"), object: nil)
        //                let tempOcnPart = ocn_parts.last!
        //                //                    let selectedPrdct = CommonDataHelper.sharedInstance.selected_product
        //                ConfigService().getHubs(tempOcnPart , product: self.selectedProduct!)
        //            }
        //        }
        
        //        getHubListFromServer()
        
        showHubList()
    }
    
    
    func callHublistAPI() {
        var err_msg : String?
        
        let ocn = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "selected_ocn")
        
        if ocn != nil
        {
            CommonDataHelper.sharedInstance.search_in_progress_hublist = true
            
            let ocn_parts = ocn!.components(separatedBy: "|BREAK|")
            
            if ocn_parts.last != nil
            {
                //                UIHelper.sharedInstance.showLoading()
                //                NotificationCenter.default.addObserver(self, selector: #selector(getHubListFromServer), name: NSNotification.Name(rawValue: "finish_loading_hub_list"), object: nil)
                let tempOcnPart = ocn_parts.last!
                //                    let selectedPrdct = CommonDataHelper.sharedInstance.selected_product
                ConfigService().getHubs(tempOcnPart , product: self.selectedProduct!)
            }
        }
    }
    
    func showHubList() {
        DispatchQueue.main.async {
            print("getHubListFromServer")
            //            UIHelper.sharedInstance.hideLoading()
            //            SwiftOverlays.removeAllBlockingOverlays()
            self.hubList=CommonDataHelper.sharedInstance.hub_list as? [Dictionary<String, String>]
            self.tableType = TableViewType.HubListType
            self.productBandwidthPopupBackgroundBtn.isHidden = false
            self.productAndBandwidthPopUpView.isHidden = false
            self.productAndBandwidthPopUpViewTopConstraint.constant = 64
            self.refreshPopupTable()
        }
    }
    
    //    func getHubListFromServer() {
    //        DispatchQueue.main.async {
    //            print("getHubListFromServer")
    //            //            UIHelper.sharedInstance.hideLoading()
    ////            SwiftOverlays.removeAllBlockingOverlays()
    //            self.hubList=CommonDataHelper.sharedInstance.hub_list as? [Dictionary<String, String>]
    //            self.tableType = TableViewType.HubListType
    //            self.productBandwidthPopupBackgroundBtn.isHidden = false
    //            self.productAndBandwidthPopUpView.isHidden = false
    //            self.productAndBandwidthPopUpViewTopConstraint.constant = 64
    //            self.popUpTableVIEW.reloadData()
    //        }
    //    }
    
    @IBAction func productBandwidthPopupBackgroundBtnAction(_ sender: Any) {
        productBandwidthPopupBackgroundBtn.isHidden = true
        productAndBandwidthPopUpView.isHidden = true
        
    }
    @IBAction func checkPriceBtnAction(_ sender: Any) {
        
        AnalyticsHelper().analyticsEventsAction(category: "Check Price", action: "checkPrice_click", label: "Check Price")
        
        var err_msg : String?
        let tempKey = UserDefaults.standard.value(forKey: "eType")
        let employeeType =  CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: tempKey as! String)
        
        if employeeType == "Non Employee" {
            
            //            UIHelper.sharedInstance.showError("You are not eligible  to check price")
            let youAreNotEligible = NSLocalizedString("You are not eligible to check price", comment: "")
            UIHelper.showAlert(message: youAreNotEligible, inViewController: popupController)
            //            showAlertWithNotification("You are not eligible to check price")
            
        }else{
            if selectedProduct == nil || selectedProduct == "Select Product" {
                let pleaseSelectProduct = NSLocalizedString("Please select Product", comment: "")
                UIHelper.showAlert(message: pleaseSelectProduct, inViewController: popupController)

            }else if (selectedProduct == "Colt VoIP Access") && (selectedBandwidth == "Voice Channels" || selectedBandwidth == nil ) {
                let pleaseSelectVoiceChannel = NSLocalizedString("Please select Voice Channel", comment: "")
                UIHelper.showAlert(message: pleaseSelectVoiceChannel, inViewController: popupController)
            }else if selectedBandwidth == nil || selectedBandwidth == "Select Bandwidth"{
                let pleaseSelectBandwidth = NSLocalizedString("Please select Bandwidth", comment: "")
                UIHelper.showAlert(message: pleaseSelectBandwidth, inViewController: popupController)
            }else if selectedBandwidth == "Voice Channels"  {
                let pleaseSelectVoiceChannel = NSLocalizedString("Please select Voice Channel", comment: "")
                UIHelper.showAlert(message: pleaseSelectVoiceChannel, inViewController: popupController)
            }
            else{
                delegate?.dismissProductBandwithPopup(selectionType: "CheckPrice", productBandwithPopUpView: self)
            }
            
            //        NotificationCenter.default.post(name: Notification.Name(rawValue: "GoToCheckPriceController"), object: nil)
            
        }
        
    }
    
    @IBAction func buildingDetailsBtnAction(_ sender: Any) {
        
        AnalyticsHelper().analyticsEventsAction(category: "ONNET List", action: "onnetList_AEnd_buildingDetail_click", label: "Building Detail")
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "GoToBuildingDetailsViewController"), object: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        Language.setAppleLAnguageTo(lang: "en")
        let autocompleteController = GMSAutocompleteViewController()
        UINavigationBar.appearance().tintColor=UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor(colorLiteralRed: 0, green: 165.0/255.0, blue: 155.0/255.0, alpha: 1.0)
        let searchBarTextColor = [NSForegroundColorAttributeName:UIColor.white,NSFontAttributeName:UIFont.systemFont(ofSize: 15.0)] as [String : Any]
        if #available(iOS 9.0, *) {
            let sBar = UITextField.appearance(whenContainedInInstancesOf: [UINavigationBar.classForCoder() as! UIAppearanceContainer.Type])
            sBar.defaultTextAttributes=searchBarTextColor
        } else {
            // Fallback on earlier versions
        }
        
        autocompleteController.delegate = self
        viewController?.present(autocompleteController, animated: true, completion: nil)
        //        }
        
        return false
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        if let language = LanguagesViewController.getSelectedLanguage() {
            Language.setAppleLAnguageTo(lang: language)
        }
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        self.selectedBEndBtn.setTitle(nil, for: .normal)
        selectedB_EndAddress = nil
        
        
        //        print(place.addressComponents)
        
        for item11 in place.addressComponents! {
            print(item11.name)
            print(item11.type)
            if item11.type == "street_number" {
                permisesnum = item11.name
            }else if item11.type == "route" {
                streetName = item11.name
            }else if item11.type == "locality" || item11.type == "postal_town" {
                cityTown = item11.name
            }else if item11.type == "postal_code" {
                postCode = item11.name
            }else if item11.type == "country" {
                country = item11.name
            }
        }
        selectedaddressLatitude = place.coordinate.latitude
        selectedAddressLongitude = place.coordinate.longitude
        formattedAddress = place.formattedAddress!
        selectedPlace = place
        addressTxtFld.text = formattedAddress //Do not set it anywhere else.... If do check SearchButton action
        viewController.dismiss(animated: true, completion: nil)
        //getActiveAndInactiveBuildings(place: selectedPlace!)
        
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
        if let language = LanguagesViewController.getSelectedLanguage() {
            Language.setAppleLAnguageTo(lang: language)
        }
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        if let language = LanguagesViewController.getSelectedLanguage() {
            Language.setAppleLAnguageTo(lang: language)
        }
        viewController.dismiss(animated: true, completion: nil)
    }
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
    @IBAction func searchBtnAction(_ sender: Any) {
        
        AnalyticsHelper().analyticsEventsAction(category: "ONNET List", action: "onnetList_B_End_search_click", label: "Search")
        
        clearEfmDslOloAndBuilding()
        CommonDataHelper.sharedInstance.isPopUpEFM_OLO = true
        
        if (((self.selectedBandwidth ?? "").isEmpty) || self.selectedBandwidth == "Select Bandwidth") {
            let pleaseSelectBandwidth = NSLocalizedString("Please select Bandwidth", comment: "")
            UIHelper.showAlert(message: pleaseSelectBandwidth, inViewController: popupController)
            return;
        }
        else if !((addressTxtFld.text ?? "").isEmpty) {
            
            //            self.ShowLoading_internal()
            //            UIHelper.sharedInstance.showLoading()
            let Please_wait = NSLocalizedString("Please wait", comment: "")
            SwiftOverlays.showBlockingWaitOverlayWithText(Please_wait)
            getActiveAndInactiveBuildings(place: selectedPlace!, onComplete: { (isSuccess:Bool, error:String) in
                
                if (self.activeINactiveBuildngs == nil || (self.activeINactiveBuildngs?.count)! == 0)  {
                    
                    //                    DispatchQueue.main.async(){
                    //                        UIHelper.sharedInstance.hideLoading()
                    //                    }
                    SwiftOverlays.removeAllBlockingOverlays()
                    let thislocationdoesntprovideOnnet = NSLocalizedString("MAPS_LOCAION_DOSENT_PROVIDE_ONNET", comment: "")
                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: thislocationdoesntprovideOnnet)
                    return
                }
                else{
                    let oNNET = NSLocalizedString("ONNET", comment: "")
                    if CommonDataHelper.sharedInstance.selectedProduct == "Colt Private Ethernet" {
                        if self.addressMatchedList?.count == 0{
                            SwiftOverlays.removeAllBlockingOverlays()
                            self.selectedBEndBtn.setTitle("", for: .normal)
                            UIHelper.showAlert(message: "The Colt Ethernet Private service is only available within the same metro city.  You're A-B address combination is not a Metro service.  Please consider our Ethernet Line service.", inViewController: self.popupController)
                        }else{
                            SwiftOverlays.removeAllBlockingOverlays()
                            self.selectedBEndBtn.setTitle("---\(oNNET)---", for: .normal)
                            self.selectedBEndBtn.setTitleColor(UIColor.init(colorLiteralRed: (0/255), green: (165/255), blue: (155/255), alpha: 1), for: .normal)
                        }
                    }else{
                        
                        self.selectedBEndBtn.setTitle("---\(oNNET)---", for: .normal)
                        self.selectedBEndBtn.setTitleColor(UIColor.init(colorLiteralRed: (0/255), green: (165/255), blue: (155/255), alpha: 1), for: .normal)
                        if (self.isEMF_DSLbtnselected == true) {
                            
                            if self.permisesnum == nil{
                                self.permisesnum = ""
                            }
                            if self.streetName == nil{
                                self.streetName = ""
                            }
                            if self.cityTown == nil{
                                self.cityTown = ""
                            }
                            if self.postCode == nil{
                                self.postCode = self.streetName
                            }
                            if self.country == nil{
                                self.country = ""
                            }
                            if self.selectedProduct == nil{
                                self.selectedProduct = ""
                            }
                            if self.selectedBandwidth == nil{
                                self.selectedBandwidth = ""
                            }
                            
                            DispatchQueue.main.async() {
                                
                                EmfDslService().getEFM_DSLdetails(self.permisesnum!, streetName: self.streetName!, cityTown: self.cityTown!, postCode: self.postCode!, country: self.country!, selectedProduct: self.selectedProduct!, selectedBandwidth: self.selectedBandwidth!, onComplete: { (result:Any) in
                                    
                                    self.dataAvilable()
                                    
                                    if (self.isOLObtnBtnselected == true) {
                                        //TODO: What ever you change in this block goes in below block toooo......
                                        //Need a clean approach for this....
                                        DispatchQueue.main.async() {
                                            
                                            OLOService().getOLOdetails(self.permisesnum!, streetName: self.streetName!, cityTown: self.cityTown!, postCode: self.postCode!, country: self.country!, selectedProduct: self.selectedProduct!, selectedBandwidth: self.selectedBandwidth!, latitude: "\((self.selectedaddressLatitude)!)", longitude: "\((self.selectedAddressLongitude)!)", onComplete: {(obj:Any) in
                                                
                                                self.oLOdataAvilable()
                                                
                                                //                                            DispatchQueue.main.async(){
                                                //                                                UIHelper.sharedInstance.hideLoading()
                                                //                                            }
                                                SwiftOverlays.removeAllBlockingOverlays()
                                                
                                            })
                                        }
                                    }
                                    else{
                                        //                                    DispatchQueue.main.async(){
                                        //                                        UIHelper.sharedInstance.hideLoading()
                                        //                                    }
                                        SwiftOverlays.removeAllBlockingOverlays()
                                        
                                    }
                                })
                                
                            }
                            
                        }
                        else{
                            if (self.isOLObtnBtnselected == true) {
                                //TODO: What ever you change in this block goes in above block toooo......
                                //Need a clean approach for this....
                                
                                DispatchQueue.main.async() {
                                    
                                    OLOService().getOLOdetails(self.permisesnum!, streetName: self.streetName!, cityTown: self.cityTown!, postCode: self.postCode!, country: self.country!, selectedProduct: self.selectedProduct!, selectedBandwidth: self.selectedBandwidth!, latitude: "\((self.selectedaddressLatitude)!)", longitude: "\((self.selectedAddressLongitude)!)", onComplete: {(obj:Any) in
                                        
                                        self.oLOdataAvilable()
                                        //                                    DispatchQueue.main.async(){
                                        //                                    UIHelper.sharedInstance.hideLoading()
                                        //                                    }
                                        SwiftOverlays.removeAllBlockingOverlays()
                                        
                                        
                                    })
                                }
                            }
                            else{
                                //                            DispatchQueue.main.async(){
                                //                                UIHelper.sharedInstance.hideLoading()
                                //                            }
                                SwiftOverlays.removeAllBlockingOverlays()
                            }
                        }
                        
                    }
                }
            })
        }
        else{
            //            UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select B-End Address")
            //            showAlertWithNotification("Please select B-End Address")
            let please_select_BEndaddress = NSLocalizedString("Please select B-End address", comment: "")
            UIHelper.showAlert(message: please_select_BEndaddress, inViewController: popupController)
        }
    }
    
    func clearEfmDslOloAndBuilding() {
        self.activeINactiveBuildngs = nil
        CommonDataHelper.sharedInstance.eMF_DSL_results = NSMutableArray()
        CommonDataHelper.sharedInstance.oLO_results = NSMutableArray()
        eMFDSLlist = []
        oLOlist = []
    }
    
    @objc func dataAvilable() {
        
        eMFDSLlist = CommonDataHelper.sharedInstance.eMF_DSL_results as! [Any]
        print(eMFDSLlist)
        //UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
    }
    @objc func oLOdataAvilable() {
        
        oLOlist = CommonDataHelper.sharedInstance.oLO_results as! [Any]
        //      UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
        //UIHelper.sharedInstance.isLoading = false
    }
    
    func getActiveAndInactiveBuildings(place:GMSPlace, onComplete:@escaping( (Bool,String) -> Void )) {
        
        //        DispatchQueue.main.async {
        //            UIHelper.sharedInstance.showLoading()
        //        }
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            DispatchQueue.main.async {
                let no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
                UIHelper.showAlert(message: no_internet_connection, inViewController: self.popupController)
            }
        }else{
            let requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"> <soapenv:Body> <ax:checkConnectivityRequest ax:schemaVersion=\"3.0\" xmlns:ax=\"http://aat.colt.com/connectivityservice\"> <ax:sequenceId>1</ax:sequenceId> <ax:requestType>ALL</ax:requestType> <ax:requestMode> <ax:requestID>4495</ax:requestID> <ax:siteAddress> <ax:latitude>\(place.coordinate.latitude)</ax:latitude> <ax:longitude>\(place.coordinate.longitude)</ax:longitude> <ax:radius>500</ax:radius> <ax:connectivityType>COLT Fibre</ax:connectivityType> </ax:siteAddress> </ax:requestMode> </ax:checkConnectivityRequest> </soapenv:Body> </soapenv:Envelope>"
            
            var errMsg:String?
            let tokenString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "token")
            let accountTypeString = CoreDataHelper.sharedInstance.getKeyValue(&errMsg, key: "accountType")
            let urlString = "http://loncis01/checkConnectivity/connectivity.wsdl"
            let encodedData = urlString.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64String = encodedData?.base64EncodedString(options: .init(rawValue: 0))
            let request = NSMutableURLRequest(url: URL(string: "https://dcp.colt.net/dgateway/connectnew")!)
            request.httpMethod = "POST"
            let requestData = requestBody.data(using: String.Encoding.utf8)
            request.httpBody = requestData
            request.addValue("text/xml", forHTTPHeaderField: "Content-Type")
            request.addValue("radius_search", forHTTPHeaderField: "SOAPAction")
            request.addValue("Apache-HttpClient/4.1.1 (java 1.5)", forHTTPHeaderField: "User-Agent")
            request.addValue(base64String!, forHTTPHeaderField: "X-CONNECT-U")
            request.addValue(tokenString!, forHTTPHeaderField: "X-GATEWAY-A")
            request.addValue(accountTypeString!, forHTTPHeaderField: "X-GATEWAY-F")
//            request.timeoutInterval=TimeInterval(MAXFLOAT)
            request.timeoutInterval=600
            
            
            let session = URLSession.shared
            _ = session.dataTask(with: request as URLRequest) { (data, response, error) in
                
                if error == nil
                {
                    if let data = data, let result = String(data: data, encoding: String.Encoding.utf8)
                    {
                        let xml = SWXMLHash.parse(result)
                        //                    print(xml)
                        //                        self.activeINactiveBuildngs = ActiveAndInactiveBuildings.getActiceAndInactiveBuildingsFromResponse(xmlResponseData: xml)
                        self.activeINactiveBuildngs = ActiveAndInactiveBuildings.getOnnetLocations(xmlResponseData: xml)
                        
                        DispatchQueue.main.async {
                            
                            //UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                            self.addressMatchedList = []
                            self.citiesMatchedList = []
                            for aiBuilding in self.activeINactiveBuildngs!{
                                if CommonDataHelper.sharedInstance.selectedProduct == "Colt Private Ethernet" {
                                    if aiBuilding.englishCityName?.lowercased() == self.englishCityName?.lowercased() {
                                        self.addressMatchedList?.append(aiBuilding)
                                        print("citiesMatchedListCount = ",self.citiesMatchedList?.count)
                                        print("englishCityName = ",aiBuilding.englishCityName)
                                    }
                                }else{
                                    if aiBuilding.streetName?.lowercased() == self.streetName?.lowercased() {
                                        self.addressMatchedList?.append(aiBuilding)
                                        print(self.addressMatchedList)
                                        print("street = ",aiBuilding.streetName)
                                    }
                                    
                                }
                                
                                print("street111111 = ",aiBuilding.streetName)
                                
                                let cordna2d = CLLocationCoordinate2D(latitude: aiBuilding.lattitude!, longitude: aiBuilding.longitude!)
                                let marker = GMSMarker(position: cordna2d)
                                
                                //  marker.title = selectedPlace?.name
                                //  marker.snippet = selectedPlace?.formattedAddress
                                marker.accessibilityHint=aiBuilding.buildingStatus
                                if aiBuilding.buildingStatus == "ACTIVE" {
                                    marker.icon=UIImage(named: "ActiveBuilding")
                                }else{
                                    marker.icon=UIImage(named: "InActiveBuilding")
                                }
                                
                                let permisesNumber =  aiBuilding.permisesNumber
                                let streetName =  aiBuilding.streetName
                                let englishCityName =  aiBuilding.englishCityName
                                let postCode =  aiBuilding.postCode
                                let coltOperatingCountry =  aiBuilding.coltOperatingCountry
                                let address1 = "\(permisesNumber!), \(streetName!), \(englishCityName!), \(postCode!), \(coltOperatingCountry!)"
                                marker.accessibilityValue=address1
                                // marker.title=nLocation1.name
                                
                                //                            self.filter_places = self.abArray!.filter({ (text) -> Bool in
                                //                                let tmp: NSString = text as NSString
                                //                                let range = tmp.range(of: streetName!, options: NSString.CompareOptions.caseInsensitive)
                                //                                return range.location != NSNotFound
                                //                            })
                                //
                                //                            if(self.filter_places.count == 0){
                                //
                                //                            } else {
                                //
                                //                            }
                                //                        print(self.filter_places)
                            }
                            onComplete(true, "") //Success....
                        }
                        
                    }
                    else
                    {
                        onComplete(false, "Failed to get valid response from server.") //Failure....
                        print("Failed to get valid response from server.")
                    }
                }
                else
                {
                    onComplete(false, error?.localizedDescription ?? "Error Occurred") //Failure....
                    print(error?.localizedDescription ?? "Error")
                }
                
                }
                .resume()
        }
    }
    
    @IBAction func popUpBackgroundBtnAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "CloseProductBandwidthPopUp"), object: nil)
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        
        AnalyticsHelper().analyticsEventsAction(category: "Check Price", action: "checkPriceCancel_click", label: "Cancel")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "CloseProductBandwidthPopUp"), object: nil)
    }
    
    func showAlertWithNotification(_ msg: String) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "ShowAlert"), object: msg)
    }
    
    func showAlert(_ msg: String) {
        
        let parentViewController: UIViewController = UIApplication.shared.windows[1].rootViewController!
        let alert = UIAlertController(title: "", message:msg , preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(action: UIAlertAction!) in
        }))
        DispatchQueue.main.async() {
            parentViewController.present(alert, animated: true, completion: nil)
        }
    }
    
}

