//
//  SlaCell.swift
//  FeetOnStreet
//
//  Created by Suresh Murugaiyan on 9/4/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class SlaCell: UITableViewCell {

    @IBOutlet weak var lbl_remarks: UILabel!
    @IBOutlet weak var view_Price: UIView!
    @IBOutlet weak var lbl_Availability: UILabel!
    @IBOutlet weak var lbl_TotalRepairTime: UILabel!
    @IBOutlet weak var lbl_DeliveryTime: UILabel!
    @IBOutlet weak var lbl_Presentation: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
