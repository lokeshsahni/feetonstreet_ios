//
//  UIColor+Colt.swift
//  FeetOnStreet
//
//  Created by Syed Saud Arif on 19/09/17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation

extension UIColor {
    
    static let c_DarkTeal = UIColor(red: (0/255), green: (105/255), blue: (95/255), alpha: 1.0)
    static let c_Teal = UIColor(red: (0/255), green: (165/255), blue: (155/255), alpha: 1.0)
    
}

