//
//  LoginService.swift
//  FeetOnStreet
//
//  Created by admin on 7/28/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import SwiftOverlays

class LoginService: NSObject {
    
    var no_internet_connection = ""
    
    func enumerate(_ indexer: XMLIndexer) {
        for child in indexer.children {
            print(child.element!.name + " : " + child.element!.text)
            enumerate(child)
        }
    }
    
    func authorize(_ username: String, password: String , viewController: UIViewController) {
        
        no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
        
        let base64 = ((username+":"+password).data(using: String.Encoding.utf8))!.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0))
        
        let request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dgat=\"http://dgateway2.web.colt.net/\"><soapenv:Header></soapenv:Header><soapenv:Body><dgat:authorize><dgat:auth>" + base64 + "</dgat:auth></dgat:authorize></soapenv:Body></soapenv:Envelope>"
          
        WebHelper.sharedInstance.callDirectService("https://dcp.colt.net/dgateway/services/AuthService", body: request, soapAction: "", completionHandler: {(err_msg: String?, resp: String?) in
            
            
            if(err_msg != nil)
            {
                DispatchQueue.main.async(execute: {
//                    AlertManager.shared.hideLoading()
                    SwiftOverlays.removeAllBlockingOverlays()
                    UIHelper.sharedInstance.showError(err_msg!)
                })
            }
            
            if(resp != nil)
            {
                print(resp!)
                
                let xml = SWXMLHash.parse(resp!)
                
                //TODO: store the xmlIndexer : xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"] in a variable and use that.
                
                let success = xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"]["ns1:success"].element?.text
                var msg = xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"]["ns1:msg"].element?.text
                let token = xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"]["ns1:token"].element?.text
                let accountType = xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"]["ns1:accountType"].element?.text
                let auth = xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"]["ns1:auth"].element?.text
                let country = xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"]["ns1:country"].element?.text
                let email = xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"]["ns1:email"].element?.text
                let employeeType = xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"]["ns1:employeeType"].element?.text
                let viewStaticContent = xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"]["ns1:viewStaticContent"].element?.text
                let isPartner = xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"]["ns1:isPartner"].element?.text
                let partnerType = xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"]["ns1:partnerType"].element?.text
                let onlyCC = xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"]["ns1:onlyCC"].element?.text
                let lockedByIntruder = xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"]["ns1:lockedByIntruder"].element?.text
                let preferredLanguage = xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"]["ns1:preferredLanguage"].element?.text
                let ocn_list = xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"]["ns1:OCNList"]
                let role_list = xml["soapenv:Envelope"]["soapenv:Body"]["authorizeResponse"]["ns1:authorizeReturn"]["ns1:roleList"]

                var roleListArr :[String] = []
                for roleChildren in role_list.children {
                    roleListArr.append((roleChildren.element?.text)!)
                }
                let isRDTAvailable = roleListArr.contains("RTDTOOL")


                print("ocn_list = ",ocn_list)
                
                if(success != nil)
                                   {
                    print("success = ",success!)

                    if(success == "true")
                    {
                        if(auth != nil && auth == "true")
                        {
                            var err_msg : String?
                            let result = CoreDataHelper.sharedInstance.purgeData(&err_msg)
                            
                            if(result)
                            {
                                //TODO: to hide waring of unused func return value. Use '_' as variable name. addToConfig & appendToConfig
                                //TODO: If you get time.... Use the return value to generate error. Or unsuccessfull login responce.
                                
                                DispatchQueue.main.async{
                                    _ = CoreDataHelper.sharedInstance.addToConfig(&err_msg, key: "username", value: username, viewController: viewController)
                                
                                if(token != nil)
                                {
                                  print("token = ",token!)
                                    _ = CoreDataHelper.sharedInstance.addToConfig(&err_msg, key: "token", value: token!, viewController: viewController)
                                }
                                
                                if(country != nil)
                                {
                                  _ = CoreDataHelper.sharedInstance.addToConfig(&err_msg, key: "country", value: country!, viewController: viewController)
                                }
                                
                                if(email != nil)
                                {
                                  _ = CoreDataHelper.sharedInstance.addToConfig(&err_msg, key: "email", value: email!, viewController: viewController)
                                }
                                
                                if(preferredLanguage != nil)
                                {
                                  _ = CoreDataHelper.sharedInstance.addToConfig(&err_msg, key: "preferredLanguage", value: preferredLanguage!, viewController: viewController)
                                }
                                
                                if(accountType != nil)
                                {
                                  _ = CoreDataHelper.sharedInstance.addToConfig(&err_msg, key: "accountType", value: accountType!, viewController: viewController)
                                }
                                
                                if(employeeType != nil)
                                {
                                    let eType="employeeType"
                                    _ = CoreDataHelper.sharedInstance.addToConfig(&err_msg, key: eType, value: employeeType!, viewController: viewController)
                                    UserDefaults.standard.setValue(eType, forKey: "eType")
                                    CommonDataHelper.sharedInstance.employeeType = employeeType
                                  
                                }
                                
                                if(viewStaticContent != nil)
                                {
                                  
                                  _ = CoreDataHelper.sharedInstance.addToConfig(&err_msg, key: "viewStaticContent", value: viewStaticContent!, viewController: viewController)
                                    
                                }
                                
                                if(isPartner != nil)
                                {
                                  _ = CoreDataHelper.sharedInstance.addToConfig(&err_msg, key: "isPartner", value: isPartner!, viewController: viewController)
                                }
                                
                                if(partnerType != nil)
                                {
                                 _ = CoreDataHelper.sharedInstance.addToConfig(&err_msg, key: "partnerType", value: partnerType!, viewController: viewController)
                                }
                                
                                if(onlyCC != nil)
                                {
                                 _ = CoreDataHelper.sharedInstance.addToConfig(&err_msg, key: "onlyCC", value: onlyCC!, viewController: viewController)
                                }
                                    
                                if isRDTAvailable == true {

                                    _ = CoreDataHelper.sharedInstance.addToConfig(&err_msg, key: "isRTDAvailable", value: "1", viewController: viewController)
                                }else{

                                    _ = CoreDataHelper.sharedInstance.addToConfig(&err_msg, key: "onlyCC", value: "0", viewController: viewController)
                                }

                                var selected_ocn : String?
                                
                                for ocn in ocn_list.children
                                {
                                    let data = ocn.element!.text
                                    if(data != nil)
                                    {
                                       _ =  CoreDataHelper.sharedInstance.appendToConfig(&err_msg, key: "ocn", value: data)
                                        
                                        if(selected_ocn == nil)
                                        {
                                            selected_ocn = data
                                            _ = CoreDataHelper.sharedInstance.addToConfig(&err_msg, key: "selected_ocn", value: data, viewController: viewController)
                                        }
                                    }
                                }
                                
                                    //UIHelper.sharedInstance.hideLoading()
                                    NotificationCenter.default.post(name: Notification.Name(rawValue: "reload_attributes"), object: nil)
                                }
                            }
                            else
                            {
                                DispatchQueue.main.async(execute: {
//                                    AlertManager.shared.hideLoading()
                                    SwiftOverlays.removeAllBlockingOverlays()
                                    UIHelper.sharedInstance.showError("Failed to save login data.")
                                })
                            }
                        }
                        else
                        {
                            if(lockedByIntruder != nil && lockedByIntruder == "true")
                            {
                                DispatchQueue.main.async(execute: {
//                                    AlertManager.shared.hideLoading()
                                    SwiftOverlays.removeAllBlockingOverlays()
                                    UIHelper.sharedInstance.showError("Account is locked.")
                                })
                            }
                            else
                            {
                                DispatchQueue.main.async(execute: {
//                                    AlertManager.shared.hideLoading()
                                    SwiftOverlays.removeAllBlockingOverlays()
                                    UIHelper.sharedInstance.showError("Invalid username/password.")
                                })
                            }
                        }
                        
                    }
                    else
                    {
                        if(msg == nil)
                        {
                            msg = "Failed to login."
                        }
                        DispatchQueue.main.async(execute: {
//                            UIHelper.sharedInstance.hideLoading()
//                            AlertManager.shared.hideLoading()
                            SwiftOverlays.removeAllBlockingOverlays()
                            let usernamePasswordMismatch = NSLocalizedString("Access Denied- Username/Password mismatch", comment: "")
                            UIHelper.sharedInstance.showError(usernamePasswordMismatch)
                        })
                    }
                }
                else
                {
                    DispatchQueue.main.async(execute: {
//                        UIHelper.sharedInstance.hideLoading()
//                        AlertManager.shared.hideLoading()
                        SwiftOverlays.removeAllBlockingOverlays()
                        UIHelper.sharedInstance.showError("Invalid response from server.")
                    })
                }
            }
            else
            {
                DispatchQueue.main.async(execute: {
//                    UIHelper.sharedInstance.hideLoading()
//                    AlertManager.shared.hideLoading()
                    SwiftOverlays.removeAllBlockingOverlays()
                    UIHelper.sharedInstance.showError("Failed to login.")
                })
            }
            
        })
    }
}
