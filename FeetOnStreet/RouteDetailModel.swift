//
//  RouteDetailModel.swift
//  FeetOnStreet
//
//  Created by admin on 5/3/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class PathModel
{
    var xNGWMSId = ""
    var kMZLibNo = -1
    var isVirtual = false
    // add link id and name
    var linkId: Int?
    var linkName: String?


    
    init(_ dic:[String:Any]) {
        for d in dic{
            switch(d.key){
            case "xNGWMSId":
                xNGWMSId = d.value as! String
            case "kMZLibNo":
                kMZLibNo = d.value as! Int
            case "isVirtual":
                isVirtual = d.value as! Bool
            case "linkId":
                linkId = d.value as? Int
            case "linkName":
                linkName = d.value as? String
            default:
                print("Something went wrong while parsing PathModel ")
            }
        }
    }
}

class RouteDetailModel
{
    var routeType = ""
    var Id = -1
    var path = [PathModel]()
    var distance:Double = 0.0
    var siteAId = -1
    var siteBId = -1
    var connectedSiteName = ""
    var totalPhotonicLatency100G :Double = 0.0
    var totalLatency100G :Double = 0.0
    var error = ""
    var subSeaLinkNmae = ""



    
    init(_ dic:[String:Any]) {
        for d in dic{
            switch(d.key){
            case "routeType":
                routeType = d.value as! String
            case "Id":
                Id = d.value as! Int
            case "path":
                let pathDicArr = d.value as! [[String:Any]]
                for pathDic in pathDicArr{
                    path.append(PathModel(pathDic))
                }
                let lPNmae = getLinkName(dict: pathDicArr)
                subSeaLinkNmae = lPNmae

            case "distance":
                distance = d.value as! Double
            case "siteAId":
                siteAId = d.value as! Int
            case "siteBId":
                siteBId = d.value as! Int
            case "connectedSiteName":
                connectedSiteName = d.value as! String
            case "totalPhotonicLatency100G":
                totalPhotonicLatency100G = d.value as! Double
            case "totalLatency100G":
                totalLatency100G = d.value as! Double
            case "error":
                error = d.value as! String
            default:
                print("Something went wrong while parsing RouteDetailModel ")
            }
        }
    }

    func getLinkName(dict: [[String:Any]]) -> String {

        var lStr = ""
        for d in dict{
                let tempName = d["linkName"] as? String
            if let tName = tempName {
                if !(tName.isEmpty) {
                    if lStr.isEmpty {
                        lStr = tName
                    }else{
                        lStr += ",\(tName)"
                    }
                }
            }
        }
        return lStr
    }

    static func getRoute(from data:Data)->RouteDetailModel?{
        var returnVal:RouteDetailModel? = nil
        do{
            let route = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
            returnVal = RouteDetailModel(route)
        }
        catch{}
        return returnVal
    }
    
    static func getRoutes(from data:Data)->[RouteDetailModel]{
        var returnVal = [RouteDetailModel]()
        do{
            let routesInfo = try JSONSerialization.jsonObject(with: data, options: [])
            if let routesInfoError = routesInfo as? [String:Any] {
                if(routesInfoError["error"] == nil) {
                }
            }
            else {
                if let allRoutes = routesInfo as? [[String:Any]] {
                    for route in allRoutes {
                        returnVal.append(RouteDetailModel(route))
                    }
                }
            }
        }
        catch{}
        return returnVal
    }
}
