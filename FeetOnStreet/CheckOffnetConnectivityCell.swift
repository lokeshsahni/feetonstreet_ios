//
//  CheckOffnetConnectivityCell.swift
//  FeetOnStreet
//
//  Created by Suresh Murugaiyan on 8/22/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class CheckOffnetConnectivityCell: UITableViewCell {

    @IBOutlet weak var checkOffnetConnectivityBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
