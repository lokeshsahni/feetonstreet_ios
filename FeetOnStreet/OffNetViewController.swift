//
//  OffNetViewController.swift
//  FeetOnStreet
//
//  Created by admin on 9/8/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import SwiftOverlays

class OffNetViewController: UIViewController,UIPageViewControllerDataSource,UIPageViewControllerDelegate,ProductBandwithPopupDelegate  {
    
    @IBOutlet weak var view_Btns: UIView!
    @IBOutlet weak var btn_EMF: UIButton!
    @IBOutlet weak var btn_OLO: UIButton!
    
    var eMF_DSLVC:EFMViewController?
    var oLOVC:OLOViewController?
    var vcArr:[UIViewController]?
    var pageVC:UIPageViewController?
    var currentVCIndex:Int?
    var isPageControllerAdded:Bool?
    
    var selectedProduct:String?
    var selectedBandwidth:String?
    var fosPopUp:Fos_Popup?
    var numberOfResponseGot:Int = 0
    var numberApiCall:Int = 0
    var address1:String?
    
    
    
    var productBandwithpopupView:ProductBandwithPopUpView?
    var bend_Address:String?
    var aEndAddress_id:String?
    var selectedPlace:GMSPlace?
    var selectedB_EndAddress:String?
    var eMailpopupView:EMailPopUpView?
    var isEfmOloVC = true
    
    var selectedList:[Any] = []
    var viewType:ViewType = ViewType.BuildingType
    
    var bEndAddress_id:String?
    
    // BackgroundApiCall
    var backgroundTask:UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonDataHelper.sharedInstance.eMF_DSL_results = NSMutableArray()
        CommonDataHelper.sharedInstance.oLO_results = NSMutableArray()
        
        isPageControllerAdded=false
        
        // BackgroundApiCall
        registerForBackgroundTask()
        getEMFandOLOresponse()
        
        NotificationCenter.default.addObserver(self, selector: #selector(OpenProductBandwidthPopUp), name: Notification.Name("OpenProductBandwidthPopUp"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CloseProductBandwidthPopUp), name: Notification.Name("CloseProductBandwidthPopUp"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showAlert), name: Notification.Name("ShowAlert"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(removeAllObservers), name: Notification.Name("RemoveAllObservers"), object: nil)
        
        // BackgroundApiCall
        NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    // BackgroundApiCall
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // BackgroundApiCall
    @objc func reinstateBackgroundTask() {
        if backgroundTask == UIBackgroundTaskInvalid {
            registerForBackgroundTask()
        }
    }
    // BackgroundApiCall
    func registerForBackgroundTask() {
        if backgroundTask == UIBackgroundTaskInvalid {
            backgroundTask=UIApplication.shared.beginBackgroundTask(expirationHandler: {
                self.endBackGroundTask()
            })
        }
    }
    
    // BackgroundApiCall
    func endBackGroundTask() {
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask=UIBackgroundTaskInvalid
    }
    
    func OpenProductBandwidthPopUp(notification:Notification) {
        
        let efmOloObjectsDict = notification.object as? NSDictionary
        let notificationType = efmOloObjectsDict?.object(forKey: "notificationType") as? String
        if notificationType == "EFM" {
            address1 = efmOloObjectsDict?.object(forKey: "address1") as? String
            viewType = (efmOloObjectsDict?.object(forKey: "viewType") as? ViewType)!
            bEndAddress_id = efmOloObjectsDict?.object(forKey: "bEndAddress_id") as? String
        }else{
            address1 = efmOloObjectsDict?.object(forKey: "address1") as? String
            viewType = (efmOloObjectsDict?.object(forKey: "viewType") as? ViewType)!
            selectedList = (efmOloObjectsDict?.object(forKey: "multiPriceList") as? [Any])!
            
        }
        productBandwithpopupView = Bundle.main.loadNibNamed("ProductBandwithPopUp", owner: self, options: nil)?[0] as? ProductBandwithPopUpView
        //            eMFpopupView?.eMFDSLlistArray = eMFDSLlist
        //            eMFpopupView?.oLOlistArray = oLOlist
        //            eMFpopupView?.addresslist = self.addressMatchedList!
        let viewHeight = productBandwithpopupView?.frame.size.height
        productBandwithpopupView?.viewHeight = viewHeight
        productBandwithpopupView?.productBandwithpopupViewFrame = productBandwithpopupView?.frame
        productBandwithpopupView?.viewController = self
        productBandwithpopupView?.frame = CGRect(x: 0, y: 20, width: self.view.frame.size.width, height: self.view.frame.size.height-20)
        productBandwithpopupView?.address1 = address1
        productBandwithpopupView?.isEfmOloVC = isEfmOloVC
        productBandwithpopupView?.multiPriceList = selectedList
        productBandwithpopupView?.viewType = viewType
        productBandwithpopupView?.delegate = self
        productBandwithpopupView?.popupController = self
        self.view.addSubview(productBandwithpopupView!)
        
    }
    func dismissProductBandwithPopup(selectionType:String,productBandwithPopUpView:ProductBandwithPopUpView) {
        if selectionType == "CheckPrice" {
            selectedProduct = productBandwithPopUpView.selectedProduct
            selectedBandwidth = productBandwithPopUpView.selectedBandwidth
            aEndAddress_id = productBandwithPopUpView.aEndAddress_id
            bend_Address = productBandwithPopUpView.selectedBEndBtn.currentTitle
            selectedPlace = productBandwithPopUpView.selectedPlace
            selectedB_EndAddress = productBandwithPopUpView.selectedB_EndAddress
            aEndAddress_id = productBandwithPopUpView.aEndAddress_id
            GoToCheckPriceController()
        }
    }
    
    func GoToCheckPriceController() {
        
        
        selectedProduct = CommonDataHelper.sharedInstance.selectedProduct
        selectedBandwidth = CommonDataHelper.sharedInstance.selectedBandwidth

        if selectedProduct == "Colt LANLink Point to Point (Ethernet Point to Point)" || selectedProduct == "Colt Wave" || selectedProduct == "Colt Private Ethernet" {
            if selectedPlace == nil {
                //                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please Search B-End Address")
                let please_search_BEndaddress = NSLocalizedString("Please search B-End address", comment: "")
                UIHelper.showAlert(message: please_search_BEndaddress, inViewController: self)
            }
            else if selectedB_EndAddress == nil {
                //                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select B-End Address")
                let please_select_BEndaddress = NSLocalizedString("Please select B-End address", comment: "")
                UIHelper.showAlert(message: please_select_BEndaddress, inViewController: self)
            }else{
                CloseProductBandwidthPopUp()
                let storyBoard : UIStoryboard = UIStoryboard(name: "Secondary", bundle:nil)
                let checkPriceVC = storyBoard.instantiateViewController(withIdentifier: "CheckPriceController") as! CheckPriceController
                checkPriceVC.locationStr = address1
                checkPriceVC.productType = selectedProduct
                checkPriceVC.bandWidthType = selectedBandwidth
                checkPriceVC.aEndAddress_id = aEndAddress_id
                checkPriceVC.bEndAddress_id = bEndAddress_id
                checkPriceVC.bendAddress = bend_Address
                checkPriceVC.viewType = viewType
                checkPriceVC.multiPriceList = selectedList
                self.navigationController?.pushViewController(checkPriceVC, animated: true)
            }
        }else if selectedProduct == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
            if CommonDataHelper.sharedInstance.npc == nil {
                //                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select Hub")
                let please_select_Hub = NSLocalizedString("Please_select_hub", comment: "")
                UIHelper.showAlert(message: please_select_Hub, inViewController: self)
            }else{
                CloseProductBandwidthPopUp()
                let storyBoard : UIStoryboard = UIStoryboard(name: "Secondary", bundle:nil)
                let checkPriceVC = storyBoard.instantiateViewController(withIdentifier: "CheckPriceController") as! CheckPriceController
                checkPriceVC.locationStr = address1
                checkPriceVC.productType = selectedProduct
                checkPriceVC.bandWidthType = selectedBandwidth
                checkPriceVC.aEndAddress_id = aEndAddress_id
                checkPriceVC.bEndAddress_id = bEndAddress_id
                checkPriceVC.viewType = viewType
                checkPriceVC.multiPriceList = selectedList
                self.navigationController?.pushViewController(checkPriceVC, animated: true)
            }
        }
        else{
            CloseProductBandwidthPopUp()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Secondary", bundle:nil)
            let checkPriceVC = storyBoard.instantiateViewController(withIdentifier: "CheckPriceController") as! CheckPriceController
            checkPriceVC.locationStr = address1
            checkPriceVC.productType = selectedProduct
            checkPriceVC.bandWidthType = selectedBandwidth
            checkPriceVC.aEndAddress_id = aEndAddress_id
            checkPriceVC.bEndAddress_id = bEndAddress_id
            checkPriceVC.viewType = viewType
            checkPriceVC.multiPriceList = selectedList
            self.navigationController?.pushViewController(checkPriceVC, animated: true)
        }
    }
    //        CloseProductBandwidthPopUp()
    //    }
    
    func CloseProductBandwidthPopUp() {
        
        DispatchQueue.main.async {
            self.productBandwithpopupView?.removeFromSuperview()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isPageControllerAdded == false {
            isPageControllerAdded=true
            createPageViewController()
            
            let appDel = UIApplication.shared.delegate as! AppDelegate
            
            if appDel.networkStatus! == NetworkStatus.NotReachable {
                
                DispatchQueue.main.async {
                    let no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
                    UIHelper.sharedInstance.showReachabilityWarning(no_internet_connection, _onViewController: self)
                }
                
            }else{
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden=true
        
        AnalyticsHelper().analyticLogScreen(screen: "OFFNET Screen (EFM/DSL,OLO)")
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden=false
    }
    
    private func createPageViewController(){
        
        //        DispatchQueue.main.async {
        //
        //            UIHelper.sharedInstance.showLoadingOnViewController(loadVC: self)
        //        }
        let storyboard = UIStoryboard.init(name: "Secondary", bundle: nil)
        eMF_DSLVC = storyboard.instantiateViewController(withIdentifier: "EFMViewController") as? EFMViewController
        eMF_DSLVC?.selectedProduct = selectedProduct
        eMF_DSLVC?.fosPopUpEmf=fosPopUp
        oLOVC = storyboard.instantiateViewController(withIdentifier: "OLOViewController") as? OLOViewController
        oLOVC?.fosPopUp_Olo=fosPopUp
        
        oLOVC?.selectedProduct = selectedProduct
        oLOVC?.selectedBandwidth = selectedBandwidth
        vcArr = [eMF_DSLVC!,oLOVC!]
        
        pageVC=UIPageViewController.init(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
        pageVC?.dataSource=self
        pageVC?.delegate=self
        
        
        self.addChildViewController(pageVC!)
        let tempVal = view_Btns.frame.origin.y+view_Btns.frame.size.height
        pageVC?.view.frame=CGRect(x: 0, y: tempVal, width: self.view.frame.size.width, height: self.view.frame.size.height-tempVal)
        self.view.addSubview((pageVC?.view)!)
        if CommonDataHelper.sharedInstance.efmSelected == true {
            currentVCIndex=0
            pageVC?.setViewControllers([eMF_DSLVC!], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
            btn_EMF.setTitleColor(UIColor.white, for: .normal)
            if CommonDataHelper.sharedInstance.oLOSelected == false {
                oLOVC?.isFirstTimeLoaded=false
            }
        }else{
            currentVCIndex=1
            eMF_DSLVC?.isIntialLoded=false
            pageVC?.setViewControllers([oLOVC!], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
            btn_OLO.setTitleColor(UIColor.white, for: .normal)
        }
        
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?{
        
        let vCS = pageViewController.viewControllers
        let topVC:UIViewController? = vCS?[0]
        
        var index = vcArr?.index(of: topVC!)
        if index!>=1 && index!<=1 {
            index! -= 1
            let nextVC:UIViewController? = vcArr?[index!]
            return nextVC!
        }
        return nil
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?{
        
        let vCS = pageViewController.viewControllers
        let topVC:UIViewController? = vCS?[0]
        
        
        var index = vcArr?.index(of: topVC!)
        if index!>=0 && index!<1 {
            index! += 1
            let nextVC:UIViewController? = vcArr?[index!]
            return nextVC!
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let tempVC = pageViewController.viewControllers?[0]
        let tempIndex = vcArr?.index(of: tempVC!)
        currentVCIndex=tempIndex
        
        if tempVC is EFMViewController {
            btn_EMF.setTitleColor(UIColor.white, for: .normal)
            btn_OLO.setTitleColor(UIColor.black, for: .normal)
        }else if tempVC is OLOViewController {
            btn_EMF.setTitleColor(UIColor.black, for: .normal)
            btn_OLO.setTitleColor(UIColor.white, for: .normal)
        }
    }
    @IBAction func eMFBtnAction(_ sender: Any) {
        btn_EMF.setTitleColor(UIColor.white, for: .normal)
        btn_OLO.setTitleColor(UIColor.black, for: .normal)
        if currentVCIndex != 0 {
            pageVC?.setViewControllers([eMF_DSLVC!], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: nil)
            currentVCIndex=0
        }
    }
    @IBAction func oLOBtnAction(_ sender: Any) {
        btn_EMF.setTitleColor(UIColor.black, for: .normal)
        btn_OLO.setTitleColor(UIColor.white, for: .normal)
        if currentVCIndex != 1 {
            if currentVCIndex! < 1 {
                pageVC?.setViewControllers([oLOVC!], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
            }else{
                pageVC?.setViewControllers([oLOVC!], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: nil)
            }
            currentVCIndex=1
        }
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func getEMFandOLOresponse() {
        
        if fosPopUp?.popupFosDetails?.isEfmSelected == true {
            numberApiCall+=1
            var premissesNum = fosPopUp?.popupFosDetails?.permissionNumber
            var streetName = fosPopUp?.popupFosDetails?.streetName
            var city = fosPopUp?.popupFosDetails?.cityName
            var postCode = fosPopUp?.popupFosDetails?.postcode
            var country = fosPopUp?.popupFosDetails?.country
            
            if premissesNum == nil {
                premissesNum = ""
            }
            
            if streetName == nil {
                streetName = ""
            }
            if city == nil {
                city = ""
            }
            if postCode == nil {
                postCode = ""
            }
            if country == nil {
                country = ""
            }
            
            EmfDslService().getEFM_DSLdetails(premissesNum!, streetName: streetName!, cityTown: city!, postCode: postCode!, country: country!, selectedProduct: (fosPopUp?.popupFosDetails?.selectedProductType!)!, selectedBandwidth: (fosPopUp?.popupFosDetails?.selectedBandWidth!)!, onComplete: {(Any) in
                //TODO: Remove Notification and use this...
            })
            NotificationCenter.default.removeObserver(self, name: Notification.Name("EMF_DSL_data"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.dataAvilable(notification:)), name: Notification.Name("EMF_DSL_data"), object: nil)
            self.eMF_DSLVC?.listTableView.reloadData()
            
        }
        if fosPopUp?.popupFosDetails?.isOloSelected == true {
            numberApiCall+=1
            
            OLOService().getOLOdetails((fosPopUp?.popupFosDetails?.permissionNumber ?? ""), streetName: (fosPopUp?.popupFosDetails?.streetName ?? "") , cityTown: (fosPopUp?.popupFosDetails?.cityName ?? "") , postCode: (fosPopUp?.popupFosDetails?.postcode ?? "") , country: (fosPopUp?.popupFosDetails?.country ?? "") , selectedProduct: (fosPopUp?.popupFosDetails?.selectedProductType!)!, selectedBandwidth: (fosPopUp?.popupFosDetails?.selectedBandWidth!)!, latitude: "\((fosPopUp?.popupFosDetails?.lattitudeLocation)!)", longitude: "\((fosPopUp?.popupFosDetails?.longitudeLocation)!)", onComplete: {(Any) in
                //TODO: Remove Notification and use this....
            })
            NotificationCenter.default.removeObserver(self, name: Notification.Name("OLO_data"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.dataAvilable(notification:)), name: Notification.Name("OLO_data"), object: nil)
        }
        DispatchQueue.main.async {
            
//            UIHelper.sharedInstance.showLoadingOnViewController(loadVC: self)
            let please_wait = NSLocalizedString("Please wait", comment: "")
            SwiftOverlays.showBlockingWaitOverlayWithText(please_wait)
        }
        
    }
    
    @objc func dataAvilable(notification:Notification) {
        
        if notification.object is String {
            
            if notification.object as! String == "failure" {
                DispatchQueue.main.async {
//                    UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                     SwiftOverlays.removeAllBlockingOverlays()
                    //                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Server not response.Try again")
                    let serverErrorPleaseTryTgain = NSLocalizedString("Server error. Please try again.", comment: "")
                    UIHelper.showAlert(message: serverErrorPleaseTryTgain, inViewController: self)
                    
                }
                // BackgroundApiCall
                endBackGroundTask()
                return
            }
        }
        if (CommonDataHelper.sharedInstance.eMF_DSL_results.count)>0 {
            DispatchQueue.main.async {
                self.btn_EMF.setTitle("EFM/DSL (\(CommonDataHelper.sharedInstance.eMF_DSL_results.count))", for: .normal)
            }
        }
        
        if currentVCIndex == 0 {
            
            
            if notification.name.rawValue == "OLO_data" {
                
                oLOVC?.isFirstTimeLoaded=false
                
                if (CommonDataHelper.sharedInstance.oLO_results.count)>0 {
                    if CommonDataHelper.sharedInstance.isPopUpEFM_OLO == false{
                        DispatchQueue.main.async {
                            self.btn_OLO.setTitle("OLO (\(CommonDataHelper.sharedInstance.oLO_results.count))", for: .normal)
                            
                        }
                    }
                    
                }
                //                else{
                //                    DispatchQueue.main.async {
                //                        self.oLOVC?.noDataAvailableLbl.isHidden = false
                //                    }
                //
                //                }
                
                
            }else{
                if (CommonDataHelper.sharedInstance.eMF_DSL_results.count)>0 {
                    if CommonDataHelper.sharedInstance.isPopUpEFM_OLO == false{
                        DispatchQueue.main.async {
                            self.eMF_DSLVC?.reloadTablevieWithResponse()
                            self.eMF_DSLVC?.filterTypeBtn.isHidden = false
                            self.eMF_DSLVC?.nodataAvailableLbl.isHidden = true
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        //                self.btn_EMF.setTitle("EFM/DSL", for: .normal)
                        self.eMF_DSLVC?.nodataAvailableLbl.isHidden = false
                    }
                    
                    //            UIHelper.sharedInstance.showError("No data available")
                    
                }
            }
            
            
            
        }else{
            if (CommonDataHelper.sharedInstance.oLO_results.count)>0 {
                if CommonDataHelper.sharedInstance.isPopUpEFM_OLO == false{
                    DispatchQueue.main.async {
                        self.btn_OLO.setTitle("OLO (\(CommonDataHelper.sharedInstance.oLO_results.count))", for: .normal)
                        self.oLOVC?.reloadTablevieWithOLOResponse()
                    }
                }
                
            }else{
                DispatchQueue.main.async {
//                    self.oLOVC?.noDataAvailableLbl.isHidden = false
                    //            self.btn_OLO.setTitle("OLO", for: .normal)
                    //            UIHelper.sharedInstance.showError("No data available")
                }
                
            }
        }
        
        print("response offnet")
        numberOfResponseGot+=1
        if numberApiCall==numberOfResponseGot {
            // BackgroundApiCall
            endBackGroundTask()
            DispatchQueue.main.async {
//                UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
                 SwiftOverlays.removeAllBlockingOverlays()
            }
            
        }
        //         if (CommonDataHelper.sharedInstance.eMF_DSL_results.count)<=0 {
        //        UIHelper.sharedInstance.showError("No data available")
        //        }
        
        //        eMFDSLlist = CommonDataHelper.sharedInstance.eMF_DSL_results as! [Any]
        //        print(eMFDSLlist)
        //        UIHelper.sharedInstance.hideLoadingOnViewController(hideVC: self)
    }
    func showAlert(notification: NSNotification) {
        if notification.object != nil{
            let alertMsg = notification.object as? String
            //            UIHelper.showAlert(message: alertMsg, inViewController: self)
            let alertController = UIAlertController(title: "", message: alertMsg, preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    func removeAllObservers()  {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

