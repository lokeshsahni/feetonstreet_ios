//
//  LoginViewController.swift
//  FeetOnStreet
//
//  Created by admin on 7/28/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import SwiftOverlays


class LoginViewController: UIViewController {
    
    @IBOutlet weak var userNameTxtFld: UITextField!
    @IBOutlet weak var passwordtxtFld: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var signInButton:UIButton?
    @IBOutlet weak var youCanEitherLbl: UILabel!
    
    @IBOutlet weak var dateLBL: UILabel!
    @IBOutlet weak var versionLBL: UILabel!
    
    var no_internet_connection = ""
    
    var users_list:[String] = []
    
    // BackgroundApiCall
    var backgroundTask:UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        //        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        //        let statusBarColor = UIColor(red: 0/255, green: 105/255, blue: 95/255, alpha: 1.0)
        //        statusBarView.backgroundColor = statusBarColor
        //        view.addSubview(statusBarView)
        
        CoreDataHelper.sharedInstance.clear_all_static_data()
        DashBoardViewController.ClearAllStaticData()
        self.navigationController?.isNavigationBarHidden = true
        userNameTxtFld.layer.borderColor = UIColor.white.cgColor
        userNameTxtFld.layer.borderWidth = 1.0
        userNameTxtFld.layer.cornerRadius = 3
        passwordtxtFld.layer.borderColor = UIColor.white.cgColor
        passwordtxtFld.layer.borderWidth = 1.0
        passwordtxtFld.layer.cornerRadius = 3
        if #available(iOS 9.0, *) {
            userNameTxtFld.inputAssistantItem.leadingBarButtonGroups.removeAll()
        } else {
            // Fallback on earlier versions
//            userNameTxtFld.inputAssistantItem.leadingBarButtonGroups.removeAll()
        }
        if #available(iOS 9.0, *) {
            passwordtxtFld.inputAssistantItem.trailingBarButtonGroups = []
        } else {
            // Fallback on earlier versions
        }
        
        
        //        userNameTxtFld.text = "Demo"
        //        passwordtxtFld.text = "Demo2017"
        
        clearProduct_BandwidthData()
        
        // BackgroundApiCall
        registerForBackgroundTask()
        
        // BackgroundApiCall
        NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        registerForKeyboardNotifications()
        
        AnalyticsHelper().analyticLogScreen(screen: "Login Screen")
        no_internet_connection = NSLocalizedString("No_internet_connection", comment: "")
        let youCanEither = NSLocalizedString("You can either", comment: "")
        youCanEitherLbl.text = NSLocalizedString(youCanEither, comment: "")
        let version = NSLocalizedString("Version", comment: "")
        let date = NSLocalizedString("Date", comment: "")
        let password = NSLocalizedString("Password", comment: "")
        let username = NSLocalizedString("Username", comment: "")
        let login = NSLocalizedString("LOGIN", comment: "")
        userNameTxtFld.placeholder = username
        passwordtxtFld.placeholder = password
        signInButton?.setTitle(login, for: .normal)
        let appVersionObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject
        versionLBL.text = "\(version): \(String(describing: appVersionObject!))"
//        versionLBL.text = "\(version): 2.1.2"
        let curtrentdate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let todayDate = formatter.string(from: curtrentdate)
        dateLBL.text = "\(date): \(todayDate)"
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.loginSuccess), name: Notification.Name(rawValue: "reload_attributes"), object: nil)
    }
    
    // BackgroundApiCall
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // BackgroundApiCall
    @objc func reinstateBackgroundTask() {
        if backgroundTask == UIBackgroundTaskInvalid {
            registerForBackgroundTask()
        }
    }
    // BackgroundApiCall
    func registerForBackgroundTask() {
        if backgroundTask == UIBackgroundTaskInvalid {
            backgroundTask=UIApplication.shared.beginBackgroundTask(expirationHandler: {
                self.endBackGroundTask()
            })
        }
    }
    
    // BackgroundApiCall
    func endBackGroundTask() {
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask=UIBackgroundTaskInvalid
    }
  
    func clearProduct_BandwidthData() {
        CommonDataHelper.sharedInstance.selectedProduct = nil
        CommonDataHelper.sharedInstance.selectedBandwidth = nil
        
//        CommonDataHelper.sharedInstance.product_list = NSMutableArray()
//        CommonDataHelper.sharedInstance.country_list = NSMutableArray()
//        CommonDataHelper.sharedInstance.bandwidth_list = NSMutableArray()
        
        // offnet EFM ,OLO Buttons default selections cleared
        
        CommonDataHelper.sharedInstance.efmSelected = false
        CommonDataHelper.sharedInstance.oLOSelected = false
        
        
        
    }
    
    func dismissKeyboard() {
        
        view.endEditing(true)
    }
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView?.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView?.contentInset = contentInsets
        self.scrollView?.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.bounds
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.signInButton {
            let btnRect = self.view.convert(activeField.bounds, from: activeField)
            if (!aRect.contains(CGPoint.init(x: btnRect.maxX, y: btnRect.maxY))){
                self.scrollView?.scrollRectToVisible(btnRect, animated: true)
            }
        }
    }
    func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        self.scrollView?.contentInset = contentInsets
        self.scrollView?.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView?.isScrollEnabled = false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if(textField == userNameTxtFld){
            passwordtxtFld?.becomeFirstResponder()
        }
        return true
    }
    @IBAction func loginBtnAction(_ sender: Any) {
        
        AnalyticsHelper().analyticsEventsAction(category: "Login", action: "login_click", label: "Login")
        
        userNameTxtFld.resignFirstResponder()
        passwordtxtFld.resignFirstResponder()
        
        var tempUsrName = userNameTxtFld.text
        tempUsrName=tempUsrName?.replacingOccurrences(of: " ", with: "")
        if (tempUsrName?.isEmpty)! {
           let usernameisrequired = NSLocalizedString("Username is required.", comment: "")
            UIHelper.sharedInstance.showError(usernameisrequired)
            return
        }
        
        var tempPW = passwordtxtFld.text
            tempPW=tempPW?.replacingOccurrences(of: " ", with: "")
        if (tempPW?.isEmpty)! {
            let passwordisrequired = NSLocalizedString("Password is required.", comment: "")
            UIHelper.sharedInstance.showError(passwordisrequired)
            return
        }
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
            
        }else{
            let login = LoginService()
//            AlertManager.shared.showLoading(title: "", message: "Logging In...", on: self)
            let logging_In = NSLocalizedString("Logging In", comment: "")
            SwiftOverlays.showBlockingWaitOverlayWithText(logging_In)
            
            //           UIHelper.sharedInstance.showLoading("Logging In...")
            login.authorize(userNameTxtFld.text!, password: passwordtxtFld.text!, viewController: self)
        }
    }
    
    func loginSuccess() {
        UserDefaults.standard.set(true, forKey: "isFirstTimeLaunchMap")
        UserDefaults.standard.set(false, forKey: "isTAndCAccepted")
        
        UserDefaults.standard.set((self.userNameTxtFld?.text)!, forKey: "temp_userName")
        UserDefaults.standard.set((self.passwordtxtFld?.text)!, forKey: "temp_password")
        
        
        var err_msg : String?
        
        let ocn = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "selected_ocn")
        
//        if ocn != nil
//        {
            let appDel = UIApplication.shared.delegate as! AppDelegate
            if appDel.networkStatus! == NetworkStatus.NotReachable {
                
                // BackgroundApiCall
                // endBackGroundTask()
                SwiftOverlays.removeAllBlockingOverlays()
                DispatchQueue.main.async {
                    UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
                }
                
            }else{
            
            CommonDataHelper.sharedInstance.search_in_progress_hublist = true
            
//            let ocn_parts = ocn!.components(separatedBy: "|BREAK|")
            let product = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "form_product")
            CommonDataHelper.sharedInstance.selected_product = product
            
//                if ocn_parts.last != nil {
                let appDel = UIApplication.shared.delegate as! AppDelegate
                if appDel.networkStatus! == NetworkStatus.NotReachable {
                    
                    // BackgroundApiCall
                    //            endBackGroundTask()
                    SwiftOverlays.removeAllBlockingOverlays()
                    DispatchQueue.main.async {
                        UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
                    }
                    
                }else {
                   StartDownloading(onComplete: { (isSucess:Bool) in
                    if(isSucess){
                        UserDefaults.standard.set(true, forKey: "isAlreadyLoggedIn")
                        self.FindUserAndNavigate();
                    }
                    else{
                        let loginProcessFailed = NSLocalizedString("Login Process Failed", comment: "")
                        let pleaseTryAgain = NSLocalizedString("Please Try Again.", comment: "")
                        
                        AlertManager.shared.ChangeMessage(title: loginProcessFailed, message: pleaseTryAgain)
                        AlertManager.shared.ChangeMessage_AddOK(withCompletionHandler: { (a:UIAlertAction) in
                            DispatchQueue.main.async {
                                CoreDataHelper.sharedInstance.clear_all_static_data()
                            }
                        })
                    }
                })
                return;
             }
//            }
            FindUserAndNavigate(); //TODO: Need to verify that it is possible togo to dashboard without downloading?
        }
//      }
    }
    
    
    func FindUserAndNavigate(){
        
        var isUserFound = false
        if let userCrentials = CoreDataHelper.sharedInstance.getUserCrentials() {
            users_list = userCrentials
            for username in users_list {
                if username == userNameTxtFld.text {
                    isUserFound = true
                    UserDefaults.standard.set(true, forKey: "isTAndCAccepted")
                    break
                }
            }
        }
        
        if isUserFound {
            self.performSegue(withIdentifier: "DashBoardVC", sender: nil)
            
        }else{
            self.performSegue(withIdentifier: "TermsAndConditions", sender: nil)
            
            //            var _err_msg : String?
            //            CoreDataHelper.sharedInstance.add_user_Credentials(&_err_msg, username: (self.userNameTxtFld?.text)!, password: (self.passwordtxtFld?.text)!)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool     {
        textField.resignFirstResponder()
        return true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK: Downloading................
    
    func StartDownloading( onComplete:@escaping((Bool) -> Void )) {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.networkStatus! == NetworkStatus.NotReachable {
            
            // BackgroundApiCall
            //            endBackGroundTask()
            SwiftOverlays.removeAllBlockingOverlays()
            DispatchQueue.main.async {
                UIHelper.sharedInstance.showReachabilityWarning(self.no_internet_connection, _onViewController: self)
            }
            
        }else{
        ConfigService().getCountriesFromEIX(onComplete: { (isSucess:Bool, error:String) in
            
            if(isSucess == false){
                onComplete(false)
                return
            }
            // BackgroundApiCall
//            self.endBackGroundTask()
            
//            SwiftOverlays.removeAllBlockingOverlays()
//            AlertManager.shared.ChangeMessage(title: "", message: "Downloading Countries...")
//            SwiftOverlays.showBlockingWaitOverlayWithText("Downloading Countries...")
            
            ConfigService().getBandwidthForAllProducts(onComplete: { (isSucess:Bool, error:String) in
                
                if(isSucess == false){
                    onComplete(false)
                    return
                }
                
                ConfigService().getProductsFromEIX(onComplete: { (isSucess:Bool, error:String) in
                    
                    
                    if(isSucess == false){
                        onComplete(false)
                        return
                    }
                    
                    // BackgroundApiCall
//                    self.endBackGroundTask()
                    
//                    SwiftOverlays.removeAllBlockingOverlays()
//                    AlertManager.shared.ChangeMessage(title: "", message: "Downloading Products...")
//                    SwiftOverlays.showBlockingWaitOverlayWithText("Downloading Products...")
                    
                    ConfigService().getBandwidthFromEIX(onComplete: { (isSucess:Bool, error:String) in
                        
                        if(isSucess == false){
                            onComplete(false)
                            return
                        }
                    NotificationCenter.default.removeObserver(self, name: Notification.Name("reload_attributes"), object: nil)
                        
                        // BackgroundApiCall
//                        self.endBackGroundTask()
//                        SwiftOverlays.removeAllBlockingOverlays()
//                        AlertManager.shared.ChangeMessage(title: "", message: "Downloading Bandwidths...")
//                        SwiftOverlays.showBlockingWaitOverlayWithText("Downloading Bandwidths...")
                        
                        
                        // BackgroundApiCall
                        self.endBackGroundTask()
//                        AlertManager.shared.hideLoading()
                        SwiftOverlays.removeAllBlockingOverlays()
                        onComplete(true);
                    })
                })
            })
        })
    }
  }
}

