//
//  LanguagesViewController.swift
//  FeetOnStreet
//
//  Created by admin on 4/10/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

// constants
let SELECTED_LANGUAGE_KEY = "SelectedLanguage"
let DEVICE_LANGUAGE_KEY = "en"

class LanguagesViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var mainTableView : UITableView!
    
    var language = ""
    
//    var menuListArray = ["English","French","Japanese","Portugal","Italian","Spanish","German"]
//    var languageCodeArray = ["en","fr","ja","pt-PT","it","es-ES","de"]
    
    var menuListArray = ["English","French","German","Italian","Japanese","Portuguese-Portugal","Spanish"]
    var languageCodeArray = ["en","fr","de","it","ja","pt-PT","es-ES"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let lang = LanguagesViewController.getSelectedLanguage() {
            language = lang
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return menuListArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "languagesCell", for: indexPath) as! LanguagesCell
        cell.titleLbl.text = menuListArray[indexPath.row]
        
        if(language == languageCodeArray[indexPath.row]) {
            cell.selectedImageView.image = UIImage(named: "verification-mark")
        } else {
            cell.selectedImageView.image = nil
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        cell?.imageView?.image = UIImage(named: "ic_check_box")
        LanguagesViewController.setSelectedLanguage(lang: languageCodeArray[indexPath.row])
        (UIApplication.shared.delegate as! AppDelegate).changeRoot(language: languageCodeArray[indexPath.row])
    }
    func tableView(_ tableView: UITableView,
                   heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    class func setSelectedLanguage(lang: String) {
        let userdef = UserDefaults.standard
        userdef.set(lang, forKey: SELECTED_LANGUAGE_KEY)
        userdef.synchronize()
    }
    
    class func getSelectedLanguage() -> String? {
        let userdef = UserDefaults.standard
        return userdef.value(forKey: SELECTED_LANGUAGE_KEY) as? String
    }

    class func getDeviceLanguage() -> String {
        let userdef = UserDefaults.standard
        guard let langu = userdef.value(forKey: DEVICE_LANGUAGE_KEY) as? String else {
            return "en"
        }
        return langu
    }
    
    class func setDeviceLanguage(lang: String) {
        let userdef = UserDefaults.standard
        userdef.set(lang, forKey: DEVICE_LANGUAGE_KEY)
        userdef.synchronize()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
