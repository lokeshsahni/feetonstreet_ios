//
//  UIHelper.swift
//  FeetOnStreet
//
//  Created by admin on 7/28/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class UIHelper : NSObject {
    
    static let sharedInstance = UIHelper()
    
    var loading:UIAlertController?
    var lastVC:UIViewController?
    var isLoading : Bool?
    var retainCountForDisplay:Int = 0
    
    fileprivate override init() {
        
        super.init()
        retainCountForDisplay = 0;
        loading = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        loading!.view.tintColor = UIColor.black
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        loading!.view.addSubview(loadingIndicator)
    }
    
    
    func show_map_click(_ formattedAddress: String) {
        
        let optionMenu = UIAlertController(title: nil, message: formattedAddress, preferredStyle: .actionSheet)
        
        let asset_info = UIAlertAction(title: "Asset Information", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "show_asset_information"), object: nil, userInfo: nil)
        })
        
        let check_price = UIAlertAction(title: "Mark for Check Price", style: .default, handler: {(alert: UIAlertAction!) -> Void in
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "check_price"), object: nil, userInfo: nil)
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            // will hide automatically
        })
        
        var err_msg : String?
        
        let employee_type = CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: "employeeType")
        
        if employee_type != nil && (employee_type! == "Colt Employee" || employee_type! == "Internal")
        {
            optionMenu.addAction(asset_info)
        }
        
        optionMenu.addAction(check_price)
        optionMenu.addAction(cancel)
        
        let vc = UIApplication.shared.delegate?.window!?.rootViewController!
        
        if(vc!.presentedViewController == nil) {
            
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                let popover_controller = optionMenu.popoverPresentationController
                popover_controller?.sourceView = vc!.view
                popover_controller?.permittedArrowDirections = .unknown
            }
            
            vc!.present(optionMenu, animated: true, completion: nil)
        }
    }
    
    
    func show_price_check_options() {
    
        let optionMenu = UIAlertController(title: nil, message: "Select an action", preferredStyle: .actionSheet)
        
        let priceCheck = UIAlertAction(title: "Check Price", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "price_check"), object: nil, userInfo: nil)
        })
        
        let clear = UIAlertAction(title: "Clear All Selected", style: .destructive, handler: {
            (alert: UIAlertAction!) -> Void in
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "reload_selected_data"), object: nil, userInfo: nil)
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            // will hide automatically
        })
        
        optionMenu.addAction(priceCheck)
        optionMenu.addAction(clear)
        optionMenu.addAction(cancel)
        
        let vc = UIApplication.shared.delegate?.window!?.rootViewController!
        
        if(vc!.presentedViewController == nil) {
            
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                let popover_controller = optionMenu.popoverPresentationController
                popover_controller?.sourceView = vc!.view
                popover_controller?.permittedArrowDirections = .unknown
            }
            
            vc!.present(optionMenu, animated: true, completion: nil)
        }
    }
    
    
    func show_file_download_sheet(_ fileName : String, dDocName : String) {
    
        let optionMenu = UIAlertController(title: nil, message: fileName, preferredStyle: .actionSheet)
        
        let file = UIAlertAction(title: "Download", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "download_file"), object: nil, userInfo: ["dDocName":dDocName])
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            // will hide automatically
        })
        
        optionMenu.addAction(file)
        optionMenu.addAction(cancel)
        
        let vc = UIApplication.shared.delegate?.window!?.rootViewController!
        
        if(vc!.presentedViewController == nil) {
            
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                let popover_controller = optionMenu.popoverPresentationController
                popover_controller?.sourceView = vc!.view
                popover_controller?.permittedArrowDirections = .unknown
            }
            
            vc!.present(optionMenu, animated: true, completion: nil)
        }
    }
    
    
    func show_lat_lon_map_sheet() {
    
        let optionMenu = UIAlertController(title: nil, message: "Search around this location", preferredStyle: .actionSheet)
        
        let on_net_radius = UIAlertAction(title: "LAT/LONG SEARCH (ON-NET)", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "perform_search_on_map"), object: nil, userInfo: ["search_type":"ON-NET-RADIUS"])
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            /*if(self.lastVC != nil) {
             self.lastVC!.dismissViewControllerAnimated(true, completion: nil)
             self.lastVC = nil
             }*/
        })
        
        optionMenu.addAction(on_net_radius)
        optionMenu.addAction(cancel)
        
        let vc = UIApplication.shared.delegate?.window!?.rootViewController!
        
        if(vc!.presentedViewController == nil) {
            
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                let popover_controller = optionMenu.popoverPresentationController
                popover_controller?.sourceView = vc!.view
                popover_controller?.permittedArrowDirections = .unknown
            }
            vc!.present(optionMenu, animated: true, completion: nil)
        }
    }
    
    
    func show_cc_action_sheet() {
        
        let optionMenu = UIAlertController(title: nil, message: "Search for connectivity type", preferredStyle: .actionSheet)
        
        let on_net = UIAlertAction(title: "ON-NET", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "perform_search"), object: nil, userInfo: ["search_type":"ON-NET"])
        })
        
        let on_net_radius = UIAlertAction(title: "LAT/LONG SEARCH (ON-NET)", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "perform_search"), object: nil, userInfo: ["search_type":"ON-NET-RADIUS"])
        })
        
        let dsl = UIAlertAction(title: "EFM/DSL", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "perform_search"), object: nil, userInfo: ["search_type":"DSL"])
        })
        
        let olo = UIAlertAction(title: "OLO", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "perform_search"), object: nil, userInfo: ["search_type":"OLO"])
        })
        
        let all = UIAlertAction(title: "ALL", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "perform_search"), object: nil, userInfo: ["search_type":"ALL"])
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            /*if(self.lastVC != nil) {
                self.lastVC!.dismissViewControllerAnimated(true, completion: nil)
                self.lastVC = nil
            }*/
        })
        
        optionMenu.addAction(on_net)
        optionMenu.addAction(dsl)
        optionMenu.addAction(olo)
        optionMenu.addAction(all)
        optionMenu.addAction(on_net_radius)
        optionMenu.addAction(cancel)
        
        let vc = UIApplication.shared.delegate?.window!?.rootViewController!
        
        if(vc!.presentedViewController == nil) {
            
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                let popover_controller = optionMenu.popoverPresentationController
                popover_controller?.sourceView = vc!.view
                popover_controller?.permittedArrowDirections = .unknown
            }
            vc!.present(optionMenu, animated: true, completion: nil)
        }
    }
    
    func showLoading() {
        
        if(CommonDataHelper.sharedInstance.do_not_show_loading)
        {
            CommonDataHelper.sharedInstance.do_not_show_loading = false
            return
        }
        
        let vc = UIApplication.shared.delegate?.window!?.rootViewController!
        
        if(vc!.presentedViewController == nil) {
            
            loading!.message = "Please wait..."
            lastVC = vc
            
            DispatchQueue.main.async {
                
                vc!.present(self.loading!, animated: true, completion: nil)
            }
        }
    }
    
    func showLoadingOnViewController(loadVC:UIViewController) {
        
        if !(loading?.isFirstResponder)! {
            let Please_wait = NSLocalizedString("Please wait", comment: "")
            loading!.message = Please_wait
            print("<<<<<<<<<<<<<<<< Request Loading Show");
            loadVC.present(self.loading!, animated: true, completion: {() in
                
                
                print("<<<<<<<<<<<<<<<< Loading Showing");
                
            })
        }
    }
    
    func hideLoadingOnViewController(hideVC:UIViewController) {
        print("<<<<<<<<<<<<<<<< Request Loading Hidden");
        hideVC.dismiss(animated: true, completion: {() in
            
            
            print("<<<<<<<<<<<<<<<< Loading Hidden");
            
        })
    }
    
    
    
    func showLoadingOnViewController(loadVC:UIViewController, onCompletion: @escaping(() -> Swift.Void)) {
        
        if !(loading?.isFirstResponder)! {
            loading!.message = "Please wait..."
            print("<<<<<<<<<<<<<<<< Request Loading Show");
            loadVC.present(self.loading!, animated: true, completion: onCompletion)
        }
    }
    
    func hideLoadingOnViewController(hideVC:UIViewController, onCompletion: @escaping(() -> Swift.Void)) {
        print("<<<<<<<<<<<<<<<< Request Loading Hidden");
        hideVC.dismiss(animated: true, completion: onCompletion)
    }
    
    
    func showLoadingOnViewControllerWithRetain(loadVC:UIViewController) {
        if(retainCountForDisplay == 0){
            loading!.message = "Please wait..."
            loadVC.present(self.loading!, animated: true, completion: nil)
        }
        retainCountForDisplay = retainCountForDisplay + 1;
        print("added retain %d >>>>>>>>>>>>>>>",retainCountForDisplay)
    }
    
    func hideLoadingOnViewControllerWithRetain(hideVC:UIViewController) {
        retainCountForDisplay = retainCountForDisplay - 1;
        if(retainCountForDisplay <= 0){
            retainCountForDisplay = 0
            hideVC.dismiss(animated: true, completion: nil)
        }
        print("removed retain %d >>>>>>>>>>>>>>>",retainCountForDisplay)
    }
    
    
    func hideLoading() {
        
        if(lastVC != nil) {
           DispatchQueue.main.async {
            self.lastVC!.dismiss(animated: true, completion: nil)
            self.lastVC = nil

            }
        }
    }
    
    func showLoading(_ msg: String) {
        
        if(CommonDataHelper.sharedInstance.do_not_show_loading)
        {
            CommonDataHelper.sharedInstance.do_not_show_loading = false
            return
        }
    
        let vc = UIApplication.shared.delegate?.window!?.rootViewController!
        
        if(vc!.presentedViewController == nil) {
            
            loading!.message = msg
            lastVC = vc
            
            DispatchQueue.main.async {
                vc!.present(self.loading!, animated: true, completion: {() in
                    
                    
                    
                })
            }
        }
    }
    
    
    func showError(_ msg: String) {
   
        let vc = UIApplication.shared.delegate?.window!?.rootViewController!
        
        let alertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        vc!.present(alertController, animated: true, completion: nil)
        
        
        
//        let errorDialog = UIAlertController(title: "",message: msg, preferredStyle: UIAlertControllerStyle.alert)
//        
//        errorDialog.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
//            errorDialog.removeFromParentViewController()
//        }))
//        
//        DispatchQueue.main.async {
//            print("<<<<<<<<<<<<<<<< Request Show Error");
//            vc!.present(errorDialog,animated: true, completion: {() in
//                
//                
//                print("<<<<<<<<<<<<<<<< ErrorShowing");
//                
//            })
//        }
    }
    
    
    
    //MARK: Common Alert Function
    @discardableResult static func showAlert(message: String?, title: String = "", buttonArr: [(title: String, actionBlock:() -> ()?)]! = [], inViewController: UIViewController?, completionBlock : (() -> ())? = nil) -> UIAlertController?{
        
        guard let vc = inViewController else {
            return nil
        }
        let alert = UIAlertController(title: title, message: message ?? "", preferredStyle: UIAlertControllerStyle.alert)
        
        if !buttonArr.isEmpty{
            for buttonTuple in buttonArr!{
                let title: String? = buttonTuple.title
                
                alert.addAction(UIAlertAction(title: title ?? "", style: UIAlertActionStyle.default, handler: {alertaction in
                    buttonTuple.actionBlock()
                }))
            }
        }
        else{
            let ok = NSLocalizedString("OK", comment: "")
            alert.addAction(UIAlertAction(title: ok, style: UIAlertActionStyle.default, handler: nil))
    
        }
        
        vc.present(alert, animated: true, completion: completionBlock)
        return alert
    }
    //    CommonHelper.showAlert(message: errorMsg, inViewController: self)

    func showReachabilityWarning(_ msg: String, _onViewController vc:UIViewController ) {
        
        let errorDialog = UIAlertController(title: "",message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        errorDialog.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            errorDialog.removeFromParentViewController()
        }))
        
        DispatchQueue.main.async {
            
            vc.present(errorDialog,animated: true, completion: nil)
        }
    }
    
    func showAlertWithTitleAndMessage(_alertTile title:String, _alertMesage message:String) {
        
        let vc = UIApplication.shared.delegate?.window!?.rootViewController!
        let errorDialog = UIAlertController(title: title,message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        errorDialog.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            errorDialog.removeFromParentViewController()
        }))
        
        DispatchQueue.main.async {
            
            vc!.present(errorDialog,animated: true, completion: nil)
        }
    }
    
    func showErrorLeftAlign(_ msg: String) {
        
        let vc = UIApplication.shared.delegate!.window!?.rootViewController!
        let errorDialog = UIAlertController(title: "Error",message: msg, preferredStyle: .alert)
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = NSTextAlignment.left
        
        let font = UIFont.systemFont(ofSize: 13)
        
        let message_text = NSMutableAttributedString(string:  msg, attributes: [NSParagraphStyleAttributeName: paragraph, NSFontAttributeName: font])
        
        errorDialog.setValue(message_text, forKey: "attributedMessage")
        
        errorDialog.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            errorDialog.removeFromParentViewController()
        }))
        
        DispatchQueue.main.async {
            
            vc!.present(errorDialog,animated: true, completion: nil)
        }
    }
 
    func synced(_ lock: Any, closure: () -> ()) {
        objc_sync_enter(lock)
        closure()
        objc_sync_exit(lock)
    }
}
