//
//  PriceController.swift
//  FeetOnStreet
//
//  Created by Suresh Murugaiyan on 9/1/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class PriceController: UIViewController,UITableViewDataSource {
    
    @IBOutlet weak var tbl_Price: UITableView!
    @IBOutlet weak var noDataAvailableLbl: UILabel!
    
    var priceList:[Price]=[]{
        didSet{
            tbl_Price.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tbl_Price.estimatedRowHeight = 1000
        tbl_Price.rowHeight = UITableViewAutomaticDimension
        //        CommonDataHelper.sharedInstance.price_results = priceList as! NSMutableArray
        CommonDataHelper.sharedInstance.price_results = (priceList as! NSArray).mutableCopy() as! NSMutableArray
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return priceList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let identifier = "cell"
        //        var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? PriceCell
        //        if cell == nil {
        //            cell=PriceCell(style: .default, reuseIdentifier: identifier)
        //        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? PriceCell
        cell?.view_Price.layer.borderWidth=1.0
        cell?.view_Price.layer.borderColor=UIColor(colorLiteralRed: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0).cgColor
        let tempPrice = priceList[indexPath.row]
        
        if tempPrice.bEndDeliverySupplier != nil {
            cell?.lbl_BendSuplier.text=tempPrice.bEndDeliverySupplier
        }else{
            cell?.lbl_BendSuplier.text=""
        }
        if tempPrice.aEndDeliverySupplier != nil {
            cell?.lbl_AendSuplier.text=tempPrice.aEndDeliverySupplier
        }else{
            cell?.lbl_AendSuplier.text=""
        }
        cell?.lbl_Bandwidth.text=tempPrice.bandwidthDesc
        cell?.lbl_Currency.text=tempPrice.currency
        if tempPrice.accessTypeAEnd != nil {
            cell?.lbl_AccessType.text=tempPrice.accessTypeAEnd
        }else{
            cell?.lbl_AccessType.text = "Leased Line"
        }
        
        cell?.lbl_Nrc1.text=tempPrice.priceNrc1
        cell?.lbl_Nrc2.text=tempPrice.priceNrc2
        cell?.lbl_Nrc3.text=tempPrice.priceNrc3
        cell?.lbl_Mrc1.text=tempPrice.priceMrc1
        cell?.lbl_Mrc2.text=tempPrice.priceMrc2
        cell?.lbl_Mrc3.text=tempPrice.priceMrc3
        cell?.lbl_Charge1.text=tempPrice.totalPrice1
        cell?.lbl_Charge2.text=tempPrice.totalPrice2
        cell?.lbl_Charge3.text=tempPrice.totalPrice3
        cell?.lbl_remarks.text=tempPrice.remarksPrice
        return cell!
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

