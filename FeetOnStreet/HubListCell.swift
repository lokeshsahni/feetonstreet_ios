//
//  HubListCell.swift
//  FeetOnStreet
//
//  Created by Suresh Murugaiyan on 9/5/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class HubListCell: UITableViewCell {

    @IBOutlet weak var lbl_Level: UILabel!
    @IBOutlet weak var lbl_SubAddress: UILabel!
    @IBOutlet weak var lbl_Address: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
