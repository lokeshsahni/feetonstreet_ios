//
//  SiteInfoModel.swift
//  FeetOnStreet
//
//  Created by admin on 5/2/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class SiteInfoModel
{
    var siteId:Int = 1
    var city:String = ""
    var buildingId:String = ""
    var xngSiteName:String = ""
    var siteOwner:String = ""
    var isVisible:Bool = false
    var zone:String = ""//It should be just zone; Name postfix is added due to zone() method conflit
    var siteName:String = ""
    var siteLongName:String = ""
    var countryName:String = ""
    
    init(_ dic:[String:Any]) {
        for d in dic{
            switch(d.key){
            case "siteId":
                siteId = d.value as! Int
            case "city":
                city = d.value as! String
            case "zone":
                zone = d.value as! String
            case "siteOwner":
                siteOwner = d.value as! String
            case "siteLongName":
                siteLongName = d.value as! String
            case "siteName":
                siteName = d.value as! String
            case "buildingId":
                buildingId = d.value as! String
            case "xngSiteName":
                xngSiteName = d.value as! String
            case "isVisible":
                isVisible = d.value as! Bool
            case "country":
                countryName = d.value as! String
            default:
                print("Something went wrong while parsing SiteInfoModel ")
            }
        }
    }
    
    static func getSites(from data:Data)->[SiteInfoModel]{
        var returnVal = [SiteInfoModel]()
        do{
            let allSites = try JSONSerialization.jsonObject(with: data, options: []) as! [[String:Any]]
            for var site in allSites {
                returnVal.append(SiteInfoModel(site))
            }
        }
        catch{}
        return returnVal
    }
}
