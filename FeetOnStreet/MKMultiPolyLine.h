
#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MKMultiPolyLine : NSObject <MKOverlay>
{
    NSMutableArray *_polylines;
    MKMapRect _boundingMapRect;
}

@property (nonatomic, readonly) NSMutableArray *polylines;

-(id) initWithPolylines:(NSMutableArray*) polylines;
-(void) clearPolylines;
-(void) addPolyline:(MKShape*)polyline;

@end

@interface MKMultiPolylineView: MKOverlayPathRenderer
{
    
}

@end
