//
//  OffNetEMFCell.swift
//  FeetOnStreet
//
//  Created by admin on 9/8/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class OffNetEMFCell: UITableViewCell {

    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var suplierNameLbl: UILabel!
    @IBOutlet weak var accessTypeLbl: UILabel!
    @IBOutlet weak var coltProductLbl: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
