//
//  OLOViewController.swift
//  FeetOnStreet
//
//  Created by admin on 9/8/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class OLOViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,PopupDelegate {
    
    
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var popUpTableView: UITableView!
    @IBOutlet weak var popUpBackgroundBtn: UIButton!
    @IBOutlet weak var filterTypeBtn: UIButton!
    @IBOutlet weak var filterBtnTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var checkPriceBtn: UIButton!
    @IBOutlet weak var noDataAvailableLbl: UILabel!
    
    var selectedBandwidth:String?
    var selectedProduct:String?
    var oLOlist:[OLOModel] = []
    var oLOlistTable:[OLOModel] = []
    var supplierList:[Any] = []
    var oLOsupplierName:String?
    var oLOFiltersupplierList:[Any] = []
    var fosPopUp_Olo:Fos_Popup?
    var isCheckMarkbtnselected = false
    var selectedIndexPath : [NSIndexPath] = []
    var address1:String?
    var point_ID:String?
    var selectedList:[Any] = []
    var isFirstTimeLoaded:Bool = true
    
    var bEndAddress_id:String?
    //    var oLOModelList=OLOModel()
    
    var productName:String?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filterBtnTopConstraint.constant = 8
        filterTypeBtn.contentHorizontalAlignment = .left
        filterTypeBtn.layer.borderWidth = 1
        oLOlist = CommonDataHelper.sharedInstance.oLO_results as! [OLOModel]
        oLOFiltersupplierList = oLOlist
        listTableView.reloadData()
        //        if (CommonDataHelper.sharedInstance.oLO_results.count)<=0 {
        //                listTableView.isHidden = true
        //                filterTypeBtn.isHidden = true
        //                checkPriceBtn.isHidden = true
        //        }
        listTableView.estimatedRowHeight = 1000
        listTableView.rowHeight = UITableViewAutomaticDimension
        popUpTableView.layer.borderWidth = 1.0
        if (CommonDataHelper.sharedInstance.olo_results.count)>0 {
            filterTypeBtn.isHidden = false
        }
        
        if isFirstTimeLoaded == false {
            reloadTablevieWithOLOResponse()
        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        if (CommonDataHelper.sharedInstance.olo_results.count)>0 {
//            filterTypeBtn.isHidden = false
//        }
//        if isFirstTimeLoaded == false {
//            reloadTablevieWithOLOResponse()
//        }
//    }
    
    func reloadTablevieWithOLOResponse() {
        
        oLOlist = CommonDataHelper.sharedInstance.oLO_results as! [OLOModel]
        oLOFiltersupplierList = CommonDataHelper.sharedInstance.oLO_results as! [OLOModel]
        if oLOlist.count>0 {
            
            filterTypeBtn.isHidden = false
            checkPriceBtn.isHidden = false
            removeDuplicateSupplierName()
            listTableView.reloadData()
            popUpTableView.reloadData()
        }else{
            filterTypeBtn.isHidden = true
            checkPriceBtn.isHidden = true
            noDataAvailableLbl.isHidden = false
        }
        
        
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        var count:Int?
        if tableView == popUpTableView {
            count =  2
        }else{
            count = 1
        }
        
        return count!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == popUpTableView {
            if section==0 {
                return 1
            }else{
                return oLOlistTable.count
            }
        }else{
            return oLOFiltersupplierList.count
        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == popUpTableView {
            let oLOpopUpCell = tableView.dequeueReusableCell(withIdentifier: "OLOpopUpCell")
            if indexPath.section == 0 {
                oLOpopUpCell?.textLabel?.text = "All"
            }else{
                let oLOModel = oLOlistTable[indexPath.row]
                oLOpopUpCell?.textLabel?.text = oLOModel.supplierName
            }
            return oLOpopUpCell!
        }else{
            let identifier = "OffNetOLOCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OffNetOLOCell
            cell?.backGroundView.layer.borderWidth = 0.5
            cell?.backGroundView.layer.borderColor = UIColor.lightGray.cgColor
            cell?.checkBoxBtn.addTarget(self, action: #selector(checkMarkAction), for: .touchUpInside)
            cell?.checkBoxBtn.tag = indexPath.row+10;
            
            if self.selectedIndexPath.contains(indexPath as NSIndexPath) {
                cell?.checkBoxBtn.setImage(UIImage(named: "ic_check_box"), for: .normal)
            }else{
                cell?.checkBoxBtn.setImage(UIImage(named: "ic_uncheck_box"), for: .normal)
            }
            
            let oLOModel = oLOFiltersupplierList[indexPath.row] as? OLOModel
            if (oLOModel!.premisesNumber == nil) {
                address1 = "\(oLOModel!.streetName!), \(oLOModel!.postCode!), \(oLOModel!.coltOperatingCountry!)"
            }else if (oLOModel!.streetName == nil) {
                address1 = "\(oLOModel!.premisesNumber!),\(oLOModel!.postCode!), \(oLOModel!.coltOperatingCountry!)"
            }else{
                address1 = "\(oLOModel!.premisesNumber!), \(oLOModel!.streetName!), \(oLOModel!.postCode!), \(oLOModel!.coltOperatingCountry!)"
            }
            cell?.addressLbl.text = address1
            cell?.supplierNameLbl.text = oLOModel?.supplierName
            cell?.accessTypeLbl.text = "Leased Line"
            cell?.supplierProductLbl.text = oLOModel?.supplierProduct
            selectedProduct = oLOModel?.coltProduct
            // changing product names for user
            self.setProductNames()
            cell?.coltProductLbl.text = selectedProduct
            cell?.bandwidthLbl.text = CommonDataHelper.sharedInstance.selectedBandwidth
            
            return cell!
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == popUpTableView {
            
            // Removingt selected list when click on filter type
            self.selectedIndexPath.removeAll()
            self.hideUnHideCheckPriceBtn()
            
            if indexPath.section == 0 {
                filterTypeBtn.setTitle("All", for: .normal)
                popUpBackgroundBtn.isHidden = true
                popUpTableView.isHidden = true
                oLOFiltersupplierList = oLOlist
                listTableView.reloadData()
            }else{
                let oLOModel = oLOlistTable[indexPath.row]
                oLOsupplierName = oLOModel.supplierName
                filterTypeBtn.setTitle(oLOsupplierName, for: .normal)
                popUpBackgroundBtn.isHidden = true
                popUpTableView.isHidden = true
                filterBySupplierName()
            }
        }else{
            CommonDataHelper.sharedInstance.npc = nil
            
            let oLOModel = oLOFiltersupplierList[indexPath.row] as? OLOModel
            if (oLOModel!.premisesNumber == nil) {
                address1 = "\(oLOModel!.streetName!), \(oLOModel!.postCode!), \(oLOModel!.coltOperatingCountry!)"
            }else if (oLOModel!.streetName == nil) {
                address1 = "\(oLOModel!.premisesNumber!),\(oLOModel!.postCode!), \(oLOModel!.coltOperatingCountry!)"
            }else{
                address1 = "\(oLOModel!.premisesNumber!), \(oLOModel!.streetName!), \(oLOModel!.postCode!), \(oLOModel!.coltOperatingCountry!)"
            }
            bEndAddress_id = oLOModel?.pointID
            //            if oLOModel?.streetName != nil {
            //                address1 = "\(oLOModel!.streetName!), " + address1!
            //            }
            //             selectedList = oLOFiltersupplierList
            let objectsDict: NSDictionary = [
                "bEndAddress_id" : bEndAddress_id,
                "viewType" : ViewType.BuildingType ,
                "address1" : address1!,
                "notificationType" : "EFM"
            ]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "OpenProductBandwidthPopUp"), object: objectsDict)
            
            /*
             
             tableView.deselectRow(at: indexPath, animated: false)
             if fosPopUp_Olo?.popupFosDetails?.selectedProductType == "Colt LANLink Point to Point (Ethernet Point to Point)" {
             
             let cellTypes = [PopupcellTypes.AddressCell,PopupcellTypes.ProductCell,PopupcellTypes.BandWidthCell,PopupcellTypes.BEndAddressCell,PopupcellTypes.CheckPriceCell,PopupcellTypes.CancelCell]
             let pDetails = PopupDetails()
             pDetails.pType=PopupType.MapviewPopup4
             pDetails.popupController=self
             pDetails.locationAddress=fosPopUp_Olo?.popupFosDetails?.locationAddress
             pDetails.lattitudeLocation = fosPopUp_Olo?.popupFosDetails?.lattitudeLocation
             pDetails.longitudeLocation = fosPopUp_Olo?.popupFosDetails?.longitudeLocation
             pDetails.isPopupMutable=false
             pDetails.productName=fosPopUp_Olo?.popupFosDetails?.productName
             pDetails.selectedProductType=fosPopUp_Olo?.popupFosDetails?.selectedProductType
             pDetails.selectedBandWidth=fosPopUp_Olo?.popupFosDetails?.selectedBandWidth
             let fosPopup_emf = Fos_Popup.initWithPopupElements(cells: cellTypes, popupDetails: pDetails)
             fosPopup_emf.delegate=self
             presentPopupView(fosPopup_emf)
             
             }else if fosPopUp_Olo?.popupFosDetails?.selectedProductType == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
             
             let cellTypes = [PopupcellTypes.AddressCell,PopupcellTypes.ProductCell,PopupcellTypes.BandWidthCell,PopupcellTypes.HubCell,PopupcellTypes.CheckPriceCell,PopupcellTypes.CancelCell]
             let pDetails = PopupDetails()
             pDetails.pType=PopupType.MapviewPopup3
             pDetails.popupController=self
             pDetails.locationAddress=fosPopUp_Olo?.popupFosDetails?.locationAddress
             pDetails.lattitudeLocation = fosPopUp_Olo?.popupFosDetails?.lattitudeLocation
             pDetails.longitudeLocation = fosPopUp_Olo?.popupFosDetails?.longitudeLocation
             pDetails.isPopupMutable=false
             pDetails.productName=fosPopUp_Olo?.popupFosDetails?.productName
             pDetails.selectedProductType=fosPopUp_Olo?.popupFosDetails?.selectedProductType
             pDetails.selectedBandWidth=fosPopUp_Olo?.popupFosDetails?.selectedBandWidth
             let fosPopup_emf = Fos_Popup.initWithPopupElements(cells: cellTypes, popupDetails: pDetails)
             fosPopup_emf.delegate=self
             presentPopupView(fosPopup_emf)
             }else{
             let cellTypes = [PopupcellTypes.AddressCell,PopupcellTypes.ProductCell,PopupcellTypes.BandWidthCell,PopupcellTypes.CheckPriceCell,PopupcellTypes.CancelCell]
             let pDetails = PopupDetails()
             pDetails.pType=PopupType.MapviewPopup2
             pDetails.popupController=self
             pDetails.locationAddress=fosPopUp_Olo?.popupFosDetails?.locationAddress
             pDetails.lattitudeLocation = fosPopUp_Olo?.popupFosDetails?.lattitudeLocation
             pDetails.longitudeLocation = fosPopUp_Olo?.popupFosDetails?.longitudeLocation
             pDetails.isPopupMutable=false
             pDetails.productName=fosPopUp_Olo?.popupFosDetails?.productName
             pDetails.selectedProductType=fosPopUp_Olo?.popupFosDetails?.selectedProductType
             pDetails.selectedBandWidth=fosPopUp_Olo?.popupFosDetails?.selectedBandWidth
             let fosPopup_emf = Fos_Popup.initWithPopupElements(cells: cellTypes, popupDetails: pDetails)
             fosPopup_emf.delegate=self
             presentPopupView(fosPopup_emf)
             }
             */
        }
    }
    
    func setProductNames() {
        if productName == "Colt VoIP Access" {
            productName = "Colt SIP Trunking"
        }else if productName == "Colt LANLink Point to Point (Ethernet Point to Point)" {
            productName = "Colt Ethernet Line"
            
        }else if productName == "Colt LANLink Spoke (Ethernet Hub and Spoke)" {
            productName = "Colt Ethernet Hub and Spoke"
        }else if productName == "Colt Ethernet Private Network (EPN)"{
            productName = "ColT Ethernet VPN"
        }
    }
    
    
    
    
    func dismissPopup(selectionType:String,popupFos:Fos_Popup) {
        if selectionType == "cancel" {
            dismissPopupView()
        }else if selectionType == "checkprice" {
            
            if popupFos.popupFosDetails?.selectedProductType == nil {
                UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select product")
                return
            }
            if popupFos.popupFosDetails?.selectedBandWidth == nil {
                if popupFos.popupFosDetails?.productName == "Colt SIP Trunking" {
                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select voice channel")
                }else{
                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select bandwidth")
                }
                
                return
            }
            if popupFos.popupFosDetails?.pType == PopupType.MapviewPopup3 && ((popupFos.popupFosDetails?.selectedHubName) == nil) {
                UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please select hub name")
                return
            }
            if popupFos.popupFosDetails?.pType == PopupType.MapviewPopup4 {
                if popupFos.popupFosDetails?.bEndAddress == nil {
                    let please_select_BEndaddress = NSLocalizedString("Please select B-End address", comment: "")
                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: please_select_BEndaddress)
                    return
                }
                if popupFos.popupFosDetails?.isBendAddressSearched == false {
                    UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "Please search B-End address")
                    return
                }
                
            }
            dismissPopupView()
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Secondary", bundle:nil)
            let checkPriceVC = storyBoard.instantiateViewController(withIdentifier: "CheckPriceController") as! CheckPriceController
            checkPriceVC.locationStr=popupFos.popupFosDetails?.locationAddress as String!
            checkPriceVC.productType=popupFos.popupFosDetails?.selectedProductType
            checkPriceVC.bandWidthType=popupFos.popupFosDetails?.selectedBandWidth
            self.navigationController?.pushViewController(checkPriceVC, animated: true)
        }else if selectionType == "buildingDetails" {
            dismissPopupView()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let buildingDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "BuildingDetailsViewController") as! BuildingDetailsViewController
            
            self.navigationController?.pushViewController(buildingDetailsViewController, animated: true)
        }
    }
    
    func checkMarkAction(sender: UIButton) {
        if sender.currentImage == UIImage(named: "ic_uncheck_box") {
            self.selectedIndexPath.append(NSIndexPath(row: sender.tag-10, section: 0))
           
        }else{
        
            let index = self.selectedIndexPath.index(of: NSIndexPath(row: sender.tag-10, section: 0))
            if index == nil {
            }else{
                self.selectedIndexPath.remove(at: index!)
            }
        }
        print(self.selectedIndexPath)
        self.listTableView.reloadData()
        self.hideUnHideCheckPriceBtn()
//        if self.selectedIndexPath.count != 0 {
//            filterBtnTopConstraint.constant = 40
//        }else{
//            filterBtnTopConstraint.constant = 8
//        }
    }
    
    private func hideUnHideCheckPriceBtn() {
        if self.selectedIndexPath.count != 0 {
            filterBtnTopConstraint.constant = 40
        }else{
            filterBtnTopConstraint.constant = 8
        }
    }
    
    func filterBySupplierName() {
        
        oLOFiltersupplierList = []
        for supplier in oLOlist{
            if supplier.supplierName!.lowercased() == oLOsupplierName?.lowercased() {
                oLOFiltersupplierList.append(supplier)
                print(oLOFiltersupplierList.count)
                print("supplierName = ",(supplier as AnyObject).supplierName)
            }
            print("supplierName111111 = ",(supplier as AnyObject).supplierName)
        }
        
        listTableView.reloadData()
    }
    
    func removeDuplicateSupplierName() {
        var oloData:[String]=[]
        for oloData1 in oLOlist {
            if !oloData.contains(oloData1.supplierName!) {
                oloData.append(oloData1.supplierName!)
                oLOlistTable.append(oloData1)
            }
        }
    }
    @IBAction func checkPriceBtnAction(_ sender: Any) {
        
        AnalyticsHelper().analyticsEventsAction(category: "OFFNET List", action: "OLO_multiCheckPrice_click", label: "Check Price")
        selectedList = []
        var err_msg : String?
        let tempKey = UserDefaults.standard.value(forKey: "eType")
        let employeeType =  CoreDataHelper.sharedInstance.getKeyValue(&err_msg, key: tempKey as! String)
        
        if employeeType == "Non Employee" {
            //            UIHelper.sharedInstance.showAlertWithTitleAndMessage(_alertTile: "", _alertMesage: "You are not eligible to check price")
            let youAreNotEligibleTo_Checkprice = NSLocalizedString("You are not eligible to check price", comment: "")
            UIHelper.showAlert(message: youAreNotEligibleTo_Checkprice, inViewController: self)
        }else{
            
            for indexPath in self.selectedIndexPath {
                selectedList.append(oLOFiltersupplierList[indexPath.row])
            }
            
            let oLOModel = oLOFiltersupplierList[selectedIndexPath[0].row] as? OLOModel
            if (oLOModel!.premisesNumber == nil) {
                address1 = "\(oLOModel!.streetName!), \(oLOModel!.postCode!), \(oLOModel!.coltOperatingCountry!)"
            }else if (oLOModel!.streetName == nil) {
                address1 = "\(oLOModel!.premisesNumber!),\(oLOModel!.postCode!), \(oLOModel!.coltOperatingCountry!)"
            }else{
                address1 = "\(oLOModel!.premisesNumber!), \(oLOModel!.streetName!), \(oLOModel!.postCode!), \(oLOModel!.coltOperatingCountry!)"
            }
            CommonDataHelper.sharedInstance.npc = oLOModel?.pointID
            
            let objectsDict: NSDictionary = [
                "multiPriceList" : selectedList,
                "viewType" : ViewType.AddressType,
                "address1" : address1,
                "notificationType" : "OLO"
            ]
            //        self.navigationController?.pushViewController(checkPriceVC, animated: true)
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "OpenProductBandwidthPopUp"), object: objectsDict)
            
        }
    }
    
    @IBAction func filterBtnAction(_ sender: Any) {
        AnalyticsHelper().analyticsEventsAction(category: "OFFNET List", action: "OLO_supplierFilterName_click", label: "OLO supplier filter")
        popUpBackgroundBtn.isHidden = false
        popUpTableView.isHidden = false
        popUpTableView.reloadData()
    }
    @IBAction func popUpBacgroundBtnAction(_ sender: Any) {
        popUpBackgroundBtn.isHidden = true
        popUpTableView.isHidden = true
        popUpTableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

