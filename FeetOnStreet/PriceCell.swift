//
//  PriceCell.swift
//  FeetOnStreet
//
//  Created by Suresh Murugaiyan on 9/1/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class PriceCell: UITableViewCell {
    
    @IBOutlet weak var lbl_Currency: UILabel!
    @IBOutlet weak var lbl_Bandwidth: UILabel!
    @IBOutlet weak var lbl_AendSuplier: UILabel!
    @IBOutlet weak var lbl_BendSuplier: UILabel!
    @IBOutlet weak var lbl_AccessType: UILabel!
    
    @IBOutlet weak var lbl_Nrc1: UILabel!
    @IBOutlet weak var lbl_Mrc1: UILabel!
    @IBOutlet weak var lbl_Charge1: UILabel!
    @IBOutlet weak var lbl_Nrc2: UILabel!
    @IBOutlet weak var lbl_Mrc2: UILabel!
    @IBOutlet weak var lbl_Charge2: UILabel!
    @IBOutlet weak var lbl_Nrc3: UILabel!
    @IBOutlet weak var lbl_Mrc3: UILabel!
    @IBOutlet weak var lbl_Charge3: UILabel!
    @IBOutlet weak var view_Price: UIView!
    @IBOutlet weak var lbl_remarks: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

