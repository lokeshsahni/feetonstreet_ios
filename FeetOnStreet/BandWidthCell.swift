//
//  BandWidthCell.swift
//  FeetOnStreet
//
//  Created by Suresh Murugaiyan on 8/31/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class BandWidthCell: UITableViewCell {

    @IBOutlet weak var lbl_ProductOrVoiceChannel: UILabel!
    @IBOutlet weak var btn_Bandwidth: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
